﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("CharacterActionsCollection")]
public class CharacterActionsContainer {

    [XmlArray("Actions")]
    [XmlArrayItem("CharacterAction")]
    public CharacterAction[] Actions;

    public static CharacterActionsContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(CharacterActionsContainer));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as CharacterActionsContainer;
        }
    }

    public static CharacterActionsContainer LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(CharacterActionsContainer));
        return serializer.Deserialize(new StringReader(text)) as CharacterActionsContainer;
    }
}
