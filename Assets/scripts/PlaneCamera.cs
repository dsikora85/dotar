﻿using UnityEngine;
using System.Collections;

public class PlaneCamera : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        string deviceName = devices[0].name;
        WebCamTexture wct = new WebCamTexture(deviceName, Screen.width, Screen.height);

        renderer.material.mainTexture = wct;

        wct.Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
