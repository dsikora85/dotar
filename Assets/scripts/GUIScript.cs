﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System;

public class GUIScript : MonoBehaviour
{

    //List<MenuElement> buttons;
    int btnWidth = 120;
    int btnHeight = 40;
    public Animator animator = null;
    public AudioClip soundAttack;
    public AudioClip soundRun;
    public AudioClip soundDeath;
    public AudioClip soundSkill1;
    public AudioClip soundSkill2;
    public AudioClip soundSkill3;
    public AudioClip soundSkill4;
	public AudioClip soundSkill5;
	public AudioClip soundSkill6;
    public AudioSource audio;
    public string xmlFileName;

    public GameObject[] beastsList;
    public Dictionary<string, GameObject> summons;
	public GameObject baseObject;
	public GameObject transform = null;

	public Sprite buttonSprite;

	private bool m_Light;

    CharacterActionsContainer actions;

    bool show = false;
	bool hideGUI = false;
	bool skipTouch;

	Canvas canvas;

	delegate void ActionDelegate(CharacterAction action);
	delegate void ButtonDelegate();

	/*
    void OnGUI()
    {
		if (hideGUI)
			return;

        if (show)
        {
            foreach (CharacterAction action in actions.Actions)
            {
                if (GUI.Button(new Rect(action.GetPosition().x - btnWidth / 2, action.GetPosition().y - btnHeight / 2, btnWidth, btnHeight), action.name))
                {

					skipTouch = true;

                    animator.SetTrigger(action.trigger);

                    foreach (GameObject beast in beastsList)
                        beast.GetComponent<Animator>().SetTrigger(action.trigger);

					if(transform)
						transform.GetComponent<Animator>().SetTrigger(action.trigger);

                    switch (action.sound)
                    {
                        case CharacterAction.SoundID.SoundAttack:
                            audio.PlayOneShot(soundAttack);
                            break;
                        case CharacterAction.SoundID.SoundRun:
                            audio.PlayOneShot(soundRun);
                            break;
                        case CharacterAction.SoundID.SoundDeath:
                            audio.PlayOneShot(soundDeath);
                            break;
                        case CharacterAction.SoundID.SoundSkill1:
                            audio.PlayOneShot(soundSkill1);
                            break;
                        case CharacterAction.SoundID.SoundSkill2:
                            audio.PlayOneShot(soundSkill2);
                            break;
                        case CharacterAction.SoundID.SoundSkill3:
                            audio.PlayOneShot(soundSkill3);
                            break;
                        case CharacterAction.SoundID.SoundSkill4:
                            audio.PlayOneShot(soundSkill4);
                            break;
						case CharacterAction.SoundID.SoundSkill5:
							audio.PlayOneShot(soundSkill5);
							break;
						case CharacterAction.SoundID.SoundSkill6:
							audio.PlayOneShot(soundSkill6);
							break;
                    }
                    show = false;
                    animator.SetBool("toIdle", false);
                    foreach (GameObject beast in beastsList)
                        beast.GetComponent<Animator>().SetBool("toIdle", false);

					if(transform)
						transform.GetComponent<Animator>().SetBool("toIdle", false);

                    if (String.IsNullOrEmpty(action.summon) == false)
                    {
                        foreach (GameObject beast in beastsList)
                        {
                            if (beast.tag == action.summon)
                                beast.SetActive(true);
                        }
                    }

					if(String.IsNullOrEmpty(action.transform) == false)
					{
						baseObject.SetActive(false);
						transform.SetActive(true);
					}

                }
            }
            
        }

        if (GUI.Button(new Rect(10, 10, Screen.width*0.1f, Screen.height*0.1f), "Back"))
        {
            Application.LoadLevel("MainMenu");
        }



		if (GUI.Button(new Rect(Screen.width *0.6f, 10, Screen.width*0.1f, Screen.height*0.1f), "Light"))
		{
			m_Light = !m_Light;
			CameraDevice.Instance.SetFlashTorchMode(m_Light);
		}

		if (GUI.Button(new Rect(Screen.width *0.8f, 10, Screen.width*0.1f, Screen.height*0.1f), "Screenshoot"))
		{
			hideGUI = true;
			DateTime now = DateTime.Now;
			Application.CaptureScreenshot("/Screenshot1_"+now.ToString("dMMMyyyy_HHmmss")+".png");
			StartCoroutine("TakeScreenshot");
		}
    }*/

	// Use this for initialization
	void Start () 
    {
        TextAsset textAsset = (TextAsset)Resources.Load(xmlFileName);
        actions = CharacterActionsContainer.LoadFromText(textAsset.text);

        foreach (GameObject beast in beastsList)
            beast.SetActive(false); 

		if(transform != null)
			transform.SetActive (false);

		m_Light = false;

		//CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);



		if (FindObjectOfType<UnityEngine.EventSystems.EventSystem>() == null)
		{
			var es = new GameObject("EventSystem", typeof(UnityEngine.EventSystems.EventSystem));
			es.AddComponent<UnityEngine.EventSystems.StandaloneInputModule>();
		}
		
		var mainCanvasObject = new GameObject("Canvas");
		Canvas mainCanvas = mainCanvasObject.AddComponent<Canvas>();

		mainCanvasObject.AddComponent<GraphicRaycaster>();
		mainCanvas.renderMode = RenderMode.Overlay;

		var canvasObject = new GameObject("Canvas");
		canvas = canvasObject.AddComponent<Canvas>();

		canvasObject.AddComponent<GraphicRaycaster>();
		canvas.renderMode = RenderMode.Overlay;

		canvas.transform.parent = mainCanvas.transform;


		Button buttonBack = CreateButton("Back", new Vector2(-30, 20), new Vector2(1, 0), new Vector2(1, 0), mainCanvas);
		buttonBack.onClick.AddListener(delegate() { Application.LoadLevel("MainMenu"); });

		Button buttonLight = CreateButton("Light", new Vector2(30, -20), new Vector2(0, 1), new Vector2(0, 1), mainCanvas);
		buttonLight.onClick.AddListener(delegate() { SwitchLight(); });

		Button buttonScreenshot = CreateButton("Screenshot", new Vector2(-55, -20), Vector2.one, Vector2.one, mainCanvas);
		buttonScreenshot.onClick.AddListener(delegate() { TakeScreenshot(); });

		float basic_rotation = 2 * Mathf.PI / actions.Actions.Length;
		int i = 0;

		foreach (CharacterAction action in actions.Actions) 
		{
			float elem_rotation = i * basic_rotation;
			float x = -Mathf.Cos(elem_rotation) * Screen.height / 2;
			float y = Mathf.Sin(elem_rotation) * Screen.height / 2.5f;
			action.SetPosition(new Vector2(x, y));
			i++;

			var localAction = action;
			Button button = CreateButton(action.name, new Vector2(x, y), new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), canvas);
			button.onClick.AddListener(delegate() { OnButtonClick(localAction); });
		}
		canvas.enabled = false;
	}


	public Button CreateButton(String caption, Vector2 position, Vector2 anchorMin, Vector2 anchorMax, Canvas canvas)
	{
		var buttonObject = new GameObject ("Button");
		
		var image = buttonObject.AddComponent<UnityEngine.UI.Image> ();
		image.transform.parent = canvas.transform;
		image.rectTransform.sizeDelta = new Vector2 (caption.Length*10, 25);
		image.rectTransform.anchoredPosition = new Vector3 (position.x, position.y, 0);
		image.rectTransform.anchorMin = anchorMin;
		image.rectTransform.anchorMax = anchorMax;
		image.sprite = buttonSprite;
		image.type = UnityEngine.UI.Image.Type.Sliced;
		image.color = new Color(.9f, .9f, .9f, .9f);
		
		var button = buttonObject.AddComponent<Button> ();
		button.targetGraphic = image;

		var textObject = new GameObject ("Text");
		textObject.transform.parent = buttonObject.transform;
		var text = textObject.AddComponent<Text> ();
		text.rectTransform.sizeDelta = Vector2.zero;
		text.rectTransform.anchorMin = Vector2.zero;
		text.rectTransform.anchorMax = Vector2.one;
		text.rectTransform.anchoredPosition = new Vector2 (.5f, .5f);
		text.text = caption;
		text.font = Resources.FindObjectsOfTypeAll<Font> () [0];
		text.fontSize = 14;
		text.color = Color.black;
		text.alignment = TextAnchor.MiddleCenter;

		return button;
	}

	public void SwitchLight()
	{
		m_Light = !m_Light;
		CameraDevice.Instance.SetFlashTorchMode(m_Light);
	}

	public void OnButtonClick(CharacterAction action)
	{
		Debug.Log("Action!!");

		animator.SetTrigger(action.trigger);
		
		foreach (GameObject beast in beastsList)
			beast.GetComponent<Animator>().SetTrigger(action.trigger);
		
		if(transform)
			transform.GetComponent<Animator>().SetTrigger(action.trigger);
		
		switch (action.sound)
		{
		case CharacterAction.SoundID.SoundAttack:
			audio.PlayOneShot(soundAttack);
			break;
		case CharacterAction.SoundID.SoundRun:
			audio.PlayOneShot(soundRun);
			break;
		case CharacterAction.SoundID.SoundDeath:
			audio.PlayOneShot(soundDeath);
			break;
		case CharacterAction.SoundID.SoundSkill1:
			audio.PlayOneShot(soundSkill1);
			break;
		case CharacterAction.SoundID.SoundSkill2:
			audio.PlayOneShot(soundSkill2);
			break;
		case CharacterAction.SoundID.SoundSkill3:
			audio.PlayOneShot(soundSkill3);
			break;
		case CharacterAction.SoundID.SoundSkill4:
			audio.PlayOneShot(soundSkill4);
			break;
		case CharacterAction.SoundID.SoundSkill5:
			audio.PlayOneShot(soundSkill5);
			break;
		case CharacterAction.SoundID.SoundSkill6:
			audio.PlayOneShot(soundSkill6);
			break;
		}

		animator.SetBool("toIdle", false);
		foreach (GameObject beast in beastsList)
			beast.GetComponent<Animator>().SetBool("toIdle", false);
		
		if(transform)
			transform.GetComponent<Animator>().SetBool("toIdle", false);
		
		if (String.IsNullOrEmpty(action.summon) == false)
		{
			foreach (GameObject beast in beastsList)
			{
				if (beast.tag == action.summon)
					beast.SetActive(true);
			}
		}
		
		if(String.IsNullOrEmpty(action.transform) == false)
		{
			baseObject.SetActive(false);
			transform.SetActive(true);
		}

		canvas.enabled = false;
	}


	//IEnumerator TakeScreenshot() 
	public void TakeScreenshot() 
	{
		//yield return new WaitForSeconds(0.2f);

		DateTime now = DateTime.Now;
		Application.CaptureScreenshot("/Screenshot1_"+now.ToString("dMMMyyyy_HHmmss")+".png");
//		hideGUI = false;
	}

	// Update is called once per frame
	void Update () 
    {
		if (Input.GetMouseButtonDown(0) )
		{
			if (UnityEngine.EventSystems.EventSystemManager.currentSystem.IsPointerOverEventSystemObject() == false)
			{
				animator.SetBool("toIdle", true);
				foreach (GameObject beast in beastsList)
					beast.GetComponent<Animator>().SetBool("toIdle", true);
				if(transform)
					transform.GetComponent<Animator>().SetBool("toIdle", true);
				
				//CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
				
				//show = !show;
				canvas.enabled = !canvas.enabled;
			}
		}

		/*if ((Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)))
        {
            animator.SetBool("toIdle", true);
            foreach (GameObject beast in beastsList)
                beast.GetComponent<Animator>().SetBool("toIdle", true);
			if(transform)
				transform.GetComponent<Animator>().SetBool("toIdle", true);

			CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

            show = !show;
        }*/
	}
}
