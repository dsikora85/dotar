﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    const int HEROES_NUM = 42;
    Texture2D[] textures = new Texture2D[HEROES_NUM];

	string[] heroes = new string[HEROES_NUM] { "Abaddon", "Alchemist", "Antimage", "Axe", "Bane", "Batrider", "Beastmaster", "Bloodseeker", "BountyHunter", "Bristleback", "Broodmother", "Centaur", "ChaosKnight", "Chen", "Clinkz", "CrystalMaiden", "DarkSeer", "Dazzle", "DeathProphet", "Disruptor", "Doom", "DragonKnight", "Drow", "Earthshaker", "EarthSpirit", "ElderTitan", "EmberSpirit", "Enchantress", "Huskar", "Invoker", "Kotl", "Kunkka", "Lanaya", "LegionCommander", "Leshrac", "Lich", "Lifestealer", "Lina", "Lion", "Pudge", "Riki", "Treant" };

	public GameObject mybutton;
	public Transform panel_target;
	
    private AsyncOperation async = null;

	// Use this for initialization
	void Start () 
    {
        int i=0;
        foreach (string textureName in heroes)
            textures[i++] = Resources.Load(textureName + "_portrait") as Texture2D;
	}

	public void OnHeroClick(string name)
	{
		//CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		Application.LoadLevelAsync(name);
	}

	// Update is called once per frame
	void Update () 
    {

	}
	
	public void OnHelpClick()
	{
		Application.LoadLevel ("Tutorial");
	}
	
}
