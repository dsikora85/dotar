﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{
    public GUITexture myCameraTexture;
    public GUITexture BackgroundTexture;
    private WebCamTexture webCameraTexture;
    void Start()
    {

        BackgroundTexture = gameObject.AddComponent<GUITexture>();
        BackgroundTexture.pixelInset = new Rect(0, 0, Screen.currentResolution.width, Screen.currentResolution.height);
        //set up camera
        //WebCamDevice[] devices = webCameraTexture.devices;
        /*string backCamName = "";
        for (int i = 0; i < devices.Length; i++)
        {
            //Debug.Log("Device:" + devices[i].name + "IS FRONT FACING:" + devices[i].isFrontFacing);

            if (!devices[i].isFrontFacing)
            {
                backCamName = devices[i].name;
            }
        }*/

        WebCamDevice[] devices = WebCamTexture.devices;
        string deviceName = devices[0].name;

        webCameraTexture = new WebCamTexture(deviceName, Screen.currentResolution.width, Screen.currentResolution.height);
        webCameraTexture.Play();
        BackgroundTexture.texture = webCameraTexture;






        /*
        GUITexture[] myGuiTextures = GameObject.FindObjectsOfType(typeof(GUITexture)) as GUITexture[];
        myCameraTexture = myGuiTextures[0];*/

        // Checks how many and which cameras are available on the device
        /* for (int cameraIndex = 0; cameraIndex < WebCamTexture.devices.Length; cameraIndex++)
         {
             // We want the back camera
             if (!WebCamTexture.devices[cameraIndex].isFrontFacing)
             {
                 webCameraTexture = new WebCamTexture(cameraIndex, Screen.width, Screen.height);
                 // Here we flip the GuiTexture by applying a localScale transformation
                 // works only in Landscape mode
                 myCameraTexture.transform.localScale = new Vector3(-1, -1, 1);
             }
         }*/
        /*
        //myCameraTexture.transform.localScale = new Vector3(-1, -1, 1);
        WebCamDevice[] devices = WebCamTexture.devices;
        string deviceName = devices[0].name;
        webCameraTexture = new WebCamTexture(deviceName, Screen.width, Screen.height);


        // Here we tell that the texture of coming from the camera should be applied
        // to our GUITexture. As we have flipped it before the camera preview will have the
        // correct orientation
        myCameraTexture.texture = webCameraTexture;
        // Starts the camera
        webCameraTexture.Play();*/
    }


    /*public string deviceName;
    WebCamTexture wct;
    Texture aTexture;

    // Use this for initialization
    void Start () 
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        deviceName = devices[0].name;
        wct = new WebCamTexture(deviceName, Screen.width, Screen.height);
        aTexture = wct;
        wct.Play();
    }

    void OnGUI()
    {
        if(aTexture)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), aTexture);//, ScaleMode.ScaleToFit, true, 10.0F);
    }

    // Update is called once per frame
    void Update () 
    {
	
    }*/
}
