﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

public class CharacterAction {

    public enum SoundID
    {
        SoundNone,
        SoundBegin,
        SoundAttack,
        SoundRun,
        SoundDeath,
        SoundSkill1,
        SoundSkill2,
        SoundSkill3,
        SoundSkill4,
		SoundSkill5,
		SoundSkill6
    };

    public string name;
    public string trigger;
    public SoundID sound;

    [XmlElement(IsNullable = true)]
    public string summon;

	[XmlElement(IsNullable = true)]
	public string transform;

    private Vector2 position;

    public Vector2 GetPosition()
    {
        return position;
    }

    public void SetPosition(Vector3 pos)
    {
        position = pos;
    }
}
