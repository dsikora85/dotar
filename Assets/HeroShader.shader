﻿Shader "Custom/HeroShader" {
	Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
	  _Mask1 ("Mask1", 2D) = "black" {}
	  _Mask2 ("Mask2", 2D) = "black" {}
	  _Detail ("Detail", 2D) = "gray" {}
	  _AlphaCut("Cutoff" , Range(0,1)) = .4
	  _RimColor ("Rim Color", Color) = (1,1,1,1)
    }
    SubShader {
	  Tags { "Queue" = "AlphaTest" "RenderType" = "TransparentCutout" }
      CGPROGRAM
	  #pragma surface surf SimpleSpecular alphatest:_AlphaCut


	   struct Input {
        float2 uv_MainTex;
        float2 uv_BumpMap;
		float3 viewDir;
		float2 uv_Detail;
      };
      sampler2D _MainTex;
      sampler2D _BumpMap;
	  sampler2D _Mask1;
	  sampler2D _Mask2;
	  sampler2D _Detail;
	  float4 _RimColor;

	  //float alphaT;

	  float specular;
	  float specularScale;
	  int TintSpec;
	  float metalnessMask;

      half4 LightingSimpleSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
	  {
          half3 h = normalize (lightDir + viewDir);

          half diff = max (0, dot (s.Normal, lightDir));

          float nh = max (0, dot (s.Normal, h));
          float spec = pow (nh, 96*specularScale);
		  
          half4 c;
		  if(TintSpec > 0)
			c.rgb = (s.Albedo * s.Albedo.rgb * diff + s.Albedo * spec * specular) * (atten * 2);
		  else
			c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec * specular) * (atten * 2);

		  c.rgb = lerp( c.rgb, spec, metalnessMask );

          c.a = s.Alpha;
          return c;
      }


     

      void surf (Input IN, inout SurfaceOutput o) {
		//alpha = 0.5;
        o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
		o.Albedo *= tex2D (_Detail, IN.uv_Detail).rgb * 2;
        o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
		o.Alpha = tex2D (_MainTex, IN.uv_MainTex).a;

		float4 mask1Sample = tex2D (_Mask1, IN.uv_MainTex).rgba;
		float4 mask2Sample = tex2D (_Mask2, IN.uv_MainTex).rgba;


		//MASK 1:
		//R: Detail Map Mask
		//G: Diffuse/Fresnel Mask
		//B: Metallness
		//A: Self-illumination


		//MASK 2:
		//R: Specular intensity
		//G: Rimlight intensity
		//B: Tint spec by base color
		//A: Specular exponent

		specular = mask2Sample.r;
		specularScale = mask2Sample.a;

		TintSpec = mask2Sample.b > 0 ? 1 : 0;
			
		//TintSpec = mask2Sample.b > 0;
		metalnessMask = mask1Sample.b;

		//float3 rimcol = (0.1, 0.1, 0.1);
		//half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
        //o.Emission = rimcol * pow (rim, mask1Sample.g);
      }
      ENDCG
    } 
	FallBack "Diffuse"
}
