//Maya ASCII 2011 scene
//Name: CHAR1_ATTACK_WEAK_1.ma
//Last modified: Thu, Mar 01, 2012 07:59:48 PM
//Codeset: 1252
requires maya "2011";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2011";
fileInfo "version" "2011";
fileInfo "cutIdentifier" "201003190014-771504";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -n "character_1";
createNode transform -n "SETUP" -p "character_1";
createNode transform -n "C_POSITION" -p "SETUP";
createNode locator -n "C_POSITIONShape" -p "C_POSITION";
	setAttr -k off ".v";
createNode transform -n "C_flyCTR" -p "C_POSITION";
	setAttr ".rp" -type "double3" -5.3163235944351808e-016 42.443413265848925 1.563 ;
	setAttr ".sp" -type "double3" -5.3163235944351808e-016 42.443413265848925 1.563 ;
createNode locator -n "C_flyCTRShape" -p "C_flyCTR";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" -7.5367696436854939e-016 42.443413265848925 1.563 ;
	setAttr ".los" -type "double3" 13 10 13 ;
createNode transform -n "C_pelvisCTR" -p "C_flyCTR";
	setAttr ".rp" -type "double3" -7.5367696436854939e-016 42.443413265848903 1.5629999999999973 ;
	setAttr ".sp" -type "double3" -7.5367696436854939e-016 42.443413265848903 1.5629999999999973 ;
createNode nurbsCurve -n "C_pelvisCTRShape" -p "C_pelvisCTR";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		7.4166685438094158 42.472103509604473 -5.8397232975639843
		0.0095430145322388214 42.471532634241022 -8.9159081863657921
		-7.4031726832319924 42.454489814239373 -5.8536907784569667
		-10.479210227748942 42.430958502415088 1.5531234995444756
		-7.4166685438094131 42.414723022094279 8.9657232975639811
		-0.009543014532244657 42.415293897457175 12.04190818636579
		7.4031726832319853 42.432336717458725 8.9796907784569626
		10.479210227748942 42.455868029282733 1.5728765004555214
		7.4166685438094158 42.472103509604473 -5.8397232975639843
		0.0095430145322388214 42.471532634241022 -8.9159081863657921
		-7.4031726832319924 42.454489814239373 -5.8536907784569667
		;
createNode transform -n "hands" -p "C_pelvisCTR";
	setAttr ".rp" -type "double3" 7.7507444906643741e-015 54.069466766180049 -0.63437580380986458 ;
	setAttr ".sp" -type "double3" 7.7507444906643741e-015 54.069466766180049 -0.63437580380986458 ;
createNode transform -n "drvhangR" -p "hands";
	setAttr ".r" -type "double3" 0 -174.53899999697813 34.133067819376357 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".rp" -type "double3" -6.8786074882624915 49.520735770184722 0.54364829623335553 ;
	setAttr ".rpt" -type "double3" -15.283302282973036 -4.7174560861946233 -1.7394536808199301 ;
	setAttr ".sp" -type "double3" -6.8786074882624915 49.520735770184722 0.54364829623335553 ;
createNode transform -n "R_handCTR" -p "drvhangR";
	addAttr -ci true -sn "twist" -ln "twist" -at "double";
	addAttr -ci true -sn "ikfk" -ln "ikfk" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "cns_onoff" -ln "cns_onoff" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "ikFkBlend" -ln "ikFkBlend" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -6.8786074882624995 49.520735770184629 0.54364829623334232 ;
	setAttr ".sp" -type "double3" -6.878607488262503 49.520735770184629 0.54364829623333855 ;
	setAttr -k on ".twist";
	setAttr -k on ".ikFkBlend";
createNode locator -n "R_handCTRShape" -p "R_handCTR";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" -6.8786074882624986 49.520735770184643 0.54364829623334876 ;
	setAttr ".los" -type "double3" 5 5 5 ;
createNode transform -n "drvhangL" -p "hands";
	setAttr ".r" -type "double3" -1.9969102363269923e-016 -5.4610000030218657 -34.133067819376357 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".rp" -type "double3" -6.878607488262495 49.520735770184729 -0.54364829623335686 ;
	setAttr ".rpt" -type "double3" 29.040517259498028 -4.7174560861946215 -0.65215708835321928 ;
	setAttr ".sp" -type "double3" -6.878607488262495 49.520735770184729 -0.54364829623335686 ;
createNode transform -n "L_handCTR" -p "drvhangL";
	addAttr -ci true -sn "twist" -ln "twist" -at "double";
	addAttr -ci true -sn "ikfk" -ln "ikfk" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "cns_onoff" -ln "cns_onoff" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "ikFkBlend" -ln "ikFkBlend" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -6.8786074882624986 49.520735770184636 -0.54364829623334254 ;
	setAttr ".sp" -type "double3" -6.8786074882625092 49.520735770184629 -0.54364829623333788 ;
	setAttr -k on ".twist";
	setAttr -k on ".ikFkBlend";
createNode locator -n "L_handCTRShape" -p "L_handCTR";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" -6.8786074882624995 49.520735770184643 -0.54364829623335043 ;
	setAttr ".los" -type "double3" 5 5 5 ;
createNode transform -n "C_hipCTR" -p "C_pelvisCTR";
	setAttr ".rp" -type "double3" -7.5367696436854939e-016 42.443413265848925 1.563 ;
	setAttr ".sp" -type "double3" -7.5367696436854939e-016 42.443413265848925 1.563 ;
createNode nurbsCurve -n "C_hipCTRShape" -p "C_hipCTR";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		8.8125690648531325 36.145827872091232 -7.2130044531599058
		-0.0045855918211630585 36.145829980293541 -10.874775515346398
		-8.828394568030399 36.14589291822341 -7.2296308001430658
		-12.489990237300672 36.1459798176951 1.58715335509658
		-8.8444595165000433 36.146039774176579 10.410824368749372
		-0.027304859825758827 36.146037665974184 14.072595430935865
		8.7965041163834758 36.145974728044393 10.427450715732531
		12.458099785653756 36.14588782857269 1.610666560492886
		8.8125690648531325 36.145827872091232 -7.2130044531599058
		-0.0045855918211630585 36.145829980293541 -10.874775515346398
		-8.828394568030399 36.14589291822341 -7.2296308001430658
		;
createNode transform -n "legs" -p "C_flyCTR";
	setAttr ".rp" -type "double3" -7.5367696436854939e-016 42.443413265848925 1.563 ;
	setAttr ".sp" -type "double3" -7.5367696436854939e-016 42.443413265848925 1.563 ;
createNode transform -n "drvlegL" -p "legs";
	setAttr ".t" -type "double3" 0 0 -6.1629758220391547e-033 ;
	setAttr ".r" -type "double3" 0 8.0544 0 ;
	setAttr ".rp" -type "double3" 3.5429423328825327e-018 0 -5.5562786666552443e-017 ;
	setAttr ".rpt" -type "double3" 7.3348041368618112e-019 0 1.1107393789781031e-016 ;
	setAttr ".sp" -type "double3" 3.5429423328825327e-018 0 -5.5562786666552443e-017 ;
createNode transform -n "L_legCTR" -p "drvlegL";
	addAttr -ci true -sn "twist" -ln "twist" -at "double";
	addAttr -ci true -sn "ikfk" -ln "ikfk" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "cns_onoff" -ln "cns_onoff" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "ikFkBlend" -ln "ikFkBlend" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" 8.1035728478076656 4.3256502025852619 -0.86445293110706845 ;
	setAttr ".sp" -type "double3" 8.1035728478076656 4.3256502025852619 -0.86445293110706845 ;
	setAttr -k on ".twist";
	setAttr -k on ".ikFkBlend";
createNode locator -n "L_legCTRShape" -p "L_legCTR";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 8.1035728478076621 4.3256502025852619 -0.86445293110706856 ;
	setAttr ".los" -type "double3" 8 8 8 ;
createNode transform -n "drvlegR" -p "legs";
	setAttr ".r" -type "double3" 0 -8.0537111953824656 0 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".rp" -type "double3" 3.5429423328825288e-018 0 5.556278666655243e-017 ;
	setAttr ".rpt" -type "double3" -7.8193650794512557e-018 0 -5.1635435294607137e-020 ;
	setAttr ".sp" -type "double3" 3.5429423328825288e-018 0 5.556278666655243e-017 ;
createNode transform -n "R_legCTR" -p "drvlegR";
	addAttr -ci true -sn "twist" -ln "twist" -at "double";
	addAttr -ci true -sn "ikfk" -ln "ikfk" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "cns_onoff" -ln "cns_onoff" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "ikFkBlend" -ln "ikFkBlend" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -8.1033519194036785 4.3256848055497592 -0.86458177118202917 ;
	setAttr ".sp" -type "double3" -8.1033519194036785 4.3256848055497592 -0.86458177118202917 ;
	setAttr -k on ".twist";
	setAttr -k on ".ikFkBlend";
createNode locator -n "R_legCTRShape" -p "R_legCTR";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" -8.1033519194036785 4.3256848055497592 -0.86458177118202917 ;
	setAttr ".los" -type "double3" 8 8 8 ;
createNode transform -n "IK_FK" -p "SETUP";
createNode transform -n "IK_grp" -p "IK_FK";
createNode ikHandle -n "ikHandle1" -p "IK_grp";
	setAttr ".roc" yes;
createNode pointConstraint -n "ikHandle1_pointConstraint1" -p "ikHandle1";
	addAttr -ci true -k true -sn "w0" -ln "R_handCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -22.161909771235472 44.803279683990027 -1.1958053845865622 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "ikHandle1_orientConstraint1" -p "ikHandle1";
	addAttr -ci true -k true -sn "w0" -ln "R_handCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -58.146499512967146 30.819482979657352 -104.36161760632577 ;
	setAttr ".rsrr" -type "double3" 0 -174.53899999697813 34.133067819376357 ;
	setAttr -k on ".w0";
createNode poleVectorConstraint -n "ikHandle1_poleVectorConstraint1" -p "ikHandle1";
	addAttr -ci true -k true -sn "w0" -ln "R_armW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode scaleConstraint -n "ikHandle1_scaleConstraint1" -p "ikHandle1";
	addAttr -ci true -k true -sn "w0" -ln "R_handCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode ikHandle -n "ikHandle2" -p "IK_grp";
	setAttr ".roc" yes;
createNode pointConstraint -n "ikHandle2_pointConstraint1" -p "ikHandle2";
	addAttr -ci true -k true -sn "w0" -ln "L_handCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 22.161909771235479 44.803279683990027 -1.1958053845865622 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "ikHandle2_orientConstraint1" -p "ikHandle2";
	addAttr -ci true -k true -sn "w0" -ln "L_handCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -52.494925678948782 -184.34091743036447 15.344665707071387 ;
	setAttr ".rsrr" -type "double3" 0 -5.4610000030218648 -34.133067819376357 ;
	setAttr -k on ".w0";
createNode poleVectorConstraint -n "ikHandle2_poleVectorConstraint1" -p "ikHandle2";
	addAttr -ci true -k true -sn "w0" -ln "L_armW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode scaleConstraint -n "ikHandle2_scaleConstraint1" -p "ikHandle2";
	addAttr -ci true -k true -sn "w0" -ln "L_handCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode ikHandle -n "ikHandle3" -p "IK_grp";
	setAttr ".roc" yes;
createNode pointConstraint -n "ikHandle3_pointConstraint1" -p "ikHandle3";
	addAttr -ci true -k true -sn "w0" -ln "R_legCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -7.9023008409279889 4.3256848055497592 -1.9913451203225243 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "ikHandle3_orientConstraint1" -p "ikHandle3";
	addAttr -ci true -k true -sn "w0" -ln "R_legCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 -135.86952719538243 0 ;
	setAttr ".rsrr" -type "double3" 0 -8.0537111953824692 0 ;
	setAttr -k on ".w0";
createNode poleVectorConstraint -n "ikHandle3_poleVectorConstraint1" -p "ikHandle3";
	addAttr -ci true -k true -sn "w0" -ln "R_leg1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode scaleConstraint -n "ikHandle3_scaleConstraint1" -p "ikHandle3";
	addAttr -ci true -k true -sn "w0" -ln "R_legCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode ikHandle -n "ikHandle4" -p "IK_grp";
	setAttr ".roc" yes;
createNode pointConstraint -n "ikHandle4_pointConstraint1" -p "ikHandle4";
	addAttr -ci true -k true -sn "w0" -ln "L_legCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 7.9025137018427536 4.3256502025852619 -1.9913435067844965 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "ikHandle4_orientConstraint1" -p "ikHandle4";
	addAttr -ci true -k true -sn "w0" -ln "L_legCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 -85.976862799999992 0 ;
	setAttr ".rsrr" -type "double3" 0 8.0544000000000029 0 ;
	setAttr -k on ".w0";
createNode poleVectorConstraint -n "ikHandle4_poleVectorConstraint1" -p "ikHandle4";
	addAttr -ci true -k true -sn "w0" -ln "L_leg1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode scaleConstraint -n "ikHandle4_scaleConstraint1" -p "ikHandle4";
	addAttr -ci true -k true -sn "w0" -ln "L_legCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "FK_grp" -p "IK_FK";
createNode joint -n "C_root" -p "FK_grp";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 89.999999999999986 0 90 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 1 0 0 -2.2204460492503131e-016 0 1 0
		 1 -2.2204460492503131e-016 2.2204460492503131e-016 0 -7.5367696436854939e-016 42.443413265848903 1.5629999999999973 1;
	setAttr ".radi" 0.69114266058213025;
createNode joint -n "C_hip" -p "C_root";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 1 0 0 -2.2204460492503131e-016 0 1 0
		 1 -2.2204460492503131e-016 2.2204460492503131e-016 0 -7.5367696436854998e-016 42.443413265848925 1.5629999999999999 1;
createNode joint -n "R_thigh" -p "C_hip";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 97.120721602752397 171.75255933966895 0.57193490796612889 ;
	setAttr ".bps" -type "matrix" -0.14344841520534554 -0.98960848953353286 -0.0098787457824655901 0
		 -0.98202473439751237 0.14357230046565872 -0.12253332432646213 0 0.12267833226193658 -0.0078760384812672779 -0.99241523306087542 0
		 -3.6985268363424679 36.112908020858029 1.5632365782569453 1;
createNode joint -n "R_calf" -p "R_thigh";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 -12.239999976604619 0 ;
	setAttr ".bps" -type "matrix" -0.11417888494502058 -0.96878260787216386 -0.22005326836273328 0
		 -0.98202473439751237 0.14357230046565872 -0.12253332432646213 0 0.15030170745606011 0.20210703407701566 -0.96776140836075142 0
		 -6.1453694594029455 19.232859336688911 1.3947318498616255 1;
createNode joint -n "R_heel" -p "R_calf";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 15.387533188566765 8.8817841970012523e-015 -8.8817841970012523e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 57.341904058049913 0 ;
	setAttr ".bps" -type "matrix" -0.18815360902669864 -0.69293410662471877 0.69601763144796225 0
		 -0.98202473489457043 0.14357227708539549 -0.12253334773751595 0 -0.015021300392335998 -0.70656162160756908 -0.70749207444734374 0
		 -7.9023008409280049 4.325684805549729 -1.9913451203225219 1;
createNode joint -n "R_foot" -p "R_heel";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 8.8628140587933437 43.802094493888447 -5.5085228799901275e-016 ;
	setAttr ".bps" -type "matrix" -0.12539974418984126 -0.011054300714256171 0.99204470997674354 0
		 -0.99203482324456582 -0.01060542289201731 -0.12551667010960862 0 0.011908552693135675 -0.99988265683579114 -0.0096363339374753165 0
		 -9.1140346574265116 -0.13690134385125763 2.4910993316731629 1;
createNode orientConstraint -n "R_heel_orientConstraint1" -p "R_heel";
	addAttr -ci true -k true -sn "w0" -ln "R_legCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" -0.0089856841748434425 -0.21220045679487029 -0.12324229262699685 ;
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 23.616791130677456 -195.83961240590887 63.604277720017201 ;
	setAttr ".o" -type "double3" 178.66646111899058 -45.685298293971385 -97.301461280949283 ;
	setAttr ".rsrr" -type "double3" 1.8959287283148245e-006 -9.541664254880922e-015 
		-360 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector3" -p "R_calf";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "L_thigh" -p "C_hip";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -97.121 -171.753 0.57193490796612889 ;
	setAttr ".bps" -type "matrix" 0.14344080376906582 -0.9896095927072821 -0.0098787567948740822 0
		 0.98202523301724398 0.14356470984499314 -0.12253822181079244 0 0.12268324063013802 0.0078757925855652139 0.99241462824750681 0
		 3.6989999999999976 36.112908020858029 1.5632365782569471 1;
createNode joint -n "L_calf" -p "L_thigh";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 12.24 0 ;
	setAttr ".bps" -type "matrix" 0.11417040586064085 -0.96878363375454102 -0.22005315129523731 0
		 0.98202523301724398 0.14356470984499314 -0.12253822181079244 0 0.15030489061585633 -0.20210750865930488 0.96776081487136312 0
		 6.1457127925099932 19.232840519523549 1.3947316620196744 1;
createNode joint -n "L_heel" -p "L_calf";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 15.387533188566765 8.8817841970012523e-015 -8.8817841970012523e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 -57.342000000000006 0 ;
	setAttr ".bps" -type "matrix" 0.18815168828469458 -0.69293387662825534 0.69601837965427127 0
		 0.98202523301724398 0.14356470984499314 -0.12253822181079244 0 -0.015012791647362467 0.70656338477737535 0.70749049419684207 0
		 7.90251370184275 4.3256502025852406 -1.9913435067844953 1;
createNode joint -n "L_foot" -p "L_heel";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -8.863 -43.802 0 ;
	setAttr ".bps" -type "matrix" 0.12540448007104316 -0.011054546348492789 0.99204410858748637 0
		 0.99203409389888531 -0.010616313408577785 -0.12552151382065502 0 0.011919434564161306 0.999882538548724 0.009635153607016278 0
		 9.1142351485118862 -0.13693446560848255 2.4911057637575413 1;
createNode orientConstraint -n "L_feet1_orientConstraint1" -p "L_heel";
	addAttr -ci true -k true -sn "w0" -ln "L_legCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" 0.17516677956746329 -0.0014577340619181256 0.17208403012298518 ;
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 46.624157734940923 25.012583777333276 65.53339047010715 ;
	setAttr ".o" -type "double3" 1.3341276029346982 -45.685424516935534 -82.699399325330248 ;
	setAttr ".rsrr" -type "double3" 6.3611093629270335e-015 -3.1805546814635164e-015 
		-4.7708320221952752e-015 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector4" -p "L_calf";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode pointConstraint -n "C_hip_pointConstraint1" -p "C_hip";
	addAttr -ci true -k true -sn "w0" -ln "C_hipCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.1316282072803006e-014 2.6645352591003757e-015 -4.7331654313260708e-030 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "C_hip_orientConstraint1" -p "C_hip";
	addAttr -ci true -k true -sn "w0" -ln "C_hipCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -90 -89.999999999999972 0 ;
	setAttr ".o" -type "double3" 89.999999999999986 0 89.999999999999986 ;
	setAttr ".rsrr" -type "double3" -1.2722218725854067e-014 1.4124500153760508e-030 
		1.2722218725854067e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "C_hip_scaleConstraint1" -p "C_hip";
	addAttr -ci true -k true -sn "w0" -ln "C_hipCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "C_spine_low" -p "C_root";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 1.4124500153760508e-030 0 0 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 1 0 0 -2.2204460492503131e-016 0 1 0
		 1 -2.2204460492503131e-016 2.2204460492503131e-016 0 -7.5367696436854939e-016 42.443413265848903 1.5629999999999973 1;
createNode joint -n "C_spine_mid" -p "C_spine_low";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -24.485 ;
	setAttr ".bps" -type "matrix" 2.9410358754422209e-016 0.91006980612847399 -0.41445500114401068 0
		 -1.1004859356777159e-016 0.41445500114401068 0.91006980612847399 0 1 -2.2204460492503131e-016 2.2204460492503131e-016 0
		 7.5645862043495751e-016 49.244459322848904 1.5629999999999973 1;
createNode joint -n "C_spine_up" -p "C_spine_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 17.46 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 2.4753428048985552e-016 0.99249288162712934 -0.12230241174840756 0
		 -1.9322109909374815e-016 0.12230241174840756 0.99249288162712934 0 1 -2.2204460492503131e-016 2.2204460492503131e-016 0
		 2.3157390737199554e-015 54.069473773268093 -0.63436041796122677 1;
createNode joint -n "C_neck" -p "C_spine_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 35.000000000000007 ;
	setAttr ".bps" -type "matrix" 9.1941142448845402e-017 0.88315235441492523 0.4690862595422875 0
		 -3.0025728880050126e-016 -0.4690862595422875 0.88315235441492523 0 1 -2.2204460492503131e-016 2.2204460492503131e-016 0
		 3.4667239773300428e-015 58.684367199070245 -1.2030421751665326 1;
createNode joint -n "C_head" -p "C_neck";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" 9.1941142448845402e-017 0.88315235441492523 0.4690862595422875 0
		 -3.0025728880050126e-016 -0.4690862595422875 0.88315235441492523 0 1 -2.2204460492503131e-016 2.2204460492503131e-016 0
		 3.6614554419847103e-015 60.554885085925108 -0.20951683996774095 1;
createNode joint -n "C_hair_a" -p "C_head";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -1.7988993353650006e-014 -8.506554402186945e-015 -161.93853888693286 ;
	setAttr ".bps" -type "matrix" 5.6801776279671728e-018 -0.6942004661037926 -0.71978171195250384 0
		 3.1396711409754562e-016 0.71978171195250384 -0.6942004661037926 0 1 -2.2204460492503131e-016 2.2204460492503131e-016 0
		 4.836228386815361e-015 62.90834146455321 -3.3967674579182319 1;
createNode joint -n "C_hair_b" -p "C_hair_a";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" 5.6801776279671728e-018 -0.6942004661037926 -0.71978171195250384 0
		 3.1396711409754562e-016 0.71978171195250384 -0.6942004661037926 0 1 -2.2204460492503131e-016 2.2204460492503131e-016 0
		 7.9902617784642151e-015 60.217822853469208 -6.1864315196484547 1;
createNode joint -n "R_hair_a" -p "C_head";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -90.000000000000028 -5.72293155009495 152.02549481359461 ;
	setAttr ".bps" -type "matrix" 0.099717993915552972 -0.99501573938084853 -8.5930817051772043e-006 0
		 -0.99501573941795407 -0.09971799391183428 -8.6117720060508232e-007 0 -4.440872922530662e-016 8.6361264096113111e-006 -0.99999999996270872 0
		 -2.8671152589999962 63.584947913082011 1.6851529379073205 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_hair_b" -p "R_hair_a";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" 0.099717993915552972 -0.99501573938084853 -8.5930817051772043e-006 0
		 -0.99501573941795407 -0.09971799391183428 -8.6117720060508232e-007 0 -4.440872922530662e-016 8.6361264096113111e-006 -0.99999999996270872 0
		 -2.5518069622390147 60.438708145159772 1.6851257665829682 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_hair_a" -p "C_head";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 90 5.723 152.02549481359461 ;
	setAttr ".bps" -type "matrix" -0.099719182637120429 -0.99501562024942258 -8.5930806763222991e-006 0
		 0.99501562028652824 -0.099719182633401821 -8.6118746646490427e-007 0 -1.1102422005951202e-016 -8.6361264095557999e-006 0.9999999999627085 0
		 2.867000000000004 63.584947913082011 1.6851529379073218 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_hair_b" -p "L_hair_a";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" -0.099719182637120429 -0.99501562024942258 -8.5930806763222991e-006 0
		 0.99501562028652824 -0.099719182633401821 -8.6118746646490427e-007 0 -1.1102422005951202e-016 -8.6361264095557999e-006 0.9999999999627085 0
		 2.5516879445014324 60.438708521853343 1.6851257665862227 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_shou" -p "C_spine_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 89.999999961537625 95.133067819216237 7.0254947749773784 ;
	setAttr ".bps" -type "matrix" -0.99598959486545358 -0.089469139479223636 -7.7260649659815823e-007 0
		 -0.089469139482559579 0.99598959482831184 8.6014920439243592e-006 0 -6.005995700583442e-011 8.6361210144825229e-006 -0.99999999996270883 0
		 -2.9999999999999973 56.015369202765307 -1.6658913994825033 1;
createNode joint -n "R_arm" -p "R_shou";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 1.6777232036321117e-009 -3.2330000000000099 -28.999999999999996 ;
	setAttr ".bps" -type "matrix" -0.82641966472747852 -0.5602241685133651 -0.056386334915746739 0
		 -0.56111722617498416 0.82773634596918466 7.1605907047718586e-006 0 0.046669007289781149 0.031645261495066961 -0.99840902499100803 0
		 -6.0228869367993916 55.743825107954791 -1.6658937443475579 1;
createNode joint -n "R_forearm" -p "R_arm";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 8.694 0 ;
	setAttr ".bps" -type "matrix" -0.82397998177858001 -0.55857163386149922 0.095156289720997697 0
		 -0.56111722617498416 0.82773634596918466 7.1605907047718586e-006 0 -0.078768319252493141 -0.053387933157950943 -0.9954623450815504 0
		 -13.609197563206376 50.60111766262257 -2.1835051589991346 1;
	setAttr ".radi" 0.82023496939984508;
createNode joint -n "R_hand" -p "R_forearm";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 10.379765482679861 4.9737991503207013e-014 1.7763568394002505e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" -0.82397970376510732 -0.55857001728491529 0.095168185721361626 0
		 -0.56111680983058698 0.82773662823723404 -1.3538974628912737e-017 0 -0.078774193164454789 -0.053400468769335262 -0.99546120789637238 0
		 -22.161916536490885 44.803275097862929 -1.1958051874932405 1;
	setAttr ".radi" 0.56767595449061559;
createNode joint -n "R_little_up" -p "R_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0.590970380988738 -14.482414706219664 -16.226267835295005 ;
	setAttr ".bps" -type "matrix" -0.63390623597234941 -0.75657844541218333 -0.16047410985281119 0
		 -0.7681203395710593 0.64009507152759648 0.016414729462340294 0 0.090299656326083499 0.13366882712048134 -0.98690354985866091 0
		 -25.632019898352937 42.259607151195311 -2.913063508750052 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_little_mid" -p "R_little_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -12.064788297508649 ;
	setAttr ".bps" -type "matrix" -0.45935375324058092 -0.87365819272234502 -0.16036050534005505 0
		 -0.88365157242593662 0.46781834823466117 -0.01748975712133451 0 0.090299656328037922 0.13366882712180622 -0.98690354985830264 0
		 -26.326426675430309 41.430820226640108 -3.0888534019255385 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_little_low" -p "R_little_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -12.695908440940824 ;
	setAttr ".bps" -type "matrix" -0.25391677974044113 -0.95511295819865483 -0.15259589131840781 0
		 -0.96300168277819609 0.26437072783734417 -0.052305613743800744 0 0.090299656326073383 0.13366882712049222 -0.98690354985866036 0
		 -26.772572454917807 40.582282661582411 -3.2446030004314896 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_ring_up" -p "R_hand";
	addAttr -ci true -sn "nts" -ln "notes" -dt "string";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0.14123002750477634 -5.5092581081042313 -19.536565932569978 ;
	setAttr ".bps" -type "matrix" -0.59374213539348031 -0.80463074615469599 -0.006295951153499707 0
		 -0.80440952983243308 0.59334915656812237 0.029361313231215706 0 -0.019889318066591763 0.022497571923093505 -0.99954903545769658 0
		 -25.857587616487123 42.018922732379053 -1.968358240146427 1;
	setAttr ".radi" 0.50509311612333974;
	setAttr ".nts" -type "string" "e";
createNode joint -n "R_ring_mid" -p "R_ring_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -14.431151032996956 ;
	setAttr ".bps" -type "matrix" -0.3745361762730739 -0.92711525743247269 -0.013414622568856446 0
		 -0.92699895776083263 0.37410046604277924 0.026865844801635799 0 -0.019889318064612346 0.022497571924435342 -0.99954903545770579 0
		 -26.911459979043666 40.59073016396141 -1.9795333422571346 1;
	setAttr ".radi" 0.50516461437401206;
createNode joint -n "R_ring_low" -p "R_ring_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -17.042041428667769 ;
	setAttr ".bps" -type "matrix" -0.086411613379588526 -0.99604446395901869 -0.020699248530504293 0
		 -0.99606096605578454 0.085960950868148422 0.021754696639147468 0 -0.019889318066592485 0.022497571923095122 -0.99954903545769647 0
		 -27.40223229506346 39.375887564413496 -1.9971111512678927 1;
	setAttr ".radi" 0.50516461437401206;
createNode joint -n "R_mid_up" -p "R_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -0.79051159286469874 -1.2143717114647856 -19.068999999999996 ;
	setAttr ".bps" -type "matrix" -0.59697967295576448 -0.7992983673308065 0.068828700844456331 0
		 -0.7985370133769033 0.6002728109323493 0.044846301101332572 0 -0.077161572979844523 -0.02818993504219524 -0.99661999740000873 0
		 -25.961647988794091 42.027246264316993 -0.86707750397564531 1;
	setAttr ".radi" 0.50872136540456825;
createNode joint -n "R_mid_mid" -p "R_mid_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -11.376350054507245 ;
	setAttr ".bps" -type "matrix" -0.42773704108825905 -0.90199972619369562 0.058630347325930456 0
		 -0.90060375045668228 0.43081529860125722 0.057541838295867787 0 -0.077161572977870907 -0.028189935040857331 -0.99661999740019946 0
		 -27.035763036743006 40.589109518741772 -0.74323753658238179 1;
	setAttr ".radi" 0.51085256936558676;
createNode joint -n "R_mid_low" -p "R_mid_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -10 ;
	setAttr ".bps" -type "matrix" -0.26485055424723736 -0.96310661508354156 0.047747585250506215 0
		 -0.9611973135471239 0.26763963746919073 0.066848701447016337 0 -0.077161572979840956 -0.028189935042198466 -0.99661999740000884 0
		 -27.582981917798811 39.435149881500429 -0.66822969299585988 1;
	setAttr ".radi" 0.511;
createNode joint -n "R_index_up" -p "R_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0.59308775766714306 6.3895832732851936 -16.213179438703964 ;
	setAttr ".bps" -type "matrix" -0.62183077353062755 -0.75675915564874408 0.20159878331007225 0
		 -0.77036602934929543 0.63739002642914955 0.016435785142448915 0 -0.14093498470891522 -0.14508457723151713 -0.97933027908599357 0
		 -25.540240803995278 42.034112570313489 0.12654210994687776 1;
	setAttr ".radi" 0.5223450655532379;
createNode joint -n "R_index_mid" -p "R_index_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -15.453515326680874 ;
	setAttr ".bps" -type "matrix" -0.39408067004842218 -0.89923670934779909 0.18993095075718594 0
		 -0.90820578922391337 0.41270304822144865 0.069558884471313032 0 -0.14093498470697585 -0.14508457723020243 -0.97933027908646753 0
		 -26.813087660662063 40.485076154931384 0.53920161232050123 1;
	setAttr ".radi" 0.50528357885421682;
createNode joint -n "R_index3_low" -p "R_index_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" -0.39408067004797048 -0.89923670934757816 0.18993095075917013 0
		 -0.90820578922381034 0.41270304822146542 0.069558884472557841 0 -0.14093498470890178 -0.14508457723152579 -0.97933027908599413 0
		 -27.22253747683764 39.550769214185657 0.73653987018998179 1;
	setAttr ".radi" 0.51937221256721877;
createNode joint -n "R_thumb_up" -p "R_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 14.886985293965845 15.043323263422893 -26.708259569266922 ;
	setAttr ".bps" -type "matrix" -0.44684559037498156 -0.82728936314139512 0.34047221325027238 0
		 -0.89414006128268675 0.40065321610684429 -0.19997637668643695 0 0.029027042078509174 -0.39378840772215112 -0.91874268474465992 0
		 -23.078646604160628 42.882269991719006 0.74756355378884431 1;
	setAttr ".radi" 0.51014485004768306;
createNode joint -n "R_thumb_mid" -p "R_thumb_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 -14.999999999999998 0 ;
	setAttr ".bps" -type "matrix" -0.42410694479385075 -0.90102010133172072 0.09108279954964868 0
		 -0.89414006128229084 0.40065321610711269 -0.19997637668766982 0 0.14369011861546829 -0.1662521501190373 -0.97555798002633864 0
		 -23.903983768039193 41.354241806362047 1.3764259099616392 1;
	setAttr ".radi" 0.50864846853279733;
createNode orientConstraint -n "R_hand_orientConstraint1" -p "R_hand";
	addAttr -ci true -k true -sn "w0" -ln "R_handCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" 0.011229025457863607 -0.030924284592994067 -0.24334345416648917 ;
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 50.963761770095218 -10.359332373526836 -8.0955135252939456 ;
	setAttr ".rsrr" -type "double3" 2.4675780451338428e-006 0 0 ;
	setAttr -k on ".w0";
createNode joint -n "sword_jnt" -p "R_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.0706286229204149 175.48189019540987 34.254228129883558 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".radi" 0.5;
createNode ikEffector -n "effector1" -p "R_forearm";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "L_shou" -p "C_spine_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 90.000000000000142 -84.86699999999999 -172.9745052250226 ;
	setAttr ".bps" -type "matrix" 0.99598970076656879 -0.089467960557533074 -7.7259631564143232e-007 0
		 0.089467960560868753 0.99598970072943271 8.6008216663634385e-006 0 -4.4409112729740334e-016 -8.6354524146126099e-006 0.99999999996271449 0
		 3.0000000000000027 56.015369202765299 -1.665891399482502 1;
createNode joint -n "L_arm" -p "L_shou";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 9.955077405921874e-017 3.233 -29.000000000000021 ;
	setAttr ".bps" -type "matrix" 0.82641969238185553 -0.56022437856778784 -0.056383842559865227 0
		 0.56111735892990033 0.82773625597517642 7.1924017312167299e-006 0 0.04666692137920634 -0.031643896765937179 0.99840916574641569 0
		 6.0228872582155031 55.74382868605138 -1.6658937443987663 1;
createNode joint -n "L_forearm" -p "L_arm";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 -8.694 0 ;
	setAttr ".bps" -type "matrix" 0.82398006458689765 -0.55857188651670309 0.095154089541879122 0
		 0.56111735892990033 0.82773625597517642 7.1924017312167299e-006 0 -0.078766507291525359 0.053386685019475355 0.9954625553940869 0
		 13.609198138482242 50.601119312475959 -2.1834822798925555 1;
	setAttr ".radi" 0.82023496939984508;
createNode joint -n "L_hand" -p "L_forearm";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 10.379765482679861 4.9737991503207013e-014 1.7763568394002505e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" 0.82397970376510754 -0.55857001728491529 0.095168185721361639 0
		 0.5611168098305872 0.82773662823723382 4.1386029802845115e-018 0 -0.078774193164454803 0.053400468769335206 0.99546120789637227 0
		 22.161917971297676 44.80327412521455 -1.1958051457299286 1;
	setAttr ".radi" 0.56767595449061559;
createNode joint -n "L_little_up" -p "L_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -0.591 14.482000000000001 -16.226267835295001 ;
	setAttr ".bps" -type "matrix" 0.63390683216557819 -0.75657946062010462 -0.16046696825700119 0
		 0.7681203402166964 0.64009508414297667 0.016414207302897339 0 0.090295465440900904 -0.13366302040499831 0.98690471976631267 0
		 25.632048770525735 42.25958757894923 -2.9134101901073288 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_little_mid" -p "L_little_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -12.064788297508649 ;
	setAttr ".bps" -type "matrix" 0.45935433613038712 -0.87365918814327914 -0.16035341234609887 0
		 0.88365169767223528 0.46781814837502755 -0.017488775026796684 0 0.090295465440900904 -0.13366302040499831 0.98690471976631267 0
		 26.326176751747042 41.43113306975571 -3.0891215204332179 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_little_low" -p "L_little_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -12.69590844094083 ;
	setAttr ".bps" -type "matrix" 0.25391732085247465 -0.95511388535780228 -0.15258918758491563 0
		 0.96300193306807624 0.2643703140933969 -0.052303096785057263 0 0.090295465435701605 -0.13366302040161171 0.98690471976724714 0
		 26.772209812127855 40.582809998314573 -3.2448246836451347 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_ring_up" -p "L_hand";
	addAttr -ci true -sn "nts" -ln "notes" -dt "string";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -0.141 5.509 -19.536565932569971 ;
	setAttr ".bps" -type "matrix" 0.5937420368573374 -0.80463085408272872 -0.0062914486884238924 0
		 0.80440945655781537 0.59334905732132415 0.029365326088031895 0 -0.019895222262169179 -0.022496329344904469 0.99954894592368237 0
		 25.85760040864141 42.018914060663853 -1.9685017199982591 1;
	setAttr ".radi" 0.50509311612333974;
	setAttr ".nts" -type "string" "e";
createNode joint -n "L_ring_mid" -p "L_ring_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -14.431151032996956 ;
	setAttr ".bps" -type "matrix" 0.37453609910722929 -0.92711533722122197 -0.01341126223439597 0
		 0.9269988622413079 0.37410034303002898 0.026870853133754084 0 -0.019895222262169179 -0.022496329344904469 0.99954894592368237 0
		 26.911472596299202 40.590721300677856 -1.979668830344792 1;
	setAttr ".radi" 0.50516461437401206;
createNode joint -n "L_ring_low" -p "L_ring_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -17.042041428667769 ;
	setAttr ".bps" -type "matrix" 0.086411567595818806 -0.99604450419257984 -0.020697503554567384 0
		 0.99606085211525397 0.085960809872413668 0.021760469882636812 0 -0.019895222267360832 -0.022496329341401194 0.99954894592365795 0
		 27.40224481119526 39.375878596565968 -1.9972422360014521 1;
	setAttr ".radi" 0.50516461437401206;
createNode joint -n "L_mid_up" -p "L_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0.791 1.214 -19.068999999999996 ;
	setAttr ".bps" -type "matrix" 0.59697924387259538 -0.7992981307183169 0.068835169902521975 0
		 0.79853630213192317 0.60027312279700817 0.044854790435386073 0 -0.077172252544415454 0.028190003152401964 0.99661916856916244 0
		 25.961623714578916 42.027262719631658 -0.86675258000844435 1;
	setAttr ".radi" 0.50872136540456825;
createNode joint -n "L_mid_mid" -p "L_mid_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -11.376350054507252 ;
	setAttr ".bps" -type "matrix" 0.42773676073029976 -0.90199955574607438 0.058635014742344484 0
		 0.90060296854783473 0.43081565101113961 0.057551436877601184 0 -0.077172252544415454 0.028190003152401964 0.99661916856916244 0
		 27.035737990497541 40.589126399783154 -0.74290097312923842 1;
	setAttr ".radi" 0.51085256936558676;
createNode joint -n "L_mid_low" -p "L_mid_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -10 ;
	setAttr ".bps" -type "matrix" 0.26485041392488701 -0.96310650842078571 0.04775051498507181 0
		 0.9611964948335191 0.26764001412268645 0.066858964695579726 0 -0.077172252549558604 0.028190003155932706 0.99661916856866428 0
		 27.582813307455694 39.435468968238382 -0.66790678908943013 1;
	setAttr ".radi" 0.511;
createNode joint -n "L_index_up" -p "L_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -0.593 -6.39 -16.213179438703964 ;
	setAttr ".bps" -type "matrix" 0.62182969051504389 -0.75675814843529809 0.20160590460287722 0
		 0.77036586029242049 0.63739019168334388 0.01643730031937133 0 -0.14094068713428431 0.14508910476892956 0.97932878768443565 0
		 25.540269501958946 42.034093116162545 0.12619763005206197 1;
	setAttr ".radi" 0.5223450655532379;
createNode joint -n "L_index_mid" -p "L_index_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 0 -15.453515326680874 ;
	setAttr ".bps" -type "matrix" 0.39407967123308024 -0.89923578258098846 0.18993741086680285 0
		 0.90820533770229717 0.41270347587984724 0.069562242385858675 0 -0.14094068713428431 0.14508910476892956 0.97932878768443565 0
		 26.813114141758387 40.485058762484044 0.53887170928526229 1;
	setAttr ".radi" 0.50528357885421682;
createNode joint -n "L_index_low" -p "L_index_mid";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".bps" -type "matrix" 0.39407967123150489 -0.89923578258058479 0.18993741087198379 0
		 0.90820533770220824 0.41270347587949263 0.069562242389121356 0 -0.14094068713926106 0.14508910477244191 0.97932878768319909 0
		 27.222518714653418 39.550853655516669 0.73619537331882468 1;
	setAttr ".radi" 0.51937221256721877;
createNode joint -n "L_thumb_up" -p "L_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" -14.887 -15.042999999999997 -26.708259569266922 ;
	setAttr ".bps" -type "matrix" 0.44684672815676318 -0.82729092956242711 0.34046691381003746 0
		 0.894139406124837 0.4006543141980935 -0.19997710601261226 0 0.02902970806467596 0.39378399964725669 0.9187444898727225 0
		 23.078628420254073 42.882282318456653 0.74781151516595123 1;
	setAttr ".radi" 0.51014485004768306;
createNode joint -n "L_thumb_mid" -p "L_thumb_up";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "xzy";
	setAttr ".jo" -type "double3" 0 14.999999999999998 0 ;
	setAttr ".bps" -type "matrix" 0.4241073537984838 -0.90102047348439318 0.091077213483988895 0
		 0.894139406124837 0.4006543141980935 -0.19997710601261226 0 0.14369298823802168 0.16624748682736648 0.97555835205015407 0
		 23.903967685642552 41.354251239878003 1.3766640831470058 1;
	setAttr ".radi" 0.50864846853279733;
createNode orientConstraint -n "L_hand_orientConstraint1" -p "L_hand";
	addAttr -ci true -k true -sn "w0" -ln "L_handCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" 0.044211369303856801 0.14306833380795353 0.19461563596405618 ;
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -38.139500542067665 -25.870481365180574 35.091014803710436 ;
	setAttr ".rsrr" -type "double3" 6.4909360044544591e-006 -0.00012663508036953985 
		8.7910008155518338e-006 ;
	setAttr -k on ".w0";
createNode joint -n "sword_jnt_02" -p "L_hand";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 2.956 -1.4715490535748188 -0.27516335537477377 ;
	setAttr ".r" -type "double3" 173.2452553 -56.604 155.761 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.0706286229204296 4.5181098045901082 34.254228129883579 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".radi" 0.5;
createNode ikEffector -n "effector2" -p "L_forearm";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode pointConstraint -n "C_root_pointConstraint1" -p "C_root";
	addAttr -ci true -k true -sn "w0" -ln "C_pelvisCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" 0.075865073429005558 0.21670564499845027 0.087072344262786761 ;
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -7.5367696436854939e-016 42.443413265848903 1.5629999999999973 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "C_root_orientConstraint1" -p "C_root";
	addAttr -ci true -k true -sn "w0" -ln "C_pelvisCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 180 -1.799193426557978e-014 90 ;
	setAttr ".o" -type "double3" 89.999999999999986 0 89.999999999999986 ;
	setAttr ".rsrr" -type "double3" -1.2722218725854067e-014 1.4124500153760508e-030 
		1.2722218725854067e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "C_root_scaleConstraint1" -p "C_root";
	addAttr -ci true -k true -sn "w0" -ln "C_pelvisCTRW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "MODEL" -p "character_1";
createNode transform -n "CHAR1_Body1" -p "MODEL";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "CHAR1_Body1Shape" -p "CHAR1_Body1";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".pv" -type "double2" 0.80466097593307495 0.52216202020645142 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".bnr" 0;
	setAttr ".bw" 3;
	setAttr ".dr" 1;
createNode mesh -n "CHAR1_Body1ShapeOrig" -p "CHAR1_Body1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 2799 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.99555898 0.48166999 0.95585197 
		0.46717599 0.95690799 0.44592401 0.99555898 0.44348499 0.98194402 0.73163301 0.98194402 
		0.67032099 0.98725802 0.67032099 0.98725802 0.73163301 0.93379498 0.73163301 0.93379498 
		0.67032099 0.93379498 0.85238397 0.93379498 0.82306898 0.95692801 0.82306898 0.95692801 
		0.85238397 0.93379498 0.79375398 0.95692801 0.79375398 0.98194402 0.82306892 0.98194402 
		0.85238397 0.98194402 0.79375392 0.98725802 0.82306892 0.98725802 0.85238397 0.98725802 
		0.79375392 0.98194402 0.762694 0.98725802 0.76269352 0.93379498 0.76269299 0.86589003 
		0.493653 0.99914098 0.76269299 0.874753 0.48118901 0.99914098 0.73163301 0.99914098 
		0.79375398 0.99914098 0.82306898 0.99914098 0.67032099 0.98186302 0.67024869 0.98194402 
		0.63375551 0.98725802 0.63375551 0.98719722 0.67026675 0.92847902 0.73163301 0.92847902 
		0.67032099 0.99888301 0.67032099 0.99888301 0.73163301 0.91556299 0.73163301 0.91556299 
		0.67032099 0.93379498 0.63391298 0.95692801 0.63391298 0.874753 0.48118901 0.91556299 
		0.63391298 0.92847902 0.63391298 0.86936498 0.484139 0.99888301 0.63391298 0.93379498 
		0.90600097 0.95692801 0.90600097 0.98194402 0.90600091 0.91556299 0.90600097 0.91556299 
		0.85238397 0.92847902 0.85238397 0.92847902 0.90600097 0.91556299 0.79375398 0.92847902 
		0.79375398 0.98725802 0.90600091 0.99888301 0.85238397 0.99888301 0.90600097 0.99888301 
		0.79375398 0.98194999 0.96752298 0.98726302 0.96752292 0.98726302 0.91718102 0.98194999 
		0.91718102 0.92848498 0.96752298 0.933801 0.96752298 0.933801 0.91718102 0.92848498 
		0.91718102 0.99888903 0.96752298 0.99888903 0.91718102 0.91556901 0.96752298 0.91556901 
		0.91718102 0.95693398 0.96752298 0.95693398 0.91718102 0.98726302 0.73579198 0.98194999 
		0.73579198 0.933801 0.73579198 0.92848498 0.73579198 0.99888903 0.73579198 0.91556901 
		0.73579198 0.95693398 0.73579198 0.98725802 0.93652701 0.98725802 0.96724302 0.98194402 
		0.96724302 0.98194402 0.93652701 0.93379498 0.93652701 0.92847902 0.93652701 0.93379498 
		0.96724302 0.92847902 0.96724302 0.99888301 0.93652701 0.99888301 0.96724302 0.91556299 
		0.93652701 0.91556299 0.96724302 0.95692801 0.96724302 0.95692801 0.93652701 0.95693398 
		0.99903798 0.933801 0.99903798 0.98194999 0.99903798 0.92848498 0.99903798 0.91556901 
		0.99903798 0.98726302 0.99903798 0.99888903 0.99903798 0.93379498 0.59015 0.95692801 
		0.59015 0.98194402 0.59015 0.91556299 0.59015 0.92847902 0.59015 0.98725802 0.59015 
		0.99888301 0.59015 0.95477498 0.73098302 0.95477498 0.67468399 0.98195392 0.67468399 
		0.98195392 0.73098302 0.95477498 0.78728199 0.98195392 0.78728199 0.99887002 0.67468399 
		0.99887002 0.73098302 0.98696595 0.73098302 0.98696595 0.67468399 0.99887002 0.78728199 
		0.98696595 0.78728199 0.98696595 0.95807499 0.98195398 0.95807499 0.99887002 0.95807499 
		0.95477498 0.95807499 0.98195398 0.78816402 0.95477498 0.78816402 0.99887002 0.78816402 
		0.98696601 0.78816402 0.95477498 0.99868703 0.98195392 0.99868703 0.98696595 0.99868703 
		0.99887002 0.99868703 0.98195398 0.94114202 0.98696595 0.94114202 0.99887002 0.94114202 
		0.95477498 0.94114202 0.98195398 0.83978599 0.98696595 0.83978599 0.98195398 0.89690202 
		0.98696595 0.89690202 0.99887002 0.83978599 0.99887002 0.89690202 0.95477498 0.83978599 
		0.95477498 0.89690202 0.98195398 0.59022701 0.98696601 0.59022701 0.99887002 0.59022701 
		0.95477498 0.59022701 0.81217402 0.072019003 0.82466602 0.0177 0.86677903 0.0058490001 
		0.84402001 0.075407997 0.78589398 0.058984999 0.794662 0.010955 0.75617999 0.041416001 
		0.75503498 0.004315 0.64006102 0.076154999 0.60737401 0.010756 0.645841 0.0089600002 
		0.65868199 0.062505998 0.681306 0.00046800001 0.69166303 0.044698998 0.72253001 0.001657 
		0.72602999 0.040614001 0.52156597 0.39934301 0.51183802 0.37743801 0.53997499 0.38089901 
		0.542292 0.395834 0.49509501 0.40173 0.49351901 0.37765899 0.471872 0.39818001 0.47750601 
		0.37285501 0.46577001 0.36978599 0.44945499 0.390172 0.44904301 0.372154 0.56766498 
		0.390663 0.55912298 0.37013501 0.58489799 0.38080499 0.57362503 0.35824499 0.59629703 
		0.35578001 0.58693498 0.34043401 0.87072301 0.65530002 0.87598503 0.65530002 0.87598503 
		0.685543 0.87072301 0.685543 0.87598503 0.71760798 0.87072301 0.71760798 0.87598503 
		0.76322502 0.87072301 0.76322502 0.87598503 0.80030799 0.87072301 0.80030799 0.87304002 
		0.83393401 0.874888 0.58178699 0.87598503 0.619223 0.87072301 0.619223 0.332955 0.013411 
		0.326208 0.019792 0.31779301 0.01095 0.32748601 0.0046649999 0.30798399 0.198293 
		0.298004 0.203031 0.288441 0.203952 0.29843301 0.191456 0.17206401 0.139643 0.167088 
		0.151986 0.157932 0.148339 0.162303 0.138955 0.195645 0.181517 0.20217 0.17692401 
		0.206155 0.18216801 0.200937 0.186483 0.18996599 0.175782 0.196346 0.168367 0.33102399 
		0.24658801 0.324936 0.250772 0.32467899 0.236858 0.33245 0.238444 0.390441 0.184407 
		0.39469099 0.176955 0.40213001 0.183109 0.398509 0.189548 0.37951601 0.17801601 0.38621601 
		0.17150199 0.181022 0.202296 0.17475 0.193819 0.182408 0.187047 0.188904 0.194112 
		0.175579 0.17813499 0.166821 0.186187 0.58789498 0.61893201 0.57547897 0.59608901 
		0.55457097 0.60147899 0.55730897 0.61510199 0.52726501 0.60958099 0.90665299 0.83047599 
		0.90665299 0.87167901 0.89979202 0.87167901 0.89979202 0.83047599 0.90665299 0.79916197 
		0.89979202 0.79916197 0.24746101 0.068181001 0.21705399 0.046478 0.23575699 0.045487002 
		0.082516 0.113479 0.076535001 0.090002999 0.103405 0.069472 0.11417 0.093115002;
	setAttr ".uvst[0].uvsp[250:499]" 0.202602 0.172079 0.232105 0.162429 0.228738 
		0.178124 0.22677401 0.094451003 0.201997 0.096046999 0.224042 0.120072 0.20179801 
		0.129155 0.25783801 0.170717 0.262353 0.155086 0.225004 0.97280401 0.248908 0.99251401 
		0.209446 0.98855197 0.277264 0.17840099 0.29562101 0.17337701 0.52804101 0.64049 
		0.558079 0.64149201 0.58854598 0.64237303 0.55735099 0.66921198 0.58854598 0.66820902 
		0.52726501 0.67114103 0.90665299 0.97063601 0.90665299 0.99918097 0.89979202 0.99918097 
		0.89979202 0.97063601 0.89979202 0.94243199 0.90665299 0.94243199 0.34174401 0.124415 
		0.310498 0.135911 0.31617999 0.093108997 0.353921 0.102523 0.33282501 0.164184 0.354709 
		0.145509 0.89979202 0.74309099 0.89979202 0.70958298 0.90665299 0.70958298 0.90665299 
		0.74309099 0.89979202 0.762954 0.90665299 0.762954 0.89979202 0.91163099 0.90665299 
		0.91163099 0.216943 0.016194999 0.23807301 0.021327 0.28946999 0.47085601 0.29141399 
		0.46630201 0.322164 0.47623399 0.31108299 0.480506 0.33091 0.48563299 0.26890701 
		0.50793499 0.28796399 0.51239699 0.287195 0.52178597 0.26681 0.51571101 0.269178 
		0.47593099 0.26516399 0.47286901 0.26364699 0.49179 0.25800899 0.493379 0.31095701 
		0.50337601 0.31919301 0.50896698 0.21400701 0.39483699 0.206241 0.40244001 0.147191 
		0.39228901 0.23253 0.41439599 0.954386 0.26490301 0.94121599 0.29288799 0.93554097 
		0.280112 0.96545798 0.296083 0.94917798 0.30462801 0.95321298 0.31685901 0.97470701 
		0.32247299 0.95608699 0.32760701 0.058045998 0.23476601 0.069725998 0.27008501 0.038424999 
		0.26776901 0.042234 0.230699 0.022523001 0.26663801 0.025101 0.22715899 0.042206999 
		0.18367 0.026567001 0.198946 0.006331 0.26660001 0.011666 0.23058499 0.023584001 
		0.15769599 0.043760002 0.151614 0.002383 0.157042 0.0091970004 0.19756 0.115019 0.238435 
		0.127552 0.28007901 0.185261 0.249818 0.188384 0.28036499 0.146648 0.040134002 0.152403 
		0.012647 0.096432999 0.033775002 0.091770001 0.00135 0.077613004 0.069013 0.141114 
		0.069101997 0.042948999 0.081371002 0.041558001 0.036773 0.064891003 0.033833999 
		0.060642999 0.084067002 0.058991 0.0023390001 0.043306999 0.0061840001 0.025599999 
		0.035439 0.027372999 0.0088769998 0.015961999 0.079186998 0.010754 0.121601 0.029988 
		0.124026 0.073977001 0.145778 0.151456 0.97428399 0.193156 0.952402 0.197135 0.99580902 
		0.222846 0.95815903 0.063386001 0.114661 0.050795998 0.123401 0.59111702 0.93998599 
		0.571823 0.92429203 0.58001 0.946446 0.63952702 0.96235102 0.93464202 0.249547 0.91606802 
		0.26969099 0.89221901 0.26368201 0.913252 0.24269199 0.88795698 0.26644999 0.584773 
		0.99484003 0.64522201 0.99363202 0.13222601 0.363965 0.19675601 0.34888399 0.69638699 
		0.98865098 0.68685502 0.954588 0.092840999 0.41107401 0.078984998 0.381266 0.72241002 
		0.98156899 0.70879 0.94715899 0.070127003 0.423715 0.053456001 0.39477599 0.91390598 
		0.81821603 0.91390598 0.86107099 0.90834099 0.86107099 0.90834099 0.81821603 0.90834099 
		0.77535999 0.91390598 0.77535999 0.083145998 0.59118301 0.083247997 0.62257099 0.029565999 
		0.61831999 0.029461002 0.58563602 0.083374001 0.66138202 0.029671 0.65063 0.082621001 
		0.42896 0.082718998 0.45916 0.029053001 0.459784 0.028960999 0.43143401 0.082834996 
		0.494899 0.029162997 0.49358201 0.082950003 0.53054798 0.029281998 0.53038198 0.083054997 
		0.56305403 0.029382998 0.56180102 0.019316001 0.561032 0.019402999 0.58770198 0.0051139998 
		0.58836901 0.00502 0.55946702 0.019504 0.61921102 0.0052140001 0.61939299 0.019626001 
		0.65670401 0.0053500002 0.66152102 0.018975999 0.455861 0.0046740002 0.45254299 0.0045710001 
		0.420672 0.018883999 0.42756999 0.019096 0.49299201 0.0047940002 0.48969501 0.019206 
		0.52683502 0.0049069999 0.52436602 0.40335101 0.28204301 0.40114799 0.29040399 0.379605 
		0.26801601 0.38791201 0.264691 0.381666 0.235709 0.38862899 0.238557 0.41649401 0.210798 
		0.418446 0.222302 0.466474 0.233334 0.458123 0.23890901 0.43371999 0.227267 0.43621299 
		0.21586999 0.466474 0.27056399 0.45737299 0.26450601 0.43747199 0.28922701 0.43590599 
		0.280967 0.120054 0.49498001 0.120188 0.536466 0.119931 0.45685101 0.119807 0.418789 
		0.120477 0.625651 0.120602 0.66428101 0.120383 0.596524 0.120292 0.56845301 0.257572 
		0.018285001 0.26242301 0.034341998 0.096404001 0.124536 0.079512 0.132912 0.241832 
		0.190303 0.265017 0.05486 0.291428 0.062838003 0.275745 0.080857001 0.075347997 0.158066 
		0.097782999 0.157241 0.128117 0.173683 0.130743 0.18300501 0.119646 0.18864 0.115722 
		0.18453801 0.109536 0.14393599 0.30850399 0.18514501 0.33055699 0.18532699 0.322943 
		0.197248 0.344069 0.20263 0.337098 0.20547 0.31193301 0.24890099 0.30914599 0.23872399 
		0.31573999 0.235489 0.30996099 0.033569999 0.31819001 0.037939999 0.31330401 0.045832001 
		0.29932001 0.047414001 0.067097999 0.17415801 0.072221003 0.16481601 0.085946999 
		0.175863 0.079145998 0.183171 0.29851201 0.014607 0.30142701 0.023282999 0.289327 
		0.028777 0.288838 0.016205 0.31701499 0.028229 0.309239 0.016821001 0.30763501 0.006201 
		0.076082997 0.19092999 0.058026999 0.183871 0.324799 0.033900999 0.332196 0.213138 
		0.33152601 0.222544 0.32344499 0.218426 0.32386899 0.208396 0.17428701 0.15708201 
		0.180263 0.147572 0.185973 0.156424 0.180718 0.16336399 0.30644301 0.222651 0.30561799 
		0.211882 0.31169999 0.20770501 0.31352901 0.21842 0.394541 0.197111 0.38599101 0.192237 
		0.390075 0.202627;
	setAttr ".uvst[0].uvsp[500:749]" 0.38133901 0.20037401 0.376203 0.152006 0.380288 
		0.16269501 0.36532301 0.16540501 0.148839 0.17061999 0.15952501 0.15661 0.168422 
		0.16923501 0.15856799 0.178146 0.347518 0.19682001 0.35793301 0.17964 0.36153701 
		0.19562601 0.42193899 0.121136 0.44132 0.12504999 0.44075099 0.1309 0.420968 0.13101999 
		0.439165 0.138376 0.42012301 0.13852 0.170835 0.21425299 0.180434 0.231544 0.173219 
		0.2362 0.160108 0.221635 0.421974 0.113924 0.44058999 0.117386 0.39608401 0.118008 
		0.403292 0.107327 0.41216701 0.109184 0.40834099 0.119309 0.148425 0.205153 0.16026001 
		0.197006 0.16524 0.20599701 0.154475 0.212092 0.401061 0.141351 0.39514199 0.130459 
		0.40784299 0.131008 0.41089499 0.141174 0.37553701 0.131542 0.37972301 0.116098 0.38990501 
		0.146101 0.153109 0.18636 0.14102601 0.195171 0.39249399 0.101126 0.38097 0.039308 
		0.394279 0.025852 0.40018299 0.031177999 0.389256 0.044961002 0.402722 0.036442999 
		0.39707899 0.049302999 0.151342 0.22231001 0.157585 0.23346201 0.15163 0.23777901 
		0.14282 0.227909 0.376059 0.032283001 0.38779801 0.023672 0.36719999 0.053644001 
		0.36062601 0.046684001 0.36675599 0.038311999 0.37408999 0.046571001 0.130164 0.212313 
		0.141093 0.20618001 0.145632 0.21451101 0.13689101 0.219249 0.38664001 0.066543996 
		0.37813899 0.060063999 0.38362399 0.052485 0.39357799 0.059110999 0.357759 0.063054003 
		0.36929399 0.070176996 0.384051 0.077059999 0.13548701 0.19788501 0.124335 0.203811 
		0.35079101 0.051284999 0.32298699 0.072957002 0.28012899 0.033353999 0.339766 0.084164999 
		0.124467 0.106995 0.094577998 0.172672 0.14261501 0.157572 0.13884901 0.115205 0.110042 
		0.188969 0.37870899 0.092992999 0.359929 0.082723998 0.374668 0.18629 0.37222999 
		0.195134 0.25722101 0.19399101 0.34688401 0.070569001 0.33866301 0.059248 0.336108 
		0.071429998 0.33186701 0.061409 0.31809601 0.251378 0.33177999 0.232604 0.324231 
		0.228789 0.307888 0.232187 0.314868 0.228457 0.186757 0.17102499 0.192692 0.16299599 
		0.41446501 0.194461 0.413131 0.19921599 0.202621 0.21494301 0.19761699 0.218761 0.193609 
		0.21544801 0.200293 0.2101 0.40714401 0.208047 0.41061601 0.204511 0.16056 0.237791 
		0.155385 0.24262799 0.115187 0.19345701 0.31877401 0.002778 0.33417401 0.021691 0.281995 
		0.017663 0.059558999 0.210628 0.050113998 0.20475499 0.053702001 0.195775 0.065716997 
		0.204044 0.31085199 0.054053001 0.183419 0.23872299 0.178322 0.241823 0.59601003 
		0.045434002 0.54953998 0.097556002 0.55565399 0.001553 0.63315398 0.07344 0.59502798 
		0.13176399 0.90817702 0.97133303 0.91411901 0.97133303 0.91411901 0.99927998 0.90817702 
		0.99927998 0.90817702 0.94207901 0.91411901 0.94207901 0.57641798 0.213617 0.532305 
		0.18183801 0.89841098 0.936185 0.89019197 0.936185 0.89019197 0.87042397 0.89841098 
		0.87042397 0.89841098 0.99923903 0.89019197 0.99923903 0.89789402 0.80982703 0.88988101 
		0.80982703 0.88988101 0.75099403 0.89789402 0.75099403 0.89789402 0.86832798 0.88988101 
		0.86832798 0.44960201 0.095220998 0.49930599 0.084495999 0.49326801 0.16962799 0.44243699 
		0.175328 0.43169999 0.033142999 0.49357101 0.0066 0.90817702 0.896415 0.91411901 
		0.896415 0.157838 0.89788902 0.139833 0.93633699 0.13279501 0.93630201 0.153431 0.893121 
		0.162086 0.95410103 0.160175 0.96037197 0.19202401 0.94106102 0.198807 0.94383901 
		0.90817702 0.86355102 0.91411901 0.86355102 0.84603798 0.93118203 0.84406102 0.94002002 
		0.86601597 0.95133901 0.86799502 0.94401199 0.83066899 0.95634699 0.829952 0.98530698 
		0.85556901 0.98250902 0.43611401 0.96051502 0.45132101 0.93323702 0.43502799 0.92415696 
		0.43788001 0.91657001 0.45269001 0.92469901 0.463494 0.96933597 0.494304 0.98176402 
		0.48209399 0.959288 0.483722 0.95120299 0.50348097 0.95363802 0.502819 0.94085598 
		0.52421403 0.992899 0.52430701 0.95621198 0.52678502 0.94824499 0.293026 0.95995301 
		0.313059 0.99483597 0.31959099 0.94914699 0.41679299 0.91507298 0.41869199 0.925102 
		0.315855 0.94111001 0.348515 0.94447899 0.34566599 0.93531197 0.34578401 0.97124898 
		0.36911899 0.92847699 0.36942399 0.93850106 0.39492199 0.93307799 0.392894 0.92371601 
		0.46636799 0.939794 0.47241199 0.93336803 0.54980302 0.963238 0.548585 0.95317101 
		0.37558901 0.962403 0.40204799 0.95796001 0.28673199 0.9522 0.51191503 0.209205 0.50506699 
		0.190419 0.48780301 0.19998699 0.50391299 0.21735901 0.53935599 0.247326 0.534697 
		0.25253499 0.56444299 0.28665301 0.558088 0.288055 0.57207102 0.31122199 0.56392002 
		0.30835199 0.57018298 0.32758099 0.56385398 0.32381701 0.56300002 0.34198001 0.557643 
		0.338433 0.55122399 0.35327801 0.54714698 0.34883401 0.53625 0.360365 0.53461498 
		0.35507599 0.49965701 0.363271 0.50235599 0.358197 0.46472999 0.35899901 0.46714601 
		0.352707 0.43268701 0.33854201 0.42927101 0.34831399 0.81974798 0.97536099 0.81887501 
		0.987486 0.82926399 0.99698597 0.83667701 0.997648 0.82279903 0.99363202 0.42857701 
		0.307987 0.40759799 0.306301 0.40986499 0.32842499 0.46543801 0.21439099 0.47640499 
		0.23705401 0.368112 0.31198499 0.37513 0.28644201 0.35121199 0.27932599 0.34392801 
		0.300457 0.51997203 0.26451501 0.86155897 0.96749198 0.33168501 0.27243501 0.325196 
		0.29626 0.31604299 0.31926501 0.33274901 0.32613799 0.87310302 0.92947102 0.85241997 
		0.918751 0.87121999 0.92076403 0.85926002 0.91443199 0.84405601 0.99613601 0.47540301 
		0.33445501;
	setAttr ".uvst[0].uvsp[750:999]" 0.47991699 0.26249099 0.50525802 0.275094 
		0.30096999 0.28734699 0.31503099 0.265214 0.36667299 0.270201 0.38367701 0.32070401 
		0.394815 0.296078 0.50421202 0.33078599 0.53162599 0.33963999 0.54207402 0.327811 
		0.51322401 0.31175101 0.50172198 0.344706 0.52130902 0.34685299 0.54955 0.31502599 
		0.52746701 0.290941 0.54323298 0.287848 0.55277598 0.30601299 0.379188 0.34392199 
		0.41253501 0.34939 0.36102301 0.33640501 0.29039001 0.30343601 0.49160001 0.297023 
		0.469394 0.283306 0.451262 0.297362 0.48157799 0.316302 0.247188 0.71560401 0.27801201 
		0.73852801 0.281905 0.706294 0.27618501 0.673325 0.31846499 0.73619199 0.31608701 
		0.70480001 0.237491 0.65056598 0.197281 0.69752598 0.36841601 0.70646101 0.36641201 
		0.67942899 0.433788 0.70158899 0.41167 0.64918399 0.46680301 0.67749 0.453688 0.63191301 
		0.51321399 0.64731002 0.46159199 0.60563499 0.506001 0.59234297 0.505503 0.57994401 
		0.44473699 0.59108001 0.44823399 0.60219198 0.54397601 0.59712303 0.54470301 0.587475 
		0.32384399 0.68270302 0.36512399 0.65400797 0.35971001 0.642856 0.321904 0.660492 
		0.32564899 0.66992003 0.566594 0.59347099 0.56940699 0.58363301 0.406818 0.628088 
		0.402215 0.61947697 0.45159 0.46134001 0.40271801 0.46971101 0.422939 0.53364599 
		0.46454999 0.52114898 0.50641799 0.53146303 0.503295 0.45613399 0.331444 0.51008898 
		0.367659 0.56738597 0.39286 0.55063999 0.369183 0.48504901 0.29830399 0.52885503 
		0.25666901 0.551512 0.294 0.609981 0.329831 0.58753598 0.54418403 0.52684402 0.54165399 
		0.45507401 0.58209002 0.45740899 0.57950997 0.52711898 0.23343199 0.82010198 0.2793 
		0.815202 0.276575 0.75972003 0.23897199 0.74241 0.32370999 0.81127501 0.319702 0.75822002 
		0.18044899 0.81596702 0.189355 0.73275399 0.39566401 0.80115002 0.37536699 0.73189503 
		0.444435 0.78436202 0.43914399 0.72184098 0.480694 0.70184702 0.52974898 0.68202901 
		0.50102198 0.79053599 0.547019 0.76914501 0.74200499 0.40616199 0.74213701 0.42759401 
		0.76557499 0.42826501 0.75830197 0.41063401 0.74200499 0.448544 0.75786102 0.444437 
		0.80466098 0.52216202 0.78563601 0.59516901 0.85840601 0.61977202 0.87191802 0.56864297 
		0.78468299 0.61908901 0.85802102 0.63431799 0.73627299 0.492277 0.719181 0.58449602 
		0.71606702 0.61442798 0.67076302 0.489824 0.60229403 0.53468698 0.59773099 0.57663399 
		0.65826201 0.57922 0.592879 0.587946 0.65331799 0.60285002 0.58556598 0.52749598 
		0.58404601 0.57499403 0.582627 0.58556199 0.818865 0.45806301 0.76421499 0.448587 
		0.615776 0.926036 0.641545 0.91665798 0.65740001 0.839818 0.59876901 0.84999198 0.63961202 
		0.92159402 0.33144301 0.51008898 0.45158899 0.46134001 0.718045 0.94624501 0.71856999 
		0.90388298 0.70569199 0.88467401 0.66605097 0.91951799 0.83144701 0.756051 0.78516799 
		0.74787998 0.785137 0.78793502 0.83005202 0.81027299 0.823367 0.68597901 0.78947902 
		0.68652701 0.86132097 0.75094301 0.85257697 0.68497002 0.865704 0.80858701 0.86385 
		0.85293901 0.820198 0.84629798 0.83532202 0.93859202 0.85348397 0.90621901 0.80710298 
		0.88511699 0.79389 0.92014498 0.77143502 0.86624998 0.766698 0.89283901 0.76842803 
		0.97413599 0.79032302 0.99139798 0.74223298 0.908786 0.77895498 0.83342201 0.74485701 
		0.745996 0.74832898 0.782951 0.75305402 0.68263799 0.74513799 0.82113898 0.75544298 
		0.84735501 0.71202099 0.74262702 0.71365601 0.77687299 0.71359003 0.81183898 0.660519 
		0.72366703 0.66683799 0.75399899 0.67171597 0.78208101 0.59057999 0.73055398 0.595218 
		0.75988102 0.59528798 0.78711599 0.73619902 0.84343803 0.61876398 0.44350001 0.68014902 
		0.393433 0.734712 0.42256501 0.71326298 0.83905798 0.71705002 0.68553799 0.707636 
		0.86593801 0.56606197 0.91124803 0.55042303 0.947712 0.57053399 0.99003798 0.51095802 
		0.90344799 0.52989298 0.88667899 0.45817301 0.86068797 0.46020499 0.838561 0.33184001 
		0.88401997 0.30755901 0.88878697 0.30616501 0.88295001 0.360708 0.88145202 0.364353 
		0.87504703 0.29214299 0.90071303 0.40517101 0.85489202 0.415003 0.86789697 0.396992 
		0.90874302 0.36571199 0.91589898 0.372293 0.89731097 0.379428 0.895109 0.38036901 
		0.85726303 0.74606198 0.321926 0.74600601 0.33962801 0.75315702 0.345745 0.325537 
		0.84432298 0.73755699 0.35519201 0.74524301 0.36548099 0.580181 0.81243199 0.58828199 
		0.825409 0.181081 0.870296 0.22394 0.90797502 0.16606399 0.87576002 0.204767 0.90884203 
		0.282617 0.98517197 0.28165299 0.882855 0.26302701 0.99831599 0.71961403 0.35024899 
		0.697478 0.329395 0.69672501 0.333812 0.718633 0.358796 0.68863398 0.33409399 0.69076699 
		0.339248 0.88881397 0.603912 0.87886 0.603912 0.87886 0.65139699 0.88881397 0.65139699 
		0.88881397 0.58504701 0.87886 0.58504701 0.66263998 0.67281002 0.59241402 0.66546202 
		0.691993 0.83398002 0.66268498 0.82335001 0.68889302 0.84777403 0.59873199 0.837973 
		0.90791398 0.77357101 0.91438502 0.77357101 0.91438502 0.768915 0.90791398 0.76875502 
		0.79159302 0.66436601 0.85403198 0.67720097 0.71391898 0.677212 0.87886 0.99939603 
		0.88881397 0.99939603 0.88881397 0.95556498 0.87886 0.95556498 0.88881397 0.903319 
		0.87886 0.903319 0.88881397 0.852732 0.87886 0.852732 0.88881397 0.80416697 0.87886 
		0.80416697 0.88881397 0.75023699 0.87886 0.75023699 0.88881397 0.69716799 0.87886 
		0.69716799 0.65599102 0.66640598 0.60838801 0.64975601 0.91438502 0.75259697 0.90791398 
		0.752437 0.91438502 0.71937102 0.90791398 0.71921098 0.91438502 0.68173403;
	setAttr ".uvst[0].uvsp[1000:1249]" 0.90791398 0.68157399 0.91438502 0.65025997 
		0.90791398 0.65025997 0.91438502 0.62368101 0.90791398 0.62368101 0.90791398 0.63011199 
		0.91438502 0.63027197 0.89970797 0.70733899 0.90679502 0.70733899 0.90679502 0.67487299 
		0.89970797 0.67487299 0.90679502 0.63490802 0.89970797 0.63490802 0.90679502 0.59865803 
		0.89970797 0.59865803 0.06177 0.37739199 0.029486001 0.35735199 0.066434003 0.366409 
		0.000726 0.34677801 0.012446 0.32493901 0.040125001 0.33289999 0.081575997 0.34467399 
		0.077137999 0.35719901 0.047148 0.30349699 0.084399 0.30847901 0.022214999 0.297876 
		0.052016001 0.27680701 0.087173998 0.282258 0.018122001 0.277403 0.86482298 0.29125801 
		0.875929 0.27714401 0.851053 0.260692 0.85052902 0.28457201 0.86316401 0.29870099 
		0.89868999 0.29448801 0.11059 0.34865201 0.108608 0.327021 0.13696299 0.32626 0.13733999 
		0.34463099 0.16431899 0.34979999 0.90529698 0.288266 0.88130802 0.26982799 0.85529202 
		0.2516 0.122658 0.30867299 0.086560003 0.354895 0.98124999 0.363736 0.98967803 0.34946501 
		0.98052001 0.349453 0.97624302 0.35606101 0.97242302 0.34944201 0.96858901 0.355712 
		0.96393198 0.36373499 0.95517701 0.34941801 0.96437001 0.34943101 0.96860701 0.34316099 
		0.96397197 0.335125 0.981291 0.33517399 0.97626102 0.34283301 0.167229 0.323823 0.151326 
		0.31258401 0.171425 0.28693101 0.18741199 0.305271 0.147734 0.28260899 0.163802 0.330529 
		0.170522 0.34825799 0.859483 0.295091 0.84723401 0.29073399 0.83467901 0.278442 0.83103591 
		0.28736901 0.82129198 0.32386199 0.82110697 0.32063001 0.816387 0.319401 0.81581002 
		0.32505801 0.83142602 0.32141301 0.82996202 0.31743199 0.83134001 0.355533 0.82553202 
		0.361889 0.83579999 0.32965899 0.82684499 0.30707499 0.843575 0.302093 0.84438801 
		0.29509401 0.831034 0.294175 0.82723302 0.30151999 0.83935797 0.25960699 0.84829998 
		0.24763 0.82624102 0.37343201 0.83913201 0.36427599 0.87434101 0.33148199 0.83993202 
		0.314105 0.83981901 0.31989399 0.911915 0.40733799 0.88329202 0.359247 0.85274398 
		0.36616799 0.88227701 0.41684499 0.940961 0.38559201 0.93679398 0.345631 0.86743498 
		0.35065901 0.87357599 0.352896 0.86391705 0.338218 0.86224699 0.34078601 0.85078901 
		0.338763 0.85206002 0.34332299 0.84530199 0.34798601 0.84828901 0.34804699 0.85377997 
		0.35563701 0.85349703 0.35887301 0.97645998 0.38324901 0.98150599 0.375377 0.979442 
		0.37132299 0.97368801 0.38098201 0.94014603 0.392932 0.918648 0.42588499 0.95035702 
		0.42444801 0.95035702 0.40000001 0.93149102 0.439174 0.94106197 0.43989599 0.84123403 
		0.34278399 0.85011899 0.3362 0.85669601 0.32091299 0.860861 0.32810801 0.92439997 
		0.30699101 0.92830801 0.303222 0.89001697 0.347036 0.83815902 0.425771 0.962861 0.41763699 
		0.96520698 0.40227199 0.96157998 0.40080801 0.95849001 0.41822401 0.98254001 0.43424901 
		0.966371 0.424263 0.96245301 0.42778599 0.97903502 0.43936399 0.97978401 0.418513 
		0.98254001 0.40294799 0.98176199 0.386659 0.24981301 0.23487 0.23549999 0.239567 
		0.226777 0.189678 0.26333401 0.22863699 0.20981 0.240338 0.22308201 0.242158 0.226423 
		0.27372801 0.2423 0.26958501 0.203281 0.27754301 0.25916299 0.264503 0.282776 0.26144999 
		0.26162699 0.31465799 0.25134099 0.31723899 0.271934 0.31275299 0.23353501 0.31995699 
		0.242533 0.31837699 0.24617399 0.355948 0.25833401 0.355463 0.23585799 0.35215899 
		0.27034101 0.34973401 0.28099501 0.34373099 0.34539199 0.46529001 0.298715 0.46036401 
		0.321547 0.36382699 0.379103 0.35570899 0.97839499 0.24272101 0.97839499 0.19302399 
		0.99516702 0.19302399 0.99516702 0.24272101 0.97839499 0.141964 0.99516702 0.141964 
		0.264155 0.43550301 0.275273 0.36567101 0.99516702 0.28339401 0.97839499 0.28339401 
		0.42660201 0.382458 0.40710101 0.46104699 0.98653799 0.099017002 0.24692801 0.40879601 
		0.236388 0.35895699 0.211741 0.355708 0.078387998 0.86338001 0.081082001 0.82403702 
		0.031546999 0.82787198 0.031812999 0.86847401 0.117829 0.86239898 0.132465 0.81054503 
		0.133652 0.77193803 0.084756002 0.77556098 0.028607 0.78154498 0.140929 0.73546302 
		0.083912998 0.73092502 0.14352199 0.7087 0.013413 0.734779 0.987198 0.33757299 0.465426 
		0.413306 0.45746201 0.454209 0.13115899 0.673051 0.092211001 0.681669 0.00056100002 
		0.70766997 0.075625002 0.910941 0.121523 0.90965199 0.015082 0.91201001 0.077380002 
		0.94884503 0.13126101 0.95090598 0.0063809999 0.94962198 0.10769 0.99681997 0.012342 
		0.99152201 0.500269 0.442287 0.148076 0.99800497 0.56345999 0.75862497 0.557724 0.74835998 
		0.568546 0.74821198 0.55222899 0.73884201 0.57234198 0.73582798 0.59579098 0.284632 
		0.595029 0.249239 0.60057199 0.24583399 0.55090398 0.68632299 0.56445199 0.69575101 
		0.55158299 0.69275302 0.571271 0.70037001 0.55306602 0.70290703 0.587677 0.28861901 
		0.56054199 0.26458099 0.56824601 0.263749 0.54810601 0.72743398 0.57701302 0.72182798 
		0.56802702 0.217888 0.55200797 0.71279597 0.57731199 0.70961398 0.56304699 0.22940999 
		0.212198 0.47925699 0.241419 0.46776599 0.22468799 0.41746899 0.152504 0.4524 0.92017901 
		0.121457 0.92640603 0.149901 0.97088099 0.14610501 0.95497799 0.11652 0.91302198 
		0.085139997 0.16816351 0.50728846 0.152587 0.45256001 0.129302 0.50850099 0.182408 
		0.187047 0.211144 0.50066102 0.1688325 0.54729497 0.198144 0.532938 0.17475 0.193819 
		0.221536 0.58401299 0.193675 0.59118199 0.227553 0.53986198 0.225697 0.51158601 0.137428 
		0.62213999 0.145833 0.630988;
	setAttr ".uvst[0].uvsp[1250:1499]" 0.14355837 0.59260398 0.126542 0.57413203 
		0.93960601 0.19987001 0.94668001 0.227863 0.95545 0.22400901 0.95780802 0.19086599 
		0.12758 0.63573003 0.136482 0.64478201 0.59686202 0.141798 0.606094 0.172635 0.64453101 
		0.170681 0.61463797 0.139498 0.642739 0.171257 0.208869 0.65820497 0.220846 0.65118498 
		0.212281 0.644333 0.204326 0.64879203 0.242531 0.62159401 0.223692 0.61576903 0.60874802 
		0.367163 0.62921602 0.34478199 0.60053402 0.34158301 0.59686202 0.36505899 0.19389801 
		0.62680101 0.64481598 0.303983 0.60431701 0.317366 0.246186 0.59313601 0.99824297 
		0.018469 0.96672702 0.032375 0.97548997 0.043234002 0.964544 0.020563001 0.98606199 
		0.052827001 0.96534902 0.002353 0.94520903 0.0028359999 0.935085 0.029344 0.956972 
		0.060998 0.97695798 0.070701003 0.93745703 0.050113998 0.90539402 0.067258 0.92934602 
		0.078872003 0.909841 0.035486002 0.97047698 0.094241999 0.92078 0.003426 0.234873 
		0.49750799 0.249302 0.491469 0.25229201 0.50340003 0.25562999 0.539024 0.67197901 
		0.248899 0.61120701 0.25015 0.69813597 0.241863 0.72448498 0.28665799 0.694543 0.30925199 
		0.678693 0.29145601 0.78281999 0.25942901 0.83455098 0.24171799 0.82015598 0.29021099 
		0.78109097 0.28337899 0.66238099 0.334016 0.67279202 0.33627599 0.65465498 0.32250401 
		0.65353602 0.361967 0.63043302 0.36498299 0.87688601 0.039928999 0.91346598 0.0027449999 
		0.884637 0.057766002 0.90272701 0.042116001 0.889328 0.065595999 0.844935 0.081042998 
		0.87247199 0.093558997 0.80245 0.110315 0.85492301 0.126867 0.79249901 0.331429 0.80202299 
		0.33332101 0.78052598 0.329162 0.79170299 0.358843 0.77741098 0.36919999 0.88183099 
		0.243844 0.88311499 0.23120201 0.84796399 0.203017 0.88358802 0.20854799 0.91619599 
		0.235999 0.93752301 0.22225399 0.78004801 0.23857901 0.81431401 0.22620399 0.75304502 
		0.27675301 0.72743797 0.31459299 0.71821499 0.23563799 0.82417297 0.19935399 0.80954701 
		0.169559 0.84792799 0.168827 0.87303501 0.182427 0.91033399 0.170095 0.86096102 0.15413 
		0.90900302 0.115794 0.89578497 0.078382 0.81044501 0.136169 0.65296602 0.191771 0.65962201 
		0.14631499 0.70458502 0.16766199 0.70177001 0.198617 0.74076599 0.222497 0.70936799 
		0.14938299 0.71812898 0.149004 0.73429602 0.16301601 0.78409803 0.20478 0.73013997 
		0.19652 0.77670699 0.128736 0.76346099 0.079037003 0.73577398 0.131318 0.70950401 
		0.084839001 0.71648097 0.139633 0.674932 0.115964 0.70723897 0.14128099 0.94378501 
		0.446973 0.93603498 0.58266801 0.92681801 0.58483797 0.92935801 0.57581902 0.92264801 
		0.58042198 0.91983998 0.57581902 0.97540301 0.579211 0.97540301 0.56562698 0.997509 
		0.56585801 0.99200398 0.58554602 0.988482 0.54754102 0.99895501 0.50222802 0.98875803 
		0.51261902 0.96983898 0.509103 0.97156203 0.48893499 0.99694699 0.53654599 0.97759801 
		0.55122298 0.95952201 0.54934603 0.97060698 0.47527999 0.98611999 0.48387501 0.93981701 
		0.462901 0.91502601 0.45676699 0.93330902 0.446035 0.89304501 0.43533099 0.90578699 
		0.42929301 0.87867498 0.43118599 0.89151102 0.42013499 0.902408 0.45429799 0.88985699 
		0.442103 0.892416 0.48407501 0.89824897 0.490601 0.889548 0.47899899 0.878425 0.434802 
		0.91150099 0.50501001 0.92575002 0.53865802 0.96701199 0.50829202 0.94662201 0.54420501 
		0.90359598 0.52924597 0.88965398 0.50347501 0.886962 0.51238298 0.88489097 0.518103 
		0.88369298 0.48909101 0.87151802 0.51236397 0.87344402 0.518103 0.88385499 0.539985 
		0.90513003 0.55496001 0.952806 0.560624 0.94143999 0.57192498 0.93831301 0.56174999 
		0.95722997 0.56688899 0.95706397 0.57378101 0.92624301 0.56764799 0.93393898 0.55293101 
		0.914015 0.559084 0.96937102 0.58034003 0.97075802 0.566966 0.95800197 0.55293101 
		0.87998402 0.57191199 0.91522199 0.57567602 0.891267 0.58406699 0.86832899 0.54867202 
		0.86043698 0.51329499 0.86043203 0.51000601 0.874753 0.48118901 0.86936498 0.484139 
		0.86262101 0.47850701 0.864227 0.475007 0.87410802 0.48453799 0.869573 0.489948 0.86605799 
		0.49010301 0.855757 0.489788 0.86750799 0.50266898 0.855757 0.496014 0.86589003 0.493653 
		0.87221098 0.49494201 0.87018597 0.49152699 0.87622797 0.48944199 0.93300599 0.174885 
		0.96434402 0.168486 0.142257 0.56143397 0.129814 0.54546899 0.60865098 0.211393 0.65601498 
		0.21059 0.65671903 0.20995 0.92970598 0.162393 0.96761203 0.157295 0.152225 0.548024 
		0.126277 0.52444798 0.60992903 0.23077101 0.66639698 0.229424 0.15726739 0.59693927 
		0.14655015 0.59551489 0.15673371 0.55357426 0.84125799 0.222367 0.81924301 0.212779 
		0.76154202 0.187043 0.76757902 0.17237601 0.79682201 0.18717 0.77361703 0.157708 
		0.76243198 0.21363799 0.74584103 0.191782 0.81004697 0.15095299 0.77498102 0.144915 
		0.349733 0.005438 0.349733 0.020748001 0.36504301 0.020748001 0.36504301 0.005438 
		0.95690799 0.44592401 0.95585197 0.46717599 0.99555898 0.48166999 0.99555898 0.44348499 
		0.95585197 0.46717599 0.99555898 0.48166999 0.99555898 0.48166999 0.95585197 0.46717599 
		0.95690799 0.44592401 0.99555898 0.44348499 0.864227 0.475007 0.874753 0.48118901 
		0.86936498 0.484139 0.86262101 0.47850701 0.86832899 0.54867202 0.88385499 0.539985 
		0.87344402 0.518103 0.87344402 0.518103 0.86750799 0.50266898 0.86043197 0.51000601 
		0.88385499 0.539985 0.87344402 0.518103 0.88385499 0.539985 0.86832899 0.54867202 
		0.108608 0.327021 0.87151802 0.51236397 0.86043698 0.51329499 0.87151802 0.51236397 
		0.98194402 0.73163301 0.122658 0.30867299;
	setAttr ".uvst[0].uvsp[1500:1749]" 0.86043698 0.51329499 0.98725802 0.73163301 
		0.93379498 0.67032099 0.93379498 0.73163301 0.87151802 0.51236397 0.151326 0.31258401 
		0.92847902 0.67032099 0.92847902 0.73163301 0.75786102 0.444437 0.084399 0.30847901 
		0.95692801 0.73163301 0.95692801 0.67032099 0.93379498 0.73163301 0.93379498 0.67032099 
		0.855757 0.496014 0.86043197 0.51000601 0.087173998 0.282258 0.86750799 0.50266898 
		0.86750799 0.50266898 0.147734 0.28260899 0.92847902 0.67032099 0.65599102 0.66640598 
		0.93379498 0.63391298 0.93379498 0.67032099 0.92847902 0.63391298 0.93379498 0.85238397 
		0.95692801 0.85238397 0.75830197 0.41063401 0.93379498 0.79375398 0.95692801 0.79375398 
		0.76557499 0.42826501 0.87018597 0.49152699 0.98194402 0.85238391 0.86936498 0.484139 
		0.86262101 0.47850701 0.98194402 0.79375392 0.855757 0.489788 0.98725802 0.85238397 
		0.87018597 0.49152699 0.98725802 0.79375398 0.92847902 0.90600097 0.92847902 0.85238397 
		0.93379498 0.85238397 0.93379498 0.90600097 0.92847902 0.85238397 0.92847902 0.79375398 
		0.93379498 0.79375398 0.93379498 0.85238397 0.1688325 0.54729497 0.171425 0.28693101 
		0.16816351 0.50728846 0.889548 0.47899899 0.892416 0.48407501 0.212281 0.644333 0.223692 
		0.61576903 0.204326 0.64879203 0.95585197 0.46717599 0.212281 0.644333 0.92848498 
		0.96752298 0.92848498 0.91718102 0.87998402 0.57191199 0.90513009 0.55496001 0.933801 
		0.96752298 0.933801 0.91718102 0.88489097 0.518103 0.83134001 0.355533 0.92848498 
		0.73579198 0.313059 0.99483597 0.90513009 0.55496001 0.933801 0.73579198 0.86605799 
		0.49010301 0.98611999 0.48387501 0.86605799 0.49010301 0.99895501 0.50222802 0.92575002 
		0.53865802 0.103405 0.069472 0.87998402 0.57191199 0.13696299 0.32626 0.902408 0.45429799 
		0.90513009 0.55496001 0.891267 0.58406699 0.91522199 0.57567602 0.39566401 0.80115002 
		0.98194402 0.73163301 0.98725802 0.73163301 0.225697 0.51158601 0.152587 0.45256001 
		0.98194402 0.67032099 0.98725802 0.67032099 0.91522199 0.57567602 0.92847902 0.79375398 
		0.92847902 0.73163301 0.93379498 0.73163301 0.93379498 0.79375398 0.92847902 0.73163301 
		0.92847902 0.67032099 0.93379498 0.67032099 0.93379498 0.73163301 0.891267 0.58406699 
		0.129302 0.50850099 0.99888301 0.73163301 0.95497799 0.11652 0.91522199 0.57567602 
		0.99888301 0.67032099 0.93393898 0.55293101 0.91556299 0.73163301 0.92847902 0.73163301 
		0.91556299 0.73163301 0.91556299 0.67032099 0.92847902 0.67032099 0.92847902 0.73163301 
		0.86605799 0.49010301 0.95692801 0.73163301 0.93831301 0.56174999 0.855757 0.489788 
		0.97156203 0.48893499 0.95692801 0.67032099 0.952806 0.560624 0.98254001 0.40294799 
		0.61463797 0.139498 0.93379498 0.73163301 0.86957294 0.489948 0.87410802 0.48453799 
		0.137428 0.62213999 0.93379498 0.67032099 0.87410802 0.48453799 0.86957294 0.489948 
		0.95800197 0.55293101 0.93393898 0.55293101 0.93379498 0.90600097 0.92847902 0.90600097 
		0.95800197 0.55293101 0.952806 0.560624 0.93831301 0.56174999 0.92847902 0.93652701 
		0.875929 0.27714401 0.92847902 0.96724302 0.93393898 0.55293101 0.95800197 0.55293101 
		0.145833 0.630988 0.93379498 0.96724302 0.93379498 0.93652701 0.95780802 0.19086599 
		0.87410802 0.48453799 0.86957294 0.489948 0.97626102 0.34283301 0.494304 0.98176402 
		0.90665299 0.79916197 0.86482298 0.29125801 0.952806 0.560624 0.93831301 0.56174999 
		0.933801 0.99903798 0.92848498 0.99903798 0.933801 0.99903798 0.92848498 0.99903798 
		0.36911899 0.92847699 0.34566599 0.93531197 0.95722997 0.56688899 0.97075802 0.566966 
		0.50348097 0.95363802 0.93379498 0.63391298 0.95692801 0.63391298 0.97060698 0.47527999 
		0.93379498 0.63391298 0.87886 0.69716799 0.98611999 0.48387501 0.502819 0.94085598 
		0.98194402 0.63391298 0.55457097 0.60147899 0.211144 0.50066102 0.97075802 0.566966 
		0.91556299 0.67032099 0.91556299 0.63391298 0.92847902 0.63391298 0.92847902 0.67032099 
		0.91556299 0.63391298 0.92847902 0.63391298 0.95722997 0.56688899 0.98725802 0.63391298 
		0.99888301 0.63391298 0.90578699 0.42929301 0.93330902 0.446035 0.97075802 0.566966 
		0.93379498 0.63391298 0.93379498 0.67032099 0.92847902 0.67032099 0.92847902 0.63391298 
		0.93379498 0.59015 0.93379498 0.63391298 0.92847902 0.63391298 0.92847902 0.59015 
		0.95722997 0.56688899 0.45269001 0.92469901 0.50391299 0.21735901 0.51191503 0.209205 
		0.95706397 0.57378101 0.96937102 0.58034003 0.91438502 0.75259697 0.96937102 0.58034003 
		0.95706397 0.57378101 0.91438502 0.71937102 0.534697 0.25253499 0.96937102 0.58034003 
		0.95706397 0.57378101 0.53935599 0.247326 0.914015 0.559084 0.92624301 0.56764799 
		0.198144 0.532938 0.89151102 0.42013499 0.914015 0.559084 0.483722 0.95120299 0.47241199 
		0.93336803 0.92624301 0.56764799 0.914015 0.559084 0.212198 0.47925699 0.152504 0.4524 
		0.93330902 0.446035 0.91502601 0.45676699 0.92624301 0.56764799 0.14355837 0.59260398 
		0.548585 0.95317101 0.94143999 0.57192498 0.52678502 0.94824499 0.97088099 0.14610501 
		0.94143999 0.57192498 0.988482 0.54754102 0.84438801 0.29509401 0.94143999 0.57192498 
		0.902408 0.45429799 0.41869199 0.925102 0.39492199 0.93307799 0.53935599 0.247326 
		0.89970797 0.67487299 0.864227 0.475007 0.40204799 0.95796001 0.83579999 0.32965899 
		0.16431899 0.34979999 0.87867498 0.43118599 0.558088 0.288055 0.34578401 0.97124898 
		0.37558901 0.962403 0.36942399 0.938501 0.348515 0.94447899 0.89979202 0.87167901 
		0.90665299 0.87167901 0.89304501 0.43533099 0.90578699 0.42929301 0.49965701 0.363271 
		0.49965701 0.363271 0.50235599 0.358197;
	setAttr ".uvst[0].uvsp[1750:1999]" 0.46714601 0.352707 0.46472999 0.35899901 
		0.42927101 0.34831399 0.52130902 0.34685299 0.47991699 0.26249099 0.43268701 0.33854201 
		0.55277598 0.30601299 0.99694699 0.53654599 0.55122399 0.35327801 0.55122399 0.35327801 
		0.85669595 0.32091299 0.88369298 0.48909101 0.719181 0.58449602 0.90665299 0.762954 
		0.89979202 0.762954 0.89979202 0.79916197 0.52726501 0.60958099 0.90817702 0.896415 
		0.91411901 0.896415 0.91411901 0.86355102 0.90817702 0.86355102 0.162086 0.95410103 
		0.160175 0.96037197 0.198807 0.94383901 0.19202401 0.94106102 0.139833 0.93633699 
		0.13279501 0.93630201 0.90665299 0.83047599 0.55730897 0.61510199 0.157838 0.89788902 
		0.153431 0.893121 0.89979202 0.83047599 0.92575002 0.53865802 0.90817702 0.94207901 
		0.91411901 0.94207901 0.45132101 0.93323702 0.43611401 0.96051502 0.49357101 0.0066 
		0.49930599 0.084495999 0.54953998 0.097556002 0.55565399 0.001553 0.06177 0.37739199 
		0.49326801 0.16962799 0.532305 0.18183801 0.83142602 0.32141301 0.43169999 0.033142999 
		0.44960201 0.095220998 0.71391898 0.677212 0.99895501 0.50222802 0.040125001 0.33289999 
		0.44243699 0.175328 0.43788001 0.91657001 0.029486001 0.35735199 0.89789402 0.86832798 
		0.89789402 0.80982703 0.88988101 0.80982703 0.88988101 0.86832798 0.90359604 0.52924597 
		0.89789402 0.75099403 0.88988101 0.75099403 0.95545 0.22400901 0.89841098 0.99923903 
		0.89841098 0.936185 0.89019197 0.936185 0.89019197 0.99923903 0.48209399 0.959288 
		0.89841098 0.87042397 0.89019197 0.87042397 0.46636799 0.939794 0.57641798 0.213617 
		0.59502798 0.13176399 0.077137999 0.35719901 0.081575997 0.34467399 0.463494 0.96933597 
		0.90817702 0.97133303 0.91411901 0.97133303 0.52421403 0.992899 0.58854598 0.64237303 
		0.90817702 0.99927998 0.91411901 0.99927998 0.90359604 0.52924597 0.59601003 0.045434002 
		0.889548 0.47899899 0.066434003 0.366409 0.63315398 0.07344 0.59601003 0.045434002 
		0.59502798 0.13176399 0.79159302 0.66436601 0.85403198 0.67720097 0.892416 0.48407501 
		0.167088 0.151986 0.17428701 0.15708201 0.180263 0.147572 0.17206401 0.139643 0.096403994 
		0.124536 0.109536 0.14393599 0.13884901 0.115205 0.124467 0.106995 0.30850399 0.18514501 
		0.29562101 0.17337701 0.277264 0.17840099 0.29843301 0.191456 0.31169999 0.20770501 
		0.32386899 0.208396 0.322943 0.197248 0.30798399 0.198293 0.354709 0.145509 0.37553701 
		0.131542 0.34174401 0.124415 0.90791398 0.76875502 0.37972301 0.116098 0.353921 0.102523 
		0.90791398 0.77357101 0.359929 0.082723998 0.339766 0.084164999 0.71705002 0.68553799 
		0.34688401 0.070569001 0.336108 0.071429998 0.87886 0.80416697 0.88881397 0.75023699 
		0.92078 0.003426 0.32298699 0.072957002 0.31617999 0.093108997 0.75305402 0.68263799 
		0.291428 0.062838003 0.28012899 0.033353999 0.26242301 0.034341998 0.265017 0.054860003 
		0.69672501 0.333812 0.39469099 0.176955 0.298004 0.203031 0.30561799 0.211882 0.25722101 
		0.19399101 0.87221098 0.49494201 0.25783801 0.170717 0.88965398 0.50347501 0.95585197 
		0.46717599 0.23210499 0.162429 0.241832 0.190303 0.84530205 0.34798601 0.33186701 
		0.061409 0.74832898 0.782951 0.40784299 0.131008 0.420968 0.13101999 0.42193899 0.121136 
		0.40834102 0.119309 0.130743 0.18300501 0.14102601 0.195171 0.153109 0.18636 0.148839 
		0.17061999 0.154475 0.212092 0.160108 0.221635 0.170835 0.21425299 0.16524 0.20599701 
		0.415003 0.86789697 0.376203 0.152006 0.38990501 0.146101 0.91438502 0.65025997 0.41089499 
		0.141174 0.42012301 0.13852 0.51095802 0.90344799 0.55042303 0.947712 0.183419 0.23872299 
		0.180434 0.231544 0.17321901 0.23620002 0.96858901 0.355712 0.178322 0.241823 0.87622797 
		0.48944196 0.41216701 0.109184 0.55042303 0.947712 0.86224699 0.34078601 0.421974 
		0.113924 0.326208 0.019792 0.31779301 0.01095 0.309239 0.016821001 0.31701499 0.028229 
		0.075347997 0.158066 0.094577998 0.172672 0.110042 0.188969 0.097782999 0.157241 
		0.058026999 0.183871 0.053702001 0.195775 0.065716997 0.204044 0.076082997 0.19092999 
		0.77143502 0.86624998 0.766698 0.89283901 0.281995 0.017663 0.257572 0.018285001 
		0.31085199 0.054053001 0.70569199 0.88467401 0.90359604 0.52924597 0.91983998 0.57581902 
		0.31330401 0.045832001 0.29932001 0.047414001 0.718045 0.94624501 0.71856999 0.90388298 
		0.33417401 0.021691 0.78563601 0.59516901 0.44823399 0.60219198 0.324799 0.033900999 
		0.059558999 0.210628 0.63961202 0.92159402 0.36504301 0.020748001 0.050113998 0.20475499 
		0.30763501 0.006201 0.97540301 0.56562698 0.92575002 0.53865802 0.31877401 0.002778 
		0.288838 0.016205 0.90791398 0.63011199 0.99200398 0.58554602 0.289327 0.028777 0.332955 
		0.013411 0.281905 0.706294 0.97540301 0.579211 0.27618501 0.673325 0.31608701 0.70480001 
		0.32748601 0.0046649999 0.95690799 0.44592401 0.36929399 0.070176996 0.357759 0.063054003 
		0.82110697 0.32063001 0.38362399 0.052485 0.389256 0.044961002 0.38097 0.039308 0.37408999 
		0.046571001 0.119646 0.18864 0.124335 0.203811 0.13548701 0.19788501 0.87221098 0.49494201 
		0.13689101 0.219249 0.14282 0.227909 0.151342 0.22231001 0.145632 0.21451101 0.115187 
		0.19345701 0.96245301 0.42778599 0.115722 0.18453801 0.234873 0.49750799 0.37870899 
		0.092992999 0.71326298 0.83905798 0.37536699 0.73189503 0.39357799 0.059110999 0.39707899 
		0.049302999 0.964544 0.020563001 0.67171597 0.78208101 0.16056 0.237791 0.157585 
		0.23346201 0.15163 0.23777901 0.59773099 0.57663399 0.155385 0.24262799;
	setAttr ".uvst[0].uvsp[2000:2249]" 0.60229403 0.53468698 0.36675599 0.038311999 
		0.87886 0.75023699 0.87886 0.65139699 0.376059 0.032283001 0.374668 0.18629 0.37951601 
		0.17801601 0.36532298 0.16540501 0.35793301 0.17964 0.843575 0.302093 0.38599101 
		0.192237 0.390441 0.184407 0.181081 0.870296 0.394541 0.197111 0.41061601 0.204511 
		0.413131 0.19921599 0.398509 0.189548 0.15856799 0.178146 0.166821 0.186187 0.175579 
		0.17813499 0.168422 0.16923501 0.181022 0.202296 0.193609 0.21544801 0.200293 0.2101 
		0.188904 0.194112 0.37222999 0.195134 0.38133901 0.20037401 0.22394 0.90797502 0.892416 
		0.48407501 0.40714401 0.208047 0.74606198 0.321926 0.74600601 0.33962801 0.390075 
		0.202627 0.202621 0.21494301 0.51321399 0.64731002 0.92681801 0.58483797 0.93149102 
		0.439174 0.89824897 0.490601 0.19761699 0.218761 0.40213001 0.183109 0.75315702 0.345745 
		0.57018298 0.32758099 0.41446501 0.194461 0.31352901 0.21842 0.314868 0.228457 0.324231 
		0.228789 0.32344499 0.218426 0.88881397 0.95556498 0.31573999 0.23548898 0.32467899 
		0.23685798 0.36504301 0.005438 0.180718 0.16336399 0.186757 0.17102499 0.192692 0.16299599 
		0.185973 0.156424 0.88489097 0.518103 0.18996599 0.175782 0.196346 0.168367 0.97242302 
		0.34944201 0.307888 0.232187 0.21705399 0.046478 0.36504301 0.005438 0.30644301 0.222651 
		0.30914599 0.23872399 0.349733 0.020748001 0.129814 0.54546899 0.68889302 0.84777403 
		0.33152601 0.222544 0.349733 0.005438 0.88881397 0.99939603 0.33177999 0.232604 0.87886 
		0.99939603 0.87886 0.95556498 0.66268498 0.82335001 0.33245 0.238444 0.31809601 0.251378 
		0.50506699 0.190419 0.31193301 0.24890099 0.96534902 0.002353 0.691993 0.83398002 
		0.324936 0.250772 0.15726739 0.59693927 0.956972 0.060998 0.97695798 0.070701003 
		0.33866301 0.059248 0.74485701 0.745996 0.142257 0.56143397 0.152225 0.548024 0.35079101 
		0.051284999 0.77895498 0.83342201 0.73627299 0.492277 0.288441 0.203952 0.82129198 
		0.32386199 0.221536 0.58401299 0.332196 0.213138 0.337098 0.20547 0.36504301 0.005438 
		0.349733 0.005438 0.36153701 0.19562601 0.82996202 0.31743199 0.206155 0.18216801 
		0.45269001 0.92469901 0.380288 0.16269501 0.580181 0.81243199 0.85011899 0.3362 0.38621601 
		0.17150199 0.734712 0.42256501 0.89824897 0.490601 0.85274398 0.36616799 0.39249399 
		0.101126 0.85840601 0.61977202 0.384051 0.077059999 0.128117 0.173683 0.33102399 
		0.24658801 0.92935801 0.57581902 0.14261501 0.157572 0.33055699 0.18532699 0.344069 
		0.20263 0.347518 0.19682001 0.148839 0.17061999 0.15952501 0.15661 0.157932 0.148339 
		0.886962 0.51238298 0.162303 0.138955 0.32370999 0.81127501 0.31846499 0.73619199 
		0.979442 0.37132299 0.96701199 0.50829202 0.94014603 0.392932 0.27801201 0.73852801 
		0.276575 0.75972003 0.23343199 0.82010198 0.74524301 0.36548099 0.079512 0.132912 
		0.94378501 0.446973 0.072221003 0.16481601 0.085946999 0.175863 0.97540301 0.579211 
		0.886962 0.51238298 0.24746101 0.068181001 0.23575699 0.045487002 0.23897199 0.74241 
		0.11417 0.093115002 0.082516 0.113479 0.24718802 0.71560401 0.84723401 0.29073399 
		0.91983998 0.57581902 0.96701199 0.50829202 0.33282501 0.164184 0.83913201 0.36427599 
		0.67197901 0.248899 0.91438502 0.77357101 0.310498 0.135911 0.91438502 0.768915 0.97368801 
		0.38098201 0.93330902 0.446035 0.87622797 0.48944196 0.275745 0.080857001 0.126277 
		0.52444798 0.96672702 0.032375 0.74223298 0.908786 0.66639698 0.229424 0.96761203 
		0.157295 0.79389 0.92014498 0.15673371 0.55357426 0.80710298 0.88511699 0.820198 
		0.84629798 0.36841598 0.70646101 0.92681801 0.58483797 0.92935801 0.57581902 0.94520903 
		0.0028359999 0.74513799 0.82113898 0.36719999 0.053644001 0.36062601 0.046684001 
		0.75544298 0.84735501 0.818865 0.45806301 0.76421499 0.448587 0.130164 0.212313 0.141093 
		0.20618001 0.57547897 0.59608901 0.909841 0.035486002 0.38664001 0.066543996 0.37813899 
		0.060063999 0.126542 0.57413203 0.047148 0.30349699 0.68014902 0.393433 0.935085 
		0.029344 0.97624302 0.35606101 0.97540301 0.56562698 0.73619902 0.84343803 0.87886 
		0.75023699 0.141114 0.069101997 0.660519 0.72366703 0.88881397 0.903319 0.60229403 
		0.53468698 0.95849001 0.41822401 0.59773099 0.57663399 0.58404601 0.57499403 0.592879 
		0.587946 0.88881397 0.69716799 0.88881397 0.75023699 0.71365601 0.77687299 0.93745703 
		0.050113998 0.394279 0.025852 0.38779801 0.023672 0.90539402 0.067258 0.248908 0.99251401 
		0.58404601 0.57499403 0.59773099 0.57663399 0.90665299 0.87167901 0.200937 0.186483 
		0.402722 0.036442999 0.40018299 0.031177999 0.66683799 0.75399899 0.97548991 0.043234002 
		0.98606199 0.052827001 0.88881397 0.65139699 0.71359003 0.81183898 0.71202099 0.74262702 
		0.88329202 0.359247 0.911915 0.40733799 0.39608401 0.118008 0.403292 0.107327 0.88369298 
		0.48909101 0.592879 0.587946 0.148425 0.205153 0.16026001 0.197006 0.40517101 0.85489196 
		0.88227701 0.41684499 0.401061 0.141351 0.39514199 0.130459 0.394815 0.296078 0.93679398 
		0.345631 0.940961 0.38559201 0.99200398 0.58554602 0.86391699 0.338218 0.85078901 
		0.338763 0.41167 0.64918399 0.87357599 0.352896 0.86743498 0.35065901 0.99200398 
		0.58554602 0.46020499 0.838561 0.45817301 0.86068797 0.65331799 0.60285002 0.93603498 
		0.58266801 0.227553 0.53986198 0.96157998 0.40080801 0.855757 0.496014 0.89151102 
		0.42013499 0.46159199 0.60563499;
	setAttr ".uvst[0].uvsp[2250:2499]" 0.52989298 0.88667899 0.453688 0.63191301 
		0.90679502 0.67487299 0.44132 0.12504999 0.44058999 0.117386 0.87622797 0.48944196 
		0.96393198 0.36373499 0.97903502 0.43936399 0.50102198 0.79053599 0.439165 0.138376 
		0.44075099 0.1309 0.85206002 0.34332299 0.707636 0.86593801 0.97978401 0.418513 0.99824303 
		0.018469 0.97047698 0.094241999 0.92934602 0.078872003 0.38036904 0.85726303 0.89824897 
		0.490601 0.293026 0.95995301 0.859483 0.295091 0.94662201 0.54420501 0.94662201 0.54420501 
		0.43502799 0.92415702 0.237491 0.65056598 0.49160001 0.297023 0.52974898 0.68202901 
		0.83993208 0.314105 0.319702 0.75822002 0.93603498 0.58266801 0.93603498 0.58266801 
		0.95035702 0.40000001 0.57207102 0.31122199 0.89970797 0.63490802 0.988482 0.54754102 
		0.396992 0.90874302 0.96701199 0.50829202 0.48157799 0.316302 0.95585197 0.46717599 
		0.988482 0.54754102 0.90791398 0.65025997 0.60838801 0.64975601 0.92264801 0.58042198 
		0.73755699 0.35519201 0.91150105 0.50501001 0.14655015 0.59551489 0.95517701 0.34941801 
		0.90791398 0.752437 0.146648 0.040134002 0.87886 0.852732 0.88881397 0.80416697 0.30561799 
		0.211882 0.46680301 0.67749 0.444435 0.78436202 0.480694 0.70184702 0.433788 0.70158899 
		0.66263998 0.67281002 0.392894 0.92371601 0.88881397 0.603912 0.87886 0.603912 0.506001 
		0.59234297 0.54397601 0.59712303 0.30996099 0.033569999 0.31819001 0.037939999 0.067097999 
		0.17415801 0.641545 0.91665798 0.65740001 0.839818 0.079145998 0.183171 0.29851201 
		0.014607 0.30142701 0.023282999 0.36641201 0.67942899 0.98611999 0.48387501 0.823367 
		0.68597901 0.506001 0.59234297 0.96434402 0.168486 0.84828901 0.34804699 0.78947902 
		0.68652701 0.997509 0.56585801 0.83005202 0.81027299 0.83144701 0.756051 0.78516799 
		0.74787998 0.785137 0.78793502 0.97540301 0.579211 0.97540301 0.56562698 0.997509 
		0.56585801 0.91150105 0.50501001 0.349733 0.005438 0.36504301 0.020748001 0.66605097 
		0.91951799 0.997509 0.56585801 0.202602 0.172079 0.224042 0.120072 0.78468299 0.61908901 
		0.59773099 0.57663399 0.91983998 0.57581902 0.325537 0.84432298 0.88985699 0.442103 
		0.56300002 0.34198001 0.92264801 0.58042198 0.51322401 0.31175101 0.98150599 0.375377 
		0.87434101 0.33148196 0.92935801 0.57581902 0.282617 0.98517197 0.91150105 0.50501001 
		0.18044899 0.81596702 0.92439997 0.30699101 0.92830801 0.303222 0.87018597 0.49152699 
		0.2793 0.815202 0.886962 0.51238298 0.95035702 0.42444801 0.76842803 0.97413599 0.50172198 
		0.344706 0.92264801 0.58042198 0.228738 0.178124 0.65826201 0.57922 0.67076302 0.48982397 
		0.197281 0.69752598 0.189355 0.73275399 0.98124999 0.363736 0.56444299 0.28665301 
		0.23807301 0.021327 0.85349703 0.35887301 0.85377997 0.35563698 0.082950003 0.53054798 
		0.083054997 0.56305403 0.120292 0.56845301 0.120188 0.536466 0.860861 0.32810801 
		0.083145998 0.59118301 0.120383 0.596524 0.53461498 0.35507599 0.82926399 0.99698597 
		0.54714698 0.34883401 0.083247997 0.62257099 0.120477 0.625651 0.42857701 0.307987 
		0.083374001 0.66138202 0.120602 0.66428101 0.40759799 0.306301 0.082621001 0.42896 
		0.082718998 0.45916 0.119931 0.45685101 0.119807 0.418789 0.40986499 0.32842499 0.082834996 
		0.494899 0.120054 0.49498001 0.53162599 0.33963999 0.46543801 0.21439099 0.47640499 
		0.23705401 0.52746701 0.290941 0.48780301 0.19998699 0.43590599 0.280967 0.40335101 
		0.28204301 0.40114799 0.29040399 0.43747199 0.28922701 0.019206 0.52683502 0.019316001 
		0.561032 0.029383 0.56180102 0.029282 0.53038198 0.45737299 0.26450601 0.91438502 
		0.63027197 0.88965398 0.50347501 0.466474 0.27056399 0.019096 0.49299201 0.37513003 
		0.28644201 0.35121199 0.27932599 0.029162999 0.49358201 0.458123 0.23890901 0.052016001 
		0.27680701 0.90791398 0.68157399 0.466474 0.233334 0.018975999 0.455861 0.34392801 
		0.300457 0.368112 0.31198499 0.029053001 0.459784 0.91438502 0.68173403 0.43621299 
		0.21586999 0.43371999 0.227267 0.97645998 0.38324901 0.018883999 0.42756999 0.96983898 
		0.509103 0.99895501 0.50222802 0.028960999 0.43143401 0.38862899 0.238557 0.418446 
		0.222302 0.41649401 0.210798 0.381666 0.235709 0.019504 0.61921102 0.019626001 0.65670401 
		0.029671 0.65063 0.029565999 0.61831999 0.38791201 0.264691 0.87191802 0.56864297 
		0.90791398 0.71921098 0.379605 0.26801601 0.019402999 0.58770198 0.81974798 0.97536099 
		0.379188 0.34392199 0.029461 0.58563602 0.36102301 0.33640501 0.41253501 0.34939 
		0.51997203 0.26451501 0.557643 0.338433 0.91438502 0.71937102 0.851053 0.260692 0.90791398 
		0.71921098 0.80466098 0.52216202 0.99694699 0.53654599 0.86601597 0.95133901 0.0049069999 
		0.52436602 0.00502 0.55946702 0.84406102 0.94002002 0.325196 0.29626 0.0047940002 
		0.48969501 0.54955 0.31502599 0.54207402 0.327811 0.33168501 0.27243501 0.0046740002 
		0.45254299 0.33274901 0.32613799 0.54323298 0.287848 0.56392002 0.30835199 0.0045710001 
		0.420672 0.31604299 0.31926501 0.53625 0.360365 0.84603798 0.93118203 0.0052140001 
		0.61939299 0.0053500002 0.66152102 0.86799502 0.94401199 0.53625 0.360365 0.0051139998 
		0.58836901 0.87310302 0.92947102 0.87121999 0.92076403 0.85926002 0.91443199 0.85241997 
		0.918751 0.99694699 0.53654599 0.86155897 0.96749198 0.83066899 0.95634699 0.95952201 
		0.54934603 0.83667701 0.997648 0.97759801 0.55122298 0.98875803 0.51261902 0.84405601 
		0.99613601 0.85556901 0.98250902 0.81887501 0.987486 0.82279903 0.99363202 0.82995212 
		0.98530686 0.201997 0.096046999 0.451262 0.297362;
	setAttr ".uvst[0].uvsp[2500:2749]" 0.88369298 0.48909101 0.47540301 0.33445501 
		0.89970797 0.59865803 0.22677401 0.094451003 0.50525802 0.275094 0.90679502 0.63490802 
		0.90679502 0.59865803 0.50421202 0.33078599 0.31503099 0.265214 0.30096999 0.28734699 
		0.56385398 0.32381701 0.36667299 0.270201 0.20179801 0.129155 0.92681801 0.58483797 
		0.38367701 0.32070401 0.90834099 0.81821603 0.91390598 0.81821603 0.91390598 0.77535999 
		0.90834099 0.77535999 0.36512399 0.65400797 0.076535001 0.090002999 0.90834099 0.86107099 
		0.91390598 0.86107099 0.078984998 0.381266 0.092840999 0.41107401 0.69638699 0.98865098 
		0.68685502 0.954588 0.13222601 0.363965 0.88985699 0.442103 0.91502601 0.45676699 
		0.147191 0.39228901 0.97156203 0.48893499 0.64522201 0.99363202 0.63952702 0.96235102 
		0.97060698 0.47527999 0.21400701 0.39483699 0.19675601 0.34888399 0.93981701 0.462901 
		0.90578699 0.42929301 0.58001 0.946446 0.98875803 0.51261902 0.96983898 0.509103 
		0.584773 0.99484003 0.913252 0.24269199 0.91606802 0.26969099 0.93464202 0.249547 
		0.89221901 0.26368201 0.918648 0.42588499 0.29039001 0.30343601 0.204326 0.64879203 
		0.193675 0.59118199 0.89001697 0.347036 0.221536 0.58401299 0.93554097 0.280112 0.954386 
		0.26490301 0.95952201 0.54934603 0.97759801 0.55122298 0.59111702 0.93998599 0.98875803 
		0.51261902 0.97759801 0.55122298 0.571823 0.92429203 0.029988 0.124026 0.042949002 
		0.081371002 0.015961999 0.079186998 0.010754 0.121601 0.369183 0.48504901 0.050795998 
		0.123401 0.060642999 0.084067002 0.90529698 0.288266 0.023584001 0.15769599 0.043760002 
		0.151614 0.96520692 0.40227199 0.85529202 0.2516 0.077613004 0.069013 0.962861 0.41763699 
		0.29830399 0.52885503 0.063386001 0.114661 0.966371 0.424263 0.98254001 0.43424901 
		0.073977001 0.145778 0.197135 0.99580902 0.222846 0.95815903 0.193156 0.952402 0.151456 
		0.97428399 0.89979202 0.87167901 0.41679299 0.91507298 0.042206999 0.18367 0.54165399 
		0.45507401 0.96437001 0.34943101 0.002383 0.157042 0.503295 0.45613399 0.95690799 
		0.44592401 0.98176199 0.386659 0.041558001 0.036773 0.025599999 0.035439 0.40271801 
		0.46971101 0.46454999 0.52114898 0.043306999 0.0061840001 0.027372999 0.0088769998 
		0.13733999 0.34463099 0.96860701 0.34316099 0.86589003 0.493653 0.064891003 0.033833999 
		0.058991 0.0023390001 0.406818 0.628088 0.44823399 0.60219198 0.096433006 0.033775002 
		0.091770001 0.00135 0.077613004 0.069013 0.20217 0.17692401 0.193675 0.59118199 0.25666901 
		0.551512 0.88130802 0.26982799 0.85052902 0.28457201 0.86316401 0.29870099 0.195645 
		0.181517 0.469394 0.283306 0.86589003 0.493653 0.11059 0.34865201 0.84123403 0.34278399 
		0.697478 0.329395 0.89868999 0.29448801 0.152403 0.012647 0.87221098 0.49494201 0.36512399 
		0.65400797 0.216943 0.016194999 0.94378501 0.446973 0.185261 0.249818 0.115019 0.238435 
		0.127552 0.28007901 0.188384 0.28036499 0.058045998 0.23476601 0.069725998 0.27008501 
		0.98967803 0.34946501 0.422939 0.53364599 0.0091970004 0.19756 0.011666 0.23058499 
		0.025101 0.22715899 0.026567001 0.198946 0.71606702 0.61442798 0.98052001 0.349453 
		0.367659 0.56738597 0.58209002 0.45740899 0.086560003 0.354895 0.96397197 0.335125 
		0.50641799 0.53146303 0.505503 0.57994401 0.56940699 0.58363301 0.57950997 0.52711898 
		0.006331 0.26660001 0.022523001 0.26663801 0.39286003 0.55063999 0.402215 0.61947697 
		0.042234 0.230699 0.54418403 0.52684402 0.54470301 0.587475 0.88985699 0.442103 0.294 
		0.609981 0.321904 0.660492 0.038424999 0.26776901 0.889548 0.47899899 0.93981701 
		0.462901 0.329831 0.58753598 0.35971001 0.642856 0.94106197 0.43989599 0.44473699 
		0.59108001 0.97470701 0.32247299 0.95321298 0.31685901 0.96545798 0.296083 0.60874802 
		0.367163 0.19389801 0.62680101 0.223692 0.61576903 0.94917798 0.30462801 0.212281 
		0.644333 0.223692 0.61576903 0.94121599 0.29288799 0.19389801 0.62680101 0.954386 
		0.26490301 0.64481598 0.303983 0.62921602 0.34478199 0.221536 0.58401299 0.93554097 
		0.280112 0.223692 0.61576903 0.23253 0.41439599 0.878425 0.434802 0.206241 0.40244001 
		0.87867498 0.43118599 0.902408 0.45429799 0.71961403 0.35024899 0.31919301 0.50896698 
		0.33091 0.48563299 0.31108299 0.480506 0.31095701 0.50337601 0.28796399 0.51239699 
		0.287195 0.52178597 0.91502601 0.45676699 0.32564899 0.66992003 0.269178 0.47593099 
		0.26516399 0.47286901 0.25800899 0.493379 0.26364699 0.49179 0.28165299 0.882855 
		0.26681 0.51571101 0.26890701 0.50793499 0.28946999 0.47085601 0.29141399 0.46630201 
		0.878425 0.434802 0.38621601 0.17150199 0.89304501 0.43533099 0.89304501 0.43533099 
		0.93981701 0.462901 0.322164 0.47623399 0.43914399 0.72184098 0.32384399 0.68270302 
		0.94662201 0.54420501 0.547019 0.76914501 0.981291 0.33517399 0.566594 0.59347099 
		0.406818 0.628088 0.55735099 0.66921198 0.89979202 0.94243199 0.90665299 0.94243199 
		0.90665299 0.91163099 0.89979202 0.91163099 0.54980302 0.963238 0.52430701 0.95621198 
		0.90665299 0.87167901 0.89979202 0.87167901 0.88965398 0.50347501 0.262353 0.155086 
		0.90665299 0.74309099 0.89979202 0.74309099 0.209446 0.98855197 0.225004 0.97280401 
		0.90665299 0.70958298 0.89979202 0.70958298 0.718633 0.358796 0.31959099 0.94914699 
		0.87886 0.903319 0.83981907 0.31989399 0.349733 0.020748001 0.88489097 0.518103 0.61876398 
		0.44350001 0.88881397 0.852732 0.36504301 0.020748001 0.54397601 0.59712303 0.89979202 
		0.97063601 0.90665299 0.97063601 0.315855 0.94111001 0.28673199 0.9522;
	setAttr ".uvst[0].uvsp[2750:2798]" 0.58789498 0.61893201 0.58854598 0.66820902 
		0.89979202 0.99918097 0.90665299 0.99918097 0.52726501 0.67114103 0.52804101 0.64049 
		0.558079 0.64149201 0.98726302 0.79312503 0.98194993 0.79312503 0.95693398 0.79312503 
		0.99887002 0.84214449 0.98696595 0.84214449 0.98195398 0.84214449 0.95477498 0.84214449 
		0.933801 0.84058356 0.933801 0.84058356 0.95693398 0.84058356 0.98194999 0.84058356 
		0.98726302 0.84058356 0.91556901 0.84058356 0.99888909 0.84058356 0.92848498 0.84058356 
		0.92848498 0.84058356 0.95477498 0.88882315 0.98195398 0.88882315 0.98696595 0.88882315 
		0.99887002 0.88882315 0.93380094 0.87984061 0.93380094 0.87984061 0.95693398 0.87984061 
		0.98194993 0.87984061 0.98726296 0.87984055 0.91556895 0.87984061 0.99888903 0.87984061 
		0.92848498 0.87984061 0.92848498 0.87984061 0.95477498 0.92211962 0.98195398 0.92211962 
		0.98696601 0.92211962 0.99887002 0.92211962 0.96210814 0.76269358 0.95793957 0.67032099 
		0.95982909 0.73163295 0.93380094 0.78797251 0.93380094 0.78797251 0.92848498 0.78918898 
		0.92848498 0.78918898 0.99888909 0.79170841 0.91556901 0.79170841;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 1819 ".vt";
	setAttr ".vt[0:165]"  -4.5162401 13.960704 1.876699 -5.5128231 12.978381 
		3.211431 -6.616734 13.142735 3.1315789 -6.5065532 14.249378 1.826896 6.616734 13.142735 
		3.1315789 5.5128231 12.978381 3.211431 4.5162401 13.960704 1.876699 6.5065532 14.249378 
		1.826896 -7.7153611 13.287283 3.2860999 -8.4915829 14.518242 2.011467 8.4915829 14.518242 
		2.011467 7.7153611 13.287283 3.2860999 5.0366759 47.716438 -2.6062155 2.838419 49.769413 
		-3.7316647 2.6403112 49.808662 -3.4021196 4.6008968 47.890938 -2.0230083 3.6419761 
		43.446903 -1.557163 1.5853339 43.997795 -2.1067941 1.2154191 44.218128 5.5759568 
		4.3185358 43.077065 4.858428 4.8460631 45.23568 4.5008168 2.2248721 47.395008 5.38905 
		5.5253072 43.756458 2.171731 5.9874291 45.477619 1.924185 5.4365458 48.084866 4.4972639 
		3.622587 51.239441 4.6873674 6.403132 47.091972 2.090245 4.918407 48.190357 4.0082426 
		3.3081672 51.153065 4.454154 5.7262688 47.189415 2.0461221 5.822402 46.497078 -0.55105472 
		5.2821369 46.647964 -0.32886672 4.7369061 43.525639 0.194811 4.6964211 45.476566 
		0.047966 3.8967359 45.449764 -1.573689 5.344481 45.645985 1.8702821 4.380826 45.303715 
		4.0278649 1.806114 46.159447 -2.11287 2.498528 45.055531 5.896595 4.9437017 43.472244 
		4.2979689 4.8573189 43.558773 4.1350288 2.433352 45.192543 5.4441671 0.14183143 49.581264 
		6.0608411 0.028883457 49.572033 5.7870378 -2.754612 44.426666 5.5250068 -0.90500903 
		42.105049 5.9399538 -0.79475802 42.002312 6.0735831 -2.645469 44.435799 5.7940559 
		0.44054696 43.737713 6.2218761 -0.96199107 46.67712 5.4750662 -1.1411051 46.46743 
		6.3385072 0.86542696 43.324421 6.4685178 1.7407589 40.66853 5.1866102 3.345794 42.968807 
		5.7467332 3.0370529 43.430901 5.047276 1.649959 40.769714 4.843081 4.5125122 59.694931 
		1.618466 2.871196 56.559818 3.902034 4.506546 56.046501 3.9311099 6.1384368 59.113609 
		1.562466 1.101599 53.512299 5.5095272 2.5019691 53.107174 5.4278131 6.3354592 55.8074 
		3.9078181 8.9322443 59.01416 1.288077 4.07515 52.780128 5.4949179 5.9011841 58.073334 
		1.3656909 4.408514 55.413258 3.134119 2.7996061 56.345127 3.6701219 4.4210968 59.41758 
		1.447668 2.391537 52.539246 4.5847569 1.038278 53.358879 5.2205019 8.8437347 58.736637 
		1.117278 6.3004971 55.587627 3.690403 4.0374622 52.61446 5.2101188 5.478178 57.426903 
		-5.7316961 5.4536018 57.2789 -5.4967461 3.969249 53.801029 -6.1623669 3.919914 53.827389 
		-6.374794 1.63415 58.256012 -5.5994611 1.6592259 58.403408 -5.8337979 0.86574101 
		54.586334 -6.0338469 0.91403502 54.560585 -5.8190789 3.4265289 57.478935 -4.7654538 
		2.413681 54.030582 -5.2732568 3.4764781 57.770973 -5.4955039 2.381129 54.209206 -5.9369001 
		0.49719101 42.237244 -2.363663 0.469643 42.252987 -2.5475399 -2.058392 43.452591 
		-2.2566919 -2.0308399 43.43684 -2.072813 -0.64096701 42.466896 -1.7238081 -0.85384399 
		42.450081 -2.3945119 1.451072 49.413021 6.3221354 1.454901 49.330009 6.0376983 -1.187323 
		45.999992 6.7197108 -1.1163059 45.986103 6.4514718 -0.932199 50.366711 5.3574262 
		-0.93602598 50.449726 5.6357765 -2.8554151 47.3433 4.9670234 -2.964664 47.374832 
		5.3019195 0.220836 49.538258 5.2793036 -1.907414 46.620949 5.5122232 -0.011537 49.738144 
		6.2881012 -2.206974 46.751713 6.5050526 8.8111563 59.237667 -0.87473601 8.4107695 
		59.377361 -2.742475 8.4487162 59.63055 -2.8633859 8.8743839 59.503021 -0.84979099 
		4.4542441 60.184441 -0.52628601 4.388206 59.919247 -0.55122799 4.027935 60.31263 
		-2.5467639 3.9875081 60.059578 -2.4258511 5.9900341 58.424427 -0.59440702 5.708869 
		58.414318 -2.430232 5.6737018 59.768536 -2.4313869 6.0902309 59.621727 -0.49659699 
		4.620234 58.946743 -4.140049 2.9571581 59.534077 -4.3707762 7.084836 58.700775 -4.4767189 
		2.923615 59.332775 -4.1909571 4.6013808 58.12141 -3.7661209 7.0606661 58.49892 -4.2969031 
		-4.0652652 45.599461 5.1073232 -3.3392439 45.134945 6.016191 -4.956882 43.230476 
		3.886776 -4.3448029 42.931591 4.944706 -2.4007189 44.14439 6.1645813 -3.756891 42.187088 
		5.0995889 -2.910949 44.639885 4.9264388 -3.9488161 45.45153 4.7894421 -3.9360509 
		42.572449 4.148356 -4.9324899 43.223721 3.6247339 -2.3176329 44.113251 5.9812121 
		-3.5692279 42.138683 4.929369 0.99023902 49.488846 5.8370667 -0.401041 45.795815 
		5.6480455 3.408828 44.565948 5.2205663 3.9803009 48.197048 5.5228219 3.3027141 52.576996 
		4.9826074 5.314024 51.862389 4.8096228 -0.269151 46.070328 5.1383667 1.132942 49.502281 
		5.1095924 3.9942131 48.192719 5.2691417 3.418365 44.588768 4.9680953 3.303335 52.369923 
		4.4615374 5.3103609 51.822838 4.5520754 4.9844022 53.515373 -5.6986508 4.9395852 
		53.504169 -5.9283509 3.2771139 54.2463 -4.9471011 3.066401 54.153023 -5.555234 0.97330397 
		42.577518 -1.9085259 -0.466703 42.599571 -2.2768321 -0.064147003 42.720692 -1.386705 
		1.308113 42.466003 -1.653621 4.1996441 57.335209 -5.3321609 6.5427241 56.197987 -5.705399 
		6.5686412 56.080971 -5.4689488 4.229805 56.661953 -4.8777728 9.7318335 58.619034 
		-2.4810619 9.6674128 58.369255 -2.3643379 6.676342 58.327515 -1.943365 6.8410749 
		59.632065 -2.2287819 7.2614431 54.901459 3.794816 7.2291422 54.746185 3.5865431 9.9639215 
		58.061462 1.001848 9.8400373 57.792126 0.84017402;
	setAttr ".vt[166:331]" 5.5072131 55.063 3.287271 7.431808 57.688854 1.005749 
		5.5322452 55.382023 3.7711129 7.9376202 58.421032 1.230172 3.32125 40.945263 4.1905932 
		3.399533 40.785027 4.0101776 -3.3005719 42.783337 4.1296682 -3.6251681 42.733646 
		4.8874969 -12.025921 49.338161 -4.855226 -10.261799 51.397068 -5.076189 -8.5794249 
		51.420277 -2.813899 -11.772566 49.09156 -2.8699689 -12.965162 51.033829 -4.9581184 
		-11.527008 52.9077 -5.2920718 -13.78269 52.753929 -3.2970519 -12.520017 54.197495 
		-3.2763939 -9.8344307 51.240864 -0.67841291 -11.925589 49.510849 -1.3496464 -11.221736 
		52.639893 0.60359114 -12.974613 50.950031 0.053261086 -12.499141 54.152477 -1.1738008 
		-14.105699 52.860806 -1.3735759 -12.641034 54.295544 -1.0519369 -10.412723 55.463219 
		-0.40262893 -11.311147 55.265869 -3.3546259 -12.663787 54.34449 -3.343519 -11.248812 
		52.646645 0.88521212 -9.1650038 54.16066 0.46773702 -9.7368107 51.121868 -0.51202291 
		-7.7807822 53.002609 0.19929203 -6.998518 52.630081 -0.799963 -6.97929 52.516144 
		-2.6386499 -11.581522 52.938522 -5.5403719 -9.7017775 54.251526 -4.8038621 -10.202591 
		51.29211 -5.3050861 -8.1240587 52.960873 -4.7130351 24.907146 39.862919 1.599088 
		24.75849 40.349663 1.588913 24.112644 40.155827 1.647046 24.416433 39.599689 1.641073 
		26.215168 42.091713 -3.26158 25.743902 41.845333 -3.220969 25.343969 41.838024 -2.9181061 
		25.775499 42.386204 -3.253839 25.89691 41.470352 -2.678638 25.523464 41.711346 -2.439405 
		26.625193 39.624714 -3.172936 26.611954 39.654354 -3.5795181 26.92057 39.526798 -3.613735 
		26.948843 39.474739 -3.2729609 26.501097 40.003395 -3.068861 26.478302 40.163002 
		-3.5409169 26.874613 40.15369 -3.1107931 27.387453 40.011845 -1.6791691 26.927286 
		39.902016 -1.705292 27.140371 39.405926 -1.708263 27.561161 39.486038 -1.708532 27.235592 
		40.701374 -1.626268 26.772867 40.43787 -1.617977 26.874054 39.923141 -2.2266309 27.116718 
		39.498924 -2.28281 26.743002 40.483032 -2.2259109 22.868992 45.984779 -0.061554998 
		22.76679 44.76067 0.85090703 24.161541 43.97913 0.69575298 24.472178 44.59375 0.22606 
		25.673407 43.219589 0.50177801 23.959463 43.728462 0.347408 25.46134 42.973938 0.154714 
		22.554724 44.515018 0.50384301 21.828629 43.829449 0.48995301 22.175903 43.653748 
		0.31339899 22.168247 43.274956 -0.56754601 21.172173 43.776527 -1.109727 21.902626 
		43.707035 -2.429605 22.601978 43.615372 -2.4258809 23.13072 44.031082 -2.7371061 
		22.665176 45.647087 -0.32649499 22.777945 45.931679 -0.390567 22.790747 45.766659 
		-1.434818 22.816952 45.641426 -2.027272 23.917025 43.601227 -3.0886562 24.275824 
		44.201172 -2.784483 22.858744 45.312515 -2.5999849 24.804941 43.079082 -3.3155911 
		25.612566 43.106709 -2.9808769 25.995295 43.620003 -1.2437789 24.687138 44.859482 
		-1.331934 23.098894 46.115833 -1.431987 24.522905 44.551502 -2.946774 23.027477 45.673176 
		-2.8592632 25.937994 43.446705 -3.046181 26.516708 42.567318 -0.79464102 25.726549 
		43.327179 -1.3846591 26.185537 42.02412 -0.30217901 26.488308 42.486153 -2.020535 
		26.373224 42.005611 -1.309373 4.4092178 11.857511 -0.84929597 3.860075 11.844913 
		-1.004225 4.4387488 13.149749 -4.482326 5.1271658 12.630701 -3.4033999 5.329453 13.738156 
		-5.4947262 8.9637918 11.869538 0.98686099 9.1582785 12.552183 -1.182907 10.222727 
		12.763681 -1.177292 9.8690691 12.004472 1.1434391 5.3330321 11.289055 1.331615 5.0538688 
		11.11033 1.8140481 7.2117758 11.474446 1.7765419 7.481667 11.3499 2.389003 7.7870588 
		12.935908 -3.7411189 8.1864481 13.674292 -4.5507712 2.771677 58.153366 2.8614581 
		2.8131671 57.885876 2.3127279 3.4807 59.436535 -0.62670398 1.605752 57.246655 3.2032177 
		6.174891 45.491497 1.886749 4.345994 42.856087 0.44593501 5.7117701 43.095074 2.037344 
		4.3238912 45.143093 -2.255156 3.392415 42.909142 -1.4605531 1.629138 42.984577 -2.236114 
		0.23867302 45.407413 -3.2785339 -5.7421812e-030 42.508209 -2.5645339 14.377168 47.698158 
		-4.6350398 15.210079 46.572079 -2.8303051 13.031939 47.792706 -2.8917811 13.8496 
		47.680393 -4.9294529 12.468046 48.479885 -2.9068658 12.787367 48.651367 -5.1739087 
		15.167428 50.309406 -4.8639708 13.933129 50.590588 -4.9357009 11.772566 49.09156 
		-2.8699689 12.025921 49.338161 -4.855226 14.579552 51.976044 -3.1829238 15.384956 
		51.653873 -2.8414476 13.78269 52.753929 -3.2970519 12.965162 51.033829 -4.9581184 
		18.199724 46.399242 -3.811394 18.030668 45.157646 -1.686968 17.796213 45.819279 -0.34840801 
		15.379931 46.98164 -1.1029741 15.212669 48.907841 -0.163523 18.223278 47.278996 0.077583998 
		13.547074 50.420986 -0.42186666 12.742613 49.08025 -1.4681559 13.374893 48.218624 
		-1.260824 14.336735 49.798496 -0.19638494 11.925589 49.510849 -1.3496464 12.974613 
		50.950031 0.053261086 14.105699 52.860806 -1.3735759 14.68574 52.087772 -1.368986 
		17.092857 50.267334 -2.135092 16.595604 52.681934 -2.5562539 16.222443 50.838367 
		-0.72722799 15.21973 51.579689 -1.0635879 2.336421 58.021301 2.160423 1.430524 57.475231 
		3.0806799 2.4433 58.291157 2.8251381 2.9448733 59.446613 -0.42314911 4.6069598 45.261906 
		5.1803732 4.136519 42.578693 4.9218659 0.62736303 42.176258 6.0580549 2.095649 44.618591 
		6.701416 -1.416053e-030 42.144104 5.892489 3.4787025 60.812836 2.8579223;
	setAttr ".vt[332:497]" 3.2374959 61.201973 -0.53267002 3.6132481 61.217354 
		-0.84897298 3.8763037 60.676975 2.850713 1.5072989 61.599152 -2.8369861 1.302758 
		59.643063 -2.574204 1.4572051 59.796432 -3.145731 1.62011 61.630741 -3.2727849 -6.6529843e-015 
		61.751507 -3.0130441 -7.0072783e-015 59.699238 -2.692065 -6.6338655e-015 59.817638 
		-3.3209541 -6.6529843e-015 61.689674 -3.453928 7.7989049 9.1793718 0.94507802 6.046381 
		9.0772285 0.38973999 6.9562101 6.7413411 -0.378158 8.3128147 6.7650208 0.18086401 
		5.01721 9.6373291 -1.581054 6.1780162 6.7178359 -1.879802 5.7146182 10.169292 -3.542614 
		6.7382379 6.9230518 -3.3473339 8.1231403 10.478281 -3.997637 8.358839 7.0081468 -3.4972529 
		9.3451681 10.175447 -1.9072289 9.2241354 6.9770961 -2.0088871 9.1546717 9.6180916 
		0.028472001 9.1476603 6.8515019 -0.49491501 9.6179161 6.5384359 -0.30071399 8.2907143 
		6.2587881 0.56746799 8.3476143 5.4019399 0.66685402 9.7665806 5.4907198 -0.421817 
		6.7064948 6.213778 -0.11964 6.5520749 5.298656 0.202811 5.8322301 6.1962652 -2.0133951 
		5.6284151 5.3538342 -2.089653 6.4792199 6.492022 -3.6369841 6.4458489 5.6282701 -3.9151311 
		8.5948935 6.644042 -3.9465029 8.5697765 5.753396 -4.1039271 9.5611763 6.606832 -2.2905991 
		9.7714243 5.5796609 -2.524708 9.3401928 5.2875772 -0.57441902 8.251112 5.2037611 
		0.198553 6.7919898 5.209209 -0.14904401 6.2800069 5.2063632 -2.0437169 6.8129268 
		5.537179 -3.4756219 8.278409 5.5591178 -3.5786121 9.3483133 5.3571739 -2.4595649 
		22.526602 42.444691 -0.060679 22.270138 42.668823 0.77552497 22.933323 42.876125 
		-0.81792003 23.0961 42.852428 -2.459944 22.948271 43.458988 1.005793 24.124582 42.992954 
		1.035988 23.252281 41.675697 0.73113102 23.9814 42.144547 0.073192999 25.402203 41.451775 
		-0.48581001 25.782627 41.221489 -0.265596 25.617092 41.323326 0.34959501 25.379316 
		41.484901 0.34923699 24.047857 42.568401 -0.68527699 26.161425 42.582962 -2.9268229 
		26.183126 42.07299 -2.4521019 26.439243 41.907551 -2.724376 26.815197 40.328613 -3.5431719 
		24.309818 41.466419 1.540782 24.38023 41.23428 1.096234 24.275703 41.677517 0.88972902 
		24.077229 42.242725 1.244067 23.666605 40.996021 1.15854 23.436954 41.399033 0.89691901 
		23.659109 41.184261 1.60032 23.173607 41.708084 1.457566 24.552549 41.025387 1.5749381 
		23.848305 40.675064 1.632339 23.80114 40.344074 1.150048 24.605207 40.886425 1.0525891 
		26.198536 41.177307 -2.8412099 26.371124 40.772987 -2.972728 26.687473 41.089016 
		-2.93136 26.500587 41.482895 -2.817832 26.150145 41.446774 -3.3434229 26.29491 40.93148 
		-3.380054 26.413496 41.67894 -3.3048301 26.601534 41.206905 -3.41119 27.511641 39.505867 
		-2.21457 27.36557 40.03513 -2.161402 25.984289 41.33477 -1.36683 26.391428 40.880833 
		-1.531161 26.76236 41.490879 -1.538972 25.853262 41.523148 -2.2396779 26.316286 40.882896 
		-2.208617 26.801126 41.462166 -2.2020991 27.763603 39.419819 -0.36024201 28.019497 
		38.365845 -0.50848502 28.029165 38.37096 -0.81503499 27.763262 39.408955 -0.92429399 
		27.638126 38.349705 -0.88386899 27.372101 39.319122 -1.00146 27.635683 38.375839 
		-0.44326001 27.376265 39.340271 -0.332739 27.223057 40.696571 -0.35923299 26.78808 
		40.179253 -0.326096 26.998192 39.760372 -0.37952599 27.439894 40.089104 -0.39499101 
		26.854683 40.204376 -1.06047 27.059015 39.722462 -1.0113339 27.276478 40.725216 -1.019557 
		27.497728 40.075508 -0.99072701 26.790728 41.445183 -1.017699 26.833809 41.372532 
		-0.375855 26.381744 40.658394 -1.089383 26.327908 40.618141 -0.32491001 27.383337 
		39.717751 1.017767 27.612196 38.764877 1.0891809 27.602367 38.694363 0.711447 27.495245 
		39.589375 0.46534199 27.333832 38.775066 0.66377002 27.060244 39.377842 0.463734 
		27.289131 38.835468 1.056145 26.991526 39.485886 1.002787 26.954466 40.665443 0.87157297 
		26.515575 40.437294 0.867782 26.663223 39.934883 0.88675302 27.163223 40.202099 0.91343999 
		26.605331 40.251427 0.21840499 26.7379 39.780579 0.37399 27.105066 40.484299 0.23615099 
		27.294292 40.04649 0.37548 26.636477 41.23016 0.75072402 26.82674 41.069656 0.184826 
		26.21069 40.608089 0.113705 26.116526 40.812946 0.76236802 25.407907 42.351612 0.74702001 
		22.896097 42.162624 1.30233 26.215641 42.301659 0.37695101 24.394226 42.078575 0.69166398 
		25.305435 41.551006 -1.5631469 23.773664 42.486736 -2.7681251 25.456463 41.680862 
		0.65822798 26.552597 41.639267 0.029611001 27.184933 40.714664 -2.1560431 26.295919 
		41.730679 0.721977 25.710344 41.418934 0.66289902 25.877029 41.992672 0.75497001 
		26.482944 40.294426 -3.048327 26.829023 40.573082 -3.0135391 26.411417 40.485958 
		-3.514262 26.771822 40.704498 -3.519325 27.44278 38.531799 -1.832682 27.702173 38.498631 
		-1.8860151 27.715321 38.508942 -2.208756 27.451305 38.551292 -2.2715621 24.167442 
		39.856365 1.316594 24.849663 40.104408 1.221808 16.34016 51.059753 -0.48763201 19.268915 
		49.398346 -1.0917979 16.861744 53.154213 -2.690187 15.283366 49.087749 0.224456 18.305386 
		47.3386 0.53801 21.85047 43.813488 0.86755008 22.917625 46.193478 -0.10401 18.399277 
		46.414707 -4.1768532 21.936468 43.523392 -2.6884167 14.974882 47.426723 -5.3618989 
		19.108259 48.767731 -3.527746 22.987463 45.926373 -2.213902 15.488618 50.482376 -5.3731508;
	setAttr ".vt[498:663]" -22.650463 45.647087 -0.32649499 -22.954018 45.984779 
		-0.061554998 -22.76679 44.76067 0.85090703 -22.554724 44.515018 0.50384301 -15.488618 
		50.482376 -5.3731508 -15.167428 50.309406 -4.8639708 -14.377168 47.698158 -4.6350398 
		-14.974882 47.426723 -5.3618989 -22.816952 45.641426 -2.027272 -22.987463 45.926373 
		-2.213902 -21.936468 43.523392 -2.6884167 -21.902626 43.707035 -2.429605 -22.777945 
		45.931679 -0.390567 -22.917625 46.193478 -0.10401 -21.828629 43.829449 0.48995301 
		-21.85047 43.813488 0.86755008 -16.861744 53.154213 -2.6901867 -16.595604 52.681934 
		-2.5562537 -19.108259 48.767731 -3.527746 -19.268915 49.398346 -1.0917978 -18.399277 
		46.414707 -4.1768532 -18.199724 46.399242 -3.811394 -15.283366 49.087749 0.22445612 
		-18.305386 47.3386 0.53801006 -18.223278 47.278996 0.077584065 -15.212669 48.907841 
		-0.16352288 -16.34016 51.059753 -0.48763189 -16.222443 50.838367 -0.72722787 -25.89691 
		41.470352 -2.678638 -26.198536 41.177307 -2.8412099 -26.150145 41.446774 -3.3434229 
		-25.743902 41.845333 -3.220969 -22.933323 42.876125 -0.81792003 -24.047857 42.568401 
		-0.68527699 -23.773664 42.486736 -2.7681251 -23.0961 42.852428 -2.459944 -26.161425 
		42.582962 -2.9268229 -25.612566 43.106709 -2.9808769 -24.804941 43.079082 -3.3155911 
		-25.775499 42.386204 -3.253839 -26.413496 41.67894 -3.3048301 -26.500587 41.482895 
		-2.817832 -26.439243 41.907551 -2.724376 -26.215168 42.091713 -3.26158 -26.373224 
		42.005611 -1.309373 -26.790728 41.445183 -1.017699 -26.516708 42.567318 -0.79464102 
		-26.833809 41.372532 -0.375855 -26.185537 42.02412 -0.30217901 -26.552597 41.639267 
		0.029611001 -26.215641 42.301659 0.37695101 -26.295919 41.730679 0.721977 -25.877029 
		41.992672 0.75497001 -25.407907 42.351612 0.74702001 -25.46134 42.973938 0.154714 
		-24.124582 42.992954 1.035988 -22.896097 42.162624 1.30233 -22.270138 42.668823 0.77552497 
		-22.948271 43.458988 1.005793 -23.917025 43.601227 -3.0886681 -23.13072 44.031082 
		-2.7371061 -25.456463 41.680862 0.65822798 -27.497728 40.075508 -0.99072701 -27.763262 
		39.408955 -0.92429399 -27.763603 39.419819 -0.36024201 -27.439894 40.089104 -0.39499101 
		-25.782627 41.221489 -0.265596 -26.327908 40.618141 -0.32491001 -26.381744 40.658394 
		-1.089383 -25.984289 41.33477 -1.36683 -26.998192 39.760372 -0.37952599 -27.376265 
		39.340271 -0.332739 -27.372101 39.319122 -1.00146 -27.059015 39.722462 -1.0113339 
		-28.029165 38.37096 -0.81503499 -27.638126 38.349705 -0.88386899 -27.635683 38.375839 
		-0.44326001 -28.019497 38.365845 -0.50848502 -24.75849 40.349663 1.588913 -24.112644 
		40.155827 1.647046 -23.848305 40.675064 1.632339 -24.552549 41.025387 1.5749381 -23.252281 
		41.675697 0.73113102 -24.394226 42.078575 0.69166398 -23.9814 42.144547 0.073192999 
		-23.80114 40.344074 1.150048 -24.167442 39.856365 1.316594 -24.849663 40.104408 1.221808 
		-24.605207 40.886425 1.0525891 -22.526602 42.444691 -0.060679 -24.275703 41.677517 
		0.88972902 -24.077229 42.242725 1.244067 -24.907146 39.862919 1.599088 -24.416433 
		39.599689 1.641073 -23.436954 41.399033 0.89691901 -23.173607 41.708084 1.457566 
		-26.82674 41.069656 0.184826 -26.636477 41.23016 0.75072402 -27.294292 40.04649 0.37548 
		-27.495245 39.589375 0.46534199 -27.383337 39.717751 1.017767 -27.163223 40.202099 
		0.91343999 -25.617092 41.323326 0.34959501 -26.116526 40.812946 0.76236802 -26.21069 
		40.608089 0.113705 -26.663223 39.934883 0.88675302 -26.991526 39.485886 1.002787 
		-27.060244 39.377842 0.463734 -26.7379 39.780579 0.37399 -25.710344 41.418934 0.66289902 
		-25.379316 41.484901 0.34923699 -27.602367 38.694363 0.711447 -27.333832 38.775066 
		0.66377002 -27.289131 38.835468 1.056145 -27.612196 38.764877 1.0891809 -27.184933 
		40.714664 -2.1560431 -27.235592 40.701374 -1.626268 -26.76236 41.490879 -1.538972 
		-26.801126 41.462166 -2.2020991 -27.36557 40.03513 -2.161402 -27.387453 40.011845 
		-1.6791691 -27.511641 39.505867 -2.21457 -27.715321 38.508942 -2.208756 -27.702173 
		38.498631 -1.8860151 -27.561161 39.486038 -1.708532 -26.391428 40.880833 -1.531161 
		-26.772867 40.43787 -1.617977 -26.743002 40.483032 -2.2259109 -26.316286 40.882896 
		-2.208617 -27.140371 39.405926 -1.708263 -27.44278 38.531799 -1.832682 -27.451305 
		38.551292 -2.2715621 -27.116718 39.498924 -2.28281 -26.874054 39.923141 -2.2266309 
		-26.601534 41.206905 -3.41119 -26.771822 40.704498 -3.519325 -26.829023 40.573082 
		-3.0135391 -26.687473 41.089016 -2.93136 -26.815197 40.328613 -3.5431719 -26.874613 
		40.15369 -3.1107931 -26.371124 40.772987 -2.972728 -26.482944 40.294426 -3.048327 
		-26.411417 40.485958 -3.514262 -26.29491 40.93148 -3.380054 -26.501097 40.003395 
		-3.068861 -26.478302 40.163002 -3.5409169 -26.92057 39.526798 -3.613735 -26.611954 
		39.654354 -3.5795181 -26.948843 39.474739 -3.2729609 -25.343969 41.838024 -2.9181061 
		-25.402203 41.451775 -0.48581001 -25.305435 41.551006 -1.5631469 -26.183126 42.07299 
		-2.4521019 -25.523464 41.711346 -2.439405 -25.853262 41.523148 -2.2396779 -22.175903 
		43.653748 0.31339899 -22.601978 43.615372 -2.4258809 -22.168247 43.274956 -0.56754601 
		-26.488308 42.486153 -2.020535 -25.726549 43.327179 -1.3846591 -23.959463 43.728462 
		0.347408 -26.954466 40.665443 0.87157297 -26.515575 40.437294 0.867782 -26.605331 
		40.251427 0.21840499 -27.105066 40.484299 0.23615099 -27.223057 40.696571 -0.35923299;
	setAttr ".vt[664:829]" -26.78808 40.179253 -0.326096 -26.854683 40.204376 
		-1.06047 -27.276478 40.725216 -1.019557 -24.309818 41.466419 1.540782 -24.38023 41.23428 
		1.096234 -23.666605 40.996021 1.15854 -23.659109 41.184261 1.60032 -9.3451681 10.175447 
		-1.9072289 -9.1546717 9.6180916 0.028472001 -8.9637918 11.869538 0.98686099 -9.1582785 
		12.552183 -1.182907 -7.7989049 9.1793718 0.94507802 -7.2117758 11.474446 1.7765419 
		-6.046381 9.0772285 0.38973999 -5.3330321 11.289055 1.331615 -5.01721 9.6373291 -1.581054 
		-4.4092178 11.857511 -0.84929597 -5.7146182 10.169292 -3.542614 -5.1271658 12.630701 
		-3.4033999 -8.1231403 10.478281 -3.997637 -7.7870588 12.935908 -3.7411189 -9.3483133 
		5.3571739 -2.4595649 -9.3401928 5.2875772 -0.57441902 -9.7665806 5.4907198 -0.421817 
		-9.7714243 5.5796609 -2.524708 -9.5611763 6.606832 -2.2905991 -9.6179161 6.5384359 
		-0.30071399 -9.1476603 6.8515019 -0.49491501 -9.2241354 6.9770961 -2.0088871 -8.278409 
		5.5591178 -3.5786121 -8.5697765 5.753396 -4.1039271 -8.5948935 6.644042 -3.9465029 
		-8.358839 7.0081468 -3.4972529 -6.8129268 5.537179 -3.4756219 -6.4458489 5.6282701 
		-3.9151311 -6.4792199 6.492022 -3.6369841 -6.7382379 6.9230518 -3.3473339 -5.6284151 
		5.3538342 -2.089653 -6.2800069 5.2063632 -2.0437169 -5.8322301 6.1962652 -2.0133951 
		-6.1780162 6.7178359 -1.879802 -6.7919898 5.209209 -0.14904401 -6.5520749 5.298656 
		0.202811 -6.7064948 6.213778 -0.11964 -6.9562101 6.7413411 -0.378158 -8.251112 5.2037611 
		0.198553 -8.3476143 5.4019399 0.66685402 -8.2907143 6.2587881 0.56746799 -8.3128147 
		6.7650208 0.18086401 -2.4433 58.291157 2.8251381 -2.771677 58.153366 2.8614581 -1.605752 
		57.246655 3.2032177 -1.430524 57.475231 3.0806799 -3.4787033 60.814259 2.8579223 
		-3.8763039 60.679493 2.850713 -1.62011 61.631538 -3.2727849 -1.4572051 59.796432 
		-3.145731 -1.5072989 61.599518 -2.8369861 -1.302758 59.643063 -2.574204 -3.6132481 
		61.222271 -0.84897298 -3.4807 59.438667 -0.62670398 -3.2374959 61.205509 -0.53267002 
		-2.9448731 59.447216 -0.42314899 -2.095649 44.618591 6.701416 -4.136519 42.578693 
		4.9218659 -4.6069598 45.261906 5.1803732 -0.62736303 42.176258 6.0580549 -5.7117701 
		43.095074 2.037344 -6.174891 45.491497 1.886749 -2.336421 58.021301 2.160423 -14.68574 
		52.087772 -1.368986 -13.547074 50.420986 -0.42186829 -15.21973 51.579689 -1.0635879 
		-14.336735 49.798496 -0.19638494 -14.579552 51.976044 -3.1829238 -15.384956 51.653873 
		-2.8414476 -17.092857 50.267334 -2.135092 -12.742613 49.08025 -1.4681569 -12.468046 
		48.479885 -2.9068658 -13.374893 48.218624 -1.260824 -13.031939 47.792706 -2.8917811 
		-15.379931 46.98164 -1.1029739 -15.210079 46.572079 -2.8303051 -17.796213 45.819279 
		-0.34840795 -18.030668 45.157646 -1.686968 -21.172173 43.776527 -1.109727 -12.787367 
		48.651367 -5.1739087 -13.933129 50.590588 -4.9357009 -13.8496 47.680393 -4.9294529 
		-0.23867302 45.407413 -3.2785339 -1.629138 42.984577 -2.236114 -4.3238912 45.143093 
		-2.255156 -3.392415 42.909142 -1.4605531 -4.345994 42.856087 0.44593501 -2.8131671 
		57.885876 2.3127279 -8.1864481 13.674292 -4.5507712 -5.329453 13.738156 -5.4947262 
		-10.222727 12.763681 -1.177292 -5.0538688 11.11033 1.8140481 -7.481667 11.3499 2.389003 
		-9.8690691 12.004472 1.1434391 -3.860075 11.844913 -1.004225 -4.4387488 13.149749 
		-4.482326 -25.937994 43.446705 -3.046181 -25.995295 43.620003 -1.2437789 -25.673407 
		43.219589 0.50177801 -22.836027 45.766659 -1.434818 -23.056164 46.115833 -1.431987 
		-22.769718 45.312515 -2.5999849 -22.938452 45.673176 -2.8592632 -24.522905 44.551502 
		-2.946774 -24.275824 44.201172 -2.784483 -24.687138 44.859482 -1.331934 -24.472178 
		44.59375 0.22606 -24.161541 43.97913 0.69575298 -26.927286 39.902016 -1.705292 -26.625193 
		39.624714 -3.172936 -6.7132812 0.168735 -1.894906 -7.0829358 0.51784599 -0.73521799 
		-10.58849 0.52306598 -0.90420699 -10.323289 0.26638401 -1.899254 -6.5682569 0.246677 
		2.171267 -8.5073738 0.17352401 5.7294788 -11.476662 0.192977 3.5872519 -9.89884 3.8137031 
		-3.5949531 -10.182364 3.2826221 -2.452143 -9.8457527 2.8945301 -2.5364449 -9.5281792 
		3.429323 -3.3445489 -7.6517048 4.93752 -4.0087938 -7.741117 4.5151758 -4.063518 -6.52245 
		3.9959919 -4.7381172 -7.0425148 3.6168351 -4.2217841 -5.9230762 3.580019 -3.205627 
		-6.306633 3.2871881 -3.1288531 -5.8341098 3.308805 -1.7597851 -6.1651869 3.587081 
		-0.306629 -9.8555555 2.933243 -1.21582 -10.409657 3.31847 -1.320143 -6.53511 3.301342 
		-0.40209699 -6.787396 4.3469362 1.217092 -7.0401592 3.8760891 1.044539 -8.5054712 
		4.0055861 1.093325 -8.5350323 4.5996451 1.481679 -9.8866329 4.1341209 0.203629 -9.5295124 
		3.6522901 0.151419 -9.0007181 4.1436119 -4.5818782 -8.6264372 3.78637 -4.2931809 
		-6.2449908 3.079268 -1.968416 -6.4795928 1.396273 -1.975178 -6.9262929 1.201014 -0.56429702 
		-6.5898929 0.67745298 2.3091979 -6.55094 0.117275 5.239676 -6.5474482 0.53676897 
		5.1503072 -7.292335 0.057679001 6.768496 -7.1462612 0.57402402 6.5580511 -8.2075319 
		0.047812 7.2366352 -8.0992489 0.51671702 7.1479359 -9.2405758 0.018188 7.1845312 
		-9.2474594 0.452683 7.1270151 -10.202786 0.055328999 6.7446151 -10.195814 0.44689301 
		6.667913 -11.008717 0.132928 6.0468502 -10.943449 0.49331599 6.022007 -11.384477 
		0.51806998 3.7669561 -11.165221 0.225977 1.34716 -11.125133 0.64186901 1.455039;
	setAttr ".vt[830:995]" -10.462408 1.149514 -0.75876498 -10.145049 1.555027 
		-2.258287 -9.0000219 2.1520259 -4.9538999 -6.9633961 2.1614399 -4.824265 -6.7273932 
		1.841638 2.470011 -6.3305001 1.693352 -3.681335 -6.6271539 0.08444 -3.847589 -7.159348 
		0.25621301 -4.842782 -9.9291286 0.23729201 -4.018229 -9.0761261 0.38466701 -4.9185362 
		-10.776542 1.801178 1.8822089 -7.0758219 2.9122231 2.532975 -9.8480186 1.822423 -4.0360832 
		-10.274756 1.896672 3.730922 -10.304242 1.478812 5.8074179 -9.1326857 1.722198 6.3581781 
		-8.935401 2.297322 3.9583521 -11.085849 1.373414 3.69629 -10.903731 1.1682841 5.0949268 
		-7.8483162 1.671893 6.5045428 -7.2779951 2.1762199 4.0937352 -6.628726 1.432063 4.6211009 
		-7.155642 1.370055 6.1541271 -8.6893167 3.140095 2.490634 -9.9906826 2.715265 2.179781 
		-0.86487967 21.309114 4.7538023 -3.3564856 21.80571 7.0156102 -3.9128101 20.76573 
		5.4510078 -3.8142669 19.235186 3.6046469 -7.5120721 22.201275 6.4270773 -6.7993741 
		20.745872 4.9631419 -3.1241529 19.399069 0.94843203 -0.60322821 21.312059 0.73328733 
		-10.328309 21.811193 1.1610134 -8.1192846 19.859995 1.025403 -6.0367408 21.584301 
		-4.1909404 -5.8137908 20.235828 -1.8812071 -1.7890184 21.521994 -3.2940085 -3.122998 
		20.043314 -1.0090261 -5.8533678 19.528708 -2.0686579 -8.1560974 19.15287 0.83795202 
		-3.1268499 19.444321 -1.2036051 -6.9245672 19.417568 3.8094981 -3.95015 18.462507 
		3.4625671 -3.179405 18.746517 0.89503902 -6.9634409 18.831785 3.635401 -9.7204628 
		15.887207 -0.24823301 -8.1295557 16.365072 -2.652354 -5.551024 17.367142 -3.1463721 
		-7.5044122 14.788438 3.243854 -9.0292597 15.28457 1.914144 -3.4421339 15.076581 0.114295 
		-4.5990939 14.399362 2.6080151 -3.741704 16.020491 -2.052393 -0.50217927 27.823112 
		4.3143272 -2.982769 28.054422 6.5329318 -3.2125192 23.76783 7.4361458 -0.31888086 
		23.35284 4.5947485 -7.2957368 28.31411 5.6861591 -7.6536326 24.252562 6.767046 -0.16683546 
		27.716238 0.99173361 -0.09841913 23.361435 0.76410878 -9.6046886 28.291132 0.85489476 
		-10.658686 24.249018 1.0846634 -6.316484 28.063604 -3.7386642 -6.2912779 24.106382 
		-4.6943426 -1.3887227 23.560335 -3.70155 -1.5060127 27.77039 -3.4548783 -1.4645574e-030 
		38.806271 7.0552511 -1.5913419e-030 40.8624 7.0149641 -2.2021959 40.923199 6.5578609 
		-1.494126 39.236012 6.5941229 2.2021959 40.923199 6.5578609 1.494126 39.236012 6.5941229 
		-2.3628461e-030 42.872742 7.0149641 -1.4497139 42.469078 6.5618858 1.4497139 42.469078 
		6.5618858 -5.816339 36.871971 -3.249454 -4.5522289 41.157162 -1.3356801 -3.7988152e-030 
		41.431263 -2.535063 -0.65503001 35.60104 -5.360508 4.5522289 41.157162 -1.33568 5.816339 
		36.871971 -3.249454 0.65503001 35.60104 -5.360508 -4.4429111 42.537582 -1.435194 
		-5.0856753e-030 42.362 -2.896651 4.4429111 42.537582 -1.435194 -8.2485819 35.916061 
		1.732402 -6.0962162 41.045982 2.03725 6.0962162 41.045982 2.03725 8.2650232 35.916061 
		1.732402 -6.0881162 42.773258 2.025456 6.0881162 42.773258 2.025456 -6.1853461 35.858246 
		5.6658478 -0.75214398 38.247021 6.6639924 -0.93705398 41.209938 6.18011 -4.420229 
		41.081944 5.0089169 0.93705398 41.209938 6.18011 0.75214398 38.247021 6.6639924 6.1853461 
		35.858246 5.6658478 4.420229 41.081944 5.0089169 -0.72037905 41.947983 6.4859462 
		-4.3289609 42.500244 5.1659951 0.72037905 41.947983 6.4859462 4.3289609 42.500244 
		5.1659951 -2.4632539e-018 37.949413 6.8180628 -1.6297484e-030 41.18742 6.1439171 
		-1.4810557e-030 41.915829 6.3203802 -7.0058036 32.289215 -3.7357919 -9.3073874 32.519253 
		0.19402499 9.4906282 32.519253 0.19402499 6.6147809 32.289215 -3.7357919 6.0797033 
		23.769173 -4.7312899 6.0884333 21.639179 -3.939903 2.125109 21.25444 -2.8699386 1.4671443 
		23.388449 -3.3618608 6.0719457 28.314886 -3.7079134 1.6228269 27.85519 -2.8935008 
		0.19578877 23.105789 0.77828181 0.42786255 27.838799 1.1274772 0.45941064 21.418196 
		0.74039692 10.494349 23.929127 1.0921934 10.259728 21.837536 1.1626636 9.674861 28.663473 
		0.84319252 7.322361 24.113422 6.7084341 7.4828777 22.180395 6.6332836 6.9351735 28.357374 
		5.9438014 0.73133487 21.27076 4.7095942 0.3796064 23.429174 4.5601077 0.65981483 
		27.826498 4.2715096 2.8284132 23.604847 7.3536911 3.3835795 21.759289 7.0989246 2.5776572 
		28.028585 6.6574821 -6.5798303e-015 57.450832 3.1359279 -3.9217429 52.775475 4.6944771 
		-3.5635985e-015 53.353195 5.2088642 3.9217429 52.775475 4.6944771 8.1560974 19.15287 
		0.83795202 9.0292597 15.28457 1.914144 9.7204628 15.887207 -0.24823301 6.9634409 
		18.831785 3.635401 7.5044122 14.788438 3.243854 3.95015 18.462507 3.4625671 4.5990939 
		14.399362 2.6080151 3.179405 18.746517 0.89503902 3.4421341 15.07658 0.114295 3.1268499 
		19.444321 -1.2036051 3.7417045 16.020491 -2.0523939 5.8533678 19.528708 -2.0686579 
		5.551024 17.367142 -3.1463721 8.1295557 16.365072 -2.652354 8.1192846 19.859995 1.025403 
		6.9245672 19.417568 3.8094981 3.8142669 19.235186 3.6046469 3.1241529 19.399069 0.94843203 
		3.122998 20.043314 -1.0090261 6.7993741 20.745872 4.9631419 3.9128101 20.76573 5.4510078 
		5.8137908 20.235828 -1.8812071 -6.272018 58.596447 0.15172197 -6.3052621 56.738159 
		1.4436128 6.3052621 56.720592 1.4436131 6.272018 58.580044 0.151722 -1.629138 46.909737 
		-2.8158939 -4.0562038 46.364414 -1.926831 -4.2539411 48.648071 -2.380898;
	setAttr ".vt[996:1161]" -1.9561059 49.839561 -3.583411 4.2539411 48.648071 
		-2.3808997 4.0562038 46.364414 -1.926831 1.6291379 46.909737 -2.8158941 1.9561061 
		49.839561 -3.58341 -1.4648119e-015 46.830631 -2.4006491 1.1920929e-007 49.907852 
		-3.21083 -4.8029788e-015 51.867046 -4.5647869 -2.5595191 51.827644 -4.4375219 2.5595191 
		51.827644 -4.4375219 -5.3290697e-015 56.524529 -4.9288702 -5.3289735e-015 54.616337 
		-5.2198391 -2.9641969 54.315468 -5.7770128 -2.6841459 57.096226 -5.3365731 2.9641969 
		54.314056 -5.7770128 2.6841459 57.092476 -5.3365731 -6.0854249 53.786648 -4.4240432 
		-6.159215 56.474506 -4.8261838 6.159215 56.453072 -4.8261838 6.0854249 53.780952 
		-4.4240432 -6.400033 58.610092 -2.3414719 6.400033 58.596107 -2.3414719 -5.2795558 
		51.225037 -3.4552741 5.2795558 51.225037 -3.4552741 -5.412303 46.392929 0.001426 
		-5.412909 48.58432 -0.56717098 5.412303 46.392929 0.001426 5.412909 48.58432 -0.56717098 
		-6.471415 50.806511 -1.0783989 6.97929 52.516144 -2.6386499 6.471415 50.806511 -1.0783989 
		-5.440145 46.476521 1.875023 -5.2656169 48.514439 1.495885 5.2656169 48.514439 1.495885 
		5.440145 46.476521 1.875023 -6.1250401 50.6278 1.307681 6.1250401 50.6278 1.307681 
		-3.968677 45.62252 4.7115421 -4.0625658 47.355934 4.0996232 4.0625658 47.355934 4.0996232 
		3.968677 45.62252 4.6189752 -4.6604762 48.998745 4.0934029 4.6604762 48.998745 4.0934029 
		-2.9485953e-016 46.055233 6.1300783 -2.9802322e-008 47.704441 5.540781 -2.1017255e-015 
		49.450394 5.5147018 6.998518 52.630081 -0.799963 -2.391443 33.14286 6.7028251 -7.8043089 
		30.506411 6.1099739 7.4132862 30.506411 6.1099739 2.391443 33.14286 6.7028251 -9.3722496 
		32.129341 2.178699 9.575778 32.129341 2.178699 -6.870019 52.604656 1.3055151 6.870019 
		52.604065 1.3055151 -6.6296167 55.077656 1.527621 6.6296172 55.063709 1.527621 1.0308569e-015 
		36.605404 -3.8860559 -1.805753 38.473045 -3.2399702 -2.8131e-030 40.808178 -2.440522 
		1.805753 38.473045 -3.23997 -3.430896 36.088112 -3.7683661 -1.6227623 35.120808 -3.82532 
		3.430896 36.088112 -3.7683661 1.6227623 35.120808 -3.82532 -6.8159175 33.078094 -2.9602029 
		-6.6271448 31.628504 -3.2219279 6.4248948 33.078094 -2.9602029 6.2361221 31.628504 
		-3.2219279 0.624717 35.429901 -4.9677701 6.4383941 32.075752 -3.4075091 -8.6150913 
		32.331367 0.55672097 9.033844 32.331367 0.55672097 -8.1546707 33.232975 0.157389 
		8.5734234 33.232975 0.157389 -7.7565794 35.735188 1.677313 7.7730212 35.735188 1.677313 
		9.1489544 32.29565 0.327106 -8.4954166 32.19912 2.203923 8.9141693 32.19912 2.203923 
		-8.9951553 31.96752 2.0519569 9.1986837 31.96752 2.0519569 -7.1116767 30.978817 5.2783122 
		6.720654 30.978817 5.2783122 -7.5596638 30.2824 5.8189192 7.1686411 30.2824 5.8189192 
		-0.254381 32.714695 1.343591 0.254381 32.714695 1.343591 1.8788015e-015 32.916767 
		0.78870898 -0.45250848 33.182461 4.3615689 0.45250848 33.182461 4.3615689 -2.1619525e-017 
		33.47237 3.8985 -0.78135127 38.724525 5.9793801 -2.4452624 33.305412 5.9067159 2.4452624 
		33.305412 5.9067159 0.78135127 38.724525 5.9793801 5.5208711e-017 38.694054 6.1801872 
		-0.67029601 38.169991 6.2658114 -2.347301 32.974411 6.3044701 0.67029601 38.169991 
		6.2658114 2.347301 32.974411 6.3044701 6.4685441e-018 37.872387 6.4198818 -5.9251971 
		52.340168 2.9229231 5.9251971 52.340168 2.9229231 -4.0810189 51.745811 4.1036639 
		4.0810189 51.745811 4.1036639 -6.013412 53.49815 3.2944901 6.013412 53.496136 3.2944901 
		-3.6365263e-015 52.626282 5.085197 -0.23867302 45.35228 -3.703434 0.23867302 45.35228 
		-3.703434 -4.5912452 44.925472 -2.608726 4.5912452 44.925472 -2.608726 -6.6245532 
		45.287403 1.888448 6.6245532 45.287403 1.888448 -0.62471461 35.429901 -4.9677715 
		-6.8294168 32.075752 -3.4075091 -8.9657135 32.29565 0.327106 -4.8950462 45.026821 
		5.5053229 -2.1886649 44.390312 7.1293068 4.8950462 45.026821 5.5053229 2.1886649 
		44.390312 7.1293068 10.323289 0.26638401 -1.899254 10.145049 1.555027 -2.258287 10.462408 
		1.149514 -0.75876498 10.58849 0.52306598 -0.90420699 8.6893167 3.140095 2.490634 
		7.0758219 2.9122231 2.532975 7.2779951 2.1762199 4.0937352 8.935401 2.297322 3.9583521 
		6.628726 1.432063 4.6211009 6.7273932 1.841638 2.470011 11.125133 0.64186901 1.455039 
		10.776542 1.801178 1.8822089 11.085849 1.373414 3.69629 11.384477 0.51806998 3.7669561 
		9.9906826 2.715265 2.179781 10.274756 1.896672 3.730922 8.5054712 4.0055861 1.093325 
		7.0401592 3.8760891 1.044539 9.8555555 2.933243 -1.21582 9.5295124 3.6522901 0.151419 
		6.7132812 0.168735 -1.894906 6.4795928 1.396273 -1.975178 6.3305001 1.693352 -3.681335 
		6.6271539 0.08444 -3.847589 7.159348 0.25621301 -4.842782 6.9633961 2.1614399 -4.824265 
		9.0000219 2.1520259 -4.9538999 9.0761261 0.38466701 -4.9185362 9.8480186 1.822423 
		-4.0360832 9.9291286 0.23729201 -4.018229 6.5474482 0.53676897 5.1503072 6.5898929 
		0.67745298 2.3091979 8.0992489 0.51671702 7.1479359 7.8483162 1.671893 6.5045428 
		7.155642 1.370055 6.1541271 7.1462612 0.57402402 6.5580511 9.2474594 0.452683 7.1270151 
		9.1326857 1.722198 6.3581781 10.195814 0.44689301 6.667913 10.304242 1.478812 5.8074179 
		10.943449 0.49331599 6.022007 10.903731 1.1682841 5.0949268 9.5281792 3.429323 -3.3445489 
		9.8457527 2.8945301 -2.5364449;
	setAttr ".vt[1162:1327]" 8.6264372 3.78637 -4.2931809 7.0425148 3.6168351 -4.2217841 
		7.741117 4.5151758 -4.063518 6.2449908 3.079268 -1.968416 6.306633 3.2871881 -3.1288531 
		6.53511 3.301342 -0.40209699 8.5073738 0.17352401 5.7294788 8.2075319 0.047812 7.2366352 
		7.292335 0.057679001 6.768496 11.476662 0.192977 3.5872519 11.008717 0.132928 6.0468502 
		10.202786 0.055328999 6.7446151 6.5682569 0.246677 2.171267 11.165221 0.225977 1.34716 
		7.0829358 0.51784599 -0.73521799 6.9262929 1.201014 -0.56429702 6.55094 0.117275 
		5.239676 9.2405758 0.018188 7.1845312 6.1651869 3.587081 -0.306629 5.8341098 3.308805 
		-1.7597851 5.9230762 3.580019 -3.205627 9.0007181 4.1436119 -4.5818782 7.6517048 
		4.93752 -4.0087938 9.89884 3.8137031 -3.5949531 6.787396 4.3469362 1.217092 8.5350323 
		4.5996451 1.481679 9.8866329 4.1341209 0.203629 10.409657 3.31847 -1.320143 10.182364 
		3.2826221 -2.452143 6.52245 3.9959919 -4.7381172 -2.6849871 63.510796 0.475566 -1.546537 
		63.321575 -1.252947 -2.5950921 62.861469 0.48948601 2.6955731 62.865211 0.40406999 
		1.546537 63.321575 -1.252947 2.7890639 63.539982 0.38959301 -8.6543016e-015 63.473938 
		-1.776937 -8.6543016e-015 62.16383 -1.544216 -1.377028 61.98254 -1.090628 1.377028 
		61.98254 -1.090628 -2.20292 61.602589 0.73380202 -2.5309379 62.153351 0.728881 2.6288519 
		62.129585 0.65304202 2.20292 61.602188 0.73380202 -1.195183 60.597832 -1.1014251 
		-2.1763129 60.25948 0.037214998 2.1763129 60.25948 0.037214998 1.195183 60.597832 
		-1.1014251 -8.0842112e-015 60.576202 -1.492601 -2.037569 60.065239 -2.0730579 2.037569 
		60.065037 -2.0730579 -7.293384e-015 59.964352 -2.6235249 -1.397081 60.587036 3.710011 
		-1.528792 60.003872 3.0755141 -0.54711902 59.084896 3.9267931 0.54711902 59.084896 
		3.9267931 1.528792 60.003872 3.0755141 1.397081 60.587036 3.710011 -0.741822 60.347046 
		4.0378108 0.741822 60.347046 4.0378108 -1.454603 60.910198 3.8879001 -2.381124 60.938595 
		2.35448 1.454603 60.910198 3.8879001 2.381124 60.938564 2.35448 -2.290031 60.775082 
		1.936179 -1.7294151 60.333969 1.200245 -1.156993 59.53027 1.932709 -1.486896 59.749989 
		2.709362 1.156993 59.53027 1.932709 1.7294151 60.333969 1.200245 2.290031 60.775036 
		1.936179 1.486896 59.749989 2.709362 -0.401088 58.79945 3.4985299 0.401088 58.79945 
		3.4985299 -1.699329 59.298004 1.175523 1.699329 59.298004 1.175523 -2.6130359 61.772633 
		1.066761 2.6130359 61.771019 1.066761 -1.636173 63.967587 4.3026018 -2.184983 63.625538 
		3.9288759 -1.689029 63.400131 4.5795331 -1.491761 63.727749 4.5795331 1.689029 63.400131 
		4.5795331 2.184983 63.625538 3.9288759 1.636173 63.967587 4.3026018 1.491761 63.727749 
		4.5795331 -1.294494 63.400131 4.6808991 1.294494 63.400131 4.6808991 -1.097226 63.727749 
		4.5795331 1.097226 63.727749 4.5795331 -0.95281702 63.967587 4.3026018 0.95281702 
		63.967587 4.3026018 -0.611139 63.400131 4.3026018 -0.89995903 63.400131 4.5795331 
		0.611139 63.400131 4.3026018 0.89995903 63.400131 4.5795331 -1.097226 63.072517 4.5795331 
		1.097226 63.072517 4.5795331 -0.95281702 62.832687 4.3026018 0.95281702 62.832687 
		4.3026018 -1.6362751 62.832756 4.302608 -1.491761 63.072517 4.5795331 1.6362751 62.832756 
		4.302608 1.491761 63.072517 4.5795331 -8.2321726e-015 58.712818 2.212723 -0.96588999 
		58.679844 2.001817 0.96588999 58.679844 2.001817 -8.6543016e-015 59.108318 2.379914 
		-8.6543016e-015 58.836349 3.5488851 -1.237116 60.78091 3.8977311 1.237116 60.78091 
		3.8977311 -0.777596 60.583538 4.3132458 -8.6543016e-015 60.130051 4.4009089 -8.6543016e-015 
		60.512844 4.65451 0.777596 60.583538 4.3132458 -0.195778 61.859371 5.628386 -0.190807 
		61.676067 5.6270752 -8.6543016e-015 61.664207 5.7343488 -8.6543016e-015 61.927361 
		5.755456 0.190807 61.676067 5.6270752 0.195778 61.859371 5.628386 -0.50161701 61.838615 
		5.0994649 -0.449442 61.599796 5.1165051 0.449442 61.599796 5.1165051 0.50161701 61.838615 
		5.0994649 -0.21772499 63.119419 5.0375371 -8.6543016e-015 63.373753 5.1065841 0.21772499 
		63.119419 5.0375371 -0.453982 62.273876 4.9181108 0.453982 62.273876 4.9181108 -8.6543016e-015 
		61.325657 4.9250879 -0.70578301 61.058762 4.4891028 -0.61502898 60.79163 4.2866249 
		-8.6543016e-015 60.840073 4.5403142 -8.6543016e-015 61.047901 4.9002252 0.61502898 
		60.79163 4.2866249 0.70578301 61.058762 4.4891028 -8.6543016e-015 59.163334 4.23035 
		-8.6543016e-015 63.976604 5.3888102 -0.70323998 63.594925 5.2653079 0.70323998 63.594925 
		5.2653079 -2.209687 62.58823 4.0210009 2.209687 62.58823 4.0210009 -0.57646501 61.627937 
		4.5836639 -0.50233799 61.874283 4.677465 0.57646501 61.627937 4.5836639 0.50233799 
		61.874283 4.677465 -2.296659 66.345497 2.6841071 -2.46384 63.987518 3.8832691 -1.402123 
		63.887222 5.1683578 -1.678206 66.556412 3.8570781 1.402123 63.887222 5.1683578 2.46384 
		63.987518 3.8832691 2.296659 66.345497 2.6841071 1.678206 66.556412 3.8570781 -2.6813459 
		65.627167 1.279072 -2.8080389 63.83643 1.167662 2.8080389 63.832703 1.167662 2.6813459 
		65.626625 1.279072 -1.958943 63.543198 3.9585581 -2.150182 63.599903 4.085176 -1.736321 
		62.84322 4.4992518 -1.601457 62.99572 4.4625378 1.736321 62.84322 4.4992518 2.150182 
		63.599903 4.085176 1.958943 63.543198 3.9585581;
	setAttr ".vt[1328:1493]" 1.601457 62.99572 4.4625378 -1.018984 62.819443 4.5432749 
		-1.02284 62.935932 4.3331108 1.018984 62.819443 4.5432749 1.02284 62.935932 4.3331108 
		-0.558384 63.062984 4.3656912 -0.77917099 63.067062 4.252255 0.558384 63.062984 4.3656912 
		0.77917099 63.067062 4.252255 -1.3071229 63.433887 4.6841941 -1.3793629 63.472164 
		4.895988 1.3793629 63.472164 4.895988 1.3071229 63.433887 4.6841941 -2.739325 61.89917 
		0.72168601 -2.646297 61.888699 1.306415 -2.97352 61.865314 0.78295702 2.646297 61.886833 
		1.306415 2.845576 61.864723 0.64556003 2.97352 61.861996 0.78295702 -3.6412489 62.785748 
		0.33990002 3.6412489 62.777008 0.33989999 -3.5318229 63.939716 0.609357 -3.780535 
		63.672153 0.30332398 3.780535 63.662354 0.30332401 3.5318229 63.931618 0.609357 -0.513197 
		62.92749 4.6575422 -1.006907 62.695305 4.57442 1.006907 62.695305 4.57442 0.513197 
		62.92749 4.6575422 -1.321906 61.957169 4.364028 -1.5465291 62.327072 4.3881092 1.321906 
		61.957169 4.364028 1.5465291 62.327072 4.3881092 -2.509634 63.457119 3.397599 2.509634 
		63.456879 3.397599 -0.035845 66.559822 4.6017179 -3.4929371 63.663643 0.25399899 
		-3.359009 62.811413 0.28916699 3.490051 62.808804 0.195738 3.6293371 63.694157 0.159162 
		-2.538352 63.807362 1.026898 -3.2342939 63.906082 0.490069 3.360347 63.948032 0.404677 
		2.636564 63.849113 0.962982 -0.94748098 58.875259 -7.9495211 -0.115529 59.396671 
		-8.4561806 -0.49164999 57.269341 -9.9450283 -0.072429001 58.452408 -7.493567 0.63564497 
		58.970512 -8.0004587 1.18635 60.427132 -6.8156118 0.00101 61.24791 -7.6857119 0.082589 
		59.460625 -5.8637099 -1.342151 60.275013 -6.7342558 -0.211197 62.089333 -4.7448978 
		0.155798 62.416294 -4.8914108 0.146369 61.791656 -4.5648451 0.43182001 62.118374 
		-4.7167339 0.33482599 62.976219 -3.042089 -8.6543016e-015 63.459248 -3.2795291 -0.0057399999 
		62.522053 -3.1065049 -0.493018 62.970291 -3.235213 10.790487 55.893658 -0.30338299 
		11.688911 55.701298 -3.2553799 10.209146 55.321899 -0.487528 10.780354 55.152493 
		-2.993237 9.1239586 54.206913 0.95147699 9.1650038 54.155804 0.46773699 9.7017775 
		54.246914 -4.8038621 10.030729 54.652393 -5.3901639 7.7807822 53.001698 0.199292 
		12.256286 54.011719 -3.2763939 12.465171 53.980152 -1.1738009 11.219482 52.437111 
		0.60359102 9.7989922 50.839371 -0.67841297 8.1240587 52.9604 -4.7130351 8.5439863 
		51.018784 -2.813899 11.49157 52.506207 -5.2920718 10.22636 50.995575 -5.076189 -9.3251171 
		54.598328 -4.817544 -6.2890849 56.435276 -4.4155879 -8.7731199 54.723488 0.62357908 
		-6.4296708 60.135174 -3.7979829 -6.8928189 60.97929 -1.7834191 -4.197341 61.666504 
		-1.1161113 -7.2580118 60.070034 0.27615184 -3.243861 57.815609 -5.2438459 -4.7721615 
		57.691158 2.4520071 -4.3557749 58.030579 -1.050561 -4.0977383 55.27874 -1.2133768 
		-4.0673432 55.07642 -5.961689 -6.0673699 54.974308 3.077843 -6.1890349 56.766972 
		-5.6871681 -7.7739582 56.766972 2.0841582 -7.3804288 59.783077 -5.050148 -8.6229811 
		59.663815 1.0414299 -8.4764957 55.378174 -5.039968 -8.6996393 57.454391 -5.4112792 
		-9.9760141 57.454391 0.84714592 -9.6252851 55.378174 0.59287304 -11.859966 56.370461 
		-4.1204991 -11.418786 54.697746 -5.1915011 -12.404536 56.370716 -1.4503199 -12.391931 
		54.697704 -0.41990292 -11.26859 58.114288 -4.4883699 -11.970975 58.114288 -1.0443741 
		-8.658577 59.282684 -4.3565831 -9.5227547 59.214725 -0.10619208 -9.3635578 50.239326 
		-3.191607 -10.186904 50.19363 -3.3339591 -8.2896671 51.267735 -3.5088329 -6.6008 
		51.889996 -3.408041 -8.8537827 51.267735 -0.74281496 -10.531015 50.19363 -1.6466839 
		-9.7227716 50.239326 -1.4302828 -7.2619228 51.889996 -0.16636997 -7.265686 51.661171 
		-1.855389 -9.714057 50.249062 -2.345798 -10.564059 50.222233 -2.532151 -9.2029066 
		51.175808 -2.25455 -14.051476 52.586739 -3.2408788 -14.693018 53.119473 -3.3717177 
		-14.374996 52.996902 -4.2030201 -13.910117 52.586838 -4.0036011 -14.725934 52.996902 
		-2.4822707 -14.220089 52.586838 -2.4837148 -14.870348 55.15485 -3.4468839 -14.422021 
		54.85498 -4.5151892 -14.876176 54.85498 -2.2883348 -13.08995 53.117176 -4.9384928 
		-13.517445 53.076782 -3.134479 -13.830546 53.117176 -1.3071359 -12.564572 53.912575 
		-2.940145 -12.707185 56.40543 -2.9026639 -5.3784518 63.534843 -3.9926791 -6.2176137 
		62.28735 -4.3158159 -6.3075185 62.367035 -3.773545 -5.9189878 62.243988 -4.3660269 
		-5.8034892 62.039028 -3.755229 -6.1183348 60.985233 -3.7762351 -6.5986671 60.918945 
		-4.774704 -7.0631037 61.422817 -3.5549581 -7.22475 61.082375 -4.394578 -8.3221369 
		60.14793 -3.354779 -8.4221048 60.22728 -2.09531 -8.8273191 60.1082 -0.87007809 -9.1643791 
		59.861168 -2.2466929 -11.764635 58.47374 -2.7116511 -9.3534718 55.238194 -2.2852571 
		3.2341211 64.00457 0.84719503 2.6354539 63.513405 2.954057 2.569345 61.913387 2.674067 
		3.1773429 61.935738 1.846261 1.999915 66.528503 4.512835 -0.28241199 65.693741 5.561985 
		0.81464899 64.630051 5.5205789 2.1895649 64.97477 4.6649618 2.5771861 60.166664 2.4610519 
		2.3856499 60.427959 2.792177 2.929852 60.261417 1.985886 2.107162 59.070717 2.882051 
		2.3298638 58.261917 2.3077712 -2.8950429 60.071304 2.056458 -2.2409949 58.112236 
		2.488308 -2.620615 60.53735 2.5210891 -2.3343811 59.563763 2.868778 -2.473907 60.721725 
		2.759182;
	setAttr ".vt[1494:1659]" -3.190156 61.939899 1.746755 -2.569345 61.913979 2.674067 
		-3.343354 63.96204 0.73926997 -2.677552 63.68861 2.639868 1.991618 62.990044 5.1121721 
		1.631935 62.891182 5.270874 2.4430001 63.114113 4.9130101 2.0270309 61.86528 4.9675231 
		2.514781 61.500431 4.5240712 -0.555516 64.575623 5.6796999 -0.96860099 64.531624 
		5.4977441 -2.153718 66.067688 4.8967009 -1.773648 64.48455 5.2446709 -0.83086002 
		63.33952 5.5946112 -1.146032 62.487499 5.1839838 1.895708 67.625801 4.0285478 -0.14317299 
		67.553246 4.698905 2.5353899 64.70871 3.488914 2.7808745 62.758415 3.5396442 3.580631 
		65.790939 0.90399301 -2.1791811 67.449913 4.3309212 -2.8322749 68.176788 2.0156109 
		-2.687119 65.286819 3.6066799 -2.4011159 64.600021 4.4482899 -2.4094729 63.168636 
		4.377121 -2.6013839 64.369431 3.4115219 -2.8233089 62.213486 3.348284 -2.7393479 
		61.341915 2.8235149 -3.692307 65.513596 1.731485 2.2159359 61.909073 -0.211634 1.294167 
		61.202488 -1.46304 1.086441 64.105072 -2.2272789 2.934087 64.825134 -1.125232 2.9682231 
		67.956978 0.91159898 0.003607 63.477558 -3.3529451 0.051734999 65.986267 -2.9977601 
		0.14061201 69.150055 2.235569 2.186336 66.927559 -1.19576 -3.4462509 65.075043 -0.44220099 
		-2.4682839 62.416557 0.014245003 -1.3324341 64.004097 -2.2968459 -1.474417 61.2817 
		-1.221471 0.077422999 60.83128 -1.7165819 -6.6070919 13.073055 3.695868 6.6070919 
		13.073055 3.695868 -5.7148342 13.994567 3.794838 -6.0985169 14.078122 4.086812 -5.7848802 
		14.421232 4.2473249 5.7848802 14.421232 4.2473249 6.0985169 14.078122 4.086812 5.7148342 
		13.994567 3.794838 -6.988554 14.590045 4.2881289 -6.7927041 14.175482 4.1103449 -7.2647748 
		14.206713 3.847301 7.2647748 14.206713 3.847301 6.7927041 14.175482 4.1103449 6.988554 
		14.590045 4.2881289 -5.7819939 14.025232 4.0809441 5.7819939 14.025232 4.0809441 
		-7.1112299 14.211656 4.1260061 7.1112299 14.211656 4.1260061 -4.9295149 15.341546 
		5.0411239 -5.5610342 15.298352 5.0274792 -5.4864068 14.547444 4.377223 5.4864068 
		14.547444 4.377223 5.5610342 15.298352 5.0274792 4.9295149 15.341546 5.0411239 -7.2314591 
		14.792186 4.4363809 -6.9099469 15.487535 5.0732079 -7.502934 15.702469 5.128366 7.502934 
		15.702469 5.128366 6.9099469 15.487535 5.0732079 7.2314591 14.792186 4.4363809 -4.921 
		15.294559 4.3536501 4.921 15.294559 4.3536501 -7.5703168 15.666122 4.4434652 7.5703168 
		15.666122 4.4434652 -5.0127659 13.816954 3.119066 -5.2198782 15.239779 3.3001781 
		-6.3046799 15.184721 3.8866091 -6.4579792 14.095467 3.866519 6.3046799 15.184721 
		3.8866091 5.2198782 15.239779 3.3001781 5.0127659 13.816954 3.119066 6.4579792 14.095467 
		3.866519 -7.3696408 15.541279 3.3730569 -7.8389759 14.213325 3.2148781 7.8389759 
		14.213325 3.2148781 7.3696408 15.541279 3.3730569 -4.5592499 15.267381 4.7120709 
		4.5592499 15.267381 4.7120709 -7.9006271 15.736008 4.8253469 7.9006271 15.736008 
		4.8253469 -5.3265219 15.332431 5.4617019 5.3265219 15.332431 5.4617019 -7.0962849 
		15.580639 5.521699 7.0962849 15.580639 5.521699 -6.227747 15.313589 5.5988798 6.227747 
		15.313589 5.5988798 -6.4061661 14.147102 4.4708819 -5.6251578 14.046964 4.0368309 
		6.4061661 14.147102 4.4708819 5.6251578 14.046964 4.0368309 -7.258677 14.276063 4.0922089 
		7.258677 14.276063 4.0922089 -4.172019 15.194795 1.902992 -4.2723589 13.720963 2.853723 
		4.2723589 13.720963 2.853723 4.172019 15.194795 1.902992 -8.5934124 14.326986 3.000211 
		-8.481245 15.799159 2.049078 8.481245 15.799159 2.049078 8.5934124 14.326986 3.000211 
		-5.60847 13.502004 4.0203242 5.60847 13.502004 4.0203242 -7.425385 13.756824 4.0819201 
		7.425385 13.756824 4.0819201 -6.477066 13.785694 4.573565 6.477066 13.785694 4.573565 
		-4.1641159 15.38059 4.8491468 -3.667927 15.853909 4.7881169 4.1641159 15.38059 4.8491468 
		3.667927 15.853909 4.7881169 -8.239337 15.952136 4.9873009 -8.5894775 16.544151 4.9549608 
		8.239337 15.952136 4.9873009 8.5894775 16.544151 4.9549608 -2.776062 17.034273 4.35074 
		2.776062 17.034273 4.35074 -9.1504383 17.928272 4.5668392 9.1504383 17.928272 4.5668392 
		-2.8002911 19.48695 4.15347 -3.9938879 18.488903 1.623445 2.8002911 19.48695 4.15347 
		3.9938879 18.488903 1.623445 -8.4672422 20.281734 4.3455849 -7.7667341 19.01804 1.751348 
		8.4672422 20.281734 4.3455849 7.7667341 19.01804 1.751348 -3.5635791 20.198933 3.4414661 
		3.5635791 20.198933 3.4414661 -7.5876169 20.763304 3.5778861 7.5876169 20.763304 
		3.5778861 -2.9719081 18.69581 5.214797 2.9719081 18.69581 5.214797 -8.4482832 19.463869 
		5.4004531 8.4482832 19.463869 5.4004531 -4.0129499 16.728455 5.4827271 -3.999171 
		17.307663 5.8636532 4.0129499 16.728455 5.4827271 3.999171 17.307663 5.8636532 -7.971673 
		17.283661 5.6169319 -7.8002372 17.840754 5.9925132 7.971673 17.283661 5.6169319 7.8002372 
		17.840754 5.9925132 -4.0371132 17.718882 6.110476 4.0371132 17.718882 6.110476 -7.6343379 
		18.223385 6.2324262 7.6343379 18.223385 6.2324262 -4.57622 15.669288 5.430747 4.57622 
		15.669288 5.430747 -7.7256069 16.110987 5.5375142 7.7256069 16.110987 5.5375142 -5.0674591 
		17.23768 6.4632678 -4.8804498 17.701984 6.5032492 5.0674591 17.23768 6.4632678;
	setAttr ".vt[1660:1818]" 4.8804498 17.701984 6.5032492 -6.7543278 17.47426 6.5204558 
		-6.8035688 17.971699 6.5684452 6.7543278 17.47426 6.5204558 6.8035688 17.971699 6.5684452 
		-4.3981781 19.430086 6.1094708 4.3981781 19.430086 6.1094708 -6.8183498 19.769512 
		6.1915169 6.8183498 19.769512 6.1915169 -3.6738639 20.400646 5.1366148 3.6738639 
		20.400646 5.1366148 -7.3122559 20.910923 5.2599602 7.3122559 20.910923 5.2599602 
		-2.6433151 20.148455 4.3817959 -2.74826 20.952549 5.1209569 -3.1264989 20.496681 
		4.9105039 3.1264989 20.496681 4.9105039 2.74826 20.952549 5.1209569 2.6433151 20.148455 
		4.3817959 -7.826169 21.155804 5.0698271 -8.049674 21.696066 5.3006811 -8.4208565 
		20.958752 4.577661 8.4208565 20.958752 4.577661 8.049674 21.696066 5.3006811 7.826169 
		21.155804 5.0698271 -2.826153 20.591831 4.0763431 2.826153 20.591831 4.0763431 -8.1443491 
		21.337704 4.2566352 8.1443491 21.337704 4.2566352 -3.2845039 20.719528 4.3057032 
		3.2845039 20.719528 4.3057032 -7.6541872 21.332373 4.4538422 7.6541872 21.332373 
		4.4538422 -4.2574968 20.449575 4.0283961 4.2574968 20.449575 4.0283961 -6.8136568 
		20.808075 4.1150508 6.8136568 20.808075 4.1150508 -5.4078588 21.171658 5.5741792 
		-5.4161401 21.321159 4.7242599 5.4078588 21.171658 5.5741792 5.4161401 21.321159 
		4.7242599 -5.5671749 19.858473 6.29145 5.5671749 19.858473 6.29145 -5.903996 17.401443 
		6.5071859 5.903996 17.401443 6.5071859 -5.934608 17.208605 6.4026279 5.934608 17.208605 
		6.4026279 -5.5447888 15.661595 5.672348 -6.186686 15.596071 5.6419969 6.186686 15.596071 
		5.6419969 5.5447888 15.661595 5.672348 -6.7823749 15.835167 5.714304 6.7823749 15.835167 
		5.714304 -5.2785621 15.564472 5.5386848 5.2785621 15.564472 5.5386848 -7.07341 15.816198 
		5.5995321 7.07341 15.816198 5.5995321 -5.2005658 15.809131 5.943357 -5.4480228 15.830744 
		6.1085272 5.4480228 15.830744 6.1085272 5.2005658 15.809131 5.943357 -6.7993779 16.020271 
		6.1543398 -7.0538039 16.069046 6.006187 7.0538039 16.069046 6.006187 6.7993779 16.020271 
		6.1543398 -6.1283741 15.820148 6.4247522 6.1283741 15.820148 6.4247522 -5.5714889 
		16.759607 6.003747 5.5714889 16.759607 6.003747 -6.4330382 16.880436 6.0329552 6.4330382 
		16.880436 6.0329552 -6.064126 16.249107 6.545424 -5.449461 16.095972 6.1614571 6.064126 
		16.249107 6.545424 5.449461 16.095972 6.1614571 -6.7216301 16.274393 6.2045832 6.7216301 
		16.274393 6.2045832 -5.2572799 16.158779 5.7592258 5.2572799 16.158779 5.7592258 
		-6.915832 16.39139 5.815454 6.915832 16.39139 5.815454 -5.0629559 15.93714 5.965508 
		5.0629559 15.93714 5.965508 -7.1491718 16.229729 6.036232 7.1491718 16.229729 6.036232 
		-5.0713382 15.817924 5.5362411 5.0713382 15.817924 5.5362411 -7.202775 16.116856 
		5.6084991 7.202775 16.116856 5.6084991 -5.6333141 53.273052 -1.5265501 -5.2110629 
		52.994652 -5.0428629 -6.7936788 53.122288 1.829402 -6.8445139 52.907894 -4.444849 
		-7.8957739 52.808079 0.52202708 -8.4385977 53.25288 -2.160152 -6.9497528 60.833771 
		0.36338398 -7.7643132 60.725864 1.1096458 -7.7407327 61.320831 -0.15639015 -8.2044783 
		60.9366 0.5395298 -7.1628594 62.181133 0.91937983 -7.0562329 62.280041 0.3836529 
		-6.9010358 62.126144 1.0675389 -6.5819521 61.934822 0.52328682 -6.3041453 63.296513 
		1.0118482 -4.8655272 54.275898 -1.369963 -4.6392031 53.964531 -5.6187792 -6.4903474 
		53.980427 2.5398662 -7.139308 54.181782 -4.9309912 -8.3321285 54.176956 0.88696098 
		-8.4331369 54.341358 -2.1794281 -13.452595 53.542454 -1.1324738 -12.473698 53.251575 
		-0.64681989 -12.77553 54.19585 -0.80993485 -12.050144 54.341675 -4.993969 -11.865638 
		53.167183 -4.9685569 -12.687593 53.648613 -4.9202061 -1.261795 66.165726 5.2080698 
		-1.2049069 67.786583 4.4936399 -0.0045429999 68.492691 -0.84538698 -1.035875 68.170135 
		-0.76592201 -1.389562 68.948433 2.1043169 -2.2217729 67.468178 -0.72002 1.757218 
		68.824295 1.508847 1.311178 67.873688 -1.1228141 -3.51736 66.803818 1.893476 -2.9128881 
		66.436745 -0.524872 1.5654302 46.379189 -3.1960871 1.5378823 46.394932 -3.3799644 
		0.21439597 46.592026 -3.2269363 1.2816335 46.642448 -2.3135452 2.8505807 45.95459 
		-2.6480024 2.7260571 45.978718 -2.8762977 0.80141681 46.59362 -3.1358485 -0.2950407 
		49.976837 -3.989635 0.99272084 49.59325 -4.0129309 2.3510251 49.318787 -4.2938075 
		2.3814092 49.301064 -4.1022801 1.1111134 49.796524 -3.371069 -0.26528001 49.959518 
		-3.7971115 1.7662849 49.734261 -3.9482589 3.4694145 49.157101 -3.9796495 3.5071957 
		49.169617 -3.7590101 2.1960893 49.814911 -3.1411653 0.23310986 52.235271 -4.9535179 
		1.6393199 51.887035 -4.8645072 3.0695772 51.529835 -5.2767425 3.1069388 51.508442 
		-5.0743318 1.7214216 51.942909 -4.2943249 0.26929674 52.214638 -4.7490582 2.3799233 
		51.815163 -4.6508656 4.1218615 51.263664 -4.929235 4.1619368 51.276024 -4.704073 
		2.7003541 51.939552 -3.885345 5.3957596 45.516258 -0.25722161 1.9569094 46.087505 
		-2.7701035 4.3630881 45.487148 -2.123533 -1.1509811 46.838432 -3.0143058 -1.1007646 
		46.901611 -2.8480887 0.34687254 46.688885 -2.4773469;
	setAttr -s 3766 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 
		1 3 0 0 4 5 1 5 6 1 6 7 
		0 7 4 1 2 8 1 8 9 1 9 3 
		0 10 11 1 11 4 1 7 10 0 12 13 
		1 13 14 0 14 15 1 15 12 1 17 1814 
		0 12 1815 1 16 17 0 18 19 0 19 20 
		1 20 21 1 21 18 0 19 22 0 22 23 
		1 23 20 1 20 24 1 24 25 1 25 21 
		0 23 26 1 26 24 1 24 27 1 27 28 
		0 28 25 0 26 29 1 29 27 1 26 30 
		1 30 31 1 31 29 1 30 12 1 15 31 
		1 22 32 0 32 1813 1 32 16 0 33 31 
		1 15 34 1 34 33 0 35 29 1 33 35 
		0 36 27 0 35 36 0 14 37 0 37 34 
		0 38 39 1 39 40 0 40 41 1 41 38 
		1 42 38 1 41 43 1 43 42 0 44 45 
		1 45 46 1 46 47 1 47 44 0 41 48 
		1 48 49 1 49 43 0 48 45 1 44 49 
		0 42 50 0 50 51 1 51 38 1 46 51 
		1 50 47 0 46 52 1 52 53 0 53 51 
		1 53 39 0 48 54 1 54 55 0 55 45 
		1 40 54 0 55 52 0 56 57 1 57 58 
		1 58 59 1 59 56 1 57 60 1 60 61 
		1 61 58 1 58 62 1 62 63 1 63 59 
		1 61 64 1 64 62 1 65 66 1 66 67 
		1 67 68 1 68 65 1 66 69 1 69 70 
		1 70 67 1 71 72 1 72 66 1 65 71 
		1 72 73 1 73 69 1 67 57 1 56 68 
		1 70 60 1 62 72 1 71 63 1 64 73 
		1 74 75 1 75 76 1 76 77 1 77 74 
		1 78 79 1 79 80 1 80 81 1 81 78 
		1 75 82 1 82 83 1 83 76 1 82 78 
		1 81 83 1 84 74 1 77 85 1 85 84 
		1 79 84 1 85 80 1 86 87 0 87 1787 
		1 88 89 0 89 1817 1 90 86 0 89 90 
		0 87 91 0 91 1788 1 91 88 0 85 1804 
		1 1816 1793 1 77 1805 1 81 1808 1 1786 1796 
		1 64 92 1 92 93 1 93 73 1 92 94 
		1 94 95 1 95 93 1 70 96 1 96 97 
		1 97 60 1 96 98 1 98 99 1 99 97 
		1 93 100 1 100 69 1 95 101 1 101 100 
		1 100 96 1 101 98 1 61 102 1;
	setAttr ".ed[166:331]" 102 92 1 102 103 1 103 94 
		1 97 102 1 99 103 1 104 105 1 105 106 
		1 106 107 1 107 104 1 107 63 1 71 104 
		1 56 108 1 108 109 1 109 68 1 108 110 
		1 110 111 1 111 109 1 104 112 1 112 113 
		1 113 105 1 65 112 1 109 112 1 111 113 
		1 114 115 1 115 107 1 106 114 1 115 59 
		1 108 115 1 114 110 1 114 116 1 116 117 
		1 117 110 1 116 84 1 79 117 1 106 118 
		1 118 116 1 118 74 1 111 119 1 119 120 
		1 120 113 1 119 78 1 82 120 1 121 105 
		1 120 121 1 75 121 1 117 119 1 121 118 
		1 99 122 1 122 123 1 123 103 1 122 124 
		1 124 125 0 125 123 1 123 126 1 126 94 
		1 125 127 0 127 126 1 101 128 1 128 129 
		1 129 98 1 128 130 1 130 131 0 131 129 
		1 95 132 1 132 128 1 132 133 1 133 130 
		0 129 122 1 131 124 0 126 132 1 127 133 
		0 134 135 0 135 136 1 136 137 1 137 134 
		1 138 134 0 137 139 1 139 138 1 140 141 
		0 141 142 1 142 143 1 143 140 1 141 144 
		0 144 145 1 145 142 1 136 143 1 142 137 
		1 145 139 1 146 1811 1 147 146 1 1812 1802 
		0 148 1812 0 146 148 1 1792 1799 0 1799 1809 
		0 149 147 1 1800 1791 1 150 151 0 151 1792 
		0 1802 1789 0 152 153 0 153 1790 1 153 150 
		0 154 155 1 155 147 1 149 154 0 156 157 
		1 157 148 0 146 156 1 155 156 1 158 159 
		1 159 156 1 155 158 1 159 160 1 160 157 
		0 161 158 1 154 161 0 162 139 1 145 163 
		1 163 162 1 164 162 1 163 165 1 165 164 
		1 144 166 0 166 163 1 166 167 0 167 165 
		1 168 138 0 162 168 1 169 168 0 164 169 
		1 161 169 0 167 160 0 158 164 1 165 159 
		1 136 170 1 170 171 0 171 143 1 171 172 
		0 172 140 0 173 170 0 135 173 0 174 175 
		1 175 176 1 176 177 1 177 174 1 174 178 
		1 178 179 1 179 175 1 178 180 1 180 181 
		1 181 179 1 176 182 1 182 183 1 183 177 
		1 182 184 1 184 185 1 185 183 1 184 186 
		1 186 187 1 187 185 1 186 181 1 180 187 
		1 188 189 1 189 190 1 190 191 1;
	setAttr ".ed[332:497]" 191 188 1 192 193 1 193 189 
		1 188 192 1 194 195 1 195 193 1 192 194 
		1 194 196 1 196 195 1 176 197 1 197 196 
		1 194 176 1 198 191 1 190 199 1 199 198 
		1 200 198 1 199 201 1 201 200 1 176 200 
		1 201 176 1 201 197 1 198 179 1 181 191 
		1 186 188 1 184 192 1 182 194 1 175 200 
		1 202 203 1 203 204 1 204 205 1 205 202 
		1 206 207 1 207 208 1 208 209 1 209 206 
		1 207 210 1 210 211 1 211 208 1 212 213 
		1 213 214 1 214 215 1 215 212 1 212 216 
		1 216 217 1 217 213 1 215 218 1 218 212 
		1 218 216 1 219 220 1 220 221 1 221 222 
		1 222 219 1 223 224 1 224 220 1 219 223 
		1 220 225 1 225 226 1 226 221 1 227 225 
		1 224 227 1 228 229 1 229 230 1 230 231 
		1 231 228 1 230 232 1 232 231 1 233 234 
		1 234 232 1 230 233 1 235 233 1 229 235 
		1 235 236 1 236 237 1 237 235 1 238 239 
		1 239 240 1 240 241 1 241 238 1 240 242 
		1 242 241 1 235 243 1 243 244 1 244 236 
		1 243 245 1 245 246 1 246 244 1 240 246 
		1 245 242 1 247 242 1 245 248 1 248 247 
		1 249 248 1 245 249 1 250 247 1 248 251 
		1 251 250 1 232 252 1 252 253 1 253 231 
		1 253 254 1 254 228 1 255 256 1 256 254 
		1 253 255 1 257 255 1 252 257 1 249 256 
		1 255 248 1 257 251 1 258 259 1 259 234 
		1 234 258 1 234 260 1 260 258 1 261 251 
		1 251 259 1 259 261 1 262 261 1 259 262 
		1 245 254 1 243 228 1 259 252 1 236 239 
		1 238 237 1 263 264 1 264 265 1 265 266 
		1 266 263 1 265 267 1 267 266 1 268 269 
		1 269 270 1 270 271 1 271 268 1 263 272 
		1 272 273 1 273 264 1 274 268 1 271 275 
		1 275 274 1 272 274 1 275 273 1 269 276 
		1 276 277 1 277 270 1 276 266 1 267 277 
		1 278 279 1 279 280 1 280 278 1 281 279 
		1 278 281 1 282 283 1 283 284 1 284 282 
		1 282 285 1 285 283 1 285 286 1 286 283 
		1 285 287 1 287 286 1 285 288 1 288 287 
		1 288 289 1 289 287 1 290 291 1;
	setAttr ".ed[498:663]" 291 292 1 292 293 1 293 290 
		1 294 295 1 295 293 1 292 294 1 296 293 
		1 295 297 1 297 296 1 296 290 1 298 299 
		1 299 295 1 294 298 1 300 301 1 301 296 
		1 297 300 1 302 300 1 297 303 1 303 302 
		1 299 303 1 290 304 1 304 305 1 305 291 
		1 239 305 1 304 240 1 306 305 1 239 306 
		1 307 291 1 306 307 1 308 307 1 306 309 
		1 309 308 1 310 311 1 311 312 1 312 313 
		1 313 310 1 308 313 1 312 307 1 312 292 
		1 311 294 1 311 314 1 314 298 1 310 315 
		1 315 314 1 302 316 1 316 317 1 317 300 
		1 301 318 1 318 296 1 318 319 1 319 296 
		1 318 320 1 320 319 1 301 321 1 321 320 
		1 308 320 1 321 313 1 317 321 1 317 310 
		1 316 315 1 322 323 1 323 324 1 324 322 
		1 325 322 1 324 325 1 326 282 1 284 327 
		1 327 326 1 328 329 1 329 327 1 327 328 
		1 327 330 1 330 328 1 329 326 1 324 331 
		1 331 332 1 332 325 1 280 333 1 333 334 
		1 334 278 1 335 336 1 336 325 0 332 335 
		1 280 337 1 337 338 1 338 333 1 339 340 
		1 340 336 0 335 339 1 337 341 1 341 342 
		1 342 338 1 334 331 1 324 278 1 323 281 
		1 343 344 1 344 345 1 345 346 1 346 343 
		1 344 347 1 347 348 1 348 345 1 347 349 
		1 349 350 1 350 348 1 349 351 1 351 352 
		1 352 350 1 351 353 1 353 354 1 354 352 
		1 353 355 1 355 356 1 356 354 1 355 343 
		1 346 356 1 357 358 1 358 359 1 359 360 
		1 360 357 1 361 362 1 362 359 1 358 361 
		1 363 364 1 364 362 1 361 363 1 365 366 
		1 366 364 1 363 365 1 367 368 1 368 366 
		1 365 367 1 369 370 1 370 368 1 367 369 
		1 360 370 1 369 357 1 371 360 1 359 372 
		1 372 371 1 346 358 1 357 356 1 345 361 
		1 362 373 1 373 372 1 348 363 1 364 374 
		1 374 373 1 350 365 1 366 375 1 375 374 
		1 352 367 1 368 376 1 376 375 1 354 369 
		1 370 377 1 377 376 1 371 377 1 351 276 
		1 269 353 1 349 266 1 347 263 1 344 272 
		1 343 274 1 355 268 1 378 379 1;
	setAttr ".ed[664:829]" 379 237 1 238 378 1 238 380 
		1 380 378 1 242 381 1 381 241 1 382 383 
		1 383 233 1 233 382 1 384 378 1 378 385 
		1 385 384 1 386 387 1 387 388 1 388 389 
		1 389 386 1 389 385 1 385 390 1 390 386 
		1 209 391 1 391 206 1 391 392 1 392 393 
		1 393 206 1 392 211 1 210 393 1 217 394 
		1 394 213 1 395 396 1 396 397 1 397 398 
		1 398 395 1 399 400 1 400 397 1 396 399 
		1 399 401 1 401 402 1 402 400 1 401 395 
		1 398 402 1 403 395 1 401 404 1 404 403 
		1 399 405 1 405 404 1 396 406 1 406 405 
		1 403 406 1 407 408 1 408 409 1 409 410 
		1 410 407 1 407 411 1 411 412 1 412 408 
		1 411 413 1 413 414 1 414 412 1 413 410 
		1 409 414 1 222 415 1 415 416 1 416 219 
		1 415 226 1 225 416 1 417 418 1 418 419 
		1 419 262 1 262 417 1 417 420 1 420 421 
		1 421 418 1 420 392 1 392 422 1 422 421 
		1 392 261 1 261 422 1 419 422 1 423 424 
		1 424 425 1 425 426 1 426 423 1 427 428 
		1 428 426 1 425 427 1 427 429 1 429 430 
		1 430 428 1 429 424 1 423 430 1 431 432 
		1 432 433 1 433 434 1 434 431 1 432 435 
		1 435 436 1 436 433 1 435 437 1 437 438 
		1 438 436 1 437 431 1 434 438 1 437 439 
		1 439 440 1 440 431 1 435 441 1 441 439 
		1 432 442 1 442 441 1 440 442 1 443 444 
		1 444 445 1 445 446 1 446 443 1 447 448 
		1 448 446 1 445 447 1 447 449 1 449 450 
		1 450 448 1 443 450 1 449 444 1 451 452 
		1 452 453 1 453 454 1 454 451 1 452 455 
		1 455 456 1 456 453 1 455 457 1 457 458 
		1 458 456 1 457 451 1 454 458 1 459 451 
		1 457 460 1 460 459 1 455 461 1 461 460 
		1 452 462 1 462 461 1 459 462 1 383 463 
		1 463 234 1 383 464 1 464 402 1 398 383 
		1 379 382 1 382 237 1 235 382 1 465 260 
		1 234 465 1 258 262 1 392 251 1 391 251 
		1 381 380 1 400 384 1 384 466 1 466 397 
		1 380 390 1 467 386 1 390 468 1 468 467 
		1 389 469 1 469 385 1 211 467 1;
	setAttr ".ed[830:995]" 468 208 1 211 420 1 417 467 
		1 417 387 1 387 470 1 470 460 1 461 387 
		1 387 442 1 440 260 1 260 387 1 418 224 
		1 223 419 1 422 471 1 471 227 1 227 421 
		1 410 393 1 210 407 1 468 250 1 250 209 
		1 472 473 1 473 462 1 459 472 1 474 469 
		1 469 473 1 472 474 1 394 218 1 214 394 
		1 475 216 1 218 476 1 476 475 1 408 475 
		1 476 409 1 217 477 1 477 478 1 478 394 
		1 477 412 1 414 478 1 475 477 1 478 476 
		1 221 479 1 479 480 1 480 222 1 481 480 
		1 479 481 1 479 482 1 482 481 1 482 226 
		1 415 481 1 471 416 1 471 223 1 453 450 
		1 443 454 1 449 445 1 458 446 1 448 456 
		1 260 470 1 473 388 1 388 462 1 470 472 
		1 483 205 1 204 483 1 202 484 1 484 203 
		1 464 384 1 405 483 1 204 404 1 483 484 
		1 484 406 1 403 203 1 466 383 1 466 469 
		1 469 463 1 464 379 1 433 430 1 423 434 
		1 429 425 1 438 426 1 428 436 1 262 439 
		1 441 417 1 474 463 1 468 381 1 247 468 
		1 411 207 1 206 413 1 474 465 1 470 465 
		1 439 258 1 485 486 1 486 487 1 487 485 
		1 488 489 1 489 485 1 485 488 1 489 486 
		1 485 320 1 308 488 1 487 319 1 490 491 
		1 491 486 1 489 490 1 489 309 1 236 490 
		1 304 492 1 492 493 1 493 240 1 290 494 
		1 494 492 1 492 495 1 495 496 1 496 493 
		1 494 497 1 497 495 1 495 486 1 491 496 
		1 497 487 1 497 296 1 244 491 1 246 496 
		1 498 499 1 499 500 1 500 501 1 501 498 
		1 502 503 1 503 504 1 504 505 1 505 502 
		1 506 507 1 507 508 1 508 509 1 509 506 
		1 510 511 1 511 507 1 506 510 1 512 513 
		1 513 511 1 510 512 1 514 515 1 515 503 
		1 502 514 1 502 516 1 516 517 1 517 514 
		1 516 507 1 511 517 1 505 518 1 518 516 
		1 518 508 1 504 519 1 519 518 1 519 509 
		1 520 521 1 521 522 1 522 523 1 523 520 
		1 521 513 1 512 522 1 521 517 1 514 524 
		1 524 525 1 525 515 1 524 520 1 523 525 
		1 524 517 1 521 524 1 526 527 1;
	setAttr ".ed[996:1161]" 527 528 1 528 529 1 529 526 
		1 530 531 1 531 532 1 532 533 1 533 530 
		1 534 535 1 535 536 1 536 537 1 537 534 
		1 538 539 1 539 540 1 540 541 1 541 538 
		1 542 543 1 543 544 1 544 542 1 543 545 
		1 545 546 1 546 544 1 547 548 1 548 546 
		1 546 547 1 549 550 1 550 548 1 547 549 
		1 550 551 1 551 552 1 552 548 1 553 554 
		1 554 555 1 555 556 1 556 553 1 541 529 
		1 528 538 1 532 536 1 536 557 1 557 532 
		1 557 558 1 558 533 1 550 559 1 559 551 
		1 560 561 1 561 562 1 562 563 1 563 560 
		1 564 565 1 565 566 1 566 567 1 567 564 
		1 568 569 1 569 570 1 570 571 1 571 568 
		1 542 567 1 566 543 1 570 561 1 560 571 
		1 572 573 1 573 574 1 574 572 1 574 575 
		1 575 572 1 568 563 1 562 569 1 576 577 
		1 577 578 1 578 579 1 579 576 1 580 581 
		1 581 559 1 559 582 1 582 580 1 583 584 
		1 584 585 1 585 586 1 586 583 1 554 580 
		1 580 587 1 587 555 1 581 553 1 553 551 
		1 588 589 1 589 553 1 581 588 1 585 576 
		1 579 586 1 590 585 1 584 591 1 591 590 
		1 583 578 1 577 584 1 592 580 1 554 593 
		1 593 592 1 590 576 1 577 591 1 547 594 
		1 594 595 1 595 549 1 596 597 1 597 598 
		1 598 599 1 599 596 1 600 601 1 601 602 
		1 602 564 1 564 600 1 603 604 1 604 605 
		1 605 606 1 606 603 1 559 607 1 607 600 
		1 600 608 1 608 559 1 607 601 1 564 547 
		1 546 564 1 605 597 1 596 606 1 609 610 
		1 610 611 1 611 609 1 611 612 1 612 609 
		1 603 599 1 598 604 1 613 614 1 614 615 
		1 615 616 1 616 613 1 613 617 1 617 618 
		1 618 614 1 619 620 1 620 621 1 621 622 
		1 622 619 1 623 624 1 624 625 1 625 626 
		1 626 623 1 627 628 1 628 629 1 629 630 
		1 630 627 1 625 631 1 631 617 1 613 625 
		1 629 620 1 619 630 1 628 620 1 628 621 
		1 627 622 1 632 633 1 633 634 1 634 635 
		1 635 632 1 633 636 1 636 637 1 637 634 
		1 638 639 1 639 640 1 640 641 1;
	setAttr ".ed[1162:1327]" 641 638 1 639 642 1 642 643 
		1 643 640 1 640 633 1 632 641 1 643 636 
		1 638 635 1 634 639 1 637 642 1 644 636 
		1 636 645 1 645 644 1 644 646 1 646 637 
		1 549 607 1 595 601 1 532 647 1 647 537 
		1 526 540 1 539 527 1 616 626 1 623 615 
		1 614 624 1 545 565 1 602 594 1 648 564 
		1 567 649 1 649 648 1 650 651 1 651 652 
		1 652 650 1 567 652 1 651 649 1 651 647 
		1 532 649 1 608 582 1 531 648 1 530 587 
		1 587 582 1 582 531 1 592 588 1 501 556 
		1 556 653 1 653 501 1 533 654 1 654 655 
		1 655 530 1 534 650 1 650 535 1 656 535 
		1 650 656 1 544 657 1 657 542 1 552 546 
		1 658 556 1 501 658 1 555 653 1 589 593 
		1 553 658 1 658 552 1 595 659 1 659 660 
		1 660 601 1 660 661 1 661 602 1 661 662 
		1 662 594 1 662 659 1 662 596 1 599 659 
		1 661 606 1 660 603 1 611 604 1 598 612 
		1 610 605 1 609 597 1 545 663 1 663 664 
		1 664 565 1 664 665 1 665 566 1 665 666 
		1 666 543 1 666 663 1 666 560 1 563 663 
		1 665 571 1 664 568 1 562 575 1 574 569 
		1 573 570 1 572 561 1 615 542 1 542 656 
		1 656 616 1 650 616 1 652 626 1 567 623 
		1 631 630 1 619 617 1 622 618 1 538 632 
		1 635 539 1 528 641 1 527 638 1 579 667 
		1 667 668 1 668 586 1 669 583 1 668 669 
		1 669 670 1 670 578 1 670 667 1 589 667 
		1 670 593 1 669 592 1 668 588 1 643 645 
		1 526 651 1 650 540 1 534 541 1 537 541 
		1 608 648 1 558 654 1 655 587 1 655 653 
		1 671 672 1 672 673 1 673 674 1 674 671 
		1 672 675 1 675 676 1 676 673 1 675 677 
		1 677 678 1 678 676 1 677 679 1 679 680 
		1 680 678 1 679 681 1 681 682 1 682 680 
		1 681 683 1 683 684 1 684 682 1 683 671 
		1 674 684 1 685 686 1 686 687 1 687 688 
		1 688 685 1 689 690 1 690 691 1 691 692 
		1 692 689 1 693 685 1 688 694 1 694 693 
		1 695 689 1 692 696 1 696 695 1 697 693 
		1 694 698 1 698 697 1 699 695 1;
	setAttr ".ed[1328:1493]" 696 700 1 700 699 1 698 701 
		1 701 702 1 702 697 1 703 699 1 700 704 
		1 704 703 1 705 702 1 701 706 1 706 705 
		1 707 703 1 704 708 1 708 707 1 709 705 
		1 706 710 1 710 709 1 711 707 1 708 712 
		1 712 711 1 690 711 1 712 691 1 686 709 
		1 710 687 1 689 688 1 687 690 1 695 694 
		1 699 698 1 703 701 1 707 706 1 711 710 
		1 672 691 1 712 675 1 671 692 1 683 696 
		1 681 700 1 679 704 1 677 708 1 713 714 
		1 714 715 1 715 716 1 716 713 1 713 717 
		1 717 718 1 718 714 1 719 342 1 341 720 
		1 720 719 1 339 721 1 721 722 1 722 340 
		0 723 719 1 720 724 1 724 723 1 721 725 
		1 725 726 1 726 722 0 718 723 1 724 714 
		1 713 726 1 725 717 1 727 728 1 728 729 
		1 729 727 1 730 330 1 330 728 1 728 730 
		1 727 730 1 728 731 1 731 732 1 732 729 
		1 713 733 1 733 726 1 716 733 1 734 735 
		1 735 185 1 187 734 1 734 736 1 736 737 
		1 737 735 1 738 739 1 739 736 1 734 738 
		1 523 737 1 736 525 1 739 740 1 740 525 
		1 740 515 1 740 503 1 739 503 1 180 738 
		1 735 741 1 741 183 1 741 742 1 742 177 
		1 741 743 1 743 744 1 744 742 1 743 745 
		1 745 746 1 746 744 1 523 745 1 743 737 
		1 522 747 1 747 745 1 747 748 1 748 746 
		1 747 749 1 749 748 1 519 748 1 749 509 
		1 504 746 1 174 750 1 750 751 1 751 178 
		1 751 738 1 751 503 1 742 750 1 503 752 
		1 752 504 1 750 752 1 744 752 1 753 754 
		1 754 289 1 289 753 1 755 754 1 753 755 
		1 755 756 1 756 754 1 755 757 1 757 756 
		1 732 757 1 755 732 1 731 757 1 714 758 
		1 758 715 1 724 758 1 759 760 1 760 682 
		1 684 759 1 674 761 1 761 759 1 678 762 
		1 762 763 1 763 676 1 763 764 1 764 673 
		1 680 765 1 765 762 1 764 761 1 760 766 
		1 766 682 1 766 765 1 512 653 1 655 749 
		1 749 512 1 767 535 1 657 768 1 768 767 
		1 552 769 1 769 768 1 498 770 1 770 771 
		1 771 499 1 770 772 1 772 773 1;
	setAttr ".ed[1494:1659]" 773 771 1 657 656 1 657 535 
		1 552 544 1 552 657 1 774 775 1 775 535 
		1 767 774 1 774 773 1 772 775 1 768 776 
		1 776 774 1 776 771 1 776 777 1 777 499 
		1 769 777 1 775 557 1 770 775 1 770 558 
		1 509 558 1 770 506 1 498 510 1 501 512 
		1 509 654 1 500 778 1 778 658 1 778 769 
		1 778 777 1 624 779 1 779 631 1 779 627 
		1 618 779 1 637 780 1 780 642 1 646 780 
		1 780 645 1 529 647 1 781 782 1 782 783 
		1 783 784 1 784 781 1 785 786 1 786 787 
		1 787 785 1 685 788 1 788 789 1 789 685 
		1 790 789 1 788 791 1 791 790 1 697 792 
		1 792 693 1 793 792 1 792 794 1 794 795 
		1 795 793 1 697 794 1 702 796 1 796 794 
		1 796 797 1 797 795 1 798 702 1 702 799 
		1 799 798 1 790 800 1 800 801 1 801 789 
		1 801 685 1 802 799 1 799 803 1 803 804 
		1 804 802 1 705 803 1 805 806 1 806 807 
		1 807 808 1 808 805 1 807 801 1 800 808 
		1 803 806 1 805 804 1 788 809 1 809 810 
		1 810 791 1 809 792 1 793 810 1 798 811 
		1 811 797 1 686 807 1 806 709 1 693 809 
		1 798 796 1 802 811 1 781 812 1 812 813 
		1 813 782 1 813 814 1 814 785 1 815 785 
		1 814 816 1 816 815 1 817 815 1 816 818 
		1 818 817 1 819 817 1 818 820 1 820 819 
		1 821 819 1 820 822 1 822 821 1 823 821 
		1 822 824 1 824 823 1 825 823 1 824 826 
		1 826 825 1 787 825 1 826 827 1 827 787 
		1 828 787 1 827 829 1 829 828 1 830 783 
		1 783 828 1 829 830 1 817 786 1 786 815 
		1 786 821 1 823 786 1 786 819 1 830 800 
		1 790 831 1 831 830 1 812 811 1 802 813 
		1 832 810 1 810 795 1 795 833 1 833 832 
		1 834 814 1 802 834 1 782 785 1 785 828 
		1 797 835 1 835 833 1 836 837 1 837 833 
		1 835 836 1 838 836 1 784 838 1 838 839 
		1 839 837 1 825 786 1 840 800 1 829 840 
		1 804 841 1 841 834 1 812 835 1 832 842 
		1 842 791 1 842 831 1 843 844 1 844 845 
		1 845 846 1 846 843 1 847 848 1;
	setAttr ".ed[1660:1825]" 848 844 1 843 847 1 826 848 
		1 847 827 1 845 849 1 849 850 1 850 846 
		1 816 851 1 851 852 1 852 818 1 849 852 
		1 851 850 1 824 844 1 822 845 1 820 849 
		1 834 851 1 842 838 1 784 831 1 832 839 
		1 781 836 1 853 805 1 808 854 1 854 853 
		1 840 854 1 853 841 1 854 843 1 846 853 
		1 840 847 1 841 850 1 855 856 1 856 857 
		1 857 858 1 858 855 1 856 859 1 859 860 
		1 860 857 1 858 861 1 861 862 1 862 855 
		1 859 863 1 863 864 1 864 860 1 863 865 
		1 865 866 1 866 864 1 865 867 1 867 868 
		1 868 866 1 861 868 1 867 862 1 866 869 
		1 869 870 1 870 864 1 868 871 1 871 869 
		1 872 857 1 860 872 1 872 858 1 864 872 
		1 858 873 1 873 874 1 874 861 1 874 871 
		1 872 875 1 875 873 1 870 875 1 761 876 
		1 876 877 1 877 759 1 870 877 1 876 870 
		1 869 878 1 878 877 1 878 760 1 763 879 
		1 879 880 1 880 764 1 880 876 1 765 881 
		1 881 882 1 882 762 1 882 879 1 878 883 
		1 883 766 1 883 881 1 871 883 1 874 881 
		1 873 882 1 875 879 1 870 880 1 884 885 
		1 885 886 1 886 887 1 887 884 1 886 856 
		1 855 887 1 885 888 1 888 889 1 889 886 
		1 889 859 1 890 884 1 887 891 1 891 890 
		1 862 891 1 888 892 1 892 893 1 893 889 
		1 893 863 1 892 894 1 894 895 1 895 893 
		1 895 865 1 867 896 1 896 891 1 896 897 
		1 897 890 1 894 897 1 896 895 1 898 899 
		1 899 900 1 900 901 0 901 898 0 902 899 
		1 898 903 0 903 902 0 899 904 1 904 905 
		0 905 900 0 902 906 0 906 904 0 907 908 
		1 908 909 1 909 910 1 910 907 1 909 911 
		1 911 912 1 912 913 1 913 909 1 908 914 
		1 914 915 1 915 909 1 915 916 1 916 911 
		1 907 917 1 917 918 1 918 908 1 919 920 
		1 920 912 1 911 919 1 918 921 1 921 914 
		1 922 919 1 916 922 1 923 924 1 924 925 
		1 925 926 1 926 923 1 927 928 1 928 929 
		1 929 930 1 930 927 1 925 931 1 931 932 
		1 932 926 1 933 927 1 930 934 1;
	setAttr ".ed[1826:1991]" 934 933 1 917 923 1 926 918 
		1 929 920 1 919 930 1 932 921 1 922 934 
		1 924 935 1 935 936 1 936 925 1 935 928 
		1 927 936 1 936 937 1 937 931 1 933 937 
		1 938 939 1 939 917 1 907 938 1 920 940 
		1 940 941 1 941 912 1 910 938 1 941 913 
		1 942 943 1 943 944 1 944 945 1 945 942 
		1 946 942 1 945 947 1 947 946 1 948 949 
		1 949 947 1 945 948 1 950 948 1 944 950 
		1 951 952 1 952 943 1 942 951 1 953 951 
		1 946 953 1 954 955 1 955 952 1 951 954 
		1 956 954 1 953 956 1 950 957 1 957 958 
		1 958 948 1 958 959 1 959 949 1 960 961 
		1 961 955 1 954 960 1 962 960 1 956 962 
		1 957 961 1 960 958 1 962 959 1 963 715 
		1 715 964 1 964 965 1 965 963 1 965 966 
		1 966 281 1 281 963 1 963 716 1 323 963 
		1 967 968 1 968 969 1 969 967 1 967 970 
		1 970 971 1 971 968 1 970 972 1 972 973 
		1 973 971 1 972 974 1 974 975 1 975 973 
		1 976 977 1 977 975 1 974 976 1 978 979 
		1 979 977 1 976 978 1 264 975 1 977 265 
		1 979 267 1 275 971 1 973 273 1 270 969 
		1 968 271 1 979 980 1 980 277 1 967 980 
		1 978 967 1 969 980 1 981 982 1 982 970 
		1 967 981 1 983 972 1 982 983 1 984 985 
		1 985 976 1 974 984 1 983 984 1 981 986 
		1 986 982 1 982 987 1 987 983 1 986 987 
		1 988 978 1 985 988 1 988 981 1 724 989 
		1 989 990 1 990 758 1 991 992 1 992 280 
		1 279 991 1 990 715 1 281 991 1 993 994 
		1 994 995 1 995 996 1 996 993 1 997 998 
		1 998 999 1 999 1000 1 1000 997 1 756 994 
		1 993 754 1 998 286 1 287 999 1 993 1001 
		1 1001 289 1 1001 999 1 996 1002 1 1002 1001 
		1 1002 1000 1 1003 1002 1 996 1004 1 1004 1003 
		1 1003 1005 1 1005 1000 1 1006 1007 1 1007 1008 
		1 1008 1009 1 1009 1006 1 1010 1007 1 1006 1011 
		1 1011 1010 1 1004 1008 1 1007 1003 1 1010 1005 
		1 1008 1012 1 1012 1013 0 1013 1009 1 1014 1015 
		1 1015 1010 1 1011 1014 1 1009 720 1 341 1006 
		1 337 1011 1 1013 1016 0 1016 720 1;
	setAttr ".ed[1992:2157]" 337 1017 1 1017 1014 1 1004 1018 
		1 1018 1012 1 1015 1019 1 1019 1005 1 995 1018 
		1 1019 997 1 994 1020 1 1020 1021 1 1021 995 
		1 1022 998 1 997 1023 1 1023 1022 1 757 1020 
		1 1022 283 1 1024 197 1 197 1012 1 1018 1024 
		1 1015 1025 1 1025 1026 1 1026 1019 1 1021 1024 
		1 1026 1023 1 1020 1027 1 1027 1028 1 1028 1021 
		1 1029 1030 1 1030 1022 1 1023 1029 1 1028 1031 
		1 1031 1024 1 1032 1029 1 1026 1032 1 1027 1033 
		1 1033 1034 1 1034 1028 1 1035 1036 1 1036 1030 
		1 1029 1035 1 1034 1037 1 1037 1031 1 1038 1035 
		1 1032 1038 1 1033 1039 1 1039 1040 1 1040 1034 
		1 1039 1036 1 1035 1040 1 1040 1041 1 1041 1037 
		1 1038 1041 1 1024 196 1 1042 1026 1 1025 1042 
		1 1043 923 1 923 1044 1 1044 1043 1 1045 929 
		1 929 1046 1 1046 1045 1 1043 924 1 928 1046 
		1 1047 1044 1 917 1047 1 1045 1048 1 1048 920 
		1 1031 1049 1 1049 196 1 1050 1032 1 1042 1050 
		1 731 1027 1 1030 284 1 1051 196 1 1049 1051 
		1 1042 1052 1 1052 1050 1 944 985 1 984 950 
		1 943 988 1 1053 1054 1 1054 1055 1 1055 1053 
		1 1055 1056 1 1056 1053 1 1057 1054 1 1053 1058 
		1 1058 1057 1 1056 1059 1 1059 1060 1 1060 1053 
		1 1061 1057 1 1058 1062 1 1062 1061 1 1059 1063 
		1 1063 1064 1 1064 1060 1 1056 1065 1 1065 1059 
		1 1059 1066 1 1066 1063 1 1065 1066 1 894 1062 
		1 1058 897 1 1064 946 1 947 1060 1 1055 1065 
		1 952 981 1 892 1067 1 1067 1062 1 1068 953 
		1 1064 1068 1 1067 1069 1 1069 1061 1 1063 1070 
		1 1070 1068 1 1067 1071 1 1071 1069 1 1072 1068 
		1 1070 1072 1 1070 1073 1 1073 1072 1 1066 1073 
		1 1074 1067 1 892 1074 1 1068 1075 1 1075 953 
		1 1074 1071 1 1072 1075 1 1074 1076 1 1076 1071 
		1 1077 1075 1 1072 1077 1 888 1078 1 1078 1074 
		1 1079 956 1 1075 1079 1 1078 1080 1 1080 1076 
		1 1081 1079 1 1077 1081 1 955 986 1 1058 1082 
		1 1082 890 1 949 1083 1 1083 1060 1 1053 1084 
		1 1084 1082 1 1083 1084 1 1082 1085 1 1085 884 
		1 1086 1083 1 959 1086 1 1084 1087 1 1087 1085 
		1 1086 1087 1 983 957 1 961 987 1 1085 1088 
		1 1088 1089 1 1089 1085 1 1090 1091 1 1091 1086 
		1 1086 1090 1 1089 885 1 962 1090 1;
	setAttr ".ed[2158:2323]" 1089 1078 1 1079 1090 1 1087 1092 
		1 1092 1088 1 1091 1092 1 1088 1093 1 1093 1094 
		1 1094 1089 1 1095 1091 1 1090 1096 1 1096 1095 
		1 1092 1097 1 1097 1093 1 1095 1097 1 1094 1080 
		1 1081 1096 1 1093 924 1 1043 1094 1 928 1095 
		1 1096 1046 1 1097 935 1 728 1033 1 1036 327 
		1 330 1039 1 1016 724 1 280 1017 1 1016 989 
		1 992 1017 1 1031 1098 1 1098 1049 1 1099 1032 
		1 1050 1099 1 1037 1100 1 1100 1098 1 1101 1038 
		1 1099 1101 1 1051 1102 1 1102 964 1 964 990 
		1 990 1051 1 966 1103 1 1103 1052 1 1052 991 
		1 991 966 1 1041 1104 1 1104 1100 1 1101 1104 
		1 1098 1102 1 1103 1099 1 1104 965 1 964 1100 
		1 1101 966 1 289 915 1 915 1105 1 1105 753 
		1 1106 915 1 288 1106 1 915 1107 1 1107 1105 
		1 1106 1108 1 1108 915 1 914 1107 1 1108 916 
		1 921 1109 1 1109 1107 1 1110 922 1 1108 1110 
		1 909 1055 1 1055 1111 1 1111 910 1 913 1065 
		1 1111 1112 1 1112 938 1 941 1066 1 1112 1113 
		1 1113 939 1 940 1073 1 1113 1071 1 1071 917 
		1 920 1072 1 1076 1047 1 1048 1077 1 1080 1044 
		1 1045 1081 1 1114 932 1 931 1115 1 1115 1114 
		1 934 1116 1 1116 1117 1 1117 933 1 1114 1109 
		1 1110 1116 1 1107 755 1 285 1108 1 1109 732 
		1 282 1110 1 1114 729 1 326 1116 1 1115 727 
		1 329 1117 1 937 330 1 730 931 1 933 328 
		1 339 342 1 719 721 1 335 338 1 723 725 
		1 332 333 1 1118 1119 1 1119 1120 1 1120 1121 
		1 1121 1118 1 1122 1123 1 1123 1124 1 1124 1125 
		1 1125 1122 1 1126 1124 1 1123 1127 1 1127 1126 
		1 1128 1129 1 1129 1130 1 1130 1131 1 1131 1128 
		1 1129 1132 1 1132 1133 1 1133 1130 1 1132 1122 
		1 1125 1133 1 1122 1134 1 1134 1135 1 1135 1123 
		1 1129 1136 1 1136 1137 1 1137 1132 1 1137 1134 
		1 1138 1139 1 1139 1140 1 1140 1141 1 1141 1138 
		1 1142 1143 1 1143 1144 1 1144 1145 1 1145 1142 
		1 1144 1146 1 1146 1147 1 1147 1145 1 1118 1147 
		1 1146 1119 1 1148 1126 1 1127 1149 1 1149 1148 
		1 1150 1151 1 1151 1152 1 1152 1153 1 1153 1150 
		1 1154 1155 1 1155 1151 1 1150 1154 1 1156 1157 
		1 1157 1155 1 1154 1156 1 1158 1159 1 1159 1157 
		1 1156 1158 1 1126 1152 1 1151 1124 1;
	setAttr ".ed[2324:2489]" 1148 1153 1 1155 1125 1 1130 1159 
		1 1158 1131 1 1133 1157 1 1146 1160 1 1160 1161 
		1 1161 1119 1 1162 1160 1 1144 1162 1 1163 1164 
		1 1164 1162 1 1162 1163 1 1139 1165 1 1165 1166 
		1 1166 1140 1 1135 1167 1 1167 1127 1 1128 1120 
		1 1120 1136 1 1168 1169 1 1169 1170 1 1170 1168 
		1 1171 1172 1 1172 1168 1 1168 1171 1 1172 1173 
		1 1173 1168 1 1174 1175 1 1175 1171 1 1171 1174 
		1 1147 1141 1 1118 1138 1 1140 1143 1 1142 1141 
		1 1166 1163 1 1163 1143 1 1176 1121 1 1121 1175 
		1 1174 1176 1 1167 1177 1 1177 1149 1 1178 1174 
		1 1174 1168 1 1168 1178 1 1139 1177 1 1167 1165 
		1 1161 1136 1 1168 1179 1 1179 1169 1 1173 1179 
		1 1170 1178 1 1128 1175 1 1131 1171 1 1158 1172 
		1 1156 1173 1 1154 1179 1 1150 1169 1 1153 1170 
		1 1148 1178 1 1149 1174 1 1177 1176 1 1180 1181 
		1 1181 1165 1 1167 1180 1 1182 1181 1 1181 374 
		1 374 1182 1 376 1183 1 1183 1184 1 1184 376 
		1 377 1185 1 1185 1183 1 1186 1187 1 1187 372 
		1 373 1186 1 371 1188 1 1188 1189 1 1189 377 
		1 1187 1188 1 1182 1166 1 1164 1184 1 1183 1162 
		1 1185 1160 1 1134 1187 1 1186 1135 1 1136 1189 
		1 1188 1137 1 374 1180 1 1186 1180 1 1190 377 
		1 1189 1190 1 1161 1190 1 1182 1191 1 1191 1163 
		1 375 1191 1 375 1184 1 1184 1191 1 1190 1185 
		1 1176 1138 1 1192 1193 0 1193 1194 1 1194 1192 
		1 1195 1196 1 1196 1197 0 1197 1195 1 1198 1199 
		1 1199 1200 1 1200 1193 1 1193 1198 0 1201 1199 
		1 1198 1196 0 1196 1201 1 1194 1202 1 1202 1203 
		1 1203 1194 1 1204 1205 1 1205 1195 1 1195 1204 
		1 1200 1202 1 1205 1201 1 1206 1207 1 1207 1202 
		1 1200 1206 1 1205 1208 1 1208 1209 1 1209 1201 
		1 1199 1210 1 1210 1206 1 1209 1210 1 1211 726 
		0 726 1207 1 1206 1211 1 1208 325 1 325 1212 
		0 1212 1209 1 1213 1211 0 1210 1213 1 1212 1213 
		0 1214 1215 1 1215 1216 1 1216 1214 1 1217 1218 
		1 1218 1219 1 1219 1217 1 1220 1214 1 1216 1220 
		1 1219 1221 1 1221 1217 1 1214 1222 1 1222 1223 
		1 1223 1215 1 1224 1219 1 1218 1225 1 1225 1224 
		1 1226 1227 1 1227 1228 1 1228 1229 1 1229 1226 
		1 1230 1231 1 1231 1232 1 1232 1233 1 1233 1230 
		1 1228 1234 1 1234 1229 1 1233 1235 1;
	setAttr ".ed[2490:2655]" 1235 1230 1 1223 1226 1 1229 1215 
		1 1232 1225 1 1218 1233 1 1234 1216 1 1217 1235 
		1 1227 1202 1 1207 1236 1 1236 1227 1 1205 1231 
		1 1231 1237 1 1237 1208 1 1226 1238 1 1238 1202 
		1 1205 1239 1 1239 1232 1 1238 1203 1 1204 1239 
		1 1240 1241 0 1241 1242 1 1242 1243 1 1243 1240 
		1 1244 1245 1 1245 1246 0 1246 1247 1 1247 1244 
		1 1242 1248 1 1248 1243 1 1249 1244 1 1247 1249 
		1 1250 1243 1 1248 1250 1 1247 1251 1 1251 1249 
		1 1252 1240 0 1250 1252 1 1246 1253 0 1253 1251 
		1 1254 1252 0 1250 1255 1 1255 1254 1 1253 1256 
		0 1256 1257 1 1257 1251 1 1248 1255 1 1257 1249 
		1 1258 1255 1 1248 1258 1 1257 1259 1 1259 1249 
		1 1260 1254 0 1258 1260 1 1256 1261 0 1261 1259 
		1 1262 1260 0 1258 1263 1 1263 1262 1 1261 1264 
		0 1264 1265 1 1265 1259 1 1248 1263 1 1265 1249 
		1 1242 1263 1 1265 1244 1 1241 1262 0 1264 1245 
		0 1266 1267 1 1267 716 1 963 1266 1 323 1268 
		1 1268 1266 1 1267 733 1 322 1268 1 1267 1236 
		1 1236 733 1 322 1237 1 1237 1268 1 1236 726 
		1 325 1237 1 1267 1228 1 1230 1268 1 1269 1228 
		1 1266 1269 1 1230 1269 1 1269 1270 1 1270 1234 
		1 1235 1270 1 1214 1271 1 1271 1222 1 1272 1219 
		1 1224 1272 1 1273 1220 1 1220 1274 1 1274 1275 
		1 1275 1273 1 1274 1221 1 1221 1276 1 1276 1275 
		1 1273 1271 1 1272 1276 1 1277 1278 1 1278 1279 
		1 1279 1280 1 1280 1277 1 1279 1281 1 1281 1282 
		1 1282 1280 1 1283 1284 1 1284 1278 1 1277 1283 
		1 1281 1285 1 1285 1286 1 1286 1282 1 1287 1277 
		1 1280 1288 1 1288 1287 1 1282 1289 1 1289 1288 
		1 1290 1283 1 1277 1290 1 1286 1291 1 1291 1282 
		1 1287 1290 1 1291 1289 1 1284 1292 1 1292 1279 
		1 1292 1285 1 1293 1294 1 1294 1295 1 1295 1296 
		1 1296 1293 1 1295 1297 1 1297 1298 1 1298 1296 
		1 1271 1294 1 1293 1222 1 1297 1272 1 1224 1298 
		1 1273 1294 1 1297 1276 1 1275 1295 1 1216 1299 
		1 1299 1274 1 1299 1217 1 1270 1299 1 1300 1301 
		1 1301 1287 1 1288 1300 1 1289 1302 1 1302 1300 
		1 1303 1223 1 1222 1303 1 1225 1304 1 1304 1224 
		1 1305 1284 1 1283 1306 1 1306 1305 1 1285 1307 
		1 1307 1308 1 1308 1286 1 1290 1306 1 1308 1291 
		1 1305 1292 1 1292 1307 1 1305 1293 1;
	setAttr ".ed[2656:2821]" 1296 1292 1 1298 1307 1 1305 1222 
		1 1224 1307 1 1309 1310 1 1310 1311 1 1311 1312 
		1 1312 1309 0 1313 1314 1 1314 1315 1 1315 1316 
		0 1316 1313 1 1317 1318 0 1318 1310 1 1309 1317 
		0 1314 1319 1 1319 1320 0 1320 1315 0 1321 1322 
		1 1322 1323 1 1323 1324 1 1324 1321 0 1325 1326 
		1 1326 1327 1 1327 1328 0 1328 1325 1 1323 1329 
		1 1329 1330 1 1330 1324 0 1331 1325 1 1328 1332 
		0 1332 1331 1 1329 1333 1 1333 1334 1 1334 1330 
		1 1335 1331 1 1332 1336 1 1336 1335 1 1337 1338 
		1 1338 1322 1 1321 1337 0 1326 1339 1 1339 1340 
		1 1340 1327 0 1333 1338 1 1337 1334 1 1339 1335 
		1 1336 1340 1 1337 1330 0 1332 1340 0 1341 1238 
		1 1238 1342 1 1342 1343 1 1343 1341 1 1344 1239 
		1 1239 1345 1 1345 1346 1 1346 1344 1 1342 1318 
		1 1318 1347 1 1347 1342 1 1348 1319 1 1319 1344 
		1 1344 1348 1 1347 1343 1 1346 1348 1 1347 1349 
		1 1349 1350 1 1350 1347 1 1351 1352 1 1352 1348 
		1 1348 1351 1 1318 1349 1 1352 1319 1 1353 1354 
		1 1354 1306 1 1290 1353 1 1308 1355 1 1355 1356 
		1 1356 1291 1 1329 1354 1 1353 1333 1 1355 1331 
		1 1335 1356 1 1287 1353 1 1356 1289 1 1287 1333 
		1 1335 1289 1 1357 1306 1 1354 1358 1 1358 1357 
		1 1308 1359 1 1359 1360 1 1360 1355 1 1357 1222 
		1 1224 1359 1 1354 1303 1 1303 1358 1 1304 1355 
		1 1360 1304 1 1223 1342 1 1344 1225 1 1301 1333 
		1 1335 1302 1 1323 1303 1 1304 1325 1 1301 1311 
		1 1311 1338 1 1339 1313 1 1313 1302 1 1310 1322 
		1 1326 1314 1 1310 1361 1 1361 1322 1 1326 1362 
		1 1362 1314 1 1361 1303 1 1304 1362 1 1342 1361 
		1 1362 1344 1 1363 1301 1 1300 1363 1 1302 1363 
		1 1363 1312 0 1316 1363 0 1364 1365 1 1365 1347 
		1 1350 1364 1 1348 1366 1 1366 1367 1 1367 1351 
		1 1365 1341 1 1345 1366 1 1368 1369 1 1369 1349 
		1 1318 1368 0 1352 1370 1 1370 1371 1 1371 1319 
		0 1369 1364 1 1367 1370 1 1368 1192 0 1192 1364 
		1 1367 1197 1 1197 1371 0 1194 1365 1 1366 1195 
		1 1203 1341 1 1345 1204 1 1372 1373 1 1373 1374 
		1 1374 1372 1 1374 1375 1 1375 1372 1 1374 1376 
		1 1376 1375 1 1373 1376 1 1377 1376 1 1373 1378 
		1 1378 1377 1 1379 1375 1 1377 1379 1 1380 1372 
		1 1379 1380 1 1380 1378 1 1381 1382 1;
	setAttr ".ed[2822:2987]" 1382 1378 1 1380 1381 1 1379 1383 
		1 1383 1381 1 1377 1384 1 1384 1383 1 1382 1384 
		1 1385 1384 1 1382 1386 1 1386 1385 0 1387 1383 
		1 1385 1387 1 1388 1381 1 1387 1388 1 1388 1386 
		0 992 1389 1 1389 1390 1 1390 1017 1 1389 1391 
		1 1391 1392 1 1392 1390 1 1389 1393 1 1393 1394 
		1 1394 1391 1 991 1393 1 1392 1395 1 1395 1396 
		1 1396 1390 1 1396 1014 1 1393 1397 1 1397 1394 
		1 1052 1397 1 1042 1397 1 1398 1399 1 1399 316 
		1 302 1398 1 1391 1399 1 1398 1392 1 1394 1400 
		1 1400 1399 1 1400 315 1 1397 1401 1 1401 1400 
		1 1042 1401 1 1401 314 1 1395 1402 1 1402 1396 
		1 1402 1015 1 1025 1403 1 1403 1401 1 1403 298 
		1 1398 1404 1 1404 1395 1 303 1404 1 1404 1405 
		1 1405 1402 1 299 1405 1 1405 1403 1 1403 1402 
		1 1402 1025 1 201 1012 1 201 1406 1 1406 1407 
		1 1407 1012 0 199 1406 1 1051 195 1 990 1408 
		1 1408 195 1 1408 193 1 1406 190 1 190 1016 
		1 1016 1407 0 189 1408 1 989 189 1 1409 1410 
		1 1410 1411 1 1411 1409 1 1410 1412 1 1412 1411 
		1 1413 1409 1 1411 1413 1 1412 1414 1 1414 1411 
		1 1415 1416 1 1416 1417 1 1417 1413 1 1413 1415 
		1 1418 1416 1 1415 1414 1 1414 1418 1 1411 1415 
		1 1419 1413 1 1417 1419 1 1414 1420 1 1420 1418 
		1 1419 1421 1 1421 1409 1 1422 1420 1 1412 1422 
		1 1419 1423 1 1423 1424 1 1424 1419 1 1425 1426 
		1 1426 1420 1 1420 1425 1 1424 1421 1 1422 1425 
		1 1417 1423 1 1426 1418 1 1427 1424 1 1423 1428 
		1 1428 1427 1 1425 1429 1 1429 1430 1 1430 1426 
		1 1427 1431 1 1431 1424 1 1432 1429 1 1425 1432 
		1 1431 1433 1 1433 1421 1 1422 1434 1 1434 1432 
		1 1435 1436 1 1436 1437 1 1437 1438 1 1438 1435 
		1 1439 1440 1 1440 1441 1 1441 1442 1 1442 1439 
		1 1443 1444 1 1444 1435 1 1438 1443 1 1441 1444 
		1 1443 1442 1 1444 1445 1 1445 1436 1 1440 1445 
		1 1445 1446 1 1446 1437 1 1439 1446 1 1447 1448 
		1 1448 1449 1 1449 1450 1 1450 1447 1 1451 1448 
		1 1447 1452 1 1452 1451 1 1448 1453 1 1453 1454 
		1 1454 1449 1 1455 1453 1 1451 1455 1 1450 1456 
		1 1456 1457 1 1457 1447 1 1457 1458 1 1458 1452 
		1 1454 1456 1 1458 1455 1 1454 1427 1 1428 1456 
		1 1429 1455 1 1458 1430 1 1428 1459 1;
	setAttr ".ed[2988:3153]" 1459 1457 1 1459 1430 1 1453 1460 
		1 1460 1427 1 1429 1460 1 1461 1462 1 1462 1463 
		1 1463 1461 1 1461 1464 1 1464 1462 1 1463 1465 
		1 1465 1461 1 1465 1464 1 1465 1466 1 1466 1467 
		1 1467 1464 1 1463 1468 1 1468 1466 1 1467 1469 
		1 1469 1462 1 1469 1468 1 1469 1433 1 1433 1470 
		1 1470 1468 1 1467 1421 1 1470 1409 1 1409 1466 
		1 1470 1471 1 1471 1410 1 1472 1412 1 1471 1472 
		1 1473 1471 1 1433 1473 1 1473 1434 1 1434 1472 
		1 1431 1474 1 1474 1473 1 1474 1432 1 1460 1474 
		1 1423 1475 1 1475 1459 1 1475 1426 1 1476 1477 
		1 1477 1478 0 1478 1479 1 1479 1476 0 1480 1481 
		1 1481 1482 0 1482 1483 1 1483 1480 1 1484 1479 
		1 1478 1484 1 1485 1484 1 1478 1485 0 1484 1486 
		1 1486 1479 0 1485 1487 0 1487 1484 0 1484 1488 
		0 1488 1486 0 1489 1490 0 1490 1491 0 1491 1489 
		1 1491 1492 0 1492 1493 0 1493 1491 1 1491 1494 
		1 1494 1489 0 1493 1495 0 1495 1491 1 1495 1494 
		1 1496 1494 0 1495 1497 0 1497 1496 1 1498 1483 
		1 1482 1498 1 1499 1498 1 1482 1499 0 1498 1500 
		1 1500 1483 0 1499 1501 0 1501 1498 0 1498 1502 
		0 1502 1500 0 1503 1481 0 1481 1504 1 1504 1503 
		1 1504 1505 1 1505 1506 1 1506 1504 1 1504 1507 
		0 1507 1503 0 1506 1508 0 1508 1504 0 1480 1509 
		1 1509 1510 1 1510 1481 1 1511 1483 1 1483 1512 
		0 1512 1511 0 1476 1513 1 1513 1511 1 1511 1477 
		0 1513 1480 1 1505 1514 1 1514 1515 1 1515 1516 
		1 1516 1505 1 1516 1517 1 1517 1505 1 1517 1506 
		1 1517 1518 0 1518 1506 0 1519 1520 0 1520 1517 
		0 1517 1519 1 1497 1521 0 1521 1519 0 1519 1497 
		1 1516 1519 1 1519 1522 1 1522 1496 1 1516 1522 
		1 1523 1524 0 1524 1525 1 1525 1526 1 1526 1523 
		1 1476 1523 0 1526 1513 1 1513 1527 1 1527 1509 
		1 1524 1385 1 1385 1525 1 1528 1525 1 1385 1528 
		0 1529 1525 1 1528 1529 1 1509 1530 1 1530 1510 
		1 1526 1531 1 1531 1527 1 1529 1531 1 1522 1532 
		1 1532 1533 1 1533 1496 0 1528 1534 1 1534 1529 
		1 1534 1535 1 1535 1533 0 1532 1534 1 1388 1534 
		1 1528 1388 0 1388 1535 1 1536 1535 0 1387 1536 
		1 1524 1536 0 1 1537 1 1537 2 1 4 1538 
		1 1538 5 1 1537 8 1 11 1538 1 1539 1540 
		0 1540 1541 1 1541 1539 1 1542 1543 1;
	setAttr ".ed[3154:3319]" 1543 1544 0 1544 1542 1 1545 1546 
		1 1546 1547 0 1547 1545 1 1548 1549 0 1549 1550 
		1 1550 1548 1 1540 1551 0 1551 1541 1 1542 1552 
		1 1552 1543 0 1545 1553 1 1553 1546 0 1549 1554 
		0 1554 1550 1 1551 1539 0 1544 1552 0 1547 1553 
		0 1554 1548 0 1555 1556 0 1556 1557 1 1557 1555 
		1 1558 1559 1 1559 1560 0 1560 1558 1 1561 1562 
		1 1562 1563 0 1563 1561 1 1564 1565 0 1565 1566 
		1 1566 1564 1 1567 1555 0 1557 1567 1 1560 1568 
		0 1568 1558 1 1563 1569 0 1569 1561 1 1570 1564 
		0 1566 1570 1 1556 1567 0 1568 1559 0 1569 1562 
		0 1565 1570 0 1571 1572 1 1572 1573 1 1573 1574 
		1 1574 1571 1 1575 1576 1 1576 1577 1 1577 1578 
		1 1578 1575 1 1573 1579 1 1579 1580 1 1580 1574 
		1 1581 1582 1 1582 1575 1 1578 1581 1 1583 1572 
		1 1571 1583 1 1576 1584 1 1584 1577 1 1579 1585 
		1 1585 1580 1 1586 1582 1 1581 1586 1 1583 1587 
		1 1587 1572 1 1576 1588 1 1588 1584 1 1579 1589 
		1 1589 1585 1 1586 1590 1 1590 1582 1 1587 1591 
		1 1591 1573 1 1575 1592 1 1592 1588 1 1591 1589 
		1 1590 1592 1 1574 1593 1 1593 1594 1 1594 1571 
		1 1595 1578 1 1577 1596 1 1596 1595 1 1580 1597 
		1 1597 1593 1 1595 1598 1 1598 1581 1 1599 1600 
		1 1600 0 1 0 1599 0 6 1601 1 1601 1602 
		1 1602 6 0 9 1603 1 1603 1604 1 1604 9 
		0 1605 1606 1 1606 10 1 10 1605 0 1607 1 
		1 1600 1607 1 5 1608 1 1608 1601 1 8 1609 
		1 1609 1603 1 1610 11 1 1606 1610 1 1607 1611 
		1 1611 1537 1 1538 1612 1 1612 1608 1 1611 1609 
		1 1610 1612 1 1594 1607 1 1600 1571 1 1608 1596 
		1 1577 1601 1 1609 1597 1 1580 1603 1 1598 1610 
		1 1606 1581 1 1613 1600 1 1599 1614 1 1614 1613 
		1 1601 1615 1 1615 1616 1 1616 1602 1 1603 1617 
		1 1617 1618 1 1618 1604 1 1619 1606 1 1605 1620 
		1 1620 1619 1 1613 1583 1 1584 1615 1 1585 1617 
		1 1619 1586 1 1593 1611 1 1612 1595 1 1599 1621 
		1 1621 1614 1 1622 1602 1 1616 1622 1 1623 1604 
		1 1618 1623 1 1605 1624 1 1624 1620 1 1625 1621 
		1 1599 1626 0 1626 1625 1 1622 1627 1 1627 1628 
		1 1628 1602 0 1623 1629 1 1629 1630 1 1630 1604 
		0 1631 1624 1 1605 1632 0 1632 1631 1 1633 1625 
		1 1626 1633 0 1627 1634 1 1634 1628 0;
	setAttr ".ed[3320:3485]" 1629 1635 1 1635 1630 0 1636 1631 
		1 1632 1636 0 1625 1637 1 1637 1621 1 1622 1638 
		1 1638 1627 1 1623 1639 1 1639 1629 1 1631 1640 
		1 1640 1624 1 1641 1621 1 1637 1642 1 1642 1641 
		1 1622 1643 1 1643 1644 1 1644 1638 1 1623 1645 
		1 1645 1646 1 1646 1639 1 1647 1624 1 1640 1648 
		1 1648 1647 1 1637 1649 1 1649 1642 1 1650 1638 
		1 1644 1650 1 1651 1639 1 1646 1651 1 1640 1652 
		1 1652 1648 1 1653 1613 1 1614 1653 1 1615 1654 
		1 1654 1616 1 1617 1655 1 1655 1618 1 1656 1619 
		1 1620 1656 1 1641 1653 1 1654 1643 1 1655 1645 
		1 1647 1656 1 1657 1642 1 1649 1658 1 1658 1657 
		1 1644 1659 1 1659 1660 1 1660 1650 1 1646 1661 
		1 1661 1662 1 1662 1651 1 1663 1648 1 1652 1664 
		1 1664 1663 1 1637 1665 1 1665 1658 1 1660 1666 
		1 1666 1638 1 1662 1667 1 1667 1639 1 1640 1668 
		1 1668 1664 1 1625 1669 1 1669 1665 1 1666 1670 
		1 1670 1627 1 1667 1671 1 1671 1629 1 1631 1672 
		1 1672 1668 1 1673 1674 1 1674 1675 1 1675 1673 
		1 1676 1677 1 1677 1678 1 1678 1676 1 1679 1680 
		1 1680 1681 1 1681 1679 1 1682 1683 1 1683 1684 
		1 1684 1682 1 1685 1674 1 1673 1685 1 1677 1686 
		1 1686 1678 1 1680 1687 1 1687 1681 1 1688 1683 
		1 1682 1688 1 1689 1674 1 1685 1689 1 1677 1690 
		1 1690 1686 1 1680 1691 1 1691 1687 1 1692 1683 
		1 1688 1692 1 1689 1675 1 1676 1690 1 1679 1691 
		1 1692 1684 1 1669 1675 1 1689 1693 1 1693 1669 
		1 1676 1670 1 1670 1694 1 1694 1690 1 1679 1671 
		1 1671 1695 1 1695 1691 1 1672 1684 1 1692 1696 
		1 1696 1672 1 1685 1633 1 1633 1693 0 1694 1634 
		0 1634 1686 1 1695 1635 0 1635 1687 1 1688 1636 
		1 1636 1696 0 1673 1625 1 1627 1678 1 1629 1681 
		1 1682 1631 1 1697 1669 1 1693 1698 0 1698 1697 
		1 1670 1699 1 1699 1700 1 1700 1694 0 1671 1697 
		1 1698 1695 0 1699 1672 1 1696 1700 0 1701 1665 
		1 1697 1701 1 1666 1702 1 1702 1699 1 1667 1701 
		1 1702 1668 1 1703 1658 1 1658 1701 1 1701 1703 
		1 1702 1660 1 1660 1704 1 1704 1702 1 1701 1662 
		1 1662 1703 1 1704 1664 1 1664 1702 1 1705 1657 
		1 1657 1703 1 1703 1705 1 1704 1659 1 1659 1706 
		1 1706 1704 1 1703 1661 1 1661 1705 1 1706 1663 
		1 1663 1704 1 1587 1707 1 1707 1708 1;
	setAttr ".ed[3486:3651]" 1708 1591 1 1709 1710 1 1710 1588 
		1 1592 1709 1 1708 1711 1 1711 1589 1 1590 1712 
		1 1712 1709 1 1713 1707 1 1587 1713 1 1710 1714 
		1 1714 1588 1 1711 1715 1 1715 1589 1 1716 1712 
		1 1590 1716 1 1713 1717 1 1717 1718 1 1718 1707 
		1 1719 1720 1 1720 1714 1 1710 1719 1 1721 1722 
		1 1722 1715 1 1711 1721 1 1716 1723 1 1723 1724 
		1 1724 1712 1 1718 1725 1 1725 1708 1 1709 1726 
		1 1726 1719 1 1725 1721 1 1724 1726 1 1727 1657 
		1 1705 1727 1 1659 1728 1 1728 1706 1 1661 1729 
		1 1729 1705 1 1730 1663 1 1706 1730 1 1705 1731 
		1 1731 1732 1 1732 1727 1 1733 1706 1 1728 1734 
		1 1734 1733 1 1729 1735 1 1735 1731 1 1733 1736 
		1 1736 1730 1 1718 1732 1 1731 1725 1 1734 1719 
		1 1726 1733 1 1735 1721 1 1724 1736 1 1732 1737 
		1 1737 1727 1 1738 1734 1 1728 1738 1 1739 1735 
		1 1729 1739 1 1736 1740 1 1740 1730 1 1732 1741 
		1 1741 1737 1 1742 1734 1 1738 1742 1 1743 1735 
		1 1739 1743 1 1736 1744 1 1744 1740 1 1718 1741 
		1 1742 1719 1 1743 1721 1 1724 1744 1 1717 1741 
		1 1742 1720 1 1743 1722 1 1723 1744 1 1741 1745 
		1 1745 1737 1 1746 1742 1 1738 1746 1 1747 1743 
		1 1739 1747 1 1744 1748 1 1748 1740 1 1713 1745 
		1 1746 1714 1 1747 1715 1 1716 1748 1 1583 1745 
		1 1746 1584 1 1747 1585 1 1586 1748 1 1653 1745 
		1 1746 1654 1 1747 1655 1 1656 1748 1 1653 1737 
		1 1738 1654 1 1739 1655 1 1656 1740 1 1641 1727 
		1 1728 1643 1 1729 1645 1 1647 1730 1 1749 1443 
		1 1438 1750 1 1750 1749 1 1749 1751 1 1751 1442 
		1 1437 1752 1 1752 1750 1 1753 1439 1 1751 1753 
		1 1446 1754 1 1754 1752 1 1753 1754 1 1755 1756 
		1 1756 1422 1 1412 1755 1 1757 1755 1 1472 1757 
		1 1756 1758 1 1758 1434 1 1758 1757 1 1759 1760 
		1 1760 1757 1 1758 1759 1 1761 1759 1 1756 1761 
		1 1760 1762 1 1762 1755 1 1762 1761 1 1763 1761 
		1 1762 1763 1 1760 1763 1 1763 1759 1 1416 1764 
		1 1764 1765 1 1765 1417 1 1764 1749 1 1750 1765 
		1 1764 1766 1 1766 1751 1 1418 1766 1 1752 1767 
		1 1767 1765 1 1767 1423 1 1426 1768 1 1768 1766 
		1 1768 1753 1 1754 1769 1 1769 1767 1 1769 1475 
		1 1769 1768 1 1770 1771 0 1771 1772 0 1772 1770 
		0 1773 1774 0 1774 1775 0 1775 1773 0;
	setAttr ".ed[3652:3765]" 1481 1776 1 1776 1504 1 1776 1505 
		1 1510 1777 1 1777 1776 1 1777 1514 1 1778 1779 
		1 1779 1780 1 1780 1530 1 1530 1778 1 1779 1781 
		1 1781 1515 1 1515 1780 1 1780 1777 1 1779 1529 
		1 1529 1781 1 1778 1529 1 1530 1782 1 1782 1783 
		1 1783 1778 1 1782 1527 1 1531 1783 1 1782 1509 
		1 1783 1529 1 1515 1784 1 1784 1516 1 1784 1522 
		1 1784 1785 1 1785 1532 1 1781 1785 1 1785 1529 
		1 1534 1785 1 1069 1113 1 1112 1061 1 1111 1057 
		1 747 512 1 236 309 1 306 236 1 911 913 
		1 908 910 1 908 917 1 911 920 1 918 923 
		1 919 929 1 930 928 1 926 924 1 1797 1818 
		1 1786 86 1 1786 1787 1 1787 1788 1 1789 152 
		0 1789 1790 1 1791 150 1 1790 1791 1 1791 1792 
		1 1793 1803 1 1794 1788 1 1793 1794 1 1795 1787 
		1 1794 1795 1 1796 1806 1 1795 1796 1 1797 1807 
		1 1796 1797 1 1797 1798 1 1798 1793 1 1800 1810 
		1 1799 1800 1 1801 1790 1 1800 1801 1 1801 1802 
		1 1803 80 1 1804 1794 1 1803 1804 1 1805 1795 
		1 1804 1805 1 1806 76 1 1805 1806 1 1807 83 
		1 1806 1807 1 1808 1798 1 1807 1808 1 1808 1803 
		1 1809 149 0 1810 147 1 1809 1810 1 1811 1801 
		1 1810 1811 1 1811 1812 1 1813 30 1 1814 13 
		0 1813 23 1 1815 16 1 1813 1815 1 1815 1814 
		1 1816 88 1 1788 1816 1 1817 1798 1 1816 1817 
		1 1818 90 1 1817 1818 1 1818 1786 1 1089 884 
		1 959 1090 1 949 1086 1 890 1085 1 947 1083 
		1 897 1082 1 1064 947 1 897 1062 1 888 1089 
		1 956 1090 1 1064 953 1 1062 892 1;
	setAttr -s 1963 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 0 1 2 3 
		mu 0 4 0 1 2 3 
		f 4 4 5 6 7 
		mu 0 4 1470 1471 1472 1473 
		f 4 8 9 10 -3 
		mu 0 4 2 1474 1475 3 
		f 4 11 12 -8 13 
		mu 0 4 1476 1477 1478 1479 
		f 4 14 15 16 17 
		mu 0 4 4 5 6 7 
		f 4 3746 3742 -15 19 
		mu 0 4 2792 2791 5 4 
		f 4 21 22 23 24 
		mu 0 4 10 11 12 13 
		f 4 25 26 27 -23 
		mu 0 4 11 14 15 12 
		f 4 -24 28 29 30 
		mu 0 4 13 12 16 17 
		f 4 -28 31 32 -29 
		mu 0 4 12 15 18 16 
		f 4 -30 33 34 35 
		mu 0 4 17 16 19 20 
		f 4 -33 36 37 -34 
		mu 0 4 16 18 21 19 
		f 4 38 39 40 -37 
		mu 0 4 18 22 23 21 
		f 4 41 -18 42 -40 
		mu 0 4 22 4 7 23 
		f 4 3743 -27 43 44 
		mu 0 4 2790 15 14 24 
		f 4 3745 -20 -42 -3742 
		mu 0 4 2790 2792 4 22 
		f 4 46 -43 47 48 
		mu 0 4 26 23 7 28 
		f 4 49 -41 -47 50 
		mu 0 4 29 21 23 26 
		f 4 51 -38 -50 52 
		mu 0 4 30 19 21 29 
		f 4 -48 -17 53 54 
		mu 0 4 28 7 6 31 
		f 4 55 56 57 58 
		mu 0 4 32 33 34 35 
		f 4 59 -59 60 61 
		mu 0 4 1498 32 35 1501 
		f 4 62 63 64 65 
		mu 0 4 36 37 1502 1503 
		f 4 -61 66 67 68 
		mu 0 4 1501 35 38 39 
		f 4 -68 69 -63 70 
		mu 0 4 40 41 1506 1507 
		f 4 -60 71 72 73 
		mu 0 4 32 1498 1510 1511 
		f 4 -65 74 -73 75 
		mu 0 4 1512 1513 1511 1510 
		f 4 76 77 78 -75 
		mu 0 4 1513 42 43 1511 
		f 4 -56 -74 -79 79 
		mu 0 4 33 32 1511 43 
		f 4 80 81 82 -70 
		mu 0 4 41 45 46 1520 
		f 4 -58 83 -81 -67 
		mu 0 4 35 34 48 38 
		f 4 -77 -64 -83 84 
		mu 0 4 1522 1523 37 1524 
		f 4 85 86 87 88 
		mu 0 4 49 1525 1526 50 
		f 4 89 90 91 -87 
		mu 0 4 1525 1528 1529 1526 
		f 4 -88 92 93 94 
		mu 0 4 50 1526 1532 51 
		f 4 -92 95 96 -93 
		mu 0 4 1526 1529 1535 1532 
		f 4 97 98 99 100 
		mu 0 4 52 53 54 55 
		f 4 101 102 103 -99 
		mu 0 4 53 56 57 54 
		f 4 104 105 -98 106 
		mu 0 4 58 1537 59 60 
		f 4 107 108 -102 -106 
		mu 0 4 1537 1539 61 59 
		f 4 -100 109 -86 110 
		mu 0 4 1540 1541 1542 1543 
		f 4 -104 111 -90 -110 
		mu 0 4 1544 1545 1546 1547 
		f 4 -94 112 -105 113 
		mu 0 4 51 1532 1537 58 
		f 4 -97 114 -108 -113 
		mu 0 4 1532 1535 1539 1537 
		f 4 115 116 117 118 
		mu 0 4 62 63 64 65 
		f 4 119 120 121 122 
		mu 0 4 66 67 68 69 
		f 4 123 124 125 -117 
		mu 0 4 63 70 71 64 
		f 4 126 -123 127 -125 
		mu 0 4 72 1558 1559 73 
		f 4 128 -119 129 130 
		mu 0 4 74 62 65 75 
		f 4 131 -131 132 -121 
		mu 0 4 1562 74 75 1563 
		f 4 3700 -3711 3713 -147 
		mu 0 4 2757 2758 2767 2768 
		f 4 3717 -144 3750 3749 
		mu 0 4 2772 2765 2793 2795 
		f 4 146 3715 3698 3753 
		mu 0 4 2757 2768 2770 2797 
		f 4 3716 -3750 3752 -3699 
		mu 0 4 2769 2771 2796 2798 
		f 4 3701 -3709 3711 3710 
		mu 0 4 2758 2759 2766 2767 
		f 4 143 3709 3708 3748 
		mu 0 4 2794 2764 2766 2759 
		f 4 3725 3724 -3710 3707 
		mu 0 4 2777 2779 2766 2764 
		f 4 -3725 3727 3726 -3712 
		mu 0 4 2766 2779 2780 2767 
		f 4 3733 3732 -3717 3714 
		mu 0 4 2782 2784 2771 2769 
		f 4 3731 -3715 -3716 3712 
		mu 0 4 2781 2783 2770 2768 
		f 4 -3733 3734 -3708 -3718 
		mu 0 4 2772 2785 2778 2765 
		f 4 -3727 3729 -3713 -3714 
		mu 0 4 2767 2780 2781 2768 
		f 4 147 148 149 -115 
		mu 0 4 1535 1583 1584 1539 
		f 4 150 151 152 -149 
		mu 0 4 1583 1587 1588 1584 
		f 4 153 154 155 -112 
		mu 0 4 1590 1591 1592 1593 
		f 4 156 157 158 -155 
		mu 0 4 1594 1595 1596 1597 
		f 4 -150 159 160 -109 
		mu 0 4 1539 1584 1600 61 
		f 4 -153 161 162 -160 
		mu 0 4 1584 1588 1603 1600 
		f 4 -161 163 -154 -103 
		mu 0 4 56 1605 1606 57 
		f 4 -163 164 -157 -164 
		mu 0 4 1607 1608 1609 1610 
		f 4 165 166 -148 -96 
		mu 0 4 1529 1612 1583 1535 
		f 4 167 168 -151 -167 
		mu 0 4 1612 1616 1587 1583 
		f 4 -156 169 -166 -91 
		mu 0 4 1528 1620 1612 1529 
		f 4 -159 170 -168 -170 
		mu 0 4 1620 1624 1616 1612 
		f 4 171 172 173 174 
		mu 0 4 83 84 85 86 
		f 4 175 -114 176 -175 
		mu 0 4 86 51 58 83 
		f 4 177 178 179 -111 
		mu 0 4 1629 87 88 1630 
		f 4 180 181 182 -179 
		mu 0 4 87 89 90 88 
		f 4 -172 183 184 185 
		mu 0 4 84 83 91 92 
		f 4 -177 -107 186 -184 
		mu 0 4 83 58 60 91 
		f 4 -180 187 -187 -101 
		mu 0 4 55 1634 93 52 
		f 4 -183 188 -185 -188 
		mu 0 4 1634 1636 94 93 
		f 4 189 190 -174 191 
		mu 0 4 95 96 86 85 
		f 4 192 -95 -176 -191 
		mu 0 4 96 50 51 86 
		f 4 -181 193 -190 194 
		mu 0 4 1640 1641 96 95 
		f 4 -178 -89 -193 -194 
		mu 0 4 1641 49 50 96 
		f 4 195 196 197 -195 
		mu 0 4 95 97 98 1640 
		f 4 198 -132 199 -197 
		mu 0 4 97 74 1562 98 
		f 4 200 201 -196 -192 
		mu 0 4 85 99 97 95 
		f 4 202 -129 -199 -202 
		mu 0 4 99 62 74 97 
		f 4 203 204 205 -189 
		mu 0 4 1636 100 101 94 
		f 4 206 -127 207 -205 
		mu 0 4 100 1558 72 101 
		f 4 208 -186 -206 209 
		mu 0 4 102 84 92 103 
		f 4 -208 -124 210 -210 
		mu 0 4 103 70 63 102 
		f 4 -198 211 -204 -182 
		mu 0 4 89 1651 1652 90 
		f 4 -200 -120 -207 -212 
		mu 0 4 1653 67 66 1654 
		f 4 -209 212 -201 -173 
		mu 0 4 84 102 99 85 
		f 4 -211 -116 -203 -213 
		mu 0 4 102 63 62 99 
		f 4 213 214 215 -171 
		mu 0 4 1624 1660 1661 1616 
		f 4 216 217 218 -215 
		mu 0 4 1663 104 105 1661 
		f 4 -216 219 220 -169 
		mu 0 4 1616 1661 1667 1587 
		f 4 -219 221 222 -220 
		mu 0 4 1661 105 106 1667 
		f 4 223 224 225 -165 
		mu 0 4 1671 1672 1673 1674 
		f 4 226 227 228 -225 
		mu 0 4 1675 107 108 1676 
		f 4 229 230 -224 -162 
		mu 0 4 1588 1678 1679 1603 
		f 4 231 232 -227 -231 
		mu 0 4 1678 109 110 1679 
		f 4 -214 -158 -226 233 
		mu 0 4 1683 1684 1685 1686 
		f 4 -217 -234 -229 234 
		mu 0 4 1687 1688 1689 1690 
		f 4 -221 235 -230 -152 
		mu 0 4 1587 1667 1678 1588 
		f 4 -223 236 -232 -236 
		mu 0 4 1667 106 109 1678 
		f 4 237 238 239 240 
		mu 0 4 111 112 113 114 
		f 4 241 -241 242 243 
		mu 0 4 115 111 114 116 
		f 4 244 245 246 247 
		mu 0 4 117 118 119 120 
		f 4 248 249 250 -246 
		mu 0 4 118 121 122 119 
		f 4 -240 251 -247 252 
		mu 0 4 114 113 120 119 
		f 4 -243 -253 -251 253 
		mu 0 4 116 114 119 122 
		f 4 3721 3720 3705 -263 
		mu 0 4 2774 2775 2761 2762 
		f 4 -3721 3722 265 3703 
		mu 0 4 2761 2775 2776 2760 
		f 4 259 3719 262 3706 
		mu 0 4 2763 2773 2774 2762 
		f 4 269 270 -262 271 
		mu 0 4 131 132 124 126 
		f 4 272 273 -259 274 
		mu 0 4 133 134 125 123 
		f 4 275 -275 -256 -271 
		mu 0 4 132 133 123 124 
		f 4 276 277 -276 278 
		mu 0 4 135 136 133 132 
		f 4 279 280 -273 -278 
		mu 0 4 136 137 134 133 
		f 4 281 -279 -270 282 
		mu 0 4 138 135 132 131 
		f 4 283 -254 284 285 
		mu 0 4 139 116 122 140 
		f 4 286 -286 287 288 
		mu 0 4 141 139 140 142 
		f 4 -285 -250 289 290 
		mu 0 4 140 122 121 143 
		f 4 -288 -291 291 292 
		mu 0 4 142 140 143 144 
		f 4 293 -244 -284 294 
		mu 0 4 145 115 116 139 
		f 4 295 -295 -287 296 
		mu 0 4 146 145 139 141 
		f 4 -282 297 -297 -300 
		mu 0 4 135 138 146 141 
		f 4 -280 -301 -293 298 
		mu 0 4 137 136 142 144 
		f 4 -277 299 -289 300 
		mu 0 4 136 135 141 142 
		f 4 -252 301 302 303 
		mu 0 4 120 113 147 148 
		f 4 -248 -304 304 305 
		mu 0 4 117 120 148 149 
		f 4 306 -302 -239 307 
		mu 0 4 150 147 113 112 
		f 4 308 309 310 311 
		mu 0 4 151 152 153 154 
		f 4 312 313 314 -309 
		mu 0 4 151 155 156 152 
		f 4 315 316 317 -314 
		mu 0 4 155 157 158 156 
		f 4 -311 318 319 320 
		mu 0 4 159 160 161 162 
		f 4 -320 321 322 323 
		mu 0 4 162 161 163 164 
		f 4 324 325 326 -323 
		mu 0 4 163 165 166 164 
		f 4 -326 327 -317 328 
		mu 0 4 166 165 158 157 
		f 4 329 330 331 332 
		mu 0 4 167 168 169 170 
		f 4 333 334 -330 335 
		mu 0 4 171 172 168 167 
		f 4 336 337 -334 338 
		mu 0 4 173 174 172 171 
		f 3 339 340 -337 
		mu 0 3 173 175 174 
		f 4 341 342 -340 343 
		mu 0 4 176 177 175 173 
		f 4 344 -332 345 346 
		mu 0 4 178 170 169 179 
		f 4 347 -347 348 349 
		mu 0 4 180 178 179 181 
		f 3 350 -350 351 
		mu 0 3 182 180 181 
		f 3 -342 -352 352 
		mu 0 3 183 182 181 
		f 4 353 -318 354 -345 
		mu 0 4 184 185 186 187 
		f 4 -355 -328 355 -333 
		mu 0 4 187 186 188 189 
		f 4 -356 -325 356 -336 
		mu 0 4 189 188 190 191 
		f 4 -357 -322 357 -339 
		mu 0 4 191 190 192 193 
		f 3 -358 -319 -344 
		mu 0 3 193 192 194 
		f 3 -310 358 -351 
		mu 0 3 195 196 197 
		f 4 -359 -315 -354 -348 
		mu 0 4 197 196 185 184 
		f 4 359 360 361 362 
		mu 0 4 198 199 200 201 
		f 4 363 364 365 366 
		mu 0 4 202 203 204 205 
		f 4 367 368 369 -365 
		mu 0 4 206 207 208 209 
		f 4 370 371 372 373 
		mu 0 4 210 211 212 213 
		f 4 374 375 376 -371 
		mu 0 4 210 214 215 211 
		f 3 -374 377 378 
		mu 0 3 216 217 218 
		f 3 -375 -379 379 
		mu 0 3 219 216 218 
		f 4 380 381 382 383 
		mu 0 4 220 221 222 223 
		f 4 384 385 -381 386 
		mu 0 4 224 225 221 220 
		f 4 -382 387 388 389 
		mu 0 4 226 227 228 229 
		f 4 390 -388 -386 391 
		mu 0 4 230 228 227 231 
		f 4 392 393 394 395 
		mu 0 4 232 233 234 235 
		f 3 396 397 -395 
		mu 0 3 234 236 235 
		f 4 398 399 -397 400 
		mu 0 4 237 238 239 240 
		f 4 401 -401 -394 402 
		mu 0 4 241 237 240 242 
		f 3 403 404 405 
		mu 0 3 243 244 245 
		f 4 406 407 408 409 
		mu 0 4 246 247 248 249 
		f 3 410 411 -409 
		mu 0 3 250 251 252 
		f 4 412 413 414 -404 
		mu 0 4 243 253 254 244 
		f 4 415 416 417 -414 
		mu 0 4 253 255 256 254 
		f 4 418 -417 419 -411 
		mu 0 4 250 256 255 251 
		f 4 420 -420 421 422 
		mu 0 4 257 251 255 258 
		f 3 423 -422 424 
		mu 0 3 259 260 261 
		f 4 425 -423 426 427 
		mu 0 4 262 257 258 263 
		f 4 -398 428 429 430 
		mu 0 4 235 236 264 265 
		f 4 -396 -431 431 432 
		mu 0 4 232 235 265 266 
		f 4 433 434 -432 435 
		mu 0 4 267 268 266 265 
		f 4 436 -436 -430 437 
		mu 0 4 269 267 265 264 
		f 4 -424 438 -434 439 
		mu 0 4 270 271 272 273 
		f 4 -437 440 -427 -440 
		mu 0 4 273 274 275 270 
		f 3 441 442 443 
		mu 0 3 276 277 278 
		f 3 -444 444 445 
		mu 0 3 276 278 279 
		f 3 446 447 448 
		mu 0 3 280 263 277 
		f 3 449 -449 450 
		mu 0 3 281 280 277 
		f 4 -435 -439 -425 451 
		mu 0 4 282 283 284 285 
		f 4 -433 -452 -416 452 
		mu 0 4 286 282 285 287 
		f 4 -429 -400 -443 453 
		mu 0 4 288 1743 1744 289 
		f 4 -438 -454 -448 -441 
		mu 0 4 274 288 289 275 
		f 4 454 -407 455 -405 
		mu 0 4 244 290 291 245 
		f 4 456 457 458 459 
		mu 0 4 292 293 294 295 
		f 3 -459 460 461 
		mu 0 3 295 294 296 
		f 4 462 463 464 465 
		mu 0 4 297 298 299 300 
		f 4 466 467 468 -457 
		mu 0 4 292 301 302 293 
		f 4 469 -466 470 471 
		mu 0 4 303 297 300 304 
		f 4 472 -472 473 -468 
		mu 0 4 301 303 304 302 
		f 4 474 475 476 -464 
		mu 0 4 298 305 306 299 
		f 4 -476 477 -462 478 
		mu 0 4 306 305 295 296 
		f 3 479 480 481 
		mu 0 3 307 308 309 
		f 3 482 -480 483 
		mu 0 3 310 308 307 
		f 3 484 485 486 
		mu 0 3 311 312 313 
		f 3 487 488 -485 
		mu 0 3 311 314 312 
		f 3 489 490 -489 
		mu 0 3 314 315 312 
		f 3 491 492 -490 
		mu 0 3 314 316 315 
		f 3 493 494 -492 
		mu 0 3 314 317 316 
		f 3 495 496 -495 
		mu 0 3 317 318 316 
		f 4 497 498 499 500 
		mu 0 4 319 320 321 322 
		f 4 501 502 -500 503 
		mu 0 4 323 324 322 321 
		f 4 504 -503 505 506 
		mu 0 4 325 322 324 326 
		f 3 -501 -505 507 
		mu 0 3 319 322 325 
		f 4 508 509 -502 510 
		mu 0 4 327 328 324 323 
		f 4 511 512 -507 513 
		mu 0 4 329 330 325 326 
		f 4 514 -514 515 516 
		mu 0 4 331 329 326 332 
		f 4 -516 -506 -510 517 
		mu 0 4 332 326 324 328 
		f 4 518 519 520 -498 
		mu 0 4 319 333 334 320 
		f 4 -408 521 -520 522 
		mu 0 4 335 336 334 333 
		f 3 523 -522 524 
		mu 0 3 337 338 290 
		f 4 525 -521 -524 526 
		mu 0 4 339 340 338 337 
		f 4 527 -527 528 529 
		mu 0 4 341 339 337 342 
		f 4 530 531 532 533 
		mu 0 4 343 344 345 346 
		f 4 534 -533 535 -528 
		mu 0 4 341 346 345 339 
		f 4 -499 -526 -536 536 
		mu 0 4 347 340 339 345 
		f 4 -504 -537 -532 537 
		mu 0 4 348 347 345 344 
		f 4 538 539 -511 -538 
		mu 0 4 344 349 350 348 
		f 4 540 541 -539 -531 
		mu 0 4 343 351 349 344 
		f 4 542 543 544 -515 
		mu 0 4 331 352 353 329 
		f 3 -513 545 546 
		mu 0 3 325 330 354 
		f 3 -547 547 548 
		mu 0 3 355 356 357 
		f 3 -548 549 550 
		mu 0 3 357 356 358 
		f 4 -550 -546 551 552 
		mu 0 4 359 354 330 360 
		f 4 553 -553 554 -535 
		mu 0 4 341 359 360 346 
		f 4 -545 555 -552 -512 
		mu 0 4 329 353 360 330 
		f 4 556 -534 -555 -556 
		mu 0 4 353 343 346 360 
		f 4 -544 557 -541 -557 
		mu 0 4 353 352 351 343 
		f 3 558 559 560 
		mu 0 3 361 362 363 
		f 3 561 -561 562 
		mu 0 3 364 361 363 
		f 4 563 -487 564 565 
		mu 0 4 365 311 313 366 
		f 3 566 567 568 
		mu 0 3 367 368 366 
		f 3 -569 569 570 
		mu 0 3 367 366 369 
		f 3 571 -566 -568 
		mu 0 3 368 365 366 
		f 4 572 573 574 -563 
		mu 0 4 363 370 371 364 
		f 4 -482 575 576 577 
		mu 0 4 307 309 372 373 
		f 4 578 579 -575 580 
		mu 0 4 374 375 364 371 
		f 4 -576 581 582 583 
		mu 0 4 372 309 376 377 
		f 4 584 585 -579 586 
		mu 0 4 378 379 375 374 
		f 4 -583 587 588 589 
		mu 0 4 377 376 380 381 
		f 4 -578 590 -573 591 
		mu 0 4 382 383 384 385 
		f 4 -560 592 -484 -592 
		mu 0 4 385 386 387 382 
		f 4 593 594 595 596 
		mu 0 4 388 389 390 391 
		f 4 597 598 599 -595 
		mu 0 4 389 392 393 390 
		f 4 600 601 602 -599 
		mu 0 4 394 395 396 397 
		f 4 603 604 605 -602 
		mu 0 4 395 398 399 396 
		f 4 606 607 608 -605 
		mu 0 4 398 400 401 399 
		f 4 609 610 611 -608 
		mu 0 4 400 402 403 401 
		f 4 612 -597 613 -611 
		mu 0 4 402 388 391 403 
		f 4 614 615 616 617 
		mu 0 4 404 405 406 407 
		f 4 618 619 -616 620 
		mu 0 4 408 409 406 405 
		f 4 621 622 -619 623 
		mu 0 4 410 411 409 408 
		f 4 624 625 -622 626 
		mu 0 4 412 413 414 415 
		f 4 627 628 -625 629 
		mu 0 4 416 417 413 412 
		f 4 630 631 -628 632 
		mu 0 4 418 419 417 416 
		f 4 -618 633 -631 634 
		mu 0 4 404 407 419 418 
		f 4 635 -617 636 637 
		mu 0 4 420 421 422 423 
		f 4 -614 638 -615 639 
		mu 0 4 403 391 405 404 
		f 4 -639 -596 640 -621 
		mu 0 4 405 391 390 408 
		f 4 -637 -620 641 642 
		mu 0 4 423 422 424 425 
		f 4 -641 -600 643 -624 
		mu 0 4 408 390 393 410 
		f 4 -642 -623 644 645 
		mu 0 4 425 424 426 427 
		f 4 -644 -603 646 -627 
		mu 0 4 415 397 396 412 
		f 4 647 648 -645 -626 
		mu 0 4 428 429 430 431 
		f 4 -647 -606 649 -630 
		mu 0 4 412 396 399 416 
		f 4 -648 -629 650 651 
		mu 0 4 429 428 432 433 
		f 4 -650 -609 652 -633 
		mu 0 4 416 399 401 418 
		f 4 -651 -632 653 654 
		mu 0 4 433 432 434 435 
		f 4 -653 -612 -640 -635 
		mu 0 4 418 401 403 404 
		f 4 -654 -634 -636 655 
		mu 0 4 435 434 421 420 
		f 4 656 -475 657 -607 
		mu 0 4 398 436 437 400 
		f 4 658 -478 -657 -604 
		mu 0 4 395 438 436 398 
		f 4 659 -460 -659 -601 
		mu 0 4 394 439 438 395 
		f 4 660 -467 -660 -598 
		mu 0 4 389 440 441 392 
		f 4 -473 -661 -594 661 
		mu 0 4 442 440 389 388 
		f 4 662 -470 -662 -613 
		mu 0 4 402 443 442 388 
		f 4 -658 -463 -663 -610 
		mu 0 4 400 437 443 402 
		f 4 663 664 -456 665 
		mu 0 4 444 445 245 291 
		f 3 666 667 -666 
		mu 0 3 246 446 447 
		f 3 -412 668 669 
		mu 0 3 252 251 448 
		f 3 670 671 672 
		mu 0 3 449 450 451 
		f 3 673 674 675 
		mu 0 3 452 447 453 
		f 4 676 677 678 679 
		mu 0 4 454 455 456 457 
		f 4 -680 680 681 682 
		mu 0 4 454 457 453 458 
		f 3 -367 683 684 
		mu 0 3 202 205 459 
		f 4 -685 685 686 687 
		mu 0 4 202 459 460 461 
		f 4 -687 688 -369 689 
		mu 0 4 461 460 462 463 
		f 3 -377 690 691 
		mu 0 3 464 465 466 
		f 4 692 693 694 695 
		mu 0 4 467 468 469 470 
		f 4 696 697 -694 698 
		mu 0 4 471 472 473 474 
		f 4 699 700 701 -697 
		mu 0 4 475 476 477 478 
		f 4 -701 702 -696 703 
		mu 0 4 477 476 467 470 
		f 4 704 -703 705 706 
		mu 0 4 479 467 476 480 
		f 4 707 708 -706 -700 
		mu 0 4 475 481 480 476 
		f 4 -699 709 710 -708 
		mu 0 4 471 474 482 483 
		f 4 -710 -693 -705 711 
		mu 0 4 484 468 467 479 
		f 4 712 713 714 715 
		mu 0 4 485 486 487 488 
		f 4 716 717 718 -713 
		mu 0 4 489 490 491 492 
		f 4 -718 719 720 721 
		mu 0 4 493 494 495 496 
		f 4 722 -715 723 -721 
		mu 0 4 495 488 487 496 
		f 4 -384 724 725 726 
		mu 0 4 220 223 497 498 
		f 4 -726 727 -389 728 
		mu 0 4 498 497 499 500 
		f 4 729 730 731 732 
		mu 0 4 501 502 503 281 
		f 4 733 734 735 -730 
		mu 0 4 504 505 506 507 
		f 4 736 737 738 -735 
		mu 0 4 508 460 509 510 
		f 3 739 740 -738 
		mu 0 3 460 280 509 
		f 4 -741 -450 -732 741 
		mu 0 4 509 280 281 503 
		f 4 742 743 744 745 
		mu 0 4 511 512 513 514 
		f 4 746 747 -745 748 
		mu 0 4 515 516 514 513 
		f 4 -747 749 750 751 
		mu 0 4 517 518 519 520 
		f 4 -751 752 -743 753 
		mu 0 4 521 522 512 511 
		f 4 754 755 756 757 
		mu 0 4 523 524 525 526 
		f 4 758 759 760 -756 
		mu 0 4 527 528 529 530 
		f 4 761 762 763 -760 
		mu 0 4 531 532 533 534 
		f 4 764 -758 765 -763 
		mu 0 4 532 523 526 533 
		f 4 766 767 768 -765 
		mu 0 4 532 535 536 523 
		f 4 -767 -762 769 770 
		mu 0 4 535 532 531 537 
		f 4 -770 -759 771 772 
		mu 0 4 538 528 527 539 
		f 4 -772 -755 -769 773 
		mu 0 4 540 524 523 536 
		f 4 774 775 776 777 
		mu 0 4 541 542 543 544 
		f 4 778 779 -777 780 
		mu 0 4 545 546 544 543 
		f 4 -779 781 782 783 
		mu 0 4 547 548 549 550 
		f 4 -775 784 -783 785 
		mu 0 4 542 541 551 552 
		f 4 786 787 788 789 
		mu 0 4 553 554 555 556 
		f 4 790 791 792 -788 
		mu 0 4 557 558 559 560 
		f 4 793 794 795 -792 
		mu 0 4 561 562 563 564 
		f 4 796 -790 797 -795 
		mu 0 4 562 553 556 563 
		f 4 798 -797 799 800 
		mu 0 4 565 553 562 566 
		f 4 -800 -794 801 802 
		mu 0 4 566 562 561 567 
		f 4 -802 -791 803 804 
		mu 0 4 568 558 557 569 
		f 4 805 -804 -787 -799 
		mu 0 4 565 570 554 553 
		f 4 806 807 -399 -672 
		mu 0 4 450 571 278 451 
		f 4 808 809 -704 810 
		mu 0 4 450 572 477 470 
		f 3 811 812 -665 
		mu 0 3 445 449 245 
		f 3 -402 813 -673 
		mu 0 3 451 243 449 
		f 3 814 -445 815 
		mu 0 3 573 279 278 
		f 3 816 -451 -442 
		mu 0 3 276 281 277 
		f 3 -740 817 -447 
		mu 0 3 280 460 263 
		f 3 -818 -686 818 
		mu 0 3 263 460 459 
		f 4 819 -667 -410 -670 
		mu 0 4 574 446 246 249 
		f 3 -406 -813 -814 
		mu 0 3 243 245 449 
		f 4 820 821 822 -698 
		mu 0 4 472 452 575 473 
		f 4 -682 -675 -668 823 
		mu 0 4 458 453 447 446 
		f 4 824 -683 825 826 
		mu 0 4 576 454 458 577 
		f 3 827 828 -681 
		mu 0 3 457 578 453 
		f 4 829 -827 830 -370 
		mu 0 4 208 576 577 209 
		f 4 -830 831 -734 832 
		mu 0 4 576 208 505 504 
		f 3 -737 -832 -689 
		mu 0 3 460 508 462 
		f 4 -825 -833 833 -677 
		mu 0 4 454 576 504 455 
		f 4 834 835 -803 836 
		mu 0 4 579 580 566 567 
		f 4 837 -774 838 839 
		mu 0 4 579 540 536 279 
		f 4 840 -385 841 -731 
		mu 0 4 502 225 224 503 
		f 4 -739 842 843 844 
		mu 0 4 510 509 581 582 
		f 4 -716 845 -690 846 
		mu 0 4 485 488 461 463 
		f 4 847 848 -366 -831 
		mu 0 4 583 262 205 204 
		f 4 849 850 -806 851 
		mu 0 4 584 585 570 565 
		f 4 852 853 -850 854 
		mu 0 4 586 587 585 584 
		f 4 855 -378 -373 856 
		mu 0 4 466 218 217 588 
		f 3 -372 -692 -857 
		mu 0 3 588 464 466 
		f 4 857 -380 858 859 
		mu 0 4 589 219 218 590 
		f 4 860 -860 861 -714 
		mu 0 4 486 589 590 487 
		f 4 862 863 864 -691 
		mu 0 4 465 591 592 466 
		f 4 865 -722 866 -864 
		mu 0 4 591 493 496 592 
		f 4 867 -863 -376 -858 
		mu 0 4 593 594 215 214 
		f 4 -719 -866 -868 -861 
		mu 0 4 492 491 594 593 
		f 4 868 -859 -856 -865 
		mu 0 4 592 590 218 466 
		f 4 -724 -862 -869 -867 
		mu 0 4 496 487 590 592 
		f 4 869 870 871 -383 
		mu 0 4 222 595 596 223 
		f 3 872 -871 873 
		mu 0 3 597 598 599 
		f 3 -874 874 875 
		mu 0 3 597 599 600 
		f 4 876 -728 877 -876 
		mu 0 4 601 499 497 602 
		f 4 -844 878 -729 -391 
		mu 0 4 582 581 498 500 
		f 4 -390 -877 -875 -870 
		mu 0 4 226 229 600 599 
		f 4 -736 -845 -392 -841 
		mu 0 4 507 506 230 231 
		f 4 -725 -872 -873 -878 
		mu 0 4 497 223 596 602 
		f 4 879 -387 -727 -879 
		mu 0 4 581 224 220 498 
		f 4 -843 -742 -842 -880 
		mu 0 4 581 509 503 224 
		f 4 880 -785 881 -789 
		mu 0 4 555 551 541 556 
		f 3 882 -776 -786 
		mu 0 3 549 603 604 
		f 3 -883 -782 -781 
		mu 0 3 603 549 548 
		f 4 -796 883 -780 884 
		mu 0 4 564 563 544 546 
		f 3 -840 885 -835 
		mu 0 3 579 279 580 
		f 3 886 887 -851 
		mu 0 3 605 456 569 
		f 4 -828 -679 -887 -854 
		mu 0 4 578 457 456 605 
		f 4 -793 -885 -784 -881 
		mu 0 4 560 559 547 550 
		f 4 -678 -837 -805 -888 
		mu 0 4 456 455 568 569 
		f 4 -798 -882 -778 -884 
		mu 0 4 563 556 541 544 
		f 4 888 -852 -801 -836 
		mu 0 4 580 584 565 566 
		f 3 889 -362 890 
		mu 0 3 606 201 200 
		f 3 891 892 -360 
		mu 0 3 198 607 199 
		f 4 -702 -810 893 -821 
		mu 0 4 478 477 572 608 
		f 4 894 -891 895 -709 
		mu 0 4 481 606 200 480 
		f 4 -363 -890 896 -892 
		mu 0 4 609 610 611 612 
		f 4 897 -712 898 -893 
		mu 0 4 607 484 479 199 
		f 4 -823 899 -811 -695 
		mu 0 4 469 613 450 470 
		f 4 900 901 -807 -900 
		mu 0 4 613 587 571 450 
		f 4 -664 -674 -894 902 
		mu 0 4 445 444 608 572 
		f 4 -711 -898 -897 -895 
		mu 0 4 483 482 612 611 
		f 4 -676 -829 -901 -822 
		mu 0 4 452 453 578 575 
		f 4 -899 -707 -896 -361 
		mu 0 4 199 479 480 200 
		f 4 903 -754 904 -757 
		mu 0 4 525 521 511 526 
		f 3 905 -744 -753 
		mu 0 3 519 614 615 
		f 3 -906 -750 -749 
		mu 0 3 614 519 518 
		f 4 -764 906 -748 907 
		mu 0 4 534 533 514 516 
		f 4 908 -771 909 -733 
		mu 0 4 281 535 537 501 
		f 4 -761 -908 -752 -904 
		mu 0 4 530 529 517 520 
		f 4 -834 -910 -773 -838 
		mu 0 4 455 504 538 539 
		f 4 -766 -905 -746 -907 
		mu 0 4 533 526 511 514 
		f 3 910 -902 -853 
		mu 0 3 586 571 587 
		f 4 911 -669 -421 912 
		mu 0 4 583 448 251 257 
		f 3 -913 -426 -848 
		mu 0 3 583 257 262 
		f 4 -720 913 -364 914 
		mu 0 4 495 494 203 202 
		f 4 -671 -812 -903 -809 
		mu 0 4 450 449 445 572 
		f 4 915 -816 -808 -911 
		mu 0 4 586 573 278 571 
		f 4 -889 916 -916 -855 
		mu 0 4 584 580 573 586 
		f 3 -886 -815 -917 
		mu 0 3 580 279 573 
		f 4 917 -446 -839 -768 
		mu 0 4 535 276 279 536 
		f 3 -817 -918 -909 
		mu 0 3 281 276 535 
		f 4 -915 -688 -846 -723 
		mu 0 4 495 202 461 488 
		f 4 -684 -849 -428 -819 
		mu 0 4 459 205 262 263 
		f 4 -820 -912 -826 -824 
		mu 0 4 446 574 577 458 
		f 4 -368 -914 -717 -847 
		mu 0 4 207 206 490 489 
		f 3 918 919 920 
		mu 0 3 616 617 618 
		f 3 921 922 923 
		mu 0 3 619 620 616 
		f 3 -923 924 -919 
		mu 0 3 616 620 617 
		f 4 925 -554 926 -924 
		mu 0 4 621 622 623 624 
		f 4 927 -551 -926 -921 
		mu 0 4 625 626 622 621 
		f 4 928 929 -925 930 
		mu 0 4 627 628 617 620 
		f 4 931 -3689 932 -931 
		mu 0 4 629 630 631 632 
		f 4 -927 -530 -932 -922 
		mu 0 4 633 634 630 629 
		f 4 933 934 935 -523 
		mu 0 4 635 636 637 638 
		f 4 936 937 -934 -519 
		mu 0 4 639 640 636 635 
		f 4 938 939 940 -935 
		mu 0 4 641 642 643 644 
		f 4 941 942 -939 -938 
		mu 0 4 645 646 642 641 
		f 4 943 -930 944 -940 
		mu 0 4 642 617 628 643 
		f 4 945 -920 -944 -943 
		mu 0 4 646 618 617 642 
		f 4 -946 946 -549 -928 
		mu 0 4 625 647 648 626 
		f 4 -415 947 -929 -933 
		mu 0 4 649 650 651 652 
		f 4 -418 948 -945 -948 
		mu 0 4 650 653 654 651 
		f 4 -419 -936 -941 -949 
		mu 0 4 653 655 656 654 
		f 4 -942 -937 -508 -947 
		mu 0 4 647 657 658 648 
		f 4 -413 -403 -393 -453 
		mu 0 4 287 241 242 286 
		f 4 949 950 951 952 
		mu 0 4 1763 1764 1765 1647 
		f 4 953 954 955 956 
		mu 0 4 1767 1768 1769 1770 
		f 4 957 958 959 960 
		mu 0 4 1771 1772 1773 1774 
		f 4 961 962 -958 963 
		mu 0 4 1775 1776 1772 1771 
		f 4 964 965 -962 966 
		mu 0 4 1779 1780 1776 1775 
		f 4 967 968 -954 969 
		mu 0 4 1783 1784 1768 1767 
		f 4 970 971 972 -970 
		mu 0 4 1787 1788 1789 1790 
		f 4 973 -963 974 -972 
		mu 0 4 1788 1792 1793 1789 
		f 4 975 976 -971 -957 
		mu 0 4 1795 1796 1788 1787 
		f 4 977 -959 -974 -977 
		mu 0 4 1796 1800 1792 1788 
		f 4 978 979 -976 -956 
		mu 0 4 1803 1804 1805 1806 
		f 4 980 -960 -978 -980 
		mu 0 4 1804 1808 1809 1805 
		f 4 981 982 983 984 
		mu 0 4 1811 1812 1813 1814 
		f 4 985 -965 986 -983 
		mu 0 4 1812 1816 1817 1813 
		f 4 -986 987 -975 -966 
		mu 0 4 1819 1820 1789 1793 
		f 4 988 989 990 -968 
		mu 0 4 1783 1824 1825 1784 
		f 4 991 -985 992 -990 
		mu 0 4 1824 1828 1829 1825 
		f 3 993 -988 994 
		mu 0 3 1831 1789 1820 
		f 3 -992 -995 -982 
		mu 0 3 1834 1835 1836 
		f 3 -989 -973 -994 
		mu 0 3 1831 1790 1789 
		f 4 995 996 997 998 
		mu 0 4 1840 1841 1842 1843 
		f 4 999 1000 1001 1002 
		mu 0 4 1844 1845 1846 1847 
		f 4 1003 1004 1005 1006 
		mu 0 4 1848 1849 1850 1851 
		f 4 1007 1008 1009 1010 
		mu 0 4 1852 1853 1854 1855 
		f 3 1011 1012 1013 
		mu 0 3 1856 1857 1858 
		f 4 1014 1015 1016 -1013 
		mu 0 4 1857 1860 1861 1858 
		f 3 1017 1018 1019 
		mu 0 3 1863 1864 1861 
		f 4 1020 1021 -1018 1022 
		mu 0 4 1866 1867 1864 1863 
		f 4 1023 1024 1025 -1022 
		mu 0 4 1867 1871 1872 1864 
		f 4 1026 1027 1028 1029 
		mu 0 4 1874 1875 1876 1877 
		f 4 -1011 1030 -998 1031 
		mu 0 4 1852 1855 1880 1881 
		f 3 1032 1033 1034 
		mu 0 3 1882 1850 1884 
		f 4 -1035 1035 1036 -1002 
		mu 0 4 1882 1884 1887 1888 
		f 3 1037 1038 -1024 
		mu 0 3 1867 1890 1871 
		f 4 1039 1040 1041 1042 
		mu 0 4 1892 1893 1894 1895 
		f 4 1043 1044 1045 1046 
		mu 0 4 1896 1897 1898 1899 
		f 4 1047 1048 1049 1050 
		mu 0 4 1900 1901 1902 1903 
		f 4 1051 -1046 1052 -1012 
		mu 0 4 1856 1905 1906 1857 
		f 4 -1050 1053 -1040 1054 
		mu 0 4 1908 1909 1893 1892 
		f 3 1055 1056 1057 
		mu 0 3 1912 1913 1914 
		f 3 1058 1059 -1058 
		mu 0 3 1914 1916 1912 
		f 4 1060 -1042 1061 -1048 
		mu 0 4 1918 1895 1894 1921 
		f 4 1062 1063 1064 1065 
		mu 0 4 1922 1923 1924 1925 ;
	setAttr ".fc[500:999]"
		f 4 1066 1067 1068 1069 
		mu 0 4 1926 1927 1928 1929 
		f 4 1070 1071 1072 1073 
		mu 0 4 1930 1931 1932 1933 
		f 4 -1028 1074 1075 1076 
		mu 0 4 1876 1875 1936 1937 
		f 4 1077 1078 -1039 -1068 
		mu 0 4 1938 1874 1871 1890 
		f 4 1079 1080 -1078 1081 
		mu 0 4 1942 1943 1874 1938 
		f 4 1082 -1066 1083 -1073 
		mu 0 4 1946 1922 1925 1949 
		f 4 1084 -1072 1085 1086 
		mu 0 4 1950 1932 1931 1953 
		f 4 1087 -1064 1088 -1071 
		mu 0 4 1954 1924 1923 1957 
		f 4 1089 -1075 1090 1091 
		mu 0 4 1958 1936 1875 1961 
		f 3 1092 -1083 -1085 
		mu 0 3 1962 1922 1946 
		f 3 -1089 1093 -1086 
		mu 0 3 1957 1923 1967 
		f 4 1094 1095 1096 -1023 
		mu 0 4 1863 1969 1970 1866 
		f 4 1097 1098 1099 1100 
		mu 0 4 1972 1973 1974 1975 
		f 4 1101 1102 1103 1104 
		mu 0 4 1976 1977 1978 1896 
		f 4 1105 1106 1107 1108 
		mu 0 4 1980 1981 1982 1983 
		f 4 1109 1110 1111 1112 
		mu 0 4 1928 1984 1976 1986 
		f 3 1113 -1102 -1111 
		mu 0 3 1984 1977 1976 
		f 3 1114 -1020 1115 
		mu 0 3 1988 1863 1861 
		f 4 -1108 1116 -1098 1117 
		mu 0 4 1991 1992 1973 1972 
		f 3 1118 1119 1120 
		mu 0 3 1995 1996 1997 
		f 3 1121 1122 -1121 
		mu 0 3 1997 1999 1995 
		f 4 1123 -1100 1124 -1106 
		mu 0 4 2001 1975 1974 2004 
		f 4 1125 1126 1127 1128 
		mu 0 4 2005 2006 2007 2008 
		f 4 1129 1130 1131 -1126 
		mu 0 4 2005 2010 2011 2006 
		f 4 1132 1133 1134 1135 
		mu 0 4 2013 2014 2015 2016 
		f 4 1136 1137 1138 1139 
		mu 0 4 2017 2018 2019 2020 
		f 4 1140 1141 1142 1143 
		mu 0 4 2021 2022 2023 2024 
		f 4 1144 1145 -1130 1146 
		mu 0 4 2025 2026 2010 2005 
		f 4 1147 -1133 1148 -1143 
		mu 0 4 2029 2014 2013 2032 
		f 3 -1148 -1142 1149 
		mu 0 3 2033 2023 2022 
		f 3 -1150 1150 -1134 
		mu 0 3 2033 2022 2038 
		f 4 1151 -1135 -1151 -1141 
		mu 0 4 2039 2016 2015 2042 
		f 4 1152 1153 1154 1155 
		mu 0 4 2043 2044 2045 2046 
		f 4 1156 1157 1158 -1154 
		mu 0 4 2044 2048 2049 2045 
		f 4 1159 1160 1161 1162 
		mu 0 4 2051 2052 2053 2054 
		f 4 1163 1164 1165 -1161 
		mu 0 4 2052 2056 2057 2053 
		f 4 1166 -1153 1167 -1162 
		mu 0 4 2059 2044 2043 2062 
		f 4 1168 -1157 -1167 -1166 
		mu 0 4 2063 2048 2044 2059 
		f 4 1169 -1155 1170 -1160 
		mu 0 4 2067 2046 2045 2070 
		f 4 -1171 -1159 1171 -1164 
		mu 0 4 2070 2045 2049 2074 
		f 3 1172 1173 1174 
		mu 0 3 2075 2048 2077 
		f 4 -1173 1175 1176 -1158 
		mu 0 4 2048 2075 2080 2049 
		f 4 -1021 1177 -1110 -1038 
		mu 0 4 1867 1866 2084 1890 
		f 4 -1097 1178 -1114 -1178 
		mu 0 4 1866 1970 2088 2084 
		f 4 1179 1180 -1006 -1033 
		mu 0 4 1882 2091 1851 1850 
		f 4 -996 1181 -1009 1182 
		mu 0 4 2094 2095 1854 1853 
		f 4 -1139 -1147 -1129 1183 
		mu 0 4 2098 2025 2005 2008 
		f 4 1184 -1127 1185 -1137 
		mu 0 4 2102 2007 2006 2105 
		f 4 -1116 -1016 1186 -1044 
		mu 0 4 1988 1861 1860 2109 
		f 4 -1104 1187 -1095 -1115 
		mu 0 4 1988 2111 1969 1863 
		f 4 1188 -1047 1189 1190 
		mu 0 4 2112 1896 1899 2115 
		f 3 1191 1192 1193 
		mu 0 3 2116 2117 2118 
		f 4 -1190 1194 -1193 1195 
		mu 0 4 2115 2119 2120 2121 
		f 4 1196 -1180 1197 -1196 
		mu 0 4 2121 2123 1846 2115 
		f 3 1198 -1069 -1113 
		mu 0 3 1986 1929 1928 
		f 4 -1198 -1001 1199 -1191 
		mu 0 4 2115 1846 1845 2112 
		f 4 -1000 1200 1201 1202 
		mu 0 4 1845 1844 2133 1929 
		f 4 1203 -1082 -1067 -1090 
		mu 0 4 2135 2136 1927 1926 
		f 3 1204 1205 1206 
		mu 0 3 2139 1877 2140 
		f 4 1207 1208 1209 -1003 
		mu 0 4 1847 2142 2143 1844 
		f 3 -1004 1210 1211 
		mu 0 3 1849 1848 2116 
		f 3 1212 -1212 1213 
		mu 0 3 2148 1849 2116 
		f 3 1214 1215 -1014 
		mu 0 3 1858 2152 1856 
		f 3 -1026 1216 -1019 
		mu 0 3 1864 1872 1861 
		f 3 1217 -1205 1218 
		mu 0 3 2157 1877 2139 
		f 3 1219 -1206 -1029 
		mu 0 3 1876 2140 1877 
		f 4 -1081 1220 -1091 -1027 
		mu 0 4 1874 1943 1961 1875 
		f 4 1221 1222 -1025 -1079 
		mu 0 4 1874 2157 1872 1871 
		f 4 1223 1224 1225 -1179 
		mu 0 4 1970 2172 2173 2088 
		f 4 -1103 -1226 1226 1227 
		mu 0 4 1978 1977 2177 2178 
		f 4 -1188 -1228 1228 1229 
		mu 0 4 1969 2111 2181 2182 
		f 4 -1096 -1230 1230 -1224 
		mu 0 4 1970 1969 2182 2172 
		f 4 1231 -1101 1232 -1231 
		mu 0 4 2182 1972 1975 2172 
		f 4 1233 -1118 -1232 -1229 
		mu 0 4 2181 1991 1972 2182 
		f 4 1234 -1109 -1234 -1227 
		mu 0 4 2177 1980 1983 2178 
		f 4 -1233 -1124 -1235 -1225 
		mu 0 4 2172 1975 2001 2173 
		f 4 -1122 1235 -1125 1236 
		mu 0 4 2203 2204 2004 1974 
		f 4 -1107 -1236 -1120 1237 
		mu 0 4 1982 1981 1997 1996 
		f 4 -1119 1238 -1117 -1238 
		mu 0 4 2211 2212 1973 1992 
		f 4 -1099 -1239 -1123 -1237 
		mu 0 4 1974 1973 2212 2203 
		f 4 -1187 1239 1240 1241 
		mu 0 4 2109 1860 2221 2222 
		f 4 -1045 -1242 1242 1243 
		mu 0 4 1898 1897 2225 2226 
		f 4 -1053 -1244 1244 1245 
		mu 0 4 1857 1906 2229 2230 
		f 4 1246 -1240 -1015 -1246 
		mu 0 4 2230 2221 1860 1857 
		f 4 1247 -1043 1248 -1247 
		mu 0 4 2230 1892 1895 2221 
		f 4 1249 -1055 -1248 -1245 
		mu 0 4 2229 1908 1892 2230 
		f 4 1250 -1051 -1250 -1243 
		mu 0 4 2225 1900 1903 2226 
		f 4 -1249 -1061 -1251 -1241 
		mu 0 4 2221 1895 1918 2222 
		f 4 -1062 1251 -1059 1252 
		mu 0 4 1921 1894 2253 2254 
		f 4 -1049 -1253 -1057 1253 
		mu 0 4 1902 1901 1914 1913 
		f 4 -1056 1254 -1054 -1254 
		mu 0 4 2259 2260 1893 1909 
		f 4 -1041 -1255 -1060 -1252 
		mu 0 4 1894 1893 2260 2253 
		f 4 -1128 1255 1256 1257 
		mu 0 4 2008 2007 1856 2148 
		f 3 1258 -1258 -1214 
		mu 0 3 2116 2008 2148 
		f 4 1259 -1184 -1259 -1194 
		mu 0 4 2118 2098 2008 2116 
		f 4 1260 -1140 -1260 -1195 
		mu 0 4 2119 2017 2020 2120 
		f 4 -1052 -1256 -1185 -1261 
		mu 0 4 1905 1856 2007 2102 
		f 4 -1146 1261 -1149 1262 
		mu 0 4 2010 2026 2032 2013 
		f 4 -1131 -1263 -1136 1263 
		mu 0 4 2011 2010 2013 2016 
		f 4 1264 -1156 1265 -1008 
		mu 0 4 1852 2043 2046 1853 
		f 4 -1168 -1265 -1032 1266 
		mu 0 4 2062 2043 1852 2301 
		f 4 1267 -1163 -1267 -997 
		mu 0 4 1841 2051 2054 1842 
		f 4 -1183 -1266 -1170 -1268 
		mu 0 4 2094 1853 2046 2067 
		f 4 -1084 1268 1269 1270 
		mu 0 4 1949 1925 2312 2313 
		f 4 1271 -1074 -1271 1272 
		mu 0 4 2314 1930 1933 2317 
		f 4 1273 1274 -1088 -1272 
		mu 0 4 2318 2319 1924 1954 
		f 4 -1065 -1275 1275 -1269 
		mu 0 4 1925 1924 2319 2312 
		f 4 -1221 1276 -1276 1277 
		mu 0 4 1961 1943 2312 2319 
		f 4 1278 -1092 -1278 -1274 
		mu 0 4 2318 1958 1961 2319 
		f 4 -1273 1279 -1204 -1279 
		mu 0 4 2314 2317 2136 2135 
		f 4 -1277 -1080 -1280 -1270 
		mu 0 4 2312 1943 1942 2313 
		f 3 -1174 -1169 1280 
		mu 0 3 2077 2048 2063 
		f 4 -1182 1281 -1192 1282 
		mu 0 4 1854 2095 2117 2116 
		f 4 -1010 -1283 -1211 1283 
		mu 0 4 1855 1854 2116 1848 
		f 3 -1284 -1007 1284 
		mu 0 3 1855 1848 1851 
		f 4 -1200 -1203 -1199 1285 
		mu 0 4 2112 1845 1929 1986 
		f 4 -1286 -1112 -1105 -1189 
		mu 0 4 2112 1986 1976 1896 
		f 3 -1070 -1202 -1076 
		mu 0 3 1926 1929 2133 
		f 3 -1218 -1222 -1030 
		mu 0 3 1877 2157 1874 
		f 3 -1208 -1037 1286 
		mu 0 3 2365 1888 1887 
		f 3 1287 -1201 -1210 
		mu 0 3 2143 2133 1844 
		f 4 -1288 1288 -1220 -1077 
		mu 0 4 1937 2372 2140 1876 
		f 4 1289 1290 1291 1292 
		mu 0 4 2375 2376 2377 2378 
		f 4 1293 1294 1295 -1291 
		mu 0 4 2376 2380 2381 2377 
		f 4 -1295 1296 1297 1298 
		mu 0 4 2381 2380 2385 2386 
		f 4 1299 1300 1301 -1298 
		mu 0 4 2385 2388 2389 2386 
		f 4 1302 1303 1304 -1301 
		mu 0 4 2391 2392 2393 2394 
		f 4 1305 1306 1307 -1304 
		mu 0 4 2392 2396 2397 2393 
		f 4 1308 -1293 1309 -1307 
		mu 0 4 2396 2375 2378 2397 
		f 4 1310 1311 1312 1313 
		mu 0 4 2403 2404 2405 2406 
		f 4 1314 1315 1316 1317 
		mu 0 4 2407 2408 2409 2410 
		f 4 1318 -1314 1319 1320 
		mu 0 4 2411 2403 2406 2414 
		f 4 1321 -1318 1322 1323 
		mu 0 4 2415 2407 2410 2418 
		f 4 1324 -1321 1325 1326 
		mu 0 4 2419 2411 2414 2422 
		f 4 1327 -1324 1328 1329 
		mu 0 4 2423 2415 2418 2426 
		f 4 1330 1331 1332 -1327 
		mu 0 4 2422 2428 2429 2419 
		f 4 1333 -1330 1334 1335 
		mu 0 4 2431 2423 2426 2434 
		f 4 1336 -1332 1337 1338 
		mu 0 4 2435 2436 2437 2438 
		f 4 1339 -1336 1340 1341 
		mu 0 4 2439 2440 2441 2442 
		f 4 1342 -1339 1343 1344 
		mu 0 4 2443 2435 2438 2446 
		f 4 1345 -1342 1346 1347 
		mu 0 4 2447 2439 2442 2450 
		f 4 -1316 1348 -1348 1349 
		mu 0 4 2409 2408 2447 2450 
		f 4 1350 -1345 1351 -1312 
		mu 0 4 2404 2443 2446 2405 
		f 4 -1315 1352 -1313 1353 
		mu 0 4 2408 2407 2461 2462 
		f 4 -1322 1354 -1320 -1353 
		mu 0 4 2407 2415 2465 2461 
		f 4 -1328 1355 -1326 -1355 
		mu 0 4 2415 2423 2469 2465 
		f 4 -1334 1356 -1331 -1356 
		mu 0 4 2423 2431 2473 2469 
		f 4 -1340 1357 -1338 -1357 
		mu 0 4 2440 2439 2477 2478 
		f 4 -1346 1358 -1344 -1358 
		mu 0 4 2439 2447 2481 2477 
		f 4 -1354 -1352 -1359 -1349 
		mu 0 4 2408 2462 2481 2447 
		f 4 1359 -1350 1360 -1294 
		mu 0 4 2376 2409 2450 2380 
		f 4 1361 -1317 -1360 -1290 
		mu 0 4 2375 2410 2409 2376 
		f 4 1362 -1323 -1362 -1309 
		mu 0 4 2396 2418 2410 2375 
		f 4 1363 -1329 -1363 -1306 
		mu 0 4 2392 2426 2418 2396 
		f 4 1364 -1335 -1364 -1303 
		mu 0 4 2391 2434 2426 2392 
		f 4 1365 -1341 -1365 -1300 
		mu 0 4 2385 2442 2441 2388 
		f 4 -1361 -1347 -1366 -1297 
		mu 0 4 2380 2450 2442 2385 
		f 4 1366 1367 1368 1369 
		mu 0 4 2515 2516 2517 2518 
		f 4 -1367 1370 1371 1372 
		mu 0 4 2516 2515 2521 2522 
		f 4 1373 -589 1374 1375 
		mu 0 4 2523 381 380 2524 
		f 4 1376 1377 1378 -585 
		mu 0 4 378 2525 2526 379 
		f 4 1379 -1376 1380 1381 
		mu 0 4 2527 2523 2524 2530 
		f 4 1382 1383 1384 -1378 
		mu 0 4 2525 2532 2533 2526 
		f 4 -1373 1385 -1382 1386 
		mu 0 4 2535 2536 2527 2530 
		f 4 1387 -1384 1388 -1371 
		mu 0 4 2539 2533 2532 2542 
		f 3 1389 1390 1391 
		mu 0 3 2543 2544 2545 
		f 3 1392 1393 1394 
		mu 0 3 2546 369 2544 
		f 3 -1395 -1390 1395 
		mu 0 3 2546 2544 2543 
		f 4 -1391 1396 1397 1398 
		mu 0 4 2545 2544 2553 2554 
		f 3 -1388 1399 1400 
		mu 0 3 2533 2539 2557 
		f 3 -1400 -1370 1401 
		mu 0 3 2557 2539 2560 
		f 4 1402 1403 -327 1404 
		mu 0 4 2561 2562 2563 2564 
		f 4 1405 1406 1407 -1403 
		mu 0 4 2561 2566 2567 2562 
		f 4 1408 1409 -1406 1410 
		mu 0 4 2569 2570 2566 2561 
		f 4 1411 -1407 1412 -993 
		mu 0 4 2573 2567 2566 2576 
		f 4 -1413 -1410 1413 1414 
		mu 0 4 2576 2566 2570 2579 
		f 3 -991 -1415 1415 
		mu 0 3 2580 2581 2582 
		f 3 -969 -1416 1416 
		mu 0 3 2583 2580 2582 
		f 3 -1417 -1414 1417 
		mu 0 3 2586 2579 2570 
		f 4 1418 -1411 -1405 -329 
		mu 0 4 2589 2569 2561 2564 
		f 4 1419 1420 -324 -1404 
		mu 0 4 2562 2593 2594 2563 
		f 4 1421 1422 -321 -1421 
		mu 0 4 2593 2597 2598 2594 
		f 4 -1422 1423 1424 1425 
		mu 0 4 2597 2593 2602 2603 
		f 4 -1425 1426 1427 1428 
		mu 0 4 2603 2602 2606 2607 
		f 4 1429 -1427 1430 -1412 
		mu 0 4 2608 2606 2602 2567 
		f 4 -1408 -1431 -1424 -1420 
		mu 0 4 2562 2567 2602 2593 
		f 4 -984 1431 1432 -1430 
		mu 0 4 2608 2191 2298 2606 
		f 4 -1433 1433 1434 -1428 
		mu 0 4 2606 2298 2622 2607 
		f 3 1435 1436 -1434 
		mu 0 3 2298 2625 2622 
		f 4 -981 1437 -1437 1438 
		mu 0 4 2627 2628 2629 2630 
		f 4 1439 -1435 -1438 -979 
		mu 0 4 2631 2632 2629 2628 
		f 4 -313 1440 1441 1442 
		mu 0 4 2635 2636 2637 2638 
		f 4 -316 -1443 1443 -1419 
		mu 0 4 2589 2635 2638 2569 
		f 4 -1444 1444 -1418 -1409 
		mu 0 4 2569 2638 2586 2570 
		f 4 -1441 -312 -1423 1445 
		mu 0 4 2637 2636 2649 2650 
		f 3 -955 1446 1447 
		mu 0 3 2631 2586 2653 
		f 4 -1445 -1442 1448 -1447 
		mu 0 4 2586 2638 2637 2653 
		f 4 -1426 1449 -1449 -1446 
		mu 0 4 2650 2659 2653 2637 
		f 4 -1448 -1450 -1429 -1440 
		mu 0 4 2631 2653 2659 2632 
		f 3 1450 1451 1452 
		mu 0 3 2666 2667 318 
		f 3 1453 -1451 1454 
		mu 0 3 2668 2667 2666 
		f 3 1455 1456 -1454 
		mu 0 3 2668 2672 2667 
		f 3 1457 1458 -1456 
		mu 0 3 2668 2675 2672 
		f 3 1459 -1458 1460 
		mu 0 3 2677 2675 2668 
		f 3 -1398 1461 -1460 
		mu 0 3 2677 2681 2675 
		f 3 -1368 1462 1463 
		mu 0 3 2683 2535 2685 
		f 3 -1387 1464 -1463 
		mu 0 3 2535 2530 2685 
		f 4 1465 1466 -1308 1467 
		mu 0 4 2689 2690 2691 2692 
		f 4 1468 1469 -1468 -1310 
		mu 0 4 2693 2694 2689 2692 
		f 4 1470 1471 1472 -1299 
		mu 0 4 2697 2698 2699 2700 
		f 4 -1473 1473 1474 -1296 
		mu 0 4 2700 2699 2702 2703 
		f 4 1475 1476 -1471 -1302 
		mu 0 4 2704 2705 2698 2697 
		f 4 -1475 1477 -1469 -1292 
		mu 0 4 2703 2702 2694 2693 
		f 3 -1467 1478 1479 
		mu 0 3 2691 2690 2711 
		f 4 -1305 -1480 1480 -1476 
		mu 0 4 2704 2691 2711 2705 
		f 4 1481 -1289 1482 1483 
		mu 0 4 2060 2140 2372 2625 
		f 4 1484 -1497 1485 1486 
		mu 0 4 2720 2721 2722 2723 
		f 4 -1486 -1499 1487 1488 
		mu 0 4 2723 2722 2726 2727 
		f 4 -950 1489 1490 1491 
		mu 0 4 1764 1763 2730 2731 
		f 4 -1491 1492 1493 1494 
		mu 0 4 2731 2730 2734 2735 
		f 3 -1216 1495 -1257 
		mu 0 3 1856 2152 2148 
		f 3 -1496 1496 -1213 
		mu 0 3 2148 2152 1849 
		f 3 -1017 -1217 1497 
		mu 0 3 1858 1861 1872 
		f 3 -1498 1498 -1215 
		mu 0 3 1858 1872 2152 
		f 4 1499 1500 -1485 1501 
		mu 0 4 2746 2747 2721 2720 
		f 4 -1500 1502 -1494 1503 
		mu 0 4 2747 2746 2752 2753 
		f 4 -1487 1504 1505 -1502 
		mu 0 4 2754 2755 2756 2719 
		f 4 -1506 1506 -1495 -1503 
		mu 0 4 2719 2756 1827 2751 
		f 4 -1492 -1507 1507 1508 
		mu 0 4 2750 1827 2756 1778 
		f 4 -1508 -1505 -1489 1509 
		mu 0 4 1778 2756 2755 1766 
		f 4 -1005 -1501 1510 -1034 
		mu 0 4 1850 1849 2729 1884 
		f 3 -1493 1511 -1504 
		mu 0 3 2733 2732 2206 
		f 4 -1511 -1512 1512 -1036 
		mu 0 4 1884 2729 2341 1887 
		f 4 1513 -1513 1514 -961 
		mu 0 4 2340 1887 2341 2512 
		f 4 1515 -964 -1515 -1490 
		mu 0 4 2503 2498 2512 2341 
		f 4 1516 -967 -1516 -953 
		mu 0 4 2139 2060 2498 2503 
		f 3 1517 -1287 -1514 
		mu 0 3 2340 2365 1887 
		f 4 -1209 -1518 -1439 -1483 
		mu 0 4 2143 2142 1575 2520 
		f 3 -1207 -1482 -1517 
		mu 0 3 2139 2140 2060 
		f 4 -952 1518 1519 -1219 
		mu 0 4 1647 1765 1781 1777 
		f 4 -1520 1520 -1488 -1223 
		mu 0 4 1777 1781 2584 2209 
		f 3 1521 -1510 -1521 
		mu 0 3 1668 1778 1766 
		f 4 -1509 -1522 -1519 -951 
		mu 0 4 2750 1778 1668 2179 
		f 4 -1138 1522 1523 -1145 
		mu 0 4 2019 2018 1243 1239 
		f 4 -1144 -1262 -1524 1524 
		mu 0 4 2021 2024 1239 1243 
		f 4 -1132 1525 -1523 -1186 
		mu 0 4 2006 2011 1879 2707 
		f 4 -1264 -1152 -1525 -1526 
		mu 0 4 2011 2016 2039 1879 
		f 3 -1172 1526 1527 
		mu 0 3 2074 2049 2113 
		f 3 -1527 -1177 1528 
		mu 0 3 2113 2049 2080 
		f 4 1529 -1281 -1165 -1528 
		mu 0 4 2615 2609 2057 2056 
		f 4 -1529 -1176 -1175 -1530 
		mu 0 4 2615 2210 2100 2609 
		f 4 1530 -1197 -1282 -999 
		mu 0 4 1843 2123 2121 1840 
		f 4 -1285 -1181 -1531 -1031 
		mu 0 4 1855 1851 2091 1880 
		f 4 -1087 -1094 -1063 -1093 
		mu 0 4 1962 1967 1923 1922 
		f 4 1531 1532 1533 1534 
		mu 0 4 659 660 661 662 
		f 3 1535 1536 1537 
		mu 0 3 663 664 665 
		f 3 1538 1539 1540 
		mu 0 3 666 667 668 
		f 4 1541 -1540 1542 1543 
		mu 0 4 669 668 667 670 
		f 3 -1325 1544 1545 
		mu 0 3 671 672 673 
		f 4 1546 1547 1548 1549 
		mu 0 4 674 673 675 676 
		f 3 -1545 1550 -1548 
		mu 0 3 673 672 675 
		f 4 -1333 1551 1552 -1551 
		mu 0 4 672 677 678 675 
		f 4 -1549 -1553 1553 1554 
		mu 0 4 676 675 678 679 
		f 3 1555 1556 1557 
		mu 0 3 680 681 682 
		f 4 1558 1559 1560 -1542 
		mu 0 4 669 683 684 668 
		f 3 -1561 1561 -1541 
		mu 0 3 668 684 666 
		f 4 1562 1563 1564 1565 
		mu 0 4 685 682 686 687 
		f 4 1566 -1564 -1557 -1337 
		mu 0 4 688 686 682 681 
		f 4 1567 1568 1569 1570 
		mu 0 4 689 690 691 692 
		f 4 -1570 1571 -1560 1572 
		mu 0 4 692 691 684 683 
		f 4 -1565 1573 -1568 1574 
		mu 0 4 687 686 690 689 
		f 4 -1543 1575 1576 1577 
		mu 0 4 670 667 693 694 
		f 4 -1577 1578 -1547 1579 
		mu 0 4 694 693 673 674 
		f 4 -1554 -1586 1580 1581 
		mu 0 4 679 678 695 696 
		f 4 -1351 1582 -1569 1583 
		mu 0 4 697 698 691 690 
		f 4 -1562 -1572 -1583 -1311 
		mu 0 4 666 684 691 698 
		f 4 -1567 -1343 -1584 -1574 
		mu 0 4 686 688 697 690 
		f 4 -1319 1584 -1576 -1539 
		mu 0 4 666 671 693 667 
		f 3 -1546 -1579 -1585 
		mu 0 3 671 673 693 
		f 3 -1552 -1556 1585 
		mu 0 3 678 677 695 
		f 4 -1563 1586 -1581 -1558 
		mu 0 4 682 685 699 680 
		f 4 -1532 1587 1588 1589 
		mu 0 4 700 701 702 703 
		f 4 -1636 -1590 1590 1591 
		mu 0 4 704 700 703 705 
		f 4 1592 -1592 1593 1594 
		mu 0 4 706 704 705 707 
		f 4 1595 -1595 1596 1597 
		mu 0 4 708 706 707 709 
		f 4 1598 -1598 1599 1600 
		mu 0 4 710 708 709 711 
		f 4 1601 -1601 1602 1603 
		mu 0 4 712 710 711 713 
		f 4 1604 -1604 1605 1606 
		mu 0 4 714 712 713 715 
		f 4 1607 -1607 1608 1609 
		mu 0 4 716 714 715 717 
		f 4 1610 -1610 1611 1612 
		mu 0 4 718 716 717 719 
		f 4 1613 -1613 1614 1615 
		mu 0 4 720 718 719 721 
		f 4 1616 1617 -1616 1618 
		mu 0 4 722 723 720 721 
		f 3 -1596 1619 1620 
		mu 0 3 724 725 664 
		f 3 1621 -1605 1622 
		mu 0 3 664 726 727 
		f 3 1623 -1602 -1622 
		mu 0 3 664 728 726 
		f 4 1624 -1559 1625 1626 
		mu 0 4 722 729 730 731 
		f 4 1627 -1587 1628 -1589 
		mu 0 4 702 732 733 703 
		f 4 1629 1630 1631 1632 
		mu 0 4 734 735 736 737 
		f 3 -1621 -1536 -1593 
		mu 0 3 724 664 663 
		f 4 1633 -1591 -1629 1634 
		mu 0 4 738 705 703 733 
		f 4 1635 1636 -1618 -1533 
		mu 0 4 660 663 739 661 
		f 4 1637 1638 -1632 -1555 
		mu 0 4 740 741 737 736 
		f 4 1639 1640 -1639 1641 
		mu 0 4 742 743 737 741 
		f 4 1642 -1680 -1535 1643 
		mu 0 4 744 745 659 662 
		f 4 -1643 1644 1645 -1640 
		mu 0 4 745 744 746 747 
		f 3 -1538 -1614 -1637 
		mu 0 3 663 665 739 
		f 3 1646 -1623 -1608 
		mu 0 3 748 664 727 
		f 3 -1537 -1647 -1611 
		mu 0 3 665 664 748 
		f 3 -1620 -1599 -1624 
		mu 0 3 664 725 728 
		f 4 1647 -1625 -1619 1648 
		mu 0 4 749 729 722 721 
		f 4 -1635 -1566 1649 1650 
		mu 0 4 738 733 750 751 
		f 4 1651 -1638 -1582 -1628 
		mu 0 4 752 741 740 753 
		f 3 -1631 -1580 -1550 
		mu 0 3 736 735 754 
		f 4 -1630 1652 1653 -1578 
		mu 0 4 735 734 755 756 
		f 4 1654 -1626 -1544 -1654 
		mu 0 4 755 731 730 756 
		f 4 1655 1656 1657 1658 
		mu 0 4 757 758 759 760 
		f 4 1659 1660 -1656 1661 
		mu 0 4 761 762 758 757 
		f 4 -1612 1662 -1660 1663 
		mu 0 4 719 717 762 761 
		f 4 -1658 1664 1665 1666 
		mu 0 4 760 759 763 764 
		f 4 -1597 1667 1668 1669 
		mu 0 4 709 707 765 766 
		f 4 -1666 1670 -1669 1671 
		mu 0 4 764 763 766 765 
		f 4 -1609 1672 -1661 -1663 
		mu 0 4 717 715 758 762 
		f 4 -1606 1673 -1657 -1673 
		mu 0 4 715 713 759 758 
		f 4 -1603 1674 -1665 -1674 
		mu 0 4 713 711 763 759 
		f 4 -1600 -1670 -1671 -1675 
		mu 0 4 711 709 766 763 
		f 4 -1594 -1634 1675 -1668 
		mu 0 4 707 705 738 765 
		f 4 -1655 1676 -1644 1677 
		mu 0 4 731 755 767 768 
		f 4 -1645 -1677 -1653 1678 
		mu 0 4 769 767 755 734 
		f 4 -1646 -1679 -1633 -1641 
		mu 0 4 743 769 734 737 
		f 4 1679 -1642 -1652 -1588 
		mu 0 4 770 742 741 752 
		f 4 1680 -1571 1681 1682 
		mu 0 4 771 772 773 774 
		f 4 -1682 -1573 -1648 1683 
		mu 0 4 774 773 729 749 
		f 4 -1650 -1575 -1681 1684 
		mu 0 4 751 750 772 771 
		f 4 1685 -1659 1686 -1683 
		mu 0 4 774 757 760 771 
		f 4 1687 -1662 -1686 -1684 
		mu 0 4 749 761 757 774 
		f 4 -1615 -1664 -1688 -1649 
		mu 0 4 721 719 761 749 
		f 4 -1676 -1651 1688 -1672 
		mu 0 4 765 738 751 764 
		f 4 -1687 -1667 -1689 -1685 
		mu 0 4 771 760 764 751 
		f 4 -1534 -1617 -1627 -1678 
		mu 0 4 768 723 722 731 
		f 4 1689 1690 1691 1692 
		mu 0 4 775 776 777 778 
		f 4 1693 1694 1695 -1691 
		mu 0 4 776 779 780 777 
		f 4 1696 1697 1698 -1693 
		mu 0 4 778 781 782 775 
		f 4 1699 1700 1701 -1695 
		mu 0 4 779 783 784 780 
		f 4 1702 1703 1704 -1701 
		mu 0 4 783 785 786 784 
		f 4 1705 1706 1707 -1704 
		mu 0 4 785 787 788 786 
		f 4 -1698 1708 -1707 1709 
		mu 0 4 789 790 788 787 
		f 4 1710 1711 1712 -1705 
		mu 0 4 791 792 793 794 
		f 4 -1708 1713 1714 -1711 
		mu 0 4 791 795 796 792 
		f 3 1715 -1696 1716 
		mu 0 3 797 777 780 
		f 3 -1692 -1716 1717 
		mu 0 3 778 777 797 
		f 3 -1717 -1702 1718 
		mu 0 3 797 780 784 
		f 4 1719 1720 1721 -1697 
		mu 0 4 798 799 800 801 
		f 4 -1722 1722 -1714 -1709 
		mu 0 4 802 803 796 795 
		f 4 -1718 1723 1724 -1720 
		mu 0 4 798 804 805 799 
		f 4 -1713 1725 -1724 -1719 
		mu 0 4 794 793 805 804 
		f 4 -1470 1726 1727 1728 
		mu 0 4 806 807 808 809 
		f 3 1729 -1728 1730 
		mu 0 3 793 809 808 
		f 4 -1712 1731 1732 -1730 
		mu 0 4 793 792 810 809 
		f 4 1733 -1466 -1729 -1733 
		mu 0 4 810 811 806 809 
		f 4 1734 1735 1736 -1474 
		mu 0 4 812 813 814 815 
		f 4 -1478 -1737 1737 -1727 
		mu 0 4 807 815 814 808 
		f 4 -1477 1738 1739 1740 
		mu 0 4 816 817 818 819 
		f 4 -1472 -1741 1741 -1735 
		mu 0 4 812 816 819 813 
		f 4 1742 1743 -1479 -1734 
		mu 0 4 810 820 821 811 
		f 4 -1481 -1744 1744 -1739 
		mu 0 4 822 821 820 823 
		f 4 -1715 1745 -1743 -1732 
		mu 0 4 792 796 820 810 
		f 4 -1723 1746 -1745 -1746 
		mu 0 4 796 803 823 820 
		f 4 1747 -1740 -1747 -1721 
		mu 0 4 799 819 818 800 
		f 4 1748 -1742 -1748 -1725 
		mu 0 4 805 813 819 799 
		f 4 1749 -1736 -1749 -1726 
		mu 0 4 793 814 813 805 
		f 3 -1731 -1738 -1750 
		mu 0 3 793 808 814 
		f 4 1750 1751 1752 1753 
		mu 0 4 824 825 826 827 
		f 4 -1753 1754 -1690 1755 
		mu 0 4 827 826 776 775 
		f 4 1756 1757 1758 -1752 
		mu 0 4 825 828 829 826 
		f 4 -1759 1759 -1694 -1755 
		mu 0 4 826 829 779 776 
		f 4 1760 -1754 1761 1762 
		mu 0 4 830 824 827 831 
		f 4 -1762 -1756 -1699 1763 
		mu 0 4 831 827 775 782 
		f 4 1764 1765 1766 -1758 
		mu 0 4 828 832 833 829 
		f 4 -1767 1767 -1700 -1760 
		mu 0 4 829 833 783 779 
		f 4 1768 1769 1770 -1766 
		mu 0 4 832 834 835 833 
		f 4 -1771 1771 -1703 -1768 
		mu 0 4 833 835 785 783 
		f 4 -1710 1772 1773 -1764 
		mu 0 4 789 787 836 837 
		f 4 -1774 1774 1775 -1763 
		mu 0 4 837 836 838 839 
		f 4 1776 -1775 1777 -1770 
		mu 0 4 834 838 836 835 
		f 4 -1778 -1773 -1706 -1772 
		mu 0 4 835 836 787 785 
		f 4 1778 1779 1780 1781 
		mu 0 4 840 841 842 843 
		f 4 1782 -1779 1783 1784 
		mu 0 4 1530 841 840 1527 
		f 4 -1780 1785 1786 1787 
		mu 0 4 842 841 844 845 
		f 4 -1786 -1783 1788 1789 
		mu 0 4 844 841 1530 1508 
		f 3 1790 3691 1793 
		mu 0 3 846 847 849 
		f 3 1794 3690 1797 
		mu 0 3 2110 1947 2444 
		f 4 1798 1799 1800 -1792 
		mu 0 4 847 850 851 848 
		f 4 1801 1802 -1795 -1801 
		mu 0 4 851 2342 1947 2110 
		f 3 3692 1804 1805 
		mu 0 3 847 852 853 
		f 3 3693 1807 -1796 
		mu 0 3 1947 2090 2458 
		f 4 -1806 1809 1810 -1799 
		mu 0 4 847 853 854 850 
		f 4 1811 -1809 -1803 1812 
		mu 0 4 2639 1762 1947 2342 
		f 3 3697 1814 1815 
		mu 0 3 858 856 857 
		f 3 3696 1818 1819 
		mu 0 3 2366 2194 2367 
		f 4 -1816 1821 1822 1823 
		mu 0 4 858 857 859 860 
		f 4 1824 -1821 1825 1826 
		mu 0 4 2224 2343 2366 2243 
		f 3 3694 -1817 1828 
		mu 0 3 853 855 858 
		f 3 3695 1829 -1807 
		mu 0 3 1762 2367 2090 
		f 4 -1829 -1824 1831 -1810 
		mu 0 4 853 858 860 854 
		f 4 -1826 -1831 -1812 1832 
		mu 0 4 2243 2366 1762 2639 
		f 4 1833 1834 1835 -1815 
		mu 0 4 856 861 862 857 
		f 4 -1835 1836 -1818 1837 
		mu 0 4 862 861 2000 1998 
		f 4 -1836 1838 1839 -1822 
		mu 0 4 2208 2207 863 859 
		f 4 -1839 -1838 -1825 1840 
		mu 0 4 863 2197 2196 2198 
		f 4 1841 1842 -1804 1843 
		mu 0 4 864 865 852 846 
		f 4 1844 1845 1846 -1808 
		mu 0 4 2090 2176 2175 2458 
		f 3 1847 -1844 -1794 
		mu 0 3 849 864 846 
		f 3 -1847 1848 -1797 
		mu 0 3 2458 2175 2444 
		f 4 1849 1850 1851 1852 
		mu 0 4 2712 2305 2302 2304 
		f 4 1853 -1853 1854 1855 
		mu 0 4 2303 2712 2304 2258 
		f 4 1856 1857 -1855 1858 
		mu 0 4 2276 2715 2258 2304 
		f 4 1859 -1859 -1852 1860 
		mu 0 4 2034 2276 2304 2302 
		f 4 1861 1862 -1850 1863 
		mu 0 4 1990 2167 2305 2712 
		f 4 1864 -1864 -1854 1865 
		mu 0 4 1582 1990 2712 2303 
		f 4 1866 1867 -1862 1868 
		mu 0 4 2278 2125 2167 1990 
		f 4 1869 -1869 -1865 1870 
		mu 0 4 2124 2278 1990 1582 
		f 4 -1860 1871 1872 1873 
		mu 0 4 2369 2368 2144 2141 
		f 4 -1857 -1874 1874 1875 
		mu 0 4 2355 2369 2141 2131 
		f 4 1876 1877 -1867 1878 
		mu 0 4 2130 2129 2125 2278 
		f 4 1879 -1879 -1870 1880 
		mu 0 4 2359 2130 2278 2124 
		f 4 -1873 1881 -1877 1882 
		mu 0 4 2141 2144 2129 2130 
		f 4 -1875 -1883 -1880 1883 
		mu 0 4 2131 2141 2130 2359 
		f 4 1884 1885 1886 1887 
		mu 0 4 866 867 868 869 
		f 4 -1888 1888 1889 1890 
		mu 0 4 866 869 2316 2315 
		f 3 -1885 1891 -1369 
		mu 0 3 867 866 870 
		f 3 -1891 -593 1892 
		mu 0 3 866 2315 1951 
		f 3 1893 1894 1895 
		mu 0 3 2665 2651 2634 
		f 4 1896 1897 1898 -1894 
		mu 0 4 2665 2652 2641 2651 
		f 4 1899 1900 1901 -1898 
		mu 0 4 2652 2663 2662 2641 
		f 4 1902 1903 1904 -1901 
		mu 0 4 2663 2658 2657 2662 
		f 4 1905 1906 -1904 1907 
		mu 0 4 2655 2654 2648 2647 
		f 4 1908 1909 -1906 1910 
		mu 0 4 2646 2645 2654 2655 
		f 4 1911 -1907 1912 -458 
		mu 0 4 2642 2648 2654 2587 
		f 4 1913 -461 -1913 -1910 
		mu 0 4 2645 2590 2587 2654 
		f 4 1914 -1902 1915 -474 
		mu 0 4 871 2641 2662 2575 
		f 4 -1916 -1905 -1912 -469 
		mu 0 4 2575 2662 2657 2611 
		f 4 1916 -1895 1917 -465 
		mu 0 4 2595 2634 2651 2565 
		f 4 -471 -1918 -1899 -1915 
		mu 0 4 871 2565 2651 2641 
		f 4 1918 1919 -479 -1914 
		mu 0 4 2645 2596 872 2590 
		f 4 1920 -1919 -1909 1921 
		mu 0 4 2665 2596 2645 2646 
		f 3 -1896 1922 -1921 
		mu 0 3 2665 2634 2596 
		f 4 -1920 -1923 -1917 -477 
		mu 0 4 872 2596 2634 2595 
		f 4 1923 1924 -1897 1925 
		mu 0 4 2605 2604 2652 2665 
		f 4 1926 -1900 -1925 1927 
		mu 0 4 2519 2663 2652 2718 
		f 4 1928 1929 -1908 1930 
		mu 0 4 2717 2745 2655 2647 
		f 4 1931 -1931 -1903 -1927 
		mu 0 4 2624 2696 2658 2663 
		f 3 -1924 1932 1933 
		mu 0 3 2713 2320 1966 
		f 3 -1928 1934 1935 
		mu 0 3 1965 2713 1963 
		f 3 -1934 1936 -1935 
		mu 0 3 2713 1966 1963 
		f 4 1937 -1911 -1930 1938 
		mu 0 4 2323 2646 2655 2311 
		f 4 1939 -1926 -1922 -1938 
		mu 0 4 2310 1948 2665 2646 
		f 4 1940 1941 1942 -1465 
		mu 0 4 873 874 875 876 
		f 4 1943 1944 -481 1945 
		mu 0 4 1939 1945 1944 2338 
		f 3 -1943 1946 -1464 
		mu 0 3 876 875 867 
		f 3 1947 -1946 -483 
		mu 0 3 2315 1939 2338 
		f 4 1948 1949 1950 1951 
		mu 0 4 877 878 879 880 
		f 4 1952 1953 1954 1955 
		mu 0 4 2331 2330 2329 2328 
		f 4 -1457 1956 -1949 1957 
		mu 0 4 881 882 878 877 
		f 4 1958 -493 1959 -1954 
		mu 0 4 2330 2326 2322 2329 
		f 4 -1958 1960 1961 -1452 
		mu 0 4 881 877 883 884 
		f 4 1962 -1960 -497 -1962 
		mu 0 4 883 2329 2322 884 
		f 4 -1961 -1952 1963 1964 
		mu 0 4 883 877 880 885 
		f 4 -1955 -1963 -1965 1965 
		mu 0 4 2328 2329 883 885 
		f 4 1966 -1964 1967 1968 
		mu 0 4 886 885 880 887 
		f 4 -1966 -1967 1969 1970 
		mu 0 4 2328 885 886 2166 
		f 4 1971 1972 1973 1974 
		mu 0 4 888 889 890 891 
		f 4 1975 -1972 1976 1977 
		mu 0 4 2165 889 888 2163 
		f 4 -1969 1978 -1973 1979 
		mu 0 4 886 887 890 889 
		f 4 1980 -1970 -1980 -1976 
		mu 0 4 2165 2166 886 889 
		f 4 1981 1982 1983 -1974 
		mu 0 4 890 892 893 891 
		f 4 1984 1985 -1978 1986 
		mu 0 4 1935 1934 2165 2163 
		f 4 -1975 1987 -1375 1988 
		mu 0 4 888 891 894 895 
		f 4 1989 -1977 -1989 -588 
		mu 0 4 2362 2163 888 895 
		f 4 1990 1991 -1988 -1984 
		mu 0 4 893 896 894 891 
		f 4 1992 1993 -1987 -1990 
		mu 0 4 2362 2160 1935 2163 
		f 4 1994 1995 -1982 -1979 
		mu 0 4 887 897 892 890 
		f 4 1996 1997 -1981 -1986 
		mu 0 4 1934 2089 2166 2165 
		f 4 -1951 1998 -1995 -1968 
		mu 0 4 880 879 897 887 
		f 4 1999 -1956 -1971 -1998 
		mu 0 4 2089 2331 2328 2166 
		f 4 -1950 2000 2001 2002 
		mu 0 4 879 878 898 899 
		f 4 2003 -1953 2004 2005 
		mu 0 4 2085 2330 2331 1891 
		f 4 -1459 2006 -2001 -1957 
		mu 0 4 882 900 898 878 
		f 4 2007 -491 -1959 -2004 
		mu 0 4 2085 1873 2326 2330 
		f 4 2008 2009 -1996 2010 
		mu 0 4 901 902 892 897 
		f 4 2011 2012 2013 -1997 
		mu 0 4 1934 2174 2171 2089 
		f 4 -2003 2014 -2011 -1999 
		mu 0 4 879 899 901 897 
		f 4 2015 -2005 -2000 -2014 
		mu 0 4 2171 1891 2331 2089 
		f 4 2016 2017 2018 -2002 
		mu 0 4 898 903 904 899 
		f 4 2019 2020 -2006 2021 
		mu 0 4 2201 2218 2085 1891 
		f 4 -2019 2022 2023 -2015 
		mu 0 4 899 904 905 901 
		f 4 2024 -2022 -2016 2025 
		mu 0 4 2217 2201 1891 2171 
		f 4 2026 2027 2028 -2018 
		mu 0 4 903 906 907 904 
		f 4 2029 2030 -2020 2031 
		mu 0 4 2213 2192 2218 2201 
		f 4 -2029 2032 2033 -2023 
		mu 0 4 904 907 908 905 
		f 4 2034 -2032 -2025 2035 
		mu 0 4 1994 2213 2201 2217 ;
	setAttr ".fc[1000:1499]"
		f 4 2036 2037 2038 -2028 
		mu 0 4 906 909 910 907 
		f 4 -2038 2039 -2030 2040 
		mu 0 4 910 909 2192 2213 
		f 4 -2039 2041 2042 -2033 
		mu 0 4 907 910 911 908 
		f 4 -2042 -2041 -2035 2043 
		mu 0 4 911 910 2213 1994 
		f 3 -2009 2044 -343 
		mu 0 3 902 901 912 
		f 3 2045 -2013 2046 
		mu 0 3 2189 2171 2174 
		f 3 2047 2048 2049 
		mu 0 3 913 855 914 
		f 3 2050 2051 2052 
		mu 0 3 2185 2367 2742 
		f 3 2053 -1814 -2048 
		mu 0 3 913 856 855 
		f 3 -1819 2054 -2052 
		mu 0 3 2367 2194 2742 
		f 4 2055 -2049 -1828 2056 
		mu 0 4 915 914 855 852 
		f 4 -2051 2057 2058 -1830 
		mu 0 4 2367 2185 2106 2090 
		f 4 -2024 2059 2060 -2045 
		mu 0 4 901 905 916 912 
		f 4 2061 -2026 -2046 2062 
		mu 0 4 1989 2217 2171 2189 
		f 4 -1462 2063 -2017 -2007 
		mu 0 4 900 917 903 898 
		f 4 2064 -486 -2008 -2021 
		mu 0 4 2218 1865 1873 2085 
		f 3 2065 -2061 2066 
		mu 0 3 918 912 916 
		f 3 -2063 2067 2068 
		mu 0 3 1989 2189 2262 
		f 4 -1861 2069 -1929 2070 
		mu 0 4 2034 2302 2251 2249 
		f 4 2071 -1939 -2070 -1851 
		mu 0 4 2305 2237 2251 2302 
		f 3 2072 2073 2074 
		mu 0 3 919 920 921 
		f 3 2075 2076 -2075 
		mu 0 3 921 1919 919 
		f 4 2077 -2073 2078 2079 
		mu 0 4 922 920 919 923 
		f 4 -2077 2080 2081 2082 
		mu 0 4 919 1911 1910 2250 
		f 4 2083 -2080 2084 2085 
		mu 0 4 924 922 923 925 
		f 4 -2082 2086 2087 2088 
		mu 0 4 2250 1910 2242 2241 
		f 3 -2081 2089 2090 
		mu 0 3 926 927 928 
		f 3 -2087 2091 2092 
		mu 0 3 929 926 930 
		f 3 -2091 2093 -2092 
		mu 0 3 926 928 930 
		f 3 3761 -2085 2095 
		mu 0 3 838 925 923 
		f 3 -2089 3760 2097 
		mu 0 3 2250 2241 2258 
		f 3 -2090 -2076 2098 
		mu 0 3 928 927 931 
		f 4 2099 -1940 -2072 -1863 
		mu 0 4 2167 2320 2237 2305 
		f 3 3765 2100 2101 
		mu 0 3 925 832 932 
		f 3 3764 -1866 -2097 
		mu 0 3 2241 1582 2303 
		f 4 -2086 -2102 2104 2105 
		mu 0 4 924 925 932 933 
		f 4 -2104 -2088 2106 2107 
		mu 0 4 2227 2241 2242 1904 
		f 3 -2105 2108 2109 
		mu 0 3 933 932 934 
		f 3 2110 -2108 2111 
		mu 0 3 2285 2227 1904 
		f 3 -2112 2112 2113 
		mu 0 3 935 936 937 
		f 4 2114 -2113 -2107 -2093 
		mu 0 4 930 937 936 929 
		f 3 2115 -2101 2116 
		mu 0 3 938 932 832 
		f 3 -2103 2117 2118 
		mu 0 3 1582 2227 2267 
		f 3 -2116 2119 -2109 
		mu 0 3 932 938 934 
		f 3 2120 -2118 -2111 
		mu 0 3 2285 2267 2227 
		f 3 -2120 2121 2122 
		mu 0 3 939 940 941 
		f 3 2123 -2121 2124 
		mu 0 3 2040 2031 2030 
		f 4 -1765 2125 2126 -2117 
		mu 0 4 832 828 942 938 
		f 4 2127 -1871 -2119 2128 
		mu 0 4 2345 2124 1582 2267 
		f 4 -2127 2129 2130 -2122 
		mu 0 4 940 943 944 941 
		f 4 2131 -2129 -2124 2132 
		mu 0 4 2132 2293 2031 2040 
		f 4 2133 -1933 -2100 -1868 
		mu 0 4 2125 1966 2320 2167 
		f 3 3759 2135 -1776 
		mu 0 3 838 945 839 
		f 3 3758 2137 -2098 
		mu 0 3 2258 2103 2250 
		f 4 2138 2139 -2135 -2079 
		mu 0 4 919 946 945 923 
		f 4 2140 -2139 -2083 -2138 
		mu 0 4 2103 946 919 2250 
		f 3 3757 2142 -1761 
		mu 0 3 830 948 824 
		f 3 2143 -2137 3756 
		mu 0 3 2027 2012 2355 
		f 4 2145 2146 -2142 -2140 
		mu 0 4 949 950 948 947 
		f 4 2147 -2146 -2141 -2144 
		mu 0 4 2027 950 949 2012 
		f 4 2148 -1872 -2071 -1932 
		mu 0 4 1965 2144 2368 2274 
		f 4 2149 -1937 -2134 -1878 
		mu 0 4 2129 1963 1966 2125 
		f 4 -2149 -1936 -2150 -1882 
		mu 0 4 2144 1965 1963 2129 
		f 3 2150 2151 2152 
		mu 0 3 948 951 952 
		f 3 2153 2154 2155 
		mu 0 3 2701 2353 2027 
		f 3 -2143 -2153 3754 
		mu 0 3 824 948 952 
		f 3 -2156 -2145 3755 
		mu 0 3 2701 2027 2131 
		f 3 3762 2158 -2126 
		mu 0 3 828 952 942 
		f 3 3763 -2158 -1881 
		mu 0 3 2124 2701 2359 
		f 4 -2147 2160 2161 -2151 
		mu 0 4 948 950 953 951 
		f 4 -2155 2162 -2161 -2148 
		mu 0 4 2027 2353 953 950 
		f 4 -2152 2163 2164 2165 
		mu 0 4 954 955 956 957 
		f 4 2166 -2154 2167 2168 
		mu 0 4 1878 2620 2688 2736 
		f 4 2169 2170 -2164 -2162 
		mu 0 4 958 959 956 955 
		f 4 2171 -2170 -2163 -2167 
		mu 0 4 1878 959 958 2620 
		f 4 -2166 2172 -2130 -2159 
		mu 0 4 954 957 944 943 
		f 4 2173 -2168 -2160 -2132 
		mu 0 4 2132 2736 2688 2293 
		f 4 2174 -2054 2175 -2165 
		mu 0 4 960 961 962 963 
		f 4 -2055 2176 -2169 2177 
		mu 0 4 2003 2309 2308 2216 
		f 4 2178 -1834 -2175 -2171 
		mu 0 4 964 965 961 960 
		f 4 -1837 -2179 -2172 -2177 
		mu 0 4 2309 965 964 2308 
		f 4 -1397 2179 -2027 -2064 
		mu 0 4 917 966 906 903 
		f 4 2180 -565 -2065 -2031 
		mu 0 4 2192 2306 1865 2218 
		f 4 2181 -2037 -2180 -1394 
		mu 0 4 967 909 906 966 
		f 4 -2040 -2182 -570 -2181 
		mu 0 4 2192 909 967 2306 
		f 3 2182 -1381 -1992 
		mu 0 3 896 873 894 
		f 3 -582 2183 -1993 
		mu 0 3 2362 1944 2160 
		f 3 2184 -1941 -2183 
		mu 0 3 896 874 873 
		f 3 -1945 2185 -2184 
		mu 0 3 1944 1945 2160 
		f 3 -2060 2186 2187 
		mu 0 3 916 905 968 
		f 3 2188 -2062 2189 
		mu 0 3 2079 2217 1989 
		f 4 -2034 2190 2191 -2187 
		mu 0 4 905 908 969 968 
		f 4 2192 -2036 -2189 2193 
		mu 0 4 2073 1994 2217 2079 
		f 4 2194 2195 2196 2197 
		mu 0 4 918 970 868 875 
		f 4 2198 2199 2200 2201 
		mu 0 4 2316 2066 2262 1939 
		f 3 -2197 -1886 -1947 
		mu 0 3 875 868 867 
		f 3 -1890 -2202 -1948 
		mu 0 3 2315 2316 1939 
		f 4 2202 2203 -2191 -2043 
		mu 0 4 911 971 969 908 
		f 4 2204 -2203 -2044 -2193 
		mu 0 4 2073 971 911 1994 
		f 4 -2188 2205 -2195 -2067 
		mu 0 4 916 968 970 918 
		f 4 2206 -2190 -2069 -2200 
		mu 0 4 2066 2079 1989 2262 
		f 4 2207 -1887 2208 -2204 
		mu 0 4 971 869 868 969 
		f 4 -1889 -2208 -2205 2209 
		mu 0 4 2316 869 971 2073 
		f 4 -2192 -2209 -2196 -2206 
		mu 0 4 968 969 868 970 
		f 4 -2210 -2194 -2207 -2199 
		mu 0 4 2316 2073 2079 2066 
		f 4 2210 2211 2212 -1453 
		mu 0 4 972 973 974 975 
		f 4 2213 -2211 -496 2214 
		mu 0 4 2153 2151 1862 1859 
		f 3 2215 2216 -2212 
		mu 0 3 851 976 977 
		f 3 2217 2218 -2214 
		mu 0 3 1838 1837 851 
		f 3 2219 -2216 -1800 
		mu 0 3 850 976 851 
		f 3 -2219 2220 -1802 
		mu 0 3 851 1837 2342 
		f 4 -1811 2221 2222 -2220 
		mu 0 4 850 854 978 976 
		f 4 2223 -1813 -2221 2224 
		mu 0 4 1797 2639 2342 1837 
		f 4 2225 2226 2227 -1793 
		mu 0 4 979 980 981 982 
		f 4 -2099 -2226 -1798 2228 
		mu 0 4 2047 2069 2071 2072 
		f 4 -2228 2229 2230 -1848 
		mu 0 4 982 981 983 984 
		f 4 -2094 -2229 -1849 2231 
		mu 0 4 2193 2047 2072 2738 
		f 4 -2231 2232 2233 -1842 
		mu 0 4 984 983 985 986 
		f 4 -2115 -2232 -1846 2234 
		mu 0 4 2743 2193 2738 2299 
		f 4 -2234 2235 2236 -1843 
		mu 0 4 986 985 987 988 
		f 4 -2114 -2235 -1845 2237 
		mu 0 4 2300 2743 2299 1868 
		f 4 -2237 -2123 2238 -2057 
		mu 0 4 988 987 989 990 
		f 4 -2125 -2238 -2059 2239 
		mu 0 4 1869 2300 1868 2190 
		f 4 -2131 2240 -2056 -2239 
		mu 0 4 989 991 992 990 
		f 4 2241 -2133 -2240 -2058 
		mu 0 4 1664 2199 2200 2002 
		f 4 -2173 -2176 -2050 -2241 
		mu 0 4 991 963 962 992 
		f 4 -2178 -2174 -2242 -2053 
		mu 0 4 2003 2216 2199 1664 
		f 4 2242 -1823 2243 2244 
		mu 0 4 993 860 859 994 
		f 4 -1827 2245 2246 2247 
		mu 0 4 2224 2243 1521 2291 
		f 4 -1832 -2243 2248 -2222 
		mu 0 4 854 860 993 978 
		f 4 -2246 -1833 -2224 2249 
		mu 0 4 1521 2243 2639 1797 
		f 4 -2213 -2217 2250 -1455 
		mu 0 4 975 974 995 996 
		f 4 -2218 -2215 -494 2251 
		mu 0 4 1697 2153 1859 2297 
		f 4 -2251 -2223 2252 -1461 
		mu 0 4 996 995 997 998 
		f 4 -2225 -2252 -488 2253 
		mu 0 4 1700 1697 2297 2457 
		f 4 -2253 -2249 2254 -1399 
		mu 0 4 998 997 999 1000 
		f 4 -2250 -2254 -564 2255 
		mu 0 4 2427 2455 2445 2421 
		f 4 -2255 -2245 2256 -1392 
		mu 0 4 1000 999 1001 1002 
		f 4 -2247 -2256 -572 2257 
		mu 0 4 1907 2427 2421 2290 
		f 4 2258 -1393 2259 -1840 
		mu 0 4 1003 1004 1005 1006 
		f 4 -571 -2259 -1841 2260 
		mu 0 4 1959 1004 1003 2412 
		f 4 -1396 -2257 -2244 -2260 
		mu 0 4 1005 1002 1001 1006 
		f 4 -2258 -567 -2261 -2248 
		mu 0 4 1907 2290 1959 2412 
		f 4 2261 -1374 2262 -1377 
		mu 0 4 1007 1008 1009 1010 
		f 4 -590 -2262 -587 2263 
		mu 0 4 2252 1008 1007 1732 
		f 4 -2263 -1380 2264 -1383 
		mu 0 4 1010 1009 1011 1012 
		f 4 -584 -2264 -581 2265 
		mu 0 4 2505 2252 1732 2283 
		f 4 -2265 -1386 -1372 -1389 
		mu 0 4 1012 1011 1013 1014 
		f 4 -577 -2266 -574 -591 
		mu 0 4 2506 2505 2283 2502 
		f 4 2266 2267 2268 2269 
		mu 0 4 2452 2395 1755 1752 
		f 4 2270 2271 2272 2273 
		mu 0 4 2275 2504 2401 2349 
		f 4 2274 -2272 2275 2276 
		mu 0 4 2471 2401 2504 2453 
		f 4 2277 2278 2279 2280 
		mu 0 4 1750 2501 2363 1749 
		f 4 2281 2282 2283 -2279 
		mu 0 4 2501 2287 2507 2363 
		f 4 2284 -2274 2285 -2283 
		mu 0 4 2287 2275 2349 2507 
		f 4 -2271 2286 2287 2288 
		mu 0 4 2504 2275 2616 1754 
		f 4 -2282 2289 2290 2291 
		mu 0 4 2287 2501 2387 2499 
		f 4 -2285 -2292 2292 -2287 
		mu 0 4 2275 2287 2499 2616 
		f 4 2293 2294 2295 2296 
		mu 0 4 2548 2509 2464 2474 
		f 4 2297 2298 2299 2300 
		mu 0 4 2470 2424 2425 2451 
		f 4 -2300 2301 2302 2303 
		mu 0 4 2451 2425 2514 2449 
		f 4 -2267 2304 -2303 2305 
		mu 0 4 2395 2452 2449 2514 
		f 4 2306 -2277 2307 2308 
		mu 0 4 1738 2471 2453 1701 
		f 4 2309 2310 2311 2312 
		mu 0 4 2510 2466 1756 2472 
		f 4 2313 2314 -2310 2315 
		mu 0 4 2454 2467 2466 2510 
		f 4 2316 2317 -2314 2318 
		mu 0 4 2384 2398 2467 2454 
		f 4 2319 2320 -2317 2321 
		mu 0 4 2382 1753 2398 2384 
		f 4 -2275 2322 -2311 2323 
		mu 0 4 2401 2471 1756 2466 
		f 4 -2312 -2323 -2307 2324 
		mu 0 4 2472 1756 2471 1738 
		f 4 -2273 -2324 -2315 2325 
		mu 0 4 2349 2401 2466 2467 
		f 4 -2280 2326 -2320 2327 
		mu 0 4 1749 2363 1753 2382 
		f 4 -2284 2328 -2321 -2327 
		mu 0 4 2363 2507 2398 1753 
		f 4 -2286 -2326 -2318 -2329 
		mu 0 4 2507 2349 2467 2398 
		f 4 2329 2330 2331 -2306 
		mu 0 4 2514 2231 2390 2395 
		f 4 2332 -2330 -2302 2333 
		mu 0 4 2416 2231 2514 2425 
		f 3 2334 2335 2336 
		mu 0 3 2417 2511 2416 
		f 4 2337 2338 2339 -2295 
		mu 0 4 2509 2508 2468 2464 
		f 4 -2276 -2289 2340 2341 
		mu 0 4 2453 2504 1754 2400 
		f 4 -2278 2342 2343 -2290 
		mu 0 4 2501 1750 1755 2387 
		f 3 2344 2345 2346 
		mu 0 3 2497 2496 2495 
		f 3 2347 2348 2349 
		mu 0 3 2494 2493 2497 
		f 3 2350 2351 -2349 
		mu 0 3 2493 2490 2497 
		f 3 2352 2353 2354 
		mu 0 3 2488 2487 2494 
		f 4 -2359 -2301 -2304 2355 
		mu 0 4 2485 2484 2483 2482 
		f 4 -2305 2356 -2297 -2356 
		mu 0 4 2482 2479 2476 2485 
		f 4 -2296 2357 -2298 2358 
		mu 0 4 2474 2464 2424 2470 
		f 4 2359 2360 -2358 -2340 
		mu 0 4 2468 2417 2424 2464 
		f 4 2361 2362 -2353 2363 
		mu 0 4 2463 2460 2487 2488 
		f 4 -2342 2364 2365 -2308 
		mu 0 4 2453 2400 1693 1701 
		f 3 2366 2367 2368 
		mu 0 3 2448 2488 2497 
		f 4 -2299 -2361 -2337 -2334 
		mu 0 4 2425 2424 2417 2416 
		f 4 2369 -2365 2370 -2338 
		mu 0 4 2402 1693 2400 2399 
		f 4 -2268 -2332 2371 -2344 
		mu 0 4 1755 2395 2390 2387 
		f 3 2372 2373 -2345 
		mu 0 3 2497 2383 2496 
		f 3 -2352 2374 -2373 
		mu 0 3 2497 2490 2383 
		f 3 -2369 -2347 2375 
		mu 0 3 2448 2497 2495 
		f 4 -2343 2376 -2363 -2269 
		mu 0 4 1755 1750 1751 1752 
		f 4 -2377 -2281 2377 -2354 
		mu 0 4 1751 1750 1749 1748 
		f 4 -2378 -2328 2378 -2348 
		mu 0 4 1747 1749 2382 2475 
		f 4 -2379 -2322 2379 -2351 
		mu 0 4 2480 2382 2384 1758 
		f 4 -2380 -2319 2380 -2375 
		mu 0 4 1759 2384 2454 2347 
		f 4 -2381 -2316 2381 -2374 
		mu 0 4 2347 2454 2510 2041 
		f 4 -2382 -2313 2382 -2346 
		mu 0 4 2041 2510 2472 2282 
		f 4 -2383 -2325 2383 -2376 
		mu 0 4 2282 2472 1738 2371 
		f 4 -2384 -2309 2384 -2367 
		mu 0 4 2371 1738 1701 1731 
		f 4 -2385 -2366 2385 -2364 
		mu 0 4 1704 1701 1693 1694 
		f 4 -2386 -2370 -2294 -2424 
		mu 0 4 1694 1693 2402 2076 
		f 4 2386 2387 -2371 2388 
		mu 0 4 2737 2269 2749 2748 
		f 3 2389 2390 2391 
		mu 0 3 2725 2724 1826 
		f 3 2392 2393 2394 
		mu 0 3 1823 1818 1815 
		f 4 2395 2396 -2393 -655 
		mu 0 4 1786 1785 1818 1823 
		f 4 2397 2398 -643 2399 
		mu 0 4 1742 1741 1740 1739 
		f 4 -656 2400 2401 2402 
		mu 0 4 1786 1734 1730 1729 
		f 4 -2399 2403 -2401 -638 
		mu 0 4 1740 1741 1730 1734 
		f 4 -2339 -2388 -2390 2404 
		mu 0 4 1722 1720 2724 2725 
		f 4 -2336 2405 -2394 2406 
		mu 0 4 1711 1710 1815 1818 
		f 4 -2333 -2407 -2397 2407 
		mu 0 4 1692 1711 1818 1785 
		f 4 -2288 2408 -2398 2409 
		mu 0 4 1656 1655 1741 1742 
		f 4 -2291 2410 -2402 2411 
		mu 0 4 2307 2585 1729 1730 
		f 4 -2293 -2412 -2404 -2409 
		mu 0 4 1655 2307 1730 1741 
		f 4 -646 2412 -2414 -2400 
		mu 0 4 1739 1567 2737 1742 
		f 4 -2341 -2410 2413 -2389 
		mu 0 4 2748 1656 1742 2737 
		f 3 2414 -2403 2415 
		mu 0 3 2273 1786 1729 
		f 4 2416 -2416 -2411 -2372 
		mu 0 4 1801 2273 1729 2585 
		f 3 -2387 -2413 -2391 
		mu 0 3 2269 2737 1567 
		f 4 -2360 -2405 2417 2418 
		mu 0 4 1666 1722 2725 1659 
		f 4 2419 -2418 -2392 -649 
		mu 0 4 1646 1659 2725 1826 
		f 3 -2420 2420 2421 
		mu 0 3 1659 1646 1815 
		f 4 -2335 -2419 -2422 -2406 
		mu 0 4 1710 1666 1659 1815 
		f 3 -2421 -652 -2395 
		mu 0 3 1815 1646 1823 
		f 4 -2331 -2408 -2423 -2417 
		mu 0 4 1801 2101 1785 2273 
		f 3 -2415 2422 -2396 
		mu 0 3 1786 2273 1785 
		f 3 -2355 -2350 -2368 
		mu 0 3 2488 2494 2497 
		f 4 -2357 -2270 -2362 2423 
		mu 0 4 2476 2479 2460 2463 
		f 3 2424 2425 2426 
		mu 0 3 1015 1016 1017 
		f 3 2427 2428 2429 
		mu 0 3 1833 1802 1791 
		f 4 2430 2431 2432 2433 
		mu 0 4 1018 1019 1020 1016 
		f 4 2434 -2431 2435 2436 
		mu 0 4 1799 1019 1018 1802 
		f 3 2437 2438 2439 
		mu 0 3 1017 1021 1022 
		f 3 2440 2441 2442 
		mu 0 3 1821 1822 1833 
		f 4 2443 -2438 -2426 -2433 
		mu 0 4 1020 1021 1017 1016 
		f 4 -2442 2444 -2437 -2428 
		mu 0 4 1833 1822 1799 1802 
		f 4 2445 2446 -2444 2447 
		mu 0 4 1023 1024 1021 1020 
		f 4 2448 2449 2450 -2445 
		mu 0 4 1822 1509 2184 1799 
		f 4 2451 2452 -2448 -2432 
		mu 0 4 1019 1025 1023 1020 
		f 4 2453 -2452 -2435 -2451 
		mu 0 4 2184 1025 1019 1799 
		f 4 2454 2455 -2446 2456 
		mu 0 4 1026 1027 1024 1023 
		f 4 2457 2458 2459 -2450 
		mu 0 4 1509 1516 2420 2184 
		f 4 2460 -2457 -2453 2461 
		mu 0 4 1028 1026 1023 1025 
		f 4 -2460 2462 -2462 -2454 
		mu 0 4 2184 2420 1028 1025 
		f 3 2463 2464 2465 
		mu 0 3 1029 1030 1031 
		f 3 2466 2467 2468 
		mu 0 3 2456 1635 1648 
		f 3 2469 -2466 2470 
		mu 0 3 1032 1029 1031 
		f 3 -2469 2471 2472 
		mu 0 3 2456 1648 2613 
		f 4 -2464 2473 2474 2475 
		mu 0 4 1030 1029 1033 1034 
		f 4 2476 -2468 2477 2478 
		mu 0 4 2614 1648 1635 2621 
		f 4 2479 2480 2481 2482 
		mu 0 4 1035 1036 1037 1038 
		f 4 2483 2484 2485 2486 
		mu 0 4 1577 1494 2618 2599 
		f 3 2487 2488 -2482 
		mu 0 3 1037 1039 1038 
		f 3 2489 2490 -2487 
		mu 0 3 2599 1736 1577 
		f 4 2491 -2483 2492 -2476 
		mu 0 4 1034 1040 1041 1030 
		f 4 -2486 2493 -2478 2494 
		mu 0 4 2612 2568 2621 1635 
		f 4 -2493 -2489 2495 -2465 
		mu 0 4 1030 1041 1042 1031 
		f 4 -2490 -2495 -2467 2496 
		mu 0 4 2572 2612 1635 2456 
		f 4 2497 -2447 2498 2499 
		mu 0 4 1036 1021 1024 1043 
		f 4 -2449 2500 2501 2502 
		mu 0 4 1509 1822 1494 1499 
		f 4 2503 2504 -2498 -2480 
		mu 0 4 1035 1044 1021 1036 
		f 4 2505 2506 -2485 -2501 
		mu 0 4 1822 2643 2618 1494 
		f 3 -2505 2507 -2439 
		mu 0 3 1021 1044 1022 
		f 3 2508 -2506 -2441 
		mu 0 3 1821 2643 1822 
		f 4 2509 2510 2511 2512 
		mu 0 4 1045 1046 1047 1048 
		f 4 2513 2514 2515 2516 
		mu 0 4 2640 2633 2370 2187 
		f 3 -2512 2517 2518 
		mu 0 3 1048 1047 1049 
		f 3 2519 -2517 2520 
		mu 0 3 2058 2640 2187 
		f 3 2521 -2519 2522 
		mu 0 3 1050 1048 1049 
		f 3 -2521 2523 2524 
		mu 0 3 2058 2187 1915 
		f 4 2525 -2513 -2522 2526 
		mu 0 4 1051 1045 1048 1050 
		f 4 -2516 2527 2528 -2524 
		mu 0 4 2187 2370 2256 1915 
		f 4 2529 -2527 2530 2531 
		mu 0 4 1052 1051 1050 1053 
		f 4 -2529 2532 2533 2534 
		mu 0 4 1915 2256 2296 2588 
		f 3 -2531 -2523 2535 
		mu 0 3 1053 1050 1049 
		f 3 -2525 -2535 2536 
		mu 0 3 2058 1915 2588 
		f 3 2537 -2536 2538 
		mu 0 3 1054 1053 1049 
		f 3 -2537 2539 2540 
		mu 0 3 2058 2588 2600 
		f 4 2541 -2532 -2538 2542 
		mu 0 4 1055 1052 1053 1054 
		f 4 -2534 2543 2544 -2540 
		mu 0 4 2588 2296 2644 2600 
		f 4 2545 -2543 2546 2547 
		mu 0 4 1056 1055 1054 1057 
		f 4 -2545 2548 2549 2550 
		mu 0 4 2600 2644 2716 1645 
		f 3 -2547 -2539 2551 
		mu 0 3 1057 1054 1049 
		f 3 -2541 -2551 2552 
		mu 0 3 2058 2600 1645 
		f 3 -2518 2553 -2552 
		mu 0 3 1049 1047 1057 
		f 3 2554 -2520 -2553 
		mu 0 3 1645 2640 2058 
		f 4 -2554 -2511 2555 -2548 
		mu 0 4 1057 1047 1046 1056 
		f 4 -2514 -2555 -2550 2556 
		mu 0 4 2633 2640 1645 2716 
		f 4 2557 2558 -1892 2559 
		mu 0 4 1058 1059 1060 1061 
		f 4 2560 2561 -2560 -1893 
		mu 0 4 1549 1505 1058 1061 
		f 3 -1402 -2559 2562 
		mu 0 3 1062 1060 1059 
		f 3 -2561 -559 2563 
		mu 0 3 1505 1549 1519 
		f 3 2564 2565 -2563 
		mu 0 3 1059 1043 1062 
		f 3 2566 2567 -2564 
		mu 0 3 1519 1499 1505 
		f 3 -2566 2568 -1401 
		mu 0 3 1062 1043 1027 
		f 3 2569 -2567 -562 
		mu 0 3 1516 1499 1519 
		f 3 -2499 -2456 -2569 
		mu 0 3 1043 1024 1027 
		f 3 -2458 -2503 -2570 
		mu 0 3 1516 1509 1499 
		f 4 -2500 -2565 2570 -2481 
		mu 0 4 1036 1043 1059 1037 
		f 4 -2568 -2502 -2484 2571 
		mu 0 4 1505 1499 1494 1577 
		f 4 2572 -2571 -2558 2573 
		mu 0 4 1063 1037 1059 1058 
		f 4 -2572 2574 -2574 -2562 
		mu 0 4 1505 1577 1063 1058 
		f 4 -2488 -2573 2575 2576 
		mu 0 4 1039 1037 1063 1064 
		f 4 -2575 -2491 2577 -2576 
		mu 0 4 1063 1577 1736 1064 
		f 3 -2474 2578 2579 
		mu 0 3 1033 1029 1065 
		f 3 2580 -2477 2581 
		mu 0 3 2270 1648 2614 
		f 4 2582 2583 2584 2585 
		mu 0 4 1066 1032 1067 1068 
		f 4 2586 2587 2588 -2585 
		mu 0 4 1067 2613 2145 1068 
		f 4 -2583 2589 -2579 -2470 
		mu 0 4 1032 1066 1065 1029 
		f 4 2590 -2588 -2472 -2581 
		mu 0 4 2270 2145 2613 1648 
		f 4 2591 2592 2593 2594 
		mu 0 4 1069 1070 1071 1072 
		f 4 2595 2596 2597 -2594 
		mu 0 4 1071 1971 2092 1072 
		f 4 2598 2599 -2592 2600 
		mu 0 4 1073 1074 1070 1069 
		f 4 2601 2602 2603 -2597 
		mu 0 4 1971 2099 1794 2092 
		f 4 2604 -2595 2605 2606 
		mu 0 4 1075 1069 1072 1076 
		f 4 -2598 2607 2608 -2606 
		mu 0 4 1072 2092 1565 1076 
		f 3 2609 -2601 2610 
		mu 0 3 1077 1073 1069 
		f 3 -2604 2611 2612 
		mu 0 3 2092 1794 1735 
		f 3 2613 -2611 -2605 
		mu 0 3 1075 1077 1069 
		f 3 -2613 2614 -2608 
		mu 0 3 2092 1735 1565 
		f 4 -2600 2615 2616 -2593 
		mu 0 4 1070 1074 1078 1071 
		f 4 2617 -2602 -2596 -2617 
		mu 0 4 1078 2099 1971 1071 
		f 4 2618 2619 2620 2621 
		mu 0 4 1079 1080 1081 1082 
		f 4 2622 2623 2624 -2621 
		mu 0 4 1081 1726 2009 1082 
		f 4 -2580 2625 -2619 2626 
		mu 0 4 1033 1065 1080 1079 
		f 4 2627 -2582 2628 -2624 
		mu 0 4 1726 2270 2614 2009 
		f 3 2629 -2626 -2590 
		mu 0 3 1066 1080 1065 
		f 3 -2628 2630 -2591 
		mu 0 3 2270 1726 2145 
		f 4 -2620 -2630 -2586 2631 
		mu 0 4 1081 1080 1066 1068 
		f 4 -2631 -2623 -2632 -2589 
		mu 0 4 2145 1726 1081 1068 
		f 4 -2471 2632 2633 -2584 
		mu 0 4 1032 1031 1083 1067 
		f 4 2634 -2473 -2587 -2634 
		mu 0 4 1083 2456 2613 1067 
		f 4 -2496 -2577 2635 -2633 
		mu 0 4 1031 1042 1084 1083 
		f 4 -2578 -2497 -2635 -2636 
		mu 0 4 1084 2572 2456 1083 
		f 4 2636 2637 -2607 2638 
		mu 0 4 1085 1086 1075 1076 
		f 4 2639 2640 -2639 -2609 
		mu 0 4 1565 2149 1085 1076 
		f 3 2641 -2475 2642 
		mu 0 3 1087 1034 1033 
		f 3 -2479 2643 2644 
		mu 0 3 2614 2621 2351 
		f 4 2645 -2599 2646 2647 
		mu 0 4 1088 1074 1073 1089 
		f 4 -2603 2648 2649 2650 
		mu 0 4 1794 2099 2277 2739 
		f 3 2651 -2647 -2610 
		mu 0 3 1077 1089 1073 
		f 3 -2651 2652 -2612 
		mu 0 3 1794 2739 1735 
		f 3 -2646 2653 -2616 
		mu 0 3 1074 1088 1078 
		f 3 2654 -2649 -2618 
		mu 0 3 1078 2277 2099 
		f 4 2655 -2622 2656 -2654 
		mu 0 4 1088 1079 1082 1078 
		f 4 -2625 2657 -2655 -2657 
		mu 0 4 1082 2009 2277 1078 
		f 3 -2656 2658 -2627 
		mu 0 3 1079 1088 1033 
		f 3 2659 -2658 -2629 
		mu 0 3 2614 2277 2009 
		f 4 2660 2661 2662 2663 
		mu 0 4 1090 1091 1092 1093 
		f 4 2664 2665 2666 2667 
		mu 0 4 2108 2219 2220 2228 
		f 4 2668 2669 -2661 2670 
		mu 0 4 1094 1095 1091 1090 
		f 4 2671 2672 2673 -2666 
		mu 0 4 2219 2232 2233 2220 
		f 4 2674 2675 2676 2677 
		mu 0 4 1096 1097 1098 1099 
		f 4 2678 2679 2680 2681 
		mu 0 4 2235 2238 2239 1920 
		f 4 -2677 2682 2683 2684 
		mu 0 4 1099 1098 1100 1101 
		f 4 2685 -2682 2686 2687 
		mu 0 4 2236 2235 1920 2261 
		f 4 -2684 2688 2689 2690 
		mu 0 4 1101 1100 1102 1103 
		f 4 2691 -2688 2692 2693 
		mu 0 4 1889 2236 2261 2325 
		f 4 2694 2695 -2675 2696 
		mu 0 4 1104 1105 1097 1096 
		f 4 2697 2698 2699 -2680 
		mu 0 4 2238 2373 2374 2239 
		f 4 -2690 2700 -2695 2701 
		mu 0 4 1103 1102 1105 1104 
		f 4 2702 -2694 2703 -2699 
		mu 0 4 2373 1889 2325 2374 
		f 3 -2702 2704 -2691 
		mu 0 3 1103 1104 1101 
		f 3 2705 -2704 -2693 
		mu 0 3 2261 2374 2325 
		f 4 2706 2707 2708 2709 
		mu 0 4 1106 1107 1108 1109 
		f 4 2710 2711 2712 2713 
		mu 0 4 2126 2350 2430 2154 
		f 3 2714 2715 2716 
		mu 0 3 1110 1111 1112 
		f 3 2717 2718 2719 
		mu 0 3 2361 2547 2128 
		f 3 -2717 2720 -2709 
		mu 0 3 1110 1112 1113 
		f 3 2721 -2720 -2714 
		mu 0 3 2281 2361 2128 
		f 3 2722 2723 2724 
		mu 0 3 1112 1114 1115 
		f 3 2725 2726 2727 
		mu 0 3 2664 2036 2361 
		f 3 -2716 2728 -2723 
		mu 0 3 1112 1111 1114 
		f 3 2729 -2718 -2727 
		mu 0 3 2036 2547 2361 
		f 4 2730 2731 -2652 2732 
		mu 0 4 1116 1117 1089 1077 
		f 4 2733 2734 2735 -2653 
		mu 0 4 2739 2104 2619 1735 
		f 4 -2689 2736 -2731 2737 
		mu 0 4 1102 1100 1117 1116 
		f 4 2738 -2692 2739 -2735 
		mu 0 4 2104 2236 1889 2619 
		f 3 2740 -2733 -2614 
		mu 0 3 1075 1116 1077 
		f 3 -2736 2741 -2615 
		mu 0 3 1735 2619 1565 
		f 3 2742 -2738 -2741 
		mu 0 3 1075 1102 1116 
		f 3 -2740 2743 -2742 
		mu 0 3 2619 1889 1565 
		f 4 2744 -2732 2745 2746 
		mu 0 4 1118 1089 1117 1119 
		f 4 -2734 2747 2748 2749 
		mu 0 4 2104 2739 1760 2379 
		f 4 -2745 2750 -2659 -2648 
		mu 0 4 1089 1118 1033 1088 
		f 4 2751 -2748 -2650 -2660 
		mu 0 4 2614 1760 2739 2277 
		f 3 -2746 2752 2753 
		mu 0 3 1119 1117 1087 
		f 3 2754 -2750 2755 
		mu 0 3 2351 2104 2379 
		f 4 -2754 -2643 -2751 -2747 
		mu 0 4 1119 1087 1033 1118 
		f 4 -2645 -2756 -2749 -2752 
		mu 0 4 2614 2351 2379 1760 
		f 4 -2708 -2504 -2492 2756 
		mu 0 4 1120 1121 1040 1034 
		f 4 -2507 -2711 2757 -2494 
		mu 0 4 2568 2357 2356 2621 
		f 3 2758 -2743 -2638 
		mu 0 3 1086 1102 1075 
		f 3 -2744 2759 -2640 
		mu 0 3 1565 1889 2149 
		f 4 -2753 -2737 -2683 2760 
		mu 0 4 1087 1117 1100 1098 
		f 4 -2739 -2755 2761 -2686 
		mu 0 4 2236 2104 2351 2235 
		f 4 -2701 -2759 2762 2763 
		mu 0 4 1105 1102 1086 1092 
		f 4 -2760 -2703 2764 2765 
		mu 0 4 2149 1889 2373 2108 
		f 4 -2696 -2764 -2662 2766 
		mu 0 4 1097 1105 1092 1091 
		f 4 -2765 -2698 2767 -2665 
		mu 0 4 2108 2373 2238 2219 
		f 3 2768 2769 -2767 
		mu 0 3 1091 1122 1097 
		f 3 2770 2771 -2768 
		mu 0 3 2238 2551 2219 
		f 4 -2770 2772 -2761 -2676 
		mu 0 4 1097 1122 1087 1098 
		f 4 2773 -2771 -2679 -2762 
		mu 0 4 2351 2551 2238 2235 
		f 4 -2769 -2670 -2715 2774 
		mu 0 4 1122 1091 1095 1120 
		f 4 -2672 -2772 2775 -2719 
		mu 0 4 2232 2219 2551 2356 
		f 4 -2775 -2757 -2642 -2773 
		mu 0 4 1122 1120 1034 1087 
		f 4 -2758 -2776 -2774 -2644 
		mu 0 4 2621 2356 2551 2351 
		f 3 2776 -2637 2777 
		mu 0 3 1123 1086 1085 
		f 3 -2641 2778 -2778 
		mu 0 3 1085 2149 1123 
		f 4 -2663 -2763 -2777 2779 
		mu 0 4 1093 1092 1086 1123 
		f 4 -2766 -2668 2780 -2779 
		mu 0 4 2149 2108 2228 1123 
		f 4 2781 2782 -2725 2783 
		mu 0 4 1124 1125 1126 1127 
		f 4 2784 2785 2786 -2728 
		mu 0 4 2246 2571 2574 2195 
		f 4 2787 -2710 -2721 -2783 
		mu 0 4 1125 1106 1109 1126 
		f 4 -2713 2788 -2785 -2722 
		mu 0 4 2154 2430 2571 2246 
		f 4 2789 2790 -2729 2791 
		mu 0 4 1128 1129 1130 1131 
		f 4 2792 2793 2794 -2730 
		mu 0 4 1985 2577 2578 2257 
		f 4 2795 -2784 -2724 -2791 
		mu 0 4 1129 1124 1127 1130 
		f 4 -2787 2796 -2793 -2726 
		mu 0 4 2195 2574 2577 1985 
		f 4 2797 2798 -2796 -2790 
		mu 0 4 1128 1132 1124 1129 
		f 4 2799 2800 -2794 -2797 
		mu 0 4 2574 2263 2578 2577 
		f 4 2801 -2782 -2799 -2427 
		mu 0 4 1133 1125 1124 1132 
		f 4 -2786 2802 -2430 -2800 
		mu 0 4 2574 2571 1618 2263 
		f 4 -2440 2803 -2788 -2802 
		mu 0 4 1133 1134 1106 1125 
		f 4 2804 -2443 -2803 -2789 
		mu 0 4 2430 2592 1618 2571 
		f 3 -2804 -2508 -2707 
		mu 0 3 1106 1134 1107 
		f 3 -2509 -2805 -2712 
		mu 0 3 2350 2592 2430 
		f 3 2805 2806 2807 
		mu 0 3 1135 1136 1137 
		f 3 -2808 2808 2809 
		mu 0 3 1135 1137 1138 
		f 3 -2809 2810 2811 
		mu 0 3 1139 1137 1140 
		f 3 -2811 -2807 2812 
		mu 0 3 1140 1137 1136 
		f 4 2813 -2813 2814 2815 
		mu 0 4 1141 1140 1136 1142 
		f 4 2816 -2812 -2814 2817 
		mu 0 4 1143 1139 1140 1141 
		f 4 2818 -2810 -2817 2819 
		mu 0 4 1144 1135 1138 1145 
		f 4 2820 -2815 -2806 -2819 
		mu 0 4 1144 1142 1136 1135 
		f 4 2821 2822 -2821 2823 
		mu 0 4 1146 1147 1142 1144 
		f 4 -2824 -2820 2824 2825 
		mu 0 4 1146 1144 1145 1148 
		f 4 -2825 -2818 2826 2827 
		mu 0 4 1149 1143 1141 1150 
		f 4 -2827 -2816 -2823 2828 
		mu 0 4 1150 1141 1142 1147 
		f 4 2829 -2829 2830 2831 
		mu 0 4 1151 1150 1147 1152 
		f 4 2832 -2828 -2830 2833 
		mu 0 4 1153 1149 1150 1151 
		f 4 2834 -2826 -2833 2835 
		mu 0 4 1154 1146 1148 1155 
		f 4 2836 -2831 -2822 -2835 
		mu 0 4 1154 1152 1147 1146 
		f 4 -2186 2837 2838 2839 
		mu 0 4 1156 1157 1158 1159 
		f 4 -2839 2840 2841 2842 
		mu 0 4 1160 1161 1162 1163 
		f 4 -2841 2843 2844 2845 
		mu 0 4 1162 1161 1164 1165 
		f 4 2846 -2844 -2838 -1944 
		mu 0 4 1166 1167 1158 1157 
		f 4 -2843 2847 2848 2849 
		mu 0 4 1160 1163 1168 1169 
		f 4 -2840 -2850 2850 -1994 
		mu 0 4 1156 1159 1170 1171 
		f 3 -2845 2851 2852 
		mu 0 3 1165 1164 1172 
		f 4 2853 -2852 -2847 -2201 
		mu 0 4 1173 1174 1167 1166 
		f 3 -2068 2854 -2854 
		mu 0 3 1173 1175 1174 
		f 4 2855 2856 -543 2857 
		mu 0 4 1176 1177 1178 1179 
		f 4 -2842 2858 -2856 2859 
		mu 0 4 1180 1181 1177 1176 
		f 4 -2846 2860 2861 -2859 
		mu 0 4 1181 1182 1183 1177 
		f 4 -2857 -2862 2862 -558 
		mu 0 4 1178 1177 1183 1184 
		f 4 -2853 2863 2864 -2861 
		mu 0 4 1182 1185 1186 1183 
		f 3 -2855 2865 -2864 
		mu 0 3 1185 1187 1186 
		f 4 -2865 2866 -542 -2863 
		mu 0 4 1183 1186 1188 1184 
		f 3 -2849 2867 2868 
		mu 0 3 1169 1168 1189 
		f 4 -2851 -2869 2869 -1985 
		mu 0 4 1171 1170 1190 1191 
		f 4 -2047 2870 2871 -2866 
		mu 0 4 1187 1192 1193 1186 
		f 4 -2872 2872 -540 -2867 
		mu 0 4 1186 1193 1194 1188 
		f 4 -2860 2873 2874 -2848 
		mu 0 4 1180 1176 1195 1196 
		f 4 -2858 -517 2875 -2874 
		mu 0 4 1176 1179 1197 1195 
		f 4 -2875 2876 2877 -2868 
		mu 0 4 1196 1195 1198 1199 
		f 4 2878 -2877 -2876 -518 
		mu 0 4 1200 1198 1195 1197 
		f 3 -2878 2879 2880 
		mu 0 3 1199 1198 1201 
		f 4 -509 -2873 -2880 -2879 
		mu 0 4 1200 1202 1201 1198 
		f 3 -2870 2881 -2012 
		mu 0 3 1191 1190 1203 
		f 3 -2881 -2871 -2882 
		mu 0 3 1199 1201 1204 
		f 3 -353 2882 -2010 
		mu 0 3 1205 1206 1207 
		f 4 2883 2884 2885 -2883 
		mu 0 4 1206 1208 1209 1207 
		f 3 -349 2886 -2884 
		mu 0 3 1210 1211 1212 
		f 3 -2066 2887 -341 
		mu 0 3 1213 1214 1215 
		f 4 -2198 2888 2889 -2888 
		mu 0 4 1214 1216 1217 1215 
		f 3 -2890 2890 -338 
		mu 0 3 1218 1219 1220 
		f 4 2891 2892 2893 -2885 
		mu 0 4 1208 1221 1222 1209 
		f 3 -346 -2892 -2887 
		mu 0 3 1211 1223 1212 
		f 4 2894 -2889 -1942 2895 
		mu 0 4 1224 1217 1216 1225 
		f 3 -2895 -335 -2891 
		mu 0 3 1219 1226 1220 
		f 4 -2896 -2185 -2893 -331 
		mu 0 4 1224 1225 1222 1221 
		f 3 2896 2897 2898 
		mu 0 3 1227 1228 1229 
		f 3 -2898 2899 2900 
		mu 0 3 1229 1228 1714 
		f 3 2901 -2899 2902 
		mu 0 3 1230 1227 1229 
		f 3 -2901 2903 2904 
		mu 0 3 1229 1714 1715 
		f 4 2905 2906 2907 2908 
		mu 0 4 1231 1232 1233 1234 
		f 4 2909 -2906 2910 2911 
		mu 0 4 1723 1232 1231 1601 
		f 3 -2909 -2903 2912 
		mu 0 3 1231 1234 1235 
		f 3 -2905 -2911 -2913 
		mu 0 3 1235 1601 1231 
		f 3 2913 -2908 2914 
		mu 0 3 1236 1237 1238 
		f 3 -2912 2915 2916 
		mu 0 3 1599 1586 1550 
		f 4 -2914 2917 2918 -2902 
		mu 0 4 1230 1236 1240 1227 ;
	setAttr ".fc[1500:1962]"
		f 4 2919 -2916 -2904 2920 
		mu 0 4 1669 1550 1715 1714 
		f 3 2921 2922 2923 
		mu 0 3 1236 1241 1242 
		f 3 2924 2925 2926 
		mu 0 3 1707 1548 1550 
		f 3 -2924 2927 -2918 
		mu 0 3 1236 1242 1240 
		f 3 2928 -2927 -2920 
		mu 0 3 1669 1707 1550 
		f 3 -2915 2929 -2922 
		mu 0 3 1236 1238 1241 
		f 3 2930 -2917 -2926 
		mu 0 3 1548 1599 1550 
		f 4 2931 -2923 2932 2933 
		mu 0 4 1244 1242 1241 1245 
		f 4 -2925 2934 2935 2936 
		mu 0 4 1548 1707 2093 2610 
		f 3 -2932 2937 2938 
		mu 0 3 1242 1244 1246 
		f 3 2939 -2935 2940 
		mu 0 3 2245 2093 1707 
		f 4 -2928 -2939 2941 2942 
		mu 0 4 1240 1242 1246 1247 
		f 4 -2941 -2929 2943 2944 
		mu 0 4 2245 1707 1669 1585 
		f 4 2945 2946 2947 2948 
		mu 0 4 1248 1249 1250 1251 
		f 4 2949 2950 2951 2952 
		mu 0 4 1719 1639 1623 2183 
		f 4 2953 2954 -2949 2955 
		mu 0 4 1252 1253 1254 1255 
		f 4 2956 -2954 2957 -2952 
		mu 0 4 1810 1253 1252 1642 
		f 4 2958 2959 -2946 -2955 
		mu 0 4 1256 1257 1249 1248 
		f 4 2960 -2959 -2957 -2951 
		mu 0 4 1639 1257 1256 1623 
		f 4 2961 2962 -2947 -2960 
		mu 0 4 1258 1259 1260 1261 
		f 4 2963 -2962 -2961 -2950 
		mu 0 4 1262 1259 1258 1619 
		f 4 2964 2965 2966 2967 
		mu 0 4 1263 1264 1265 1266 
		f 4 2968 -2965 2969 2970 
		mu 0 4 1557 1264 1263 1555 
		f 4 2971 2972 2973 -2966 
		mu 0 4 1264 1267 1268 1265 
		f 4 2974 -2972 -2969 2975 
		mu 0 4 1554 1267 1264 1553 
		f 4 2976 2977 2978 -2968 
		mu 0 4 1269 1270 1271 1272 
		f 4 2979 2980 -2970 -2979 
		mu 0 4 1271 2679 2669 1272 
		f 4 -2974 2981 -2977 -2967 
		mu 0 4 1265 1268 1273 1266 
		f 4 2982 -2976 -2971 -2981 
		mu 0 4 2670 2671 2673 2549 
		f 4 2983 -2934 2984 -2982 
		mu 0 4 1268 1244 1245 1273 
		f 4 -2936 2985 -2983 2986 
		mu 0 4 2550 2552 2674 2676 
		f 4 -2985 2987 2988 -2978 
		mu 0 4 1270 1274 1275 1271 
		f 4 2989 -2987 -2980 -2989 
		mu 0 4 1275 2678 2679 1271 
		f 4 2990 2991 -2984 -2973 
		mu 0 4 1267 1276 1244 1268 
		f 4 2992 -2991 -2975 -2986 
		mu 0 4 2680 1276 1267 2682 
		f 3 2993 2994 2995 
		mu 0 3 1277 1278 1279 
		f 3 2996 2997 -2994 
		mu 0 3 1277 1280 1278 
		f 3 -2996 2998 2999 
		mu 0 3 1277 1279 1281 
		f 3 -3000 3000 -2997 
		mu 0 3 1277 1282 1280 
		f 4 3001 3002 3003 -3001 
		mu 0 4 1282 1283 1284 1280 
		f 4 3004 3005 -3002 -2999 
		mu 0 4 1279 1285 1286 1281 
		f 4 -3004 3006 3007 -2998 
		mu 0 4 1280 1284 1287 1278 
		f 4 -3008 3008 -3005 -2995 
		mu 0 4 1278 1287 1285 1279 
		f 4 3009 3010 3011 -3009 
		mu 0 4 1287 1288 1289 1285 
		f 4 3012 -2943 -3010 -3007 
		mu 0 4 1284 1290 1288 1287 
		f 4 -3012 3013 3014 -3006 
		mu 0 4 1285 1289 1291 1286 
		f 4 -3015 -2919 -3013 -3003 
		mu 0 4 1283 1292 1290 1284 
		f 4 -2897 -3014 3015 3016 
		mu 0 4 1228 1227 1293 1294 
		f 4 3017 -2900 -3017 3018 
		mu 0 4 1987 1714 1228 1294 
		f 4 3019 -3016 -3011 3020 
		mu 0 4 1295 1294 1293 1247 
		f 4 -3019 -3020 3021 3022 
		mu 0 4 1987 1294 1295 1585 
		f 4 -2942 3023 3024 -3021 
		mu 0 4 1247 1246 1296 1295 
		f 4 3025 -2945 -3022 -3025 
		mu 0 4 1296 2245 1585 1295 
		f 4 3026 -3024 -2938 -2992 
		mu 0 4 1276 1296 1246 1244 
		f 4 -3026 -3027 -2993 -2940 
		mu 0 4 2245 1296 1276 2093 
		f 4 -2933 3027 3028 -2988 
		mu 0 4 1274 1297 1298 1275 
		f 4 3029 -2937 -2990 -3029 
		mu 0 4 1298 2150 2678 1275 
		f 4 3030 3031 3032 3033 
		mu 0 4 1299 1300 1301 1302 
		f 4 3034 3035 3036 3037 
		mu 0 4 1303 1304 1305 1306 
		f 3 3038 -3033 3039 
		mu 0 3 1307 1302 1301 
		f 3 3040 -3040 3041 
		mu 0 3 1308 1307 1301 
		f 3 3042 3043 -3039 
		mu 0 3 1307 1309 1302 
		f 3 -3041 3044 3045 
		mu 0 3 1307 1308 1310 
		f 3 -3043 3046 3047 
		mu 0 3 1309 1307 1311 
		f 3 3048 3049 3050 
		mu 0 3 1312 1313 1314 
		f 3 3051 3052 3053 
		mu 0 3 1314 1315 1316 
		f 3 3054 3055 -3051 
		mu 0 3 1314 1317 1312 
		f 3 3056 3057 -3054 
		mu 0 3 1316 1318 1314 
		f 3 -3058 3058 -3055 
		mu 0 3 1314 1318 1317 
		f 4 3059 -3059 3060 3061 
		mu 0 4 1319 1317 1318 1320 
		f 3 3062 -3037 3063 
		mu 0 3 1321 1306 1305 
		f 3 3064 -3064 3065 
		mu 0 3 1322 1321 1305 
		f 3 3066 3067 -3063 
		mu 0 3 1321 1323 1306 
		f 3 -3065 3068 3069 
		mu 0 3 1321 1322 1324 
		f 3 -3067 3070 3071 
		mu 0 3 1323 1321 1325 
		f 3 3072 3073 3074 
		mu 0 3 1326 1304 1327 
		f 3 3075 3076 3077 
		mu 0 3 1327 1328 1329 
		f 3 3078 3079 -3075 
		mu 0 3 1327 1330 1326 
		f 3 3080 3081 -3078 
		mu 0 3 1329 1331 1327 
		f 4 -3035 3082 3083 3084 
		mu 0 4 1304 1303 1332 1333 
		f 3 3085 3086 3087 
		mu 0 3 1334 1306 1335 
		f 4 -3031 3088 3089 3090 
		mu 0 4 1300 1299 1336 1334 
		f 4 -3086 -3090 3091 -3038 
		mu 0 4 1306 1334 1336 1303 
		f 4 3092 3093 3094 3095 
		mu 0 4 1328 1337 1338 1339 
		f 3 -3096 3096 3097 
		mu 0 3 1328 1339 1340 
		f 3 -3098 3098 -3077 
		mu 0 3 1328 1340 1329 
		f 3 -3099 3099 3100 
		mu 0 3 1329 1340 1341 
		f 3 3101 3102 3103 
		mu 0 3 1342 1343 1340 
		f 3 3104 3105 3106 
		mu 0 3 1320 1344 1342 
		f 3 3107 -3104 -3097 
		mu 0 3 1339 1342 1340 
		f 4 -3062 -3107 3108 3109 
		mu 0 4 1319 1320 1342 1345 
		f 3 3110 -3109 -3108 
		mu 0 3 1339 1345 1342 
		f 4 3111 3112 3113 3114 
		mu 0 4 1346 1347 1348 1349 
		f 4 3115 -3115 3116 -3089 
		mu 0 4 1299 1346 1349 1336 
		f 4 -3092 3117 3118 -3083 
		mu 0 4 1303 1336 1350 1332 
		f 3 3119 3120 -3113 
		mu 0 3 1347 1351 1348 
		f 3 3121 -3121 3122 
		mu 0 3 1352 1348 1351 
		f 3 3123 -3122 3124 
		mu 0 3 1353 1348 1352 
		f 3 -3084 3125 3126 
		mu 0 3 1333 1332 1354 
		f 4 -3117 3127 3128 -3118 
		mu 0 4 1336 1349 1355 1350 
		f 4 3129 -3128 -3114 -3124 
		mu 0 4 1353 1355 1349 1348 
		f 4 -3110 3130 3131 3132 
		mu 0 4 1319 1345 1356 1357 
		f 3 3133 3134 -3125 
		mu 0 3 1352 1358 1353 
		f 4 3135 3136 -3132 3137 
		mu 0 4 1358 1359 1357 1356 
		f 3 3138 -3134 3139 
		mu 0 3 1360 1358 1352 
		f 3 -3136 -3139 3140 
		mu 0 3 1359 1358 1360 
		f 4 3141 -3141 -2836 3142 
		mu 0 4 1361 1359 1360 1362 
		f 4 3143 -3143 -2834 -3120 
		mu 0 4 1347 1361 1362 1351 
		f 3 3144 3145 -2 
		mu 0 3 1 1363 2 
		f 3 3146 3147 -5 
		mu 0 3 2591 2626 1886 
		f 3 -3146 3148 -9 
		mu 0 3 2 1363 1556 
		f 3 3149 -3147 -13 
		mu 0 3 2288 2134 1968 
		f 3 3150 3151 3152 
		mu 0 3 1364 1365 1366 
		f 3 3153 3154 3155 
		mu 0 3 2169 2035 2244 
		f 3 3156 3157 3158 
		mu 0 3 2114 2513 2279 
		f 3 3159 3160 3161 
		mu 0 3 2280 2168 2352 
		f 3 3162 3163 -3152 
		mu 0 3 1365 1367 1366 
		f 3 3164 3165 -3154 
		mu 0 3 2169 2292 2035 
		f 3 3166 3167 -3157 
		mu 0 3 2114 2364 2513 
		f 3 3168 3169 -3161 
		mu 0 3 2168 2348 2352 
		f 3 3170 -3153 -3164 
		mu 0 3 1367 1368 1366 
		f 3 -3156 3171 -3165 
		mu 0 3 2169 2344 2292 
		f 3 -3159 3172 -3167 
		mu 0 3 2114 1941 2364 
		f 3 3173 -3162 -3170 
		mu 0 3 2348 2146 2352 
		f 3 3174 3175 3176 
		mu 0 3 1369 1370 1371 
		f 3 3177 3178 3179 
		mu 0 3 2327 2188 2137 
		f 3 3180 3181 3182 
		mu 0 3 2334 2333 2332 
		f 3 3183 3184 3185 
		mu 0 3 1964 1955 2339 
		f 3 3186 -3177 3187 
		mu 0 3 1372 1369 1371 
		f 3 -3180 3188 3189 
		mu 0 3 2327 2137 1960 
		f 3 -3183 3190 3191 
		mu 0 3 2334 2332 2240 
		f 3 3192 -3186 3193 
		mu 0 3 2234 1964 2339 
		f 3 3194 -3188 -3176 
		mu 0 3 1370 1373 1371 
		f 3 -3190 3195 -3178 
		mu 0 3 2327 2284 2188 
		f 3 -3192 3196 -3181 
		mu 0 3 2334 1725 2333 
		f 3 3197 -3194 -3185 
		mu 0 3 1955 2289 2339 
		f 4 3198 3199 3200 3201 
		mu 0 4 1374 1375 1376 1377 
		f 4 3202 3203 3204 3205 
		mu 0 4 2432 2492 1798 2531 
		f 4 3206 3207 3208 -3201 
		mu 0 4 1376 2558 2433 1377 
		f 4 3209 3210 -3206 3211 
		mu 0 4 1573 2540 2541 1615 
		f 3 3212 -3199 3213 
		mu 0 3 1378 1375 1374 
		f 3 -3204 3214 3215 
		mu 0 3 1798 2492 1757 
		f 3 -3208 3216 3217 
		mu 0 3 2433 2558 2459 
		f 3 3218 -3210 3219 
		mu 0 3 2486 2540 1573 
		f 3 3220 3221 -3213 
		mu 0 3 1378 1379 1375 
		f 3 3222 3223 -3215 
		mu 0 3 2492 2491 1757 
		f 3 3224 3225 -3217 
		mu 0 3 2558 2559 2459 
		f 3 3226 3227 -3219 
		mu 0 3 2486 2556 2540 
		f 4 3228 3229 -3200 -3222 
		mu 0 4 1379 1380 1376 1375 
		f 4 3230 3231 -3223 -3203 
		mu 0 4 2432 2489 2491 2492 
		f 4 -3230 3232 -3225 -3207 
		mu 0 4 1376 1380 2559 2558 
		f 4 3233 -3231 -3211 -3228 
		mu 0 4 2556 2555 2541 2540 
		f 4 -3202 3234 3235 3236 
		mu 0 4 1374 1377 1381 1382 
		f 4 3237 -3205 3238 3239 
		mu 0 4 2534 2531 1798 2321 
		f 4 -3235 -3209 3240 3241 
		mu 0 4 1381 1377 2433 1571 
		f 4 -3212 -3238 3242 3243 
		mu 0 4 1573 1615 1662 1665 
		f 3 3244 3245 3246 
		mu 0 3 1383 1384 1385 
		f 3 3247 3248 3249 
		mu 0 3 1716 1717 2710 
		f 3 3250 3251 3252 
		mu 0 3 2155 2529 2537 
		f 3 3253 3254 3255 
		mu 0 3 2661 2695 1681 
		f 4 3256 -1 -3246 3257 
		mu 0 4 1386 1387 1385 1384 
		f 4 -6 3258 3259 -3248 
		mu 0 4 1716 1746 1745 1717 
		f 4 -10 3260 3261 -3251 
		mu 0 4 2155 2538 2708 2529 
		f 4 3262 -12 -3255 3263 
		mu 0 4 2709 1680 1681 2695 
		f 4 3264 3265 -3145 -3257 
		mu 0 4 1386 1388 1389 1387 
		f 4 3266 3267 -3259 -3148 
		mu 0 4 1708 1737 1745 1746 
		f 4 -3266 3268 -3261 -3149 
		mu 0 4 1389 1388 2708 2538 
		f 4 3269 -3267 -3150 -3263 
		mu 0 4 2709 2686 2248 1680 
		f 4 -3237 3270 -3258 3271 
		mu 0 4 1390 1391 1386 1384 
		f 4 3272 -3239 3273 -3260 
		mu 0 4 1745 2346 1578 1717 
		f 4 3274 -3241 3275 -3262 
		mu 0 4 2708 2656 1728 2529 
		f 4 -3244 3276 -3264 3277 
		mu 0 4 2687 2528 2709 2695 
		f 4 3278 -3245 3279 3280 
		mu 0 4 1392 1384 1383 1393 
		f 4 -3249 3281 3282 3283 
		mu 0 4 2710 1717 2028 2037 
		f 4 -3252 3284 3285 3286 
		mu 0 4 2537 2529 1552 2268 
		f 4 3287 -3254 3288 3289 
		mu 0 4 1839 2695 2661 2107 
		f 4 -3214 -3272 -3279 3290 
		mu 0 4 1394 1390 1384 1392 
		f 4 -3274 -3216 3291 -3282 
		mu 0 4 1717 1578 1832 2028 
		f 4 -3276 -3218 3292 -3285 
		mu 0 4 2529 1728 1551 1552 
		f 4 -3220 -3278 -3288 3293 
		mu 0 4 2660 2687 2695 1839 
		f 4 -3236 3294 -3265 -3271 
		mu 0 4 1391 1395 1388 1386 
		f 4 3295 -3240 -3273 -3268 
		mu 0 4 1737 2706 2346 1745 
		f 4 -3295 -3242 -3275 -3269 
		mu 0 4 1388 1395 2656 2708 
		f 4 -3243 -3296 -3270 -3277 
		mu 0 4 2528 2684 2686 2709 
		f 3 -3280 3296 3297 
		mu 0 3 1393 1383 1396 
		f 3 3298 -3284 3299 
		mu 0 3 2335 2710 2037 
		f 3 3300 -3287 3301 
		mu 0 3 2354 2537 2268 
		f 3 -3289 3302 3303 
		mu 0 3 2107 2661 2294 
		f 4 3304 -3297 3305 3306 
		mu 0 4 1397 1396 1383 1398 
		f 4 -3299 3307 3308 3309 
		mu 0 4 2710 2335 1782 2127 
		f 4 -3301 3310 3311 3312 
		mu 0 4 2537 2354 1574 2147 
		f 4 3313 -3303 3314 3315 
		mu 0 4 1956 2294 2661 2286 
		f 3 3316 -3307 3317 
		mu 0 3 1399 1397 1398 
		f 3 -3309 3318 3319 
		mu 0 3 2127 1782 2714 
		f 3 -3312 3320 3321 
		mu 0 3 2147 1574 2271 
		f 3 3322 -3316 3323 
		mu 0 3 2272 1956 2286 
		f 3 3324 3325 -3305 
		mu 0 3 1397 1400 1396 
		f 3 3326 3327 -3308 
		mu 0 3 2335 1807 1782 
		f 3 3328 3329 -3311 
		mu 0 3 2354 1830 1574 
		f 3 3330 3331 -3314 
		mu 0 3 1956 1940 2294 
		f 4 3332 -3326 3333 3334 
		mu 0 4 1401 1396 1400 1402 
		f 4 -3327 3335 3336 3337 
		mu 0 4 1807 2335 2413 2138 
		f 4 -3329 3338 3339 3340 
		mu 0 4 1830 2354 1885 2360 
		f 4 3341 -3332 3342 3343 
		mu 0 4 2728 2294 1940 2122 
		f 3 -3334 3344 3345 
		mu 0 3 1402 1400 1403 
		f 3 3346 -3338 3347 
		mu 0 3 1564 1807 2138 
		f 3 3348 -3341 3349 
		mu 0 3 2055 1830 2360 
		f 3 -3343 3350 3351 
		mu 0 3 2122 1940 2741 
		f 3 3352 -3281 3353 
		mu 0 3 1404 1392 1393 
		f 3 -3283 3354 3355 
		mu 0 3 2037 2028 2223 
		f 3 -3286 3356 3357 
		mu 0 3 2268 1552 2500 
		f 3 3358 -3290 3359 
		mu 0 3 1761 1839 2107 
		f 4 3360 -3354 -3298 -3333 
		mu 0 4 1401 1404 1393 1396 
		f 4 -3356 3361 -3336 -3300 
		mu 0 4 2037 2223 2413 2335 
		f 4 -3358 3362 -3339 -3302 
		mu 0 4 2268 2500 1885 2354 
		f 4 3363 -3360 -3304 -3342 
		mu 0 4 2728 1761 2107 2294 
		f 4 3364 -3346 3365 3366 
		mu 0 4 1405 1402 1403 1406 
		f 4 -3348 3367 3368 3369 
		mu 0 4 1564 2138 1504 1491 
		f 4 -3350 3370 3371 3372 
		mu 0 4 2055 2360 1497 1487 
		f 4 3373 -3352 3374 3375 
		mu 0 4 1495 2122 2741 1486 
		f 4 -3366 -3345 3376 3377 
		mu 0 4 1406 1403 1400 1407 
		f 4 -3347 -3370 3378 3379 
		mu 0 4 1807 1564 1491 1492 
		f 4 -3349 -3373 3380 3381 
		mu 0 4 1830 2055 1487 1490 
		f 4 -3375 -3351 3382 3383 
		mu 0 4 1486 2741 1940 1485 
		f 4 -3377 -3325 3384 3385 
		mu 0 4 1407 1400 1397 1408 
		f 4 -3328 -3380 3386 3387 
		mu 0 4 1782 1807 1492 1579 
		f 4 -3330 -3382 3388 3389 
		mu 0 4 1574 1830 1490 1568 
		f 4 -3383 -3331 3390 3391 
		mu 0 4 1485 1940 1956 1561 
		f 3 3392 3393 3394 
		mu 0 3 1409 1410 1411 
		f 3 3395 3396 3397 
		mu 0 3 1650 1727 1649 
		f 3 3398 3399 3400 
		mu 0 3 1633 1724 1632 
		f 3 3401 3402 3403 
		mu 0 3 1617 1721 1613 
		f 3 3404 -3393 3405 
		mu 0 3 1412 1410 1409 
		f 3 -3397 3406 3407 
		mu 0 3 1649 1727 1691 
		f 3 -3400 3408 3409 
		mu 0 3 1632 1724 1677 
		f 3 3410 -3402 3411 
		mu 0 3 1657 1721 1617 
		f 3 3412 -3405 3413 
		mu 0 3 1413 1410 1412 
		f 3 -3407 3414 3415 
		mu 0 3 1691 1727 1703 
		f 3 -3409 3416 3417 
		mu 0 3 1677 1724 1699 
		f 3 3418 -3411 3419 
		mu 0 3 1695 1721 1657 
		f 3 -3394 -3413 3420 
		mu 0 3 1411 1410 1414 
		f 3 -3415 -3396 3421 
		mu 0 3 1718 1727 1650 
		f 3 -3417 -3399 3422 
		mu 0 3 1712 1724 1633 
		f 3 -3403 -3419 3423 
		mu 0 3 1613 1721 1706 
		f 4 3424 -3421 3425 3426 
		mu 0 4 1415 1411 1414 1416 
		f 4 -3422 3427 3428 3429 
		mu 0 4 1718 1650 1637 1713 
		f 4 -3423 3430 3431 3432 
		mu 0 4 1712 1633 1628 1709 
		f 4 3433 -3424 3434 3435 
		mu 0 4 1604 1613 1706 1705 
		f 4 -3426 -3414 3436 3437 
		mu 0 4 1417 1413 1412 1418 
		f 4 -3416 -3430 3438 3439 
		mu 0 4 1691 1703 1702 1682 
		f 4 -3418 -3433 3440 3441 
		mu 0 4 1677 1699 1698 1670 
		f 4 -3435 -3420 3442 3443 
		mu 0 4 1696 1695 1657 1658 
		f 4 -3437 -3406 3444 -3317 
		mu 0 4 1418 1412 1409 1419 
		f 4 -3408 -3440 -3319 3445 
		mu 0 4 1649 1691 1682 1638 
		f 4 -3410 -3442 -3321 3446 
		mu 0 4 1632 1677 1670 1631 
		f 4 -3443 -3412 3447 -3323 
		mu 0 4 1658 1657 1617 1627 
		f 4 -3445 -3395 -3425 -3385 
		mu 0 4 1419 1409 1411 1415 
		f 4 -3398 -3446 -3388 -3428 
		mu 0 4 1650 1649 1638 1637 
		f 4 -3401 -3447 -3390 -3431 
		mu 0 4 1633 1632 1631 1628 
		f 4 -3448 -3404 -3434 -3391 
		mu 0 4 1627 1617 1613 1604 
		f 4 3448 -3427 3449 3450 
		mu 0 4 1420 1408 1421 1422 
		f 4 -3429 3451 3452 3453 
		mu 0 4 1602 1579 1576 1598 
		f 4 -3432 3454 -3451 3455 
		mu 0 4 1589 1568 1420 1422 
		f 4 3456 -3436 3457 -3453 
		mu 0 4 1560 1561 1581 1580 
		f 4 3458 -3386 -3449 3459 
		mu 0 4 1423 1407 1408 1420 
		f 4 -3387 3460 3461 -3452 
		mu 0 4 1579 1492 1493 1576 
		f 4 -3389 3462 -3460 -3455 
		mu 0 4 1568 1490 1423 1420 
		f 4 3463 -3392 -3457 -3462 
		mu 0 4 1484 1485 1561 1560 
		f 3 3464 3465 3466 
		mu 0 3 1424 1406 1423 
		f 3 3467 3468 3469 
		mu 0 3 1493 1491 1500 
		f 3 3470 3471 -3467 
		mu 0 3 1423 1487 1424 
		f 3 3472 3473 -3470 
		mu 0 3 1496 1486 1484 
		f 3 3474 3475 3476 
		mu 0 3 1425 1405 1424 
		f 3 3477 3478 3479 
		mu 0 3 1500 1504 1489 
		f 3 3480 3481 -3477 
		mu 0 3 1424 1497 1425 
		f 3 3482 3483 -3480 
		mu 0 3 1515 1495 1496 
		f 3 -3476 -3367 -3465 
		mu 0 3 1424 1405 1406 
		f 3 -3369 -3478 -3469 
		mu 0 3 1491 1504 1500 
		f 3 -3372 -3481 -3472 
		mu 0 3 1487 1497 1424 
		f 3 -3484 -3376 -3473 
		mu 0 3 1496 1495 1486 
		f 3 -3378 -3459 -3466 
		mu 0 3 1406 1407 1423 
		f 3 -3461 -3379 -3468 
		mu 0 3 1493 1492 1491 
		f 3 -3463 -3381 -3471 
		mu 0 3 1423 1490 1487 
		f 3 -3384 -3464 -3474 
		mu 0 3 1486 1485 1484 
		f 4 3484 3485 3486 -3229 
		mu 0 4 1426 1427 1428 1429 
		f 4 3487 3488 -3232 3489 
		mu 0 4 1483 1482 1481 1480 
		f 4 3490 3491 -3233 -3487 
		mu 0 4 1428 47 44 1429 
		f 4 3492 3493 -3490 -3234 
		mu 0 4 27 1533 1534 1733 
		f 3 3494 -3485 3495 
		mu 0 3 1430 1427 1426 
		f 3 -3489 3496 3497 
		mu 0 3 1481 1482 1643 
		f 3 -3492 3498 3499 
		mu 0 3 44 47 1625 
		f 3 3500 -3493 3501 
		mu 0 3 1622 1533 27 
		f 4 3502 3503 3504 -3495 
		mu 0 4 1430 1431 1432 1427 
		f 4 3505 3506 -3497 3507 
		mu 0 4 1611 1644 1643 1482 
		f 4 3508 3509 -3499 3510 
		mu 0 4 1572 1626 1625 47 
		f 4 3511 3512 3513 -3501 
		mu 0 4 1622 1621 1570 1533 
		f 4 3514 3515 -3486 -3505 
		mu 0 4 1432 1433 1428 1427 
		f 4 3516 3517 -3508 -3488 
		mu 0 4 1483 1614 1611 1482 
		f 4 -3516 3518 -3511 -3491 
		mu 0 4 1428 1433 1572 47 
		f 4 3519 -3517 -3494 -3514 
		mu 0 4 1570 1536 1534 1533 
		f 3 3520 -3475 3521 
		mu 0 3 1434 1405 1425 
		f 3 -3479 3522 3523 
		mu 0 3 1489 1504 1488 
		f 3 -3482 3524 3525 
		mu 0 3 1425 1497 1518 
		f 3 3526 -3483 3527 
		mu 0 3 1517 1495 1515 
		f 4 -3522 3528 3529 3530 
		mu 0 4 1434 1425 1435 1436 
		f 4 3531 -3524 3532 3533 
		mu 0 4 1514 1489 1488 25 
		f 4 -3529 -3526 3534 3535 
		mu 0 4 1435 1425 1518 2601 
		f 4 -3528 -3532 3536 3537 
		mu 0 4 1517 1515 2247 2617 
		f 4 3538 -3530 3539 -3515 
		mu 0 4 1432 1436 1435 1433 
		f 4 -3534 3540 -3518 3541 
		mu 0 4 1514 25 1611 1614 
		f 4 -3536 3542 -3519 -3540 
		mu 0 4 1435 2601 1572 1433 
		f 4 3543 -3537 -3542 -3520 
		mu 0 4 1570 2617 2247 1536 
		f 3 -3531 3544 3545 
		mu 0 3 1434 1436 1437 
		f 3 3546 -3533 3547 
		mu 0 3 1979 25 1488 
		f 3 3548 -3535 3549 
		mu 0 3 2623 2601 1518 
		f 3 -3538 3550 3551 
		mu 0 3 1517 2617 1883 
		f 3 -3545 3552 3553 
		mu 0 3 1437 1436 1438 
		f 3 3554 -3547 3555 
		mu 0 3 1531 25 1979 
		f 3 3556 -3549 3557 
		mu 0 3 2358 2601 2623 
		f 3 -3551 3558 3559 
		mu 0 3 1883 2617 1538 
		f 3 -3553 -3539 3560 
		mu 0 3 1438 1436 1432 
		f 3 -3541 -3555 3561 
		mu 0 3 1611 25 1531 
		f 3 -3543 -3557 3562 
		mu 0 3 1572 2601 2358 
		f 3 -3559 -3544 3563 
		mu 0 3 1538 2617 1570 
		f 3 -3561 -3504 3564 
		mu 0 3 1438 1432 1431 
		f 3 -3506 -3562 3565 
		mu 0 3 1644 1611 1531 
		f 3 -3509 -3563 3566 
		mu 0 3 1626 1572 2358 
		f 3 -3564 -3513 3567 
		mu 0 3 1538 1570 1621 
		f 3 -3554 3568 3569 
		mu 0 3 1437 1438 1439 
		f 3 3570 -3556 3571 
		mu 0 3 1917 1531 1979 
		f 3 3572 -3558 3573 
		mu 0 3 2255 2358 2623 
		f 3 -3560 3574 3575 
		mu 0 3 1883 1538 2156 
		f 4 -3569 -3565 -3503 3576 
		mu 0 4 1439 1438 1431 1430 
		f 4 -3566 -3571 3577 -3507 
		mu 0 4 1644 1531 1917 1643 
		f 4 -3567 -3573 3578 -3510 
		mu 0 4 1626 2358 2255 1625 
		f 4 -3575 -3568 -3512 3579 
		mu 0 4 2156 1538 1621 1622 
		f 4 -3577 -3496 -3221 3580 
		mu 0 4 1439 1430 1426 1394 
		f 4 -3498 -3578 3581 -3224 
		mu 0 4 1481 1643 1917 1832 
		f 4 -3500 -3579 3582 -3226 
		mu 0 4 44 1625 2255 1551 
		f 4 -3580 -3502 -3227 3583 
		mu 0 4 2156 1622 27 2660 
		f 4 3584 -3581 -3291 -3353 
		mu 0 4 1404 1439 1394 1392 
		f 4 -3582 3585 -3355 -3292 
		mu 0 4 1832 1917 2223 2028 
		f 4 -3583 3586 -3357 -3293 
		mu 0 4 1551 2255 2500 1552 
		f 4 3587 -3584 -3294 -3359 
		mu 0 4 1761 2156 2660 1839 
		f 3 -3570 -3585 3588 
		mu 0 3 1437 1439 1404 
		f 3 -3586 -3572 3589 
		mu 0 3 2223 1917 1979 
		f 3 -3587 -3574 3590 
		mu 0 3 2500 2255 2623 
		f 3 -3576 -3588 3591 
		mu 0 3 1883 2156 1761 
		f 4 -3546 -3589 -3361 3592 
		mu 0 4 1434 1437 1404 1401 
		f 4 -3590 -3548 3593 -3362 
		mu 0 4 2223 1979 1488 2413 
		f 4 -3591 -3550 3594 -3363 
		mu 0 4 2500 2623 1518 1885 
		f 4 -3552 -3592 -3364 3595 
		mu 0 4 1517 1883 1761 2728 
		f 4 -3521 -3593 -3335 -3365 
		mu 0 4 1405 1434 1401 1402 
		f 4 -3594 -3523 -3368 -3337 
		mu 0 4 2413 1488 1504 2138 
		f 4 -3595 -3525 -3371 -3340 
		mu 0 4 1885 1518 1497 2360 
		f 4 -3527 -3596 -3344 -3374 
		mu 0 4 1495 1517 2728 2122 
		f 4 3596 -2956 3597 3598 
		mu 0 4 1440 1252 1255 1441 
		f 4 -3597 3599 3600 -2958 
		mu 0 4 1252 1440 2324 1642 
		f 4 3601 3602 -3598 -2948 
		mu 0 4 1250 1442 1443 1251 
		f 4 3603 -2953 -3601 3604 
		mu 0 4 2086 1719 2183 2065 
		f 4 3605 3606 -3602 -2963 
		mu 0 4 1259 1444 1445 1260 
		f 4 -3606 -2964 -3604 3607 
		mu 0 4 1444 1259 1262 1446 
		f 4 3608 3609 -2921 3610 
		mu 0 4 2170 2186 2180 1870 
		f 4 3611 -3611 -3018 3612 
		mu 0 4 2082 2083 2265 2266 
		f 4 3613 3614 -2944 -3610 
		mu 0 4 2186 2202 2205 2180 
		f 4 3615 -3613 -3023 -3615 
		mu 0 4 2202 2082 2266 2205 
		f 4 3616 3617 -3616 3618 
		mu 0 4 2159 2214 2082 2202 
		f 4 3619 -3619 -3614 3620 
		mu 0 4 1993 2159 2202 2186 
		f 4 3621 3622 -3612 -3618 
		mu 0 4 2214 2215 2083 2082 
		f 4 3623 -3621 -3609 -3623 
		mu 0 4 2078 1993 2186 2170 
		f 3 3624 -3624 3625 
		mu 0 3 2264 1993 2078 
		f 3 -3626 -3622 3626 
		mu 0 3 2264 2215 2214 
		f 3 3627 -3620 -3625 
		mu 0 3 2264 2159 1993 
		f 3 -3627 -3617 -3628 
		mu 0 3 2264 2214 2159 
		f 4 3628 3629 3630 -2907 
		mu 0 4 1232 1447 1448 1233 
		f 4 3631 -3599 3632 -3630 
		mu 0 4 1447 1440 1441 1448 
		f 4 -3632 3633 3634 -3600 
		mu 0 4 1440 1447 2162 2324 
		f 4 -3629 -2910 3635 -3634 
		mu 0 4 1447 1232 1723 2162 
		f 4 3636 3637 -3633 -3603 
		mu 0 4 1442 1449 1450 1443 
		f 4 3638 -2930 -3631 -3638 
		mu 0 4 1449 1241 1238 1450 
		f 4 3639 3640 -3636 -2931 
		mu 0 4 1548 2087 2158 1599 
		f 4 3641 -3605 -3635 -3641 
		mu 0 4 2087 2086 2065 2158 
		f 4 3642 3643 -3637 -3607 
		mu 0 4 1444 1451 1452 1445 
		f 4 3644 -3028 -3639 -3644 
		mu 0 4 1451 1298 1297 1452 
		f 4 -3645 3645 -3640 -3030 
		mu 0 4 1298 1451 2161 2150 
		f 4 -3643 -3608 -3642 -3646 
		mu 0 4 1451 1444 1446 2161 
		f 3 3646 3647 3648 
		mu 0 3 1453 1454 1455 
		f 3 3649 3650 3651 
		mu 0 3 2081 2295 2164 
		f 3 3652 3653 -3074 
		mu 0 3 1304 1456 1327 
		f 3 3654 -3076 -3654 
		mu 0 3 1456 1328 1327 
		f 4 3655 3656 -3653 -3085 
		mu 0 4 1333 1457 1456 1304 
		f 4 3657 -3093 -3655 -3657 
		mu 0 4 1457 1337 1328 1456 
		f 4 3658 3659 3660 3661 
		mu 0 4 1458 1459 1460 1354 
		f 4 3662 3663 3664 -3660 
		mu 0 4 1459 1461 1338 1460 
		f 4 -3661 3665 -3656 -3127 
		mu 0 4 1354 1460 1457 1333 
		f 4 -3665 -3094 -3658 -3666 
		mu 0 4 1460 1338 1337 1457 
		f 3 -3663 3666 3667 
		mu 0 3 1461 1459 1353 
		f 3 -3659 3668 -3667 
		mu 0 3 1459 1458 1353 
		f 4 3669 3670 3671 -3662 
		mu 0 4 1354 1462 1463 1458 
		f 4 3672 -3129 3673 -3671 
		mu 0 4 1462 1350 1355 1463 
		f 3 -3673 3674 -3119 
		mu 0 3 1350 1462 1332 
		f 3 -3670 -3126 -3675 
		mu 0 3 1462 1354 1332 
		f 3 -3672 3675 -3669 
		mu 0 3 1458 1463 1353 
		f 3 -3674 -3130 -3676 
		mu 0 3 1463 1355 1353 
		f 3 3676 3677 -3095 
		mu 0 3 1338 1464 1339 
		f 3 3678 -3111 -3678 
		mu 0 3 1464 1345 1339 
		f 4 -3679 3679 3680 -3131 
		mu 0 4 1345 1464 1465 1356 
		f 4 -3677 -3664 3681 -3680 
		mu 0 4 1464 1338 1461 1465 
		f 3 3682 -3135 3683 
		mu 0 3 1465 1353 1358 
		f 3 -3681 -3684 -3138 
		mu 0 3 1356 1465 1358 
		f 3 -3682 -3668 -3683 
		mu 0 3 1465 1461 1353 
		f 4 -2106 3684 -2233 3685 
		mu 0 4 1466 1467 1468 1469 
		f 3 -2110 -2236 -3685 
		mu 0 3 2336 1952 2096 
		f 4 -2084 -3686 -2230 3686 
		mu 0 4 2097 2064 2337 2061 
		f 4 -3687 -2227 -2074 -2078 
		mu 0 4 2068 2740 2744 2050 
		f 3 -1484 -1436 3687 
		mu 0 3 2060 2625 2298 
		f 3 -3688 -1432 -987 
		mu 0 3 2060 2298 2191 
		f 3 3688 -529 3689 
		mu 0 3 244 342 337 
		f 3 -3690 -525 -455 
		mu 0 3 244 337 290 
		f 3 -3691 1795 1796 
		mu 0 3 2444 1947 2458 
		f 3 -3692 1791 1792 
		mu 0 3 849 847 848 
		f 3 1803 -3693 -1791 
		mu 0 3 846 852 847 
		f 3 1806 -3694 1808 
		mu 0 3 1762 2090 1947 
		f 3 1827 -3695 -1805 
		mu 0 3 852 855 853 
		f 3 -1820 -3696 1830 
		mu 0 3 2366 2367 1762 
		f 3 1817 -3697 1820 
		mu 0 3 2343 2194 2366 
		f 3 1813 -3698 1816 
		mu 0 3 855 856 858 
		f 4 -3701 3699 133 134 
		mu 0 4 2758 2757 76 77 
		f 4 -3702 -135 139 140 
		mu 0 4 2759 2758 77 82 
		f 4 -3704 3702 266 267 
		mu 0 4 2761 2760 129 130 
		f 4 -3706 -268 268 -3705 
		mu 0 4 2762 2761 130 127 
		f 4 -3707 3704 263 264 
		mu 0 4 2763 2762 127 128 
		f 4 -3719 -3720 260 3737 
		mu 0 4 2787 2774 2773 2786 
		f 4 -3722 3718 3739 3738 
		mu 0 4 2775 2774 2787 2788 
		f 4 256 -3723 -3739 3740 
		mu 0 4 2789 2776 2775 2788 
		f 4 142 -3726 3723 -133 
		mu 0 4 75 2779 2777 1563 
		f 4 144 -3728 -143 -130 
		mu 0 4 65 2780 2779 75 
		f 4 -3729 -3730 -145 -118 
		mu 0 4 64 2781 2780 65 
		f 4 -3731 -3732 3728 -126 
		mu 0 4 71 2783 2781 64 
		f 4 145 -3734 3730 -128 
		mu 0 4 1559 2784 2782 73 
		f 4 -3724 -3735 -146 -122 
		mu 0 4 68 2778 2785 69 
		f 4 -3737 -3738 3735 261 
		mu 0 4 124 2787 2786 126 
		f 4 254 -3740 3736 255 
		mu 0 4 123 2788 2787 124 
		f 4 257 -3741 -255 258 
		mu 0 4 125 2789 2788 123 
		f 4 -3744 3741 -39 -32 
		mu 0 4 15 2790 22 18 
		f 4 -3745 -3746 -45 45 
		mu 0 4 8 2792 2790 24 
		f 4 18 -3747 3744 20 
		mu 0 4 9 2791 2792 8 
		f 4 -3749 -141 141 -3748 
		mu 0 4 2794 2759 82 1569 
		f 4 -3751 3747 135 136 
		mu 0 4 2795 2793 78 79 
		f 4 -3753 -137 138 -3752 
		mu 0 4 2798 2796 1566 81 
		f 4 -3754 3751 137 -3700 
		mu 0 4 2757 2797 80 76 
		f 3 -3755 2156 -1751 
		mu 0 3 824 952 825 
		f 3 -3756 -1884 2157 
		mu 0 3 2701 2131 2359 
		f 3 -3757 -1876 2144 
		mu 0 3 2027 2355 2131 
		f 3 -2136 2141 -3758 
		mu 0 3 830 947 948 
		f 3 2136 -3759 -1858 
		mu 0 3 2715 2103 2258 
		f 3 2134 -3760 -2096 
		mu 0 3 923 945 838 
		f 3 -3761 2096 -1856 
		mu 0 3 2258 2241 2303 
		f 3 2094 -3762 -1777 
		mu 0 3 834 925 838 
		f 3 -2157 -3763 -1757 
		mu 0 3 825 952 828 
		f 3 2159 -3764 -2128 
		mu 0 3 2345 2701 2124 
		f 3 2102 -3765 2103 
		mu 0 3 2227 1582 2241 
		f 3 -1769 -3766 -2095 
		mu 0 3 834 832 925 ;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".bw" 3;
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -131.04494104868184 66.546716321958172 13.446075109420988 ;
	setAttr ".r" -type "double3" 164.06164727053255 -91.000000000486992 -180.00000000000017 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".fcp" 1000000;
	setAttr ".coi" 141.63939961051722;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.1964927388464415 32.568470917403737 -22.264475164013042 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -9.876330054413156 51.566233172542042 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 23.269052041658572;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode objectSet -n "FK_Control";
	setAttr ".ihi" 0;
	setAttr -s 55 ".dsm";
createNode objectSet -n "IK_Control";
	setAttr ".ihi" 0;
	setAttr -s 8 ".dsm";
createNode objectSet -n "SkeSkinning";
	setAttr ".ihi" 0;
	setAttr -s 57 ".dsm";
createNode ikRPsolver -n "ikRPsolver";
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion4";
	setAttr ".cf" 0.017453292519943295;
createNode dagPose -n "bindPose1";
	setAttr -s 61 ".wm";
	setAttr ".wm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wm[61]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 62 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 -2.2204460492503131e-016 2.4651903288156619e-032
		 2.2204460492503131e-016 0 -7.5367696436854939e-016 42.443413265848903 1.5629999999999973 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.5 0.49999999999999989 0.5 0.50000000000000011 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 -2.2204460492503131e-016 2.4651903288156619e-032
		 2.2204460492503131e-016 0 1.4210854715202004e-014 2.6645352591003757e-015 -3.1554436208840472e-030 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 -6.3305052449908956 0.00023657825694622936
		 -3.6985268363424657 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.050612504707316798 0.66037807976664675 -0.74746574271365962 0.051323772274506879 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 7.5052833736605412e-031 -1.3002769988578649e-015
		 2.6607368480810523e-031 0 17.05729979350297 8.8817841970012523e-015 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.10661115407226027 0 0.99430079041876496 1 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 3.3090198470198124e-008 -1.110223098100157e-016
		 -6.2831853071795862 0 15.387533188566763 8.8817841970012523e-015 -8.8817841970012523e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.47977873778529395 0 0.87738951598999071 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 6.4401306079999987 0 -4.4408920985006262e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.071689258283472598 0.37188966180985727 -0.028820419143976209 0.92505573514577011 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 -6.3305052449908956 0.00023657825694622936
		 3.6989999999999998 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.050609753193857063 -0.66037643247362166 -0.74746756704692763 0.051321111913285852 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 17.057299793502967 1.0658141036401503e-014
		 4.4408920985006262e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.10661115427525991 0 0.99430079039699892 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 1.1102230246251565e-016 -5.5511151231257821e-017
		 -8.3266726846886741e-017 0 15.387533188566767 8.8817841970012523e-015 -6.6613381477509392e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.479779472380779 0 0.8773891142942345 1 1
		 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 6.4401306079999987 1.7763568394002505e-015
		 4.4408920985006262e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.071690783084932569 -0.37188885222948898 -0.028820963470878688 0.92505592548381244 1
		 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 6.8010460569999935 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.21204975108630292 0.97725887208264162 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.3018069800000021 0
		 1.5777218104420236e-030 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.15177837367788347 0.98841455133142231 1
		 1 1 no;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.6498000249999976 0
		 1.5777218104420236e-030 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.30070579950427317 0.95371695074822693 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.1180013589999973 3.5527136788005009e-015
		 1.5777218104420236e-030 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.58336507119999936 -3.9188019369999978
		 1.5777218104420236e-030 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.98760429500439273 0.15696418855546779 1
		 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0 0 0 0 3.8757084480000046 -7.1054273576010019e-015
		 3.1554436208840472e-030 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.0574460889999999 -0.78579945230000003
		 -3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.44423590130971424 0.55014040391301555 -0.49167492387330719 0.50818871416447997 1
		 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 -4.0473749934577546e-008 1.5083544672117571e-005
		 -5.0151237236918214e-007 0 3.0350587520020729 2.1316282072803006e-014 -4.1054937227613664e-011 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.0070630958785156787 -0.027310965323981886 -0.25028036100923495 0.9677623493103924 1
		 1 1 yes;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 1.063205513522339e-019 -2.2187916232086935e-005
		 -7.3357066157904162e-019 0 9.1797314974452675 -2.1316282072803006e-014 -1.3322676295501878e-015 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.075796697208104544 0 0.99712329262350652 1
		 1 1 yes;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 7.0802338251524496e-006 1.1950244863452527e-005
		 1.1821881132478402e-006 0 10.379765482679858 4.9737991503207013e-014 1.3322676295501878e-015 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[24]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.116683029999999 -0.15835380100000407
		 2.1186516959999997 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.012723678592859913 -0.12550553132194417 -0.13935717642539813 0.98217414290409943 1
		 1 1 yes;
	setAttr ".xm[25]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.0954408360000016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.10509073384932788 0.99446263763854381 1
		 1 1 yes;
	setAttr ".xm[26]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.9712466183999986 0
		 -8.8817841970012523e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.1105661771639832 0.9938687642074997 1
		 1 1 yes;
	setAttr ".xm[27]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.5268912380000002 -0.23100727190000001
		 1.208855824 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.0069406535448784702 -0.047570896417588056 -0.16940943783442577 0.98437253086167587 1
		 1 1 yes;
	setAttr ".xm[28]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.7749664370000033 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.12560292940407231 0.99208059356340383 1
		 1 1 yes;
	setAttr ".xm[29]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.3103468960000022 0
		 4.4408920985006262e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.14817225198256126 0.98896156838494809 1
		 1 1 yes;
	setAttr ".xm[30]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.7127924869999998 -0.16572755550000001
		 0.12032636350000001 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.0085580761440268126 -0.0093079492724350427 -0.16570030875667907 0.9860950913025559 1
		 1 1 yes;
	setAttr ".xm[31]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.7992489470000024 -7.1054273576010019e-015
		 -2.2204460492503131e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.099114383376220833 0.99507604684664752 1
		 1 1 yes;
	setAttr ".xm[32]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.2793347980000007 -7.1054273576010019e-015
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.087155742747658166 0.99619469809174555 1
		 1 1 yes;
	setAttr ".xm[33]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.4562871829999997 -0.39650271770000001
		 -0.90234609249999997 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.012974733490115822 0.054444402323990844 -0.14107960951692156 0.98841489624802648 1
		 1 1 yes;
	setAttr ".xm[34]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.0469344890000016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.13444896918604174 0.99092051885346022 1
		 1 1 yes;
	setAttr ".xm[35]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.0390000000000015 0
		 -8.8817841970012523e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[36]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.0133297020000001 -1.075693638
		 -1.7597509499999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.15494021952609197 0.096624143711421862 -0.24355153030219759 0.95254393878324528 1
		 1 1 yes;
	setAttr ".xm[37]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.8470298950000021 0
		 -3.5527136788005009e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.13052619222005157 0 0.99144486137381038 1
		 1 1 yes;
	setAttr ".xm[38]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.0574460889999999 -0.78579945230000003
		 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.44423622707217403 -0.55014014083384244 -0.49167462329435685 0.50818900500537423 1
		 1 1 yes;
	setAttr ".xm[39]" -type "matrix" "xform" 1 1 1 1.486210388004483e-007 -1.7579547429358906e-005
		 -1.8416002304104913e-006 0 3.0350587520020711 1.4210854715202004e-014 -4.1055381316823514e-011 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.0070630958785156631 0.027310965323981799 -0.25028036100923517 0.9677623493103924 1
		 1 1 yes;
	setAttr ".xm[40]" -type "matrix" "xform" 1 1 1 -1.2887343220314476e-019 2.689445156084431e-005
		 -8.8917681219081285e-019 0 9.1797314974452533 -7.1054273576010019e-015 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.075796697208104544 0 0.99712329262350652 1
		 1 1 yes;
	setAttr ".xm[41]" -type "matrix" "xform" 1 1 1 -7.0966419686611987e-006 -1.4160451871388101e-005
		 1.3448712734016934e-006 0 10.379765482679854 5.6843418860808015e-014 8.8817841970012523e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[42]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.1166830299999919 -0.15835380099999696
		 -2.1189999999999998 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.012722915734785348 0.1255020135226419 -0.13935722669150891 0.98217459516412831 1
		 1 1 yes;
	setAttr ".xm[43]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.0950000000000006 0
		 -8.8817841970012523e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.10509073384932788 0.99446263763854381 1
		 1 1 yes;
	setAttr ".xm[44]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.97099999999999298 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.11056617716398325 0.9938687642074997 1
		 1 1 yes;
	setAttr ".xm[45]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.5268912380000002 -0.23100727190000001
		 -1.2090000000000001 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.0069422476997880317 0.047568339176875092 -0.16940955442001779 0.9843726231341664 1
		 1 1 yes;
	setAttr ".xm[46]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.774966437000014 0 4.4408920985006262e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.12560292940407231 0.99208059356340383 1
		 1 1 yes;
	setAttr ".xm[47]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.3103468959999987 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.14817225198256126 0.98896156838494809 1
		 1 1 yes;
	setAttr ".xm[48]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.7127924869999998 -0.16572755550000001
		 -0.12 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.0085617420054457261 0.0093040442633993605 -0.16570033203958187 0.98609509242067983 1
		 1 1 yes;
	setAttr ".xm[49]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.7992489469999988 7.1054273576010019e-015
		 2.2204460492503131e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.099114383376220902 0.99507604684664752 1
		 1 1 yes;
	setAttr ".xm[50]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.2790000000000035 0
		 2.2204460492503131e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.087155742747658166 0.99619469809174555 1
		 1 1 yes;
	setAttr ".xm[51]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.4562871829999997 -0.39650271770000001
		 0.90200000000000002 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.012974487509198336 -0.054448105158389305 -0.14107955784346177 0.9884147028839807 1
		 1 1 yes;
	setAttr ".xm[52]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.0469344890000034 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.13444896918604174 0.99092051885346022 1
		 1 1 yes;
	setAttr ".xm[53]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.0133297020000001 -1.075693638
		 1.76 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.1549397477996875 -0.096621403224826777 -0.24355127475741434 0.9525443588383028 1
		 1 1 yes;
	setAttr ".xm[54]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.038887826000007 0 -1.3322676295501878e-015 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[55]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.8470298950000092 0
		 -3.5527136788005009e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.13052619222005157 0 0.99144486137381038 1
		 1 1 yes;
	setAttr ".xm[56]" -type "matrix" "xform" 1 1 1 0 0 0 0 3.564770679 0.2519212374
		 -2.8671152590000002 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.13644586469260472 -0.69381735781708975 0.67675308340416285 0.20495185801294383 1
		 1 1 yes;
	setAttr ".xm[57]" -type "matrix" "xform" 1 1 1 7.5052833736605412e-031 -1.3002769988578649e-015
		 2.6607368480810523e-031 0 3.1620000000000132 -4.4408920985006262e-016 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[58]" -type "matrix" "xform" 1 1 1 0 0 0 0 3.564770679 0.2519212374
		 2.867 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.1364454502490326 0.69381743932128048 0.67675296097848725 0.20495226226331367 1
		 1 1 yes;
	setAttr ".xm[59]" -type "matrix" "xform" 1 1 1 7.5052833736605412e-031 -1.3002769988578649e-015
		 2.6607368480810523e-031 0 3.1620000000000132 4.4408920985006262e-016 2.2204460492503131e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[60]" -type "matrix" "xform" 1 1 1 7.5052833736605412e-031 -1.3002769988578649e-015
		 2.6607368480810523e-031 0 3.161999999999999 4.4408920985006262e-016 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[61]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr -s 61 ".m";
	setAttr -s 61 ".p";
	setAttr -s 62 ".g[0:61]" yes yes yes no no no no no no no no no no 
		yes no no no no no no no no no no no no no no no no no no no no no no no no no no 
		no no no no no no no no no no no no no no no no yes no yes no no yes;
	setAttr ".bp" yes;
createNode dagPose -n "bindPose2";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".bp" yes;
createNode materialInfo -n "char_character1_sword_rig_v2:sword_final:sword:materialInfo5";
createNode shadingEngine -n "char1_swordSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode lambert -n "char1_sword";
	setAttr ".dc" 1;
createNode file -n "file2";
	setAttr ".ftn" -type "string" "D:/WORKING/UDK//sourceimages/char1_sword_d.tga";
createNode place2dTexture -n "place2dTexture2";
createNode shadingEngine -n "Char1_char1_body_d_tga";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode phong -n "Char1_char1_body_d_tga1";
	setAttr ".dc" 1;
createNode file -n "file1";
	setAttr ".ftn" -type "string" "D:/WORKING/UDK//sourceimages/char1_body_3.tga";
createNode place2dTexture -n "place2dTexture1";
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 5 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"persp\" \n                -useInteractiveMode 0\n"
		+ "                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 0\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n"
		+ "                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                $editorName;\nstereoCameraView -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 0\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n"
		+ "                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                $editorName;\nstereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 0\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n"
		+ "                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n"
		+ "            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 0\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n"
		+ "                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 0\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n"
		+ "            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 0\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 0\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n"
		+ "                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 0\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n"
		+ "                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n"
		+ "                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 0\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 0\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\n"
		+ "modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 0\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n"
		+ "            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n"
		+ "                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n"
		+ "                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n"
		+ "            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"on\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -clipTime \"on\" \n"
		+ "                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"on\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph InputOutput1\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph InputOutput1\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n"
		+ "                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph InputOutput1\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"multiListerPanel\" (localizedPanelLabel(\"Multilister\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"multiListerPanel\" -l (localizedPanelLabel(\"Multilister\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Multilister\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"devicePanel\" (localizedPanelLabel(\"Devices\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tdevicePanel -unParent -l (localizedPanelLabel(\"Devices\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tdevicePanel -edit -l (localizedPanelLabel(\"Devices\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"webBrowserPanel\" (localizedPanelLabel(\"Web Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"webBrowserPanel\" -l (localizedPanelLabel(\"Web Browser\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Web Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n"
		+ "                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 0\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 0\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 60 ";
	setAttr ".st" 6;
createNode skinCluster -n "skinCluster1";
	setAttr -s 1819 ".wl";
	setAttr ".wl[0].w[3]"  1;
	setAttr ".wl[1].w[3]"  1;
	setAttr ".wl[2].w[3]"  1;
	setAttr ".wl[3].w[3]"  1;
	setAttr ".wl[4].w[7]"  1;
	setAttr ".wl[5].w[7]"  1;
	setAttr ".wl[6].w[7]"  1;
	setAttr ".wl[7].w[7]"  1;
	setAttr ".wl[8].w[3]"  1;
	setAttr ".wl[9].w[3]"  1;
	setAttr ".wl[10].w[7]"  1;
	setAttr ".wl[11].w[7]"  1;
	setAttr -s 2 ".wl[12].w";
	setAttr ".wl[12].w[0]" 0.4;
	setAttr ".wl[12].w[11]" 0.6;
	setAttr -s 2 ".wl[13].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[14].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[15].w";
	setAttr ".wl[15].w[0]" 0.4;
	setAttr ".wl[15].w[11]" 0.6;
	setAttr ".wl[16].w[0]"  1;
	setAttr -s 2 ".wl[17].w[0:1]"  0.92660042085071603 0.07339957914928405;
	setAttr -s 3 ".wl[18].w";
	setAttr ".wl[18].w[0:1]" 0.38214002501655392 0.11785997498344611;
	setAttr ".wl[18].w[11]" 0.5;
	setAttr -s 3 ".wl[19].w";
	setAttr ".wl[19].w[0:1]" 0.52260575497826756 0.17739424502173243;
	setAttr ".wl[19].w[11]" 0.3;
	setAttr -s 2 ".wl[20].w";
	setAttr ".wl[20].w[0]" 0.7;
	setAttr ".wl[20].w[11]" 0.3;
	setAttr -s 3 ".wl[21].w";
	setAttr ".wl[21].w[0:1]" 0.17762838332114117 0.15637161667885882;
	setAttr ".wl[21].w[11]" 0.666;
	setAttr -s 2 ".wl[22].w[0:1]"  0.86454464323876401 0.13545535676123607;
	setAttr -s 2 ".wl[23].w[0:1]"  0.9752709397261744 0.024729060273825508;
	setAttr -s 3 ".wl[24].w";
	setAttr ".wl[24].w[0]" 0.4000481115640081;
	setAttr ".wl[24].w[11:12]" 0.58826519270184452 0.011686695734147382;
	setAttr -s 3 ".wl[25].w";
	setAttr ".wl[25].w[0]" 0.088888888888888865;
	setAttr ".wl[25].w[11:12]" 0.8 0.1111111111111111;
	setAttr -s 2 ".wl[26].w";
	setAttr ".wl[26].w[0]" 0.8;
	setAttr ".wl[26].w[11]" 0.2;
	setAttr -s 3 ".wl[27].w";
	setAttr ".wl[27].w[0]" 0.43785184580619857;
	setAttr ".wl[27].w[11:12]" 0.55396854800675299 0.0081796061870484434;
	setAttr -s 3 ".wl[28].w";
	setAttr ".wl[28].w[0]" 0.088888888888888865;
	setAttr ".wl[28].w[11:12]" 0.8 0.1111111111111111;
	setAttr -s 2 ".wl[29].w";
	setAttr ".wl[29].w[0]" 0.8;
	setAttr ".wl[29].w[11]" 0.2;
	setAttr -s 2 ".wl[30].w";
	setAttr ".wl[30].w[0]" 0.7;
	setAttr ".wl[30].w[11]" 0.3;
	setAttr -s 2 ".wl[31].w";
	setAttr ".wl[31].w[0]" 0.7;
	setAttr ".wl[31].w[11]" 0.3;
	setAttr ".wl[32].w[0]"  1;
	setAttr -s 2 ".wl[33].w";
	setAttr ".wl[33].w[0]" 0.75;
	setAttr ".wl[33].w[11]" 0.25;
	setAttr -s 2 ".wl[34].w";
	setAttr ".wl[34].w[0]" 0.6;
	setAttr ".wl[34].w[11]" 0.4;
	setAttr ".wl[35].w[0]"  1;
	setAttr -s 2 ".wl[36].w";
	setAttr ".wl[36].w[0]" 0.75;
	setAttr ".wl[36].w[11]" 0.25;
	setAttr -s 2 ".wl[37].w";
	setAttr ".wl[37].w[0]" 0.52;
	setAttr ".wl[37].w[11]" 0.48;
	setAttr -s 2 ".wl[38].w";
	setAttr ".wl[38].w[0]" 0.8;
	setAttr ".wl[38].w[11]" 0.2;
	setAttr -s 2 ".wl[39].w[0:1]"  0.77715521868905812 0.22284478131094185;
	setAttr -s 3 ".wl[40].w";
	setAttr ".wl[40].w[0:1]" 0.70294887651880511 0.19705112348119497;
	setAttr ".wl[40].w[11]" 0.1;
	setAttr -s 2 ".wl[41].w";
	setAttr ".wl[41].w[0]" 0.8;
	setAttr ".wl[41].w[11]" 0.2;
	setAttr -s 2 ".wl[42].w";
	setAttr ".wl[42].w[0]" 0.34159999999999924;
	setAttr ".wl[42].w[11]" 0.65840000000000076;
	setAttr -s 2 ".wl[43].w";
	setAttr ".wl[43].w[0]" 0.34159999999999924;
	setAttr ".wl[43].w[11]" 0.65840000000000076;
	setAttr -s 2 ".wl[44].w[0:1]"  0.83990179354366945 0.16009820645633047;
	setAttr -s 2 ".wl[45].w[0:1]"  0.22325890635142953 0.77674109364857047;
	setAttr -s 2 ".wl[46].w[0:1]"  0.20292992779459451 0.7970700722054056;
	setAttr -s 2 ".wl[47].w[0:1]"  0.81191749115614364 0.18808250884385633;
	setAttr -s 3 ".wl[48].w";
	setAttr ".wl[48].w[0:1]" 0.5226476688576821 0.42735233114231791;
	setAttr ".wl[48].w[11]" 0.05;
	setAttr -s 2 ".wl[49].w";
	setAttr ".wl[49].w[0]" 0.84110662192616725;
	setAttr ".wl[49].w[11]" 0.15889337807383286;
	setAttr -s 2 ".wl[50].w";
	setAttr ".wl[50].w[0]" 0.88502682422964984;
	setAttr ".wl[50].w[11]" 0.11497317577035016;
	setAttr -s 3 ".wl[51].w";
	setAttr ".wl[51].w[0:1]" 0.42210170286469745 0.52789829713530245;
	setAttr ".wl[51].w[11]" 0.05;
	setAttr -s 3 ".wl[52].w";
	setAttr ".wl[52].w[0:1]" 0.2626415339880685 0.66167999999999982;
	setAttr ".wl[52].w[6]" 0.075678466011931664;
	setAttr -s 2 ".wl[53].w[0:1]"  0.5 0.5;
	setAttr -s 2 ".wl[54].w[0:1]"  0.73352085699435998 0.26647914300564013;
	setAttr -s 2 ".wl[55].w[0:1]"  0.33832000000000018 0.66167999999999982;
	setAttr -s 3 ".wl[56].w";
	setAttr ".wl[56].w[12]" 0.53096341726300267;
	setAttr ".wl[56].w[39:40]" 0.4 0.069036582736997337;
	setAttr -s 2 ".wl[57].w[11:12]"  0.070236371321227536 0.92976362867877238;
	setAttr -s 4 ".wl[58].w";
	setAttr ".wl[58].w[11:12]" 0.072241736118776378 0.72217453297681788;
	setAttr ".wl[58].w[39:40]" 0.14390861163308402 0.061675119271321729;
	setAttr -s 2 ".wl[59].w[39:40]"  0.85096000000000016 0.14903999999999984;
	setAttr -s 2 ".wl[60].w[11:12]"  0.64871706170265098 0.35128293829734913;
	setAttr -s 2 ".wl[61].w[11:12]"  0.63246203146764146 0.36753796853235859;
	setAttr -s 4 ".wl[62].w";
	setAttr ".wl[62].w[11:12]" 0.064618656355510784 0.23694824014449811;
	setAttr ".wl[62].w[39:40]" 0.48890317244999376 0.20952993104999731;
	setAttr -s 2 ".wl[63].w[39:40]"  0.80414896205686126 0.19585103794313879;
	setAttr -s 2 ".wl[64].w[11:12]"  0.65271253582063815 0.34728746417936185;
	setAttr -s 3 ".wl[65].w";
	setAttr ".wl[65].w[12]" 0.099101005581068255;
	setAttr ".wl[65].w[39:40]" 0.83050585681614009 0.0703931376027917;
	setAttr -s 5 ".wl[66].w";
	setAttr ".wl[66].w[11:13]" 0.27474892601462964 0.35117254551659732 0.055534927197524918;
	setAttr ".wl[66].w[39:40]" 0.18352423716158633 0.1350193641096617;
	setAttr -s 5 ".wl[67].w";
	setAttr ".wl[67].w[11:13]" 0.1705042276742359 0.45303162520801132 0.23990345529422011;
	setAttr ".wl[67].w[39:40]" 0.078677445389434192 0.057883246434098506;
	setAttr -s 3 ".wl[68].w";
	setAttr ".wl[68].w[12]" 0.53140841803373906;
	setAttr ".wl[68].w[39:40]" 0.4 0.068591581966260903;
	setAttr -s 2 ".wl[69].w[11:12]"  0.66356011940395976 0.33643988059604024;
	setAttr -s 2 ".wl[70].w[11:12]"  0.65522093426347894 0.34477906573652112;
	setAttr -s 2 ".wl[71].w[39:40]"  0.8047649113023112 0.19523508869768877;
	setAttr -s 4 ".wl[72].w";
	setAttr ".wl[72].w[11:12]" 0.080747960225819038 0.23087689589128751;
	setAttr ".wl[72].w[39:40]" 0.48186260071802539 0.20651254316486803;
	setAttr -s 2 ".wl[73].w[11:12]"  0.65980085840218006 0.34019914159781994;
	setAttr -s 3 ".wl[74].w";
	setAttr ".wl[74].w[12]" 0.193092008542497;
	setAttr ".wl[74].w[39:40]" 0.59953726187932976 0.20737072957817335;
	setAttr -s 3 ".wl[75].w";
	setAttr ".wl[75].w[12]" 0.092866464739051899;
	setAttr ".wl[75].w[39:40]" 0.70416523584740431 0.20296829941354366;
	setAttr -s 3 ".wl[76].w";
	setAttr ".wl[76].w[11:12]" 0.047025778224021653 0.59376347689282194;
	setAttr ".wl[76].w[39]" 0.35921074488315646;
	setAttr -s 3 ".wl[77].w";
	setAttr ".wl[77].w[11:12]" 0.039655290060282566 0.59682442291824356;
	setAttr ".wl[77].w[39]" 0.36352028702147376;
	setAttr -s 3 ".wl[78].w";
	setAttr ".wl[78].w[12]" 0.76786962544872872;
	setAttr ".wl[78].w[39:40]" 0.1221738813427744 0.10995649320849696;
	setAttr -s 3 ".wl[79].w";
	setAttr ".wl[79].w[12]" 0.76785429059708632;
	setAttr ".wl[79].w[39:40]" 0.12218195231732303 0.10996375708559071;
	setAttr -s 2 ".wl[80].w";
	setAttr ".wl[80].w[12]" 0.92700805113878404;
	setAttr ".wl[80].w[39]" 0.072991948861215988;
	setAttr -s 2 ".wl[81].w";
	setAttr ".wl[81].w[12]" 0.9256816215244561;
	setAttr ".wl[81].w[39]" 0.074318378475543873;
	setAttr -s 2 ".wl[82].w";
	setAttr ".wl[82].w[12]" 0.56004363707167015;
	setAttr ".wl[82].w[39]" 0.43995636292832985;
	setAttr -s 3 ".wl[83].w";
	setAttr ".wl[83].w[11:12]" 0.044228809316685297 0.78971966508519398;
	setAttr ".wl[83].w[39]" 0.16605152559812075;
	setAttr -s 3 ".wl[84].w";
	setAttr ".wl[84].w[12]" 0.43170310022552683;
	setAttr ".wl[84].w[39:40]" 0.5388455870600305 0.029451312714442825;
	setAttr -s 3 ".wl[85].w";
	setAttr ".wl[85].w[11:12]" 0.029154216184777176 0.76260858451275881;
	setAttr ".wl[85].w[39]" 0.2082371993024639;
	setAttr -s 3 ".wl[86].w";
	setAttr ".wl[86].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[86].w[11]" 0.1;
	setAttr -s 3 ".wl[87].w";
	setAttr ".wl[87].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[87].w[11]" 0.1;
	setAttr -s 3 ".wl[88].w";
	setAttr ".wl[88].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[88].w[11]" 0.1;
	setAttr -s 3 ".wl[89].w";
	setAttr ".wl[89].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[89].w[11]" 0.1;
	setAttr -s 3 ".wl[90].w";
	setAttr ".wl[90].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[90].w[11]" 0.1;
	setAttr -s 3 ".wl[91].w";
	setAttr ".wl[91].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[91].w[11]" 0.1;
	setAttr -s 2 ".wl[92].w";
	setAttr ".wl[92].w[0]" 0.34442000000000017;
	setAttr ".wl[92].w[11]" 0.65557999999999983;
	setAttr -s 2 ".wl[93].w";
	setAttr ".wl[93].w[0]" 0.34442000000000017;
	setAttr ".wl[93].w[11]" 0.65557999999999983;
	setAttr -s 3 ".wl[94].w";
	setAttr ".wl[94].w[0:1]" 0.8869463539704513 0.039181147049290238;
	setAttr ".wl[94].w[11]" 0.07387249898025855;
	setAttr -s 3 ".wl[95].w";
	setAttr ".wl[95].w[0:1]" 0.88611579034670729 0.03897262398771234;
	setAttr ".wl[95].w[11]" 0.074911585665580496;
	setAttr -s 2 ".wl[96].w";
	setAttr ".wl[96].w[0]" 0.28798000000000046;
	setAttr ".wl[96].w[11]" 0.71201999999999954;
	setAttr -s 2 ".wl[97].w";
	setAttr ".wl[97].w[0]" 0.28798000000000046;
	setAttr ".wl[97].w[11]" 0.71201999999999954;
	setAttr -s 2 ".wl[98].w";
	setAttr ".wl[98].w[0]" 0.59693705240954142;
	setAttr ".wl[98].w[11]" 0.40306294759045846;
	setAttr -s 2 ".wl[99].w";
	setAttr ".wl[99].w[0]" 0.61148108439858984;
	setAttr ".wl[99].w[11]" 0.38851891560141;
	setAttr -s 2 ".wl[100].w";
	setAttr ".wl[100].w[0]" 0.33631405176922491;
	setAttr ".wl[100].w[11]" 0.66368594823077509;
	setAttr -s 3 ".wl[101].w";
	setAttr ".wl[101].w[0:1]" 0.8039829411678957 0.0017807831331848104;
	setAttr ".wl[101].w[11]" 0.19423627569891952;
	setAttr -s 2 ".wl[102].w";
	setAttr ".wl[102].w[0]" 0.31281169360994443;
	setAttr ".wl[102].w[11]" 0.68718830639005557;
	setAttr -s 3 ".wl[103].w";
	setAttr ".wl[103].w[0:1]" 0.80091575196576092 0.00041267069627033367;
	setAttr ".wl[103].w[11]" 0.19867157733796884;
	setAttr -s 2 ".wl[104].w[39:40]"  0.67797196890643696 0.32202803109356298;
	setAttr -s 2 ".wl[105].w[39:40]"  0.53099181152572605 0.469008188474274;
	setAttr -s 2 ".wl[106].w[39:40]"  0.51606825469958884 0.48393174530041116;
	setAttr -s 2 ".wl[107].w[39:40]"  0.70591083756031525 0.29408916243968475;
	setAttr -s 3 ".wl[108].w";
	setAttr ".wl[108].w[12]" 0.52861266435876142;
	setAttr ".wl[108].w[39:40]" 0.4 0.0713873356412386;
	setAttr -s 3 ".wl[109].w";
	setAttr ".wl[109].w[12]" 0.52927706956659959;
	setAttr ".wl[109].w[39:40]" 0.4 0.070722930433400347;
	setAttr -s 3 ".wl[110].w";
	setAttr ".wl[110].w[12]" 0.52987444220129465;
	setAttr ".wl[110].w[39:40]" 0.4 0.070125557798705357;
	setAttr -s 3 ".wl[111].w";
	setAttr ".wl[111].w[12]" 0.53037006501993533;
	setAttr ".wl[111].w[39:40]" 0.4 0.069629934980064631;
	setAttr -s 3 ".wl[112].w";
	setAttr ".wl[112].w[12]" 0.1606389026912004;
	setAttr ".wl[112].w[39:40]" 0.61064401902498044 0.22871707828381921;
	setAttr -s 3 ".wl[113].w";
	setAttr ".wl[113].w[12]" 0.063960266934831228;
	setAttr ".wl[113].w[39:40]" 0.6502400229350489 0.28579971013011995;
	setAttr -s 2 ".wl[114].w[39:40]"  0.85096000000000016 0.14903999999999984;
	setAttr -s 2 ".wl[115].w[39:40]"  0.85096000000000016 0.14903999999999984;
	setAttr -s 4 ".wl[116].w";
	setAttr ".wl[116].w[12:13]" 0.14984783265858365 0.048362645777705271;
	setAttr ".wl[116].w[39:40]" 0.61648865982240109 0.18530086174131;
	setAttr -s 3 ".wl[117].w";
	setAttr ".wl[117].w[12]" 0.72174267474860887;
	setAttr ".wl[117].w[39:40]" 0.14645122381652165 0.13180610143486945;
	setAttr -s 2 ".wl[118].w[39:40]"  0.60636030776346272 0.39363969223653722;
	setAttr -s 3 ".wl[119].w";
	setAttr ".wl[119].w[12]" 0.77028040746493498;
	setAttr ".wl[119].w[39:40]" 0.12090504870266586 0.10881454383239926;
	setAttr -s 4 ".wl[120].w";
	setAttr ".wl[120].w[12:13]" 0.1492808746106597 0.038950175545420802;
	setAttr ".wl[120].w[39:40]" 0.63097947160916668 0.18078947823475283;
	setAttr -s 2 ".wl[121].w[39:40]"  0.5744819958186469 0.42551800418135305;
	setAttr ".wl[122].w[0]"  1;
	setAttr -s 2 ".wl[123].w[0:1]"  0.95519717544761717 0.04480282455238277;
	setAttr -s 2 ".wl[124].w[0:1]"  0.74070384500177489 0.25929615499822511;
	setAttr -s 2 ".wl[125].w[0:1]"  0.73449667299495369 0.26550332700504636;
	setAttr -s 2 ".wl[126].w[0:1]"  0.80161946065521317 0.19838053934478675;
	setAttr -s 2 ".wl[127].w[0:1]"  0.65570939619336799 0.34429060380663207;
	setAttr -s 2 ".wl[128].w[0:1]"  0.90800025092428627 0.091999749075713788;
	setAttr ".wl[129].w[0]"  1;
	setAttr -s 2 ".wl[130].w[0:1]"  0.70424240677613303 0.29575759322386697;
	setAttr -s 2 ".wl[131].w[0:1]"  0.73221363460308109 0.26778636539691891;
	setAttr -s 3 ".wl[132].w";
	setAttr ".wl[132].w[0:1]" 0.76067566066013981 0.23808371827569319;
	setAttr ".wl[132].w[11]" 0.0012406210641670041;
	setAttr -s 2 ".wl[133].w[0:1]"  0.6479517287079416 0.35204827129205846;
	setAttr -s 3 ".wl[134].w";
	setAttr ".wl[134].w[0]" 0.25769900953486163;
	setAttr ".wl[134].w[11:12]" 0.7 0.042300990465138397;
	setAttr -s 3 ".wl[135].w";
	setAttr ".wl[135].w[0:1]" 0.59547194912693291 0.10914506835592788;
	setAttr ".wl[135].w[11]" 0.29538298251713924;
	setAttr -s 4 ".wl[136].w";
	setAttr ".wl[136].w[0:1]" 0.74896263142411323 0.00057373001035019363;
	setAttr ".wl[136].w[6]" 0.00046363742419895307;
	setAttr ".wl[136].w[11]" 0.25000000114133769;
	setAttr -s 3 ".wl[137].w";
	setAttr ".wl[137].w[0]" 0.29954829013664835;
	setAttr ".wl[137].w[11:12]" 0.7 0.00045170986335169489;
	setAttr -s 2 ".wl[138].w[11:12]"  0.65921831875405323 0.34078168124594682;
	setAttr -s 2 ".wl[139].w[11:12]"  0.65955449152169099 0.34044550847830907;
	setAttr -s 2 ".wl[140].w";
	setAttr ".wl[140].w[0]" 0.26237999999999928;
	setAttr ".wl[140].w[11]" 0.73762000000000072;
	setAttr -s 3 ".wl[141].w";
	setAttr ".wl[141].w[0]" 0.24768845769165435;
	setAttr ".wl[141].w[11:12]" 0.7 0.052311542308345685;
	setAttr -s 3 ".wl[142].w";
	setAttr ".wl[142].w[0:1]" 0.30017642163604191 5.7921342441309463e-005;
	setAttr ".wl[142].w[11]" 0.69976565702151683;
	setAttr -s 3 ".wl[143].w";
	setAttr ".wl[143].w[0:1]" 0.63877582649456455 0.11122417350543551;
	setAttr ".wl[143].w[11]" 0.25;
	setAttr -s 2 ".wl[144].w[11:12]"  0.65467087223092479 0.34532912776907532;
	setAttr -s 2 ".wl[145].w[11:12]"  0.63991811684662614 0.36008188315337392;
	setAttr -s 3 ".wl[146].w";
	setAttr ".wl[146].w[11:12]" 0.056399973074909397 0.49004749907354944;
	setAttr ".wl[146].w[39]" 0.45355252785154115;
	setAttr -s 3 ".wl[147].w";
	setAttr ".wl[147].w[11:12]" 0.069768452460025415 0.50800256172467473;
	setAttr ".wl[147].w[39]" 0.42222898581529977;
	setAttr -s 2 ".wl[148].w";
	setAttr ".wl[148].w[12]" 0.65865083414081727;
	setAttr ".wl[148].w[39]" 0.34134916585918279;
	setAttr -s 2 ".wl[149].w";
	setAttr ".wl[149].w[12]" 0.68403834635679894;
	setAttr ".wl[149].w[39]" 0.31596165364320106;
	setAttr -s 3 ".wl[150].w";
	setAttr ".wl[150].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[150].w[11]" 0.1;
	setAttr -s 3 ".wl[151].w";
	setAttr ".wl[151].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[151].w[11]" 0.1;
	setAttr -s 3 ".wl[152].w";
	setAttr ".wl[152].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[152].w[11]" 0.1;
	setAttr -s 3 ".wl[153].w";
	setAttr ".wl[153].w[0:1]" 0.4833 0.4167;
	setAttr ".wl[153].w[11]" 0.1;
	setAttr -s 3 ".wl[154].w";
	setAttr ".wl[154].w[12:13]" 0.29219521772395979 0.0070673118756056329;
	setAttr ".wl[154].w[39]" 0.70073747040043455;
	setAttr -s 3 ".wl[155].w";
	setAttr ".wl[155].w[12]" 0.071208420076511569;
	setAttr ".wl[155].w[39:40]" 0.62279438056163294 0.30599719936185549;
	setAttr -s 3 ".wl[156].w";
	setAttr ".wl[156].w[12]" 0.0017989164002400447;
	setAttr ".wl[156].w[39:40]" 0.52716447117650089 0.47103661242325889;
	setAttr -s 2 ".wl[157].w";
	setAttr ".wl[157].w[12]" 0.36143570083635845;
	setAttr ".wl[157].w[39]" 0.63856429916364155;
	setAttr -s 2 ".wl[158].w[39:40]"  0.42553669775366959 0.57446330224633047;
	setAttr -s 2 ".wl[159].w[39:40]"  0.40443809908756889 0.59556190091243111;
	setAttr -s 3 ".wl[160].w";
	setAttr ".wl[160].w[12]" 0.032535247696779901;
	setAttr ".wl[160].w[39:40]" 0.56116738086502471 0.40629737143819533;
	setAttr -s 2 ".wl[161].w[39:40]"  0.5 0.5;
	setAttr -s 4 ".wl[162].w";
	setAttr ".wl[162].w[11:12]" 0.14648365967236388 0.1275795698884882;
	setAttr ".wl[162].w[39:40]" 0.50815573930740354 0.21778103113174438;
	setAttr -s 4 ".wl[163].w";
	setAttr ".wl[163].w[11:12]" 0.14861637144730505 0.15056009957997935;
	setAttr ".wl[163].w[39:40]" 0.46714297959191431 0.23368054938080121;
	setAttr -s 2 ".wl[164].w[39:40]"  0.54554000000000014 0.45445999999999986;
	setAttr -s 2 ".wl[165].w[39:40]"  0.57024435634044746 0.42975564365955254;
	setAttr -s 4 ".wl[166].w";
	setAttr ".wl[166].w[11:12]" 0.27605622012575104 0.37553055306566563;
	setAttr ".wl[166].w[39:40]" 0.18533605636864917 0.16307717043993408;
	setAttr -s 2 ".wl[167].w[39:40]"  0.47022986033707154 0.52977013966292841;
	setAttr -s 4 ".wl[168].w";
	setAttr ".wl[168].w[11:12]" 0.13941832084683314 0.40779931497339816;
	setAttr ".wl[168].w[39:40]" 0.31694765492583804 0.1358347092539306;
	setAttr -s 2 ".wl[169].w[39:40]"  0.83834365968046642 0.16165634031953355;
	setAttr -s 3 ".wl[170].w";
	setAttr ".wl[170].w[0:1]" 0.61330000000000029 0.21411473405114148;
	setAttr ".wl[170].w[6]" 0.17258526594885823;
	setAttr -s 3 ".wl[171].w";
	setAttr ".wl[171].w[0:1]" 0.61330000000000029 0.21369348263286617;
	setAttr ".wl[171].w[6]" 0.17300651736713354;
	setAttr -s 2 ".wl[172].w[0:1]"  0.70131289369380401 0.29868710630619594;
	setAttr -s 2 ".wl[173].w[0:1]"  0.68243413562814226 0.31756586437185763;
	setAttr -s 2 ".wl[174].w[22:23]"  0.4 0.6;
	setAttr -s 4 ".wl[175].w";
	setAttr ".wl[175].w[16]" 0.034967770698456697;
	setAttr ".wl[175].w[21:23]" 0.081586254232009353 0.77782785892486572 0.10561811614466823;
	setAttr -s 2 ".wl[176].w[21:22]"  0.18857485055923462 0.81142514944076538;
	setAttr -s 2 ".wl[177].w[22:23]"  0.30000000000000004 0.7;
	setAttr -s 2 ".wl[178].w[22:23]"  0.4 0.6;
	setAttr -s 4 ".wl[179].w";
	setAttr ".wl[179].w[18]" 0.0353916206226546;
	setAttr ".wl[179].w[21:23]" 0.045777758132840456 0.809029221534729 0.10980139970977594;
	setAttr -s 2 ".wl[180].w[22:23]"  0.6 0.4;
	setAttr -s 4 ".wl[181].w";
	setAttr ".wl[181].w[18]" 0.023145055421991936;
	setAttr ".wl[181].w[21:23]" 0.032995686856073125 0.86835139989852905 0.075507857823405883;
	setAttr -s 4 ".wl[182].w";
	setAttr ".wl[182].w[18]" 0.019592629797202492;
	setAttr ".wl[182].w[21:23]" 0.050825552775245607 0.87258440256118774 0.056997414866364143;
	setAttr -s 2 ".wl[183].w[22:23]"  0.95 0.05;
	setAttr -s 4 ".wl[184].w";
	setAttr ".wl[184].w[18]" 0.017111409429563053;
	setAttr ".wl[184].w[21:23]" 0.02681355613167213 0.9083397388458252 0.047735295592939625;
	setAttr -s 2 ".wl[185].w[22:23]"  0.9 0.1;
	setAttr -s 4 ".wl[186].w";
	setAttr ".wl[186].w[18]" 0.013344131425323289;
	setAttr ".wl[186].w[21:23]" 0.016276887811923334 0.92844849824905396 0.041930482513699421;
	setAttr -s 2 ".wl[187].w[22:23]"  0.9 0.1;
	setAttr -s 4 ".wl[188].w";
	setAttr ".wl[188].w[18]" 0.002693450842597955;
	setAttr ".wl[188].w[21:23]" 0.042251536558486236 0.94685119390487671 0.0082038186940391021;
	setAttr -s 3 ".wl[189].w";
	setAttr ".wl[189].w[12]" 0.010142146945796593;
	setAttr ".wl[189].w[21:22]" 0.11672908127228203 0.87312877178192139;
	setAttr -s 3 ".wl[190].w";
	setAttr ".wl[190].w[12]" 0.018792915852737357;
	setAttr ".wl[190].w[21:22]" 0.058712017027664258 0.92249506711959839;
	setAttr -s 4 ".wl[191].w";
	setAttr ".wl[191].w[18]" 0.00025007342040942794;
	setAttr ".wl[191].w[21:23]" 0.046415729065227944 0.95253795385360718 0.00079624366075545109;
	setAttr -s 4 ".wl[192].w";
	setAttr ".wl[192].w[18]" 0.0092226537268727919;
	setAttr ".wl[192].w[21:23]" 0.060401174922802683 0.90572339296340942 0.024652778386915097;
	setAttr -s 2 ".wl[193].w[21:22]"  0.076456546783447266 0.92354345321655273;
	setAttr -s 4 ".wl[194].w";
	setAttr ".wl[194].w[18]" 0.010898573519717465;
	setAttr ".wl[194].w[21:23]" 0.11133029529579329 0.84707659482955933 0.03069453635492991;
	setAttr -s 3 ".wl[195].w";
	setAttr ".wl[195].w[12]" 0.062343542575295292;
	setAttr ".wl[195].w[21:22]" 0.18708765029961316 0.75056880712509155;
	setAttr -s 2 ".wl[196].w[21:22]"  0.372100830078125 0.627899169921875;
	setAttr -s 5 ".wl[197].w";
	setAttr ".wl[197].w[12]" 0.080944355330258747;
	setAttr ".wl[197].w[18]" 0.0433787859947855;
	setAttr ".wl[197].w[21:23]" 0.12445075927420152 0.68817013502120972 0.063055964379544471;
	setAttr -s 4 ".wl[198].w";
	setAttr ".wl[198].w[16]" 0.0064123092441961353;
	setAttr ".wl[198].w[21:23]" 0.043710276009263999 0.93064326047897339 0.019234154267566483;
	setAttr -s 5 ".wl[199].w";
	setAttr ".wl[199].w[12]" 0.0028698920133321984;
	setAttr ".wl[199].w[18]" 0.0019037911984046529;
	setAttr ".wl[199].w[21:23]" 0.0043143064607144782 0.98724567890167236 0.003666331425876307;
	setAttr -s 4 ".wl[200].w";
	setAttr ".wl[200].w[16]" 0.0073825009665171998;
	setAttr ".wl[200].w[21:23]" 0.068141491221299491 0.90300995111465454 0.021466056697528768;
	setAttr -s 4 ".wl[201].w";
	setAttr ".wl[201].w[11:12]" 0.0095364724725347984 0.047557870654528968;
	setAttr ".wl[201].w[21:22]" 0.071229986725994093 0.87167567014694214;
	setAttr ".wl[202].w[56]"  1;
	setAttr ".wl[203].w[56]"  0.99999999999999989;
	setAttr ".wl[204].w[56]"  1;
	setAttr ".wl[205].w[56]"  1;
	setAttr -s 2 ".wl[206].w[42:43]"  0.24231404298019082 0.75768595701980912;
	setAttr -s 2 ".wl[207].w[42:43]"  0.30562351562093348 0.69437648437906652;
	setAttr -s 2 ".wl[208].w[42:43]"  0.47669834756030843 0.52330165243969151;
	setAttr -s 2 ".wl[209].w[42:43]"  0.50999379053978178 0.49000620946021828;
	setAttr -s 2 ".wl[210].w[42:43]"  0.80975428761539858 0.19024571238460136;
	setAttr -s 2 ".wl[211].w";
	setAttr ".wl[211].w[42]" 0.45638219984305017;
	setAttr ".wl[211].w[46]" 0.54361780015694983;
	setAttr ".wl[212].w[45]"  1;
	setAttr ".wl[213].w[45]"  1;
	setAttr ".wl[214].w[45]"  1;
	setAttr ".wl[215].w[45]"  0.99999999999999989;
	setAttr ".wl[216].w[45]"  1;
	setAttr -s 2 ".wl[217].w[44:45]"  0.020195639499787074 0.97980436050021291;
	setAttr ".wl[218].w[45]"  1.0000000000056979;
	setAttr -s 2 ".wl[219].w[46:47]"  0.00067934503122717735 0.99932065496877276;
	setAttr -s 2 ".wl[220].w[46:47]"  0.024954183325240013 0.97504581667475998;
	setAttr -s 2 ".wl[221].w[47:48]"  0.58382747509987354 0.41617252490012646;
	setAttr -s 2 ".wl[222].w[47:48]"  0.58312049550472878 0.41687950449527111;
	setAttr -s 2 ".wl[223].w[46:47]"  0.50711088037623386 0.49288911962376614;
	setAttr -s 2 ".wl[224].w[46:47]"  0.5000018779244082 0.49999812207559174;
	setAttr -s 2 ".wl[225].w[47:48]"  0.99319216006209543 0.006807839937904527;
	setAttr -s 2 ".wl[226].w[47:48]"  0.59166739696170501 0.40833260303829488;
	setAttr -s 2 ".wl[227].w[46:47]"  0.49999979422234836 0.50000020577765159;
	setAttr -s 2 ".wl[228].w[41:42]"  0.5 0.5;
	setAttr -s 4 ".wl[229].w";
	setAttr ".wl[229].w[42]" 0.5;
	setAttr ".wl[229].w[46]" 0.14370200330790187;
	setAttr ".wl[229].w[49]" 0.1582573741083606;
	setAttr ".wl[229].w[56]" 0.19804062258373753;
	setAttr ".wl[230].w[42]"  1;
	setAttr -s 2 ".wl[231].w";
	setAttr ".wl[231].w[42]" 0.9991956713072353;
	setAttr ".wl[231].w[46]" 0.00080432869276458191;
	setAttr ".wl[232].w[42]"  0.99999999999999989;
	setAttr ".wl[233].w[42]"  1;
	setAttr ".wl[234].w[42]"  1;
	setAttr -s 2 ".wl[235].w[41:42]"  0.5 0.5;
	setAttr -s 2 ".wl[236].w[41:42]"  0.5 0.5;
	setAttr -s 2 ".wl[237].w";
	setAttr ".wl[237].w[42]" 0.95327838422375955;
	setAttr ".wl[237].w[55]" 0.046721615776240483;
	setAttr ".wl[238].w[42]"  1;
	setAttr -s 2 ".wl[239].w[41:42]"  0.7 0.30000000000000004;
	setAttr -s 2 ".wl[240].w[41:42]"  0.5 0.5;
	setAttr ".wl[241].w[42]"  1;
	setAttr ".wl[242].w[42]"  1;
	setAttr -s 2 ".wl[243].w[41:42]"  0.5 0.5;
	setAttr -s 2 ".wl[244].w[41:42]"  0.5 0.5;
	setAttr -s 2 ".wl[245].w[41:42]"  0.5 0.5;
	setAttr -s 2 ".wl[246].w[41:42]"  0.5 0.5;
	setAttr ".wl[247].w[42]"  1;
	setAttr -s 2 ".wl[248].w";
	setAttr ".wl[248].w[42]" 0.99865717980634017;
	setAttr ".wl[248].w[46]" 0.0013428201936598137;
	setAttr -s 2 ".wl[249].w[41:42]"  0.5 0.5;
	setAttr ".wl[250].w[42]"  1;
	setAttr -s 2 ".wl[251].w[42:43]"  0.93183548747731426 0.068164512522685641;
	setAttr -s 2 ".wl[252].w";
	setAttr ".wl[252].w[42]" 0.77894848366110592;
	setAttr ".wl[252].w[46]" 0.22105151633889392;
	setAttr ".wl[253].w[42]"  1;
	setAttr -s 2 ".wl[254].w[41:42]"  0.5 0.5;
	setAttr -s 2 ".wl[255].w";
	setAttr ".wl[255].w[42]" 0.99847542415585755;
	setAttr ".wl[255].w[46]" 0.0015245758441425029;
	setAttr -s 2 ".wl[256].w[41:42]"  0.5 0.5;
	setAttr -s 2 ".wl[257].w[42:43]"  0.81570227930428629 0.18429772069571373;
	setAttr -s 4 ".wl[258].w";
	setAttr ".wl[258].w[42]" 0.69083440280844099;
	setAttr ".wl[258].w[46]" 0.0058064261074468916;
	setAttr ".wl[258].w[49:50]" 0.042788211681100641 0.26057095940301139;
	setAttr -s 2 ".wl[259].w";
	setAttr ".wl[259].w[42]" 0.72108054439075053;
	setAttr ".wl[259].w[46]" 0.27891945560924941;
	setAttr -s 2 ".wl[260].w";
	setAttr ".wl[260].w[42]" 0.94876421853390802;
	setAttr ".wl[260].w[52]" 0.051235781466091909;
	setAttr -s 2 ".wl[261].w";
	setAttr ".wl[261].w[42]" 0.50774260662040493;
	setAttr ".wl[261].w[46]" 0.49225739337959507;
	setAttr -s 2 ".wl[262].w";
	setAttr ".wl[262].w[42]" 0.40470753079267896;
	setAttr ".wl[262].w[49]" 0.59529246920732093;
	setAttr ".wl[263].w[7]"  1;
	setAttr ".wl[264].w[7]"  1;
	setAttr ".wl[265].w[7]"  1;
	setAttr ".wl[266].w[7]"  1;
	setAttr ".wl[267].w[7]"  1;
	setAttr ".wl[268].w[7]"  0.99999999999999978;
	setAttr ".wl[269].w[7]"  0.99999999999999989;
	setAttr ".wl[270].w[7]"  1;
	setAttr ".wl[271].w[7]"  1;
	setAttr ".wl[272].w[7]"  0.99999999999999989;
	setAttr ".wl[273].w[7]"  1;
	setAttr ".wl[274].w[7]"  1;
	setAttr -s 2 ".wl[275].w[6:7]"  0.010837895902509158 0.98916210409749095;
	setAttr ".wl[276].w[7]"  1;
	setAttr ".wl[277].w[7]"  0.99999999999999989;
	setAttr -s 2 ".wl[278].w[12:13]"  0.4069888277509055 0.59301117224909439;
	setAttr -s 2 ".wl[279].w[12:13]"  0.45197749067997689 0.54802250932002305;
	setAttr -s 2 ".wl[280].w[12:13]"  0.44558286805189218 0.55441713194810782;
	setAttr -s 2 ".wl[281].w[12:13]"  0.45457780213439763 0.54542219786560231;
	setAttr -s 2 ".wl[282].w[0:1]"  0.98029962049986019 0.019700379500139722;
	setAttr ".wl[283].w[0]"  1;
	setAttr -s 2 ".wl[284].w[0:1]"  0.84327292267358778 0.15672707732641228;
	setAttr ".wl[285].w[0]"  1;
	setAttr -s 2 ".wl[286].w[0:1]"  0.97425270042290979 0.025747299577090238;
	setAttr -s 3 ".wl[287].w";
	setAttr ".wl[287].w[0:1]" 0.83081657709299639 0.069183422907003592;
	setAttr ".wl[287].w[11]" 0.1;
	setAttr -s 2 ".wl[288].w[0:1]"  0.99866307527009623 0.0013369247299038192;
	setAttr -s 2 ".wl[289].w[0:1]"  0.63189483327666929 0.36810516672333066;
	setAttr -s 3 ".wl[290].w[40:42]"  0.03482372565194964 0.94017627434805029 
		0.025;
	setAttr -s 3 ".wl[291].w[40:42]"  0.0053346122615039356 0.96966538773849609 
		0.025;
	setAttr -s 2 ".wl[292].w[40:41]"  0.6 0.4;
	setAttr -s 3 ".wl[293].w[40:42]"  0.56999999999999984 0.38000000000000006 
		0.05;
	setAttr -s 2 ".wl[294].w[40:41]"  0.5 0.5;
	setAttr -s 2 ".wl[295].w[40:41]"  0.5 0.5;
	setAttr -s 3 ".wl[296].w[40:42]"  0.585 0.39000000000000007 0.025;
	setAttr -s 2 ".wl[297].w[40:41]"  0.5 0.5;
	setAttr -s 2 ".wl[298].w[40:41]"  0.30000000000000004 0.7;
	setAttr -s 2 ".wl[299].w[40:41]"  0.4 0.6;
	setAttr -s 2 ".wl[300].w[40:41]"  0.5 0.5;
	setAttr -s 2 ".wl[301].w[40:41]"  0.4 0.6;
	setAttr -s 2 ".wl[302].w[40:41]"  0.6 0.4;
	setAttr -s 2 ".wl[303].w[40:41]"  0.4 0.6;
	setAttr -s 2 ".wl[304].w[41:42]"  0.9 0.1;
	setAttr -s 2 ".wl[305].w[41:42]"  0.9 0.1;
	setAttr -s 2 ".wl[306].w[41:42]"  0.9 0.1;
	setAttr -s 3 ".wl[307].w[40:42]"  0.031860533265714495 0.9431394667342855 
		0.025;
	setAttr -s 3 ".wl[308].w[40:42]"  0.034620074182748796 0.94037992581725116 
		0.025;
	setAttr -s 2 ".wl[309].w[41:42]"  0.9 0.1;
	setAttr -s 2 ".wl[310].w[40:41]"  0.49999986077934866 0.50000013922065134;
	setAttr -s 2 ".wl[311].w[40:41]"  0.49999988028063452 0.50000011971936542;
	setAttr -s 2 ".wl[312].w[40:41]"  0.05 0.95;
	setAttr -s 2 ".wl[313].w[40:41]"  0.1 0.9;
	setAttr -s 2 ".wl[314].w[40:41]"  0.95 0.05;
	setAttr -s 2 ".wl[315].w[40:41]"  0.9 0.1;
	setAttr -s 2 ".wl[316].w[40:41]"  0.9 0.1;
	setAttr -s 2 ".wl[317].w[40:41]"  0.5 0.5;
	setAttr -s 3 ".wl[318].w[40:42]"  0.093972602114081386 0.88102739788591855 
		0.025;
	setAttr -s 3 ".wl[319].w[40:42]"  0.09704652654847519 0.87795347345152475 
		0.025;
	setAttr -s 3 ".wl[320].w[40:42]"  0.057586252272078288 0.91741374772792172 
		0.025;
	setAttr -s 2 ".wl[321].w[40:41]"  0.1 0.9;
	setAttr -s 2 ".wl[322].w[12:13]"  0.43472854881670914 0.56527145118328326;
	setAttr -s 2 ".wl[323].w[12:13]"  0.44482590478717221 0.5551740952128279;
	setAttr -s 2 ".wl[324].w[12:13]"  0.38926347231449343 0.61073652768550402;
	setAttr -s 2 ".wl[325].w[12:13]"  0.435 0.565;
	setAttr -s 2 ".wl[326].w";
	setAttr ".wl[326].w[0]" 0.8;
	setAttr ".wl[326].w[11]" 0.2;
	setAttr -s 3 ".wl[327].w";
	setAttr ".wl[327].w[0:1]" 0.62995364690187616 0.27004635309812391;
	setAttr ".wl[327].w[11]" 0.1;
	setAttr -s 2 ".wl[328].w[0:1]"  0.12322621803737956 0.87677378196262046;
	setAttr -s 3 ".wl[329].w";
	setAttr ".wl[329].w[0:1]" 0.7907733416502607 0.10922665834973934;
	setAttr ".wl[329].w[11]" 0.1;
	setAttr -s 2 ".wl[330].w[0:1]"  0.0060080742238332171 0.9939919257761668;
	setAttr -s 2 ".wl[331].w[12:13]"  0.3 0.7;
	setAttr -s 2 ".wl[332].w[12:13]"  0.3 0.7;
	setAttr -s 2 ".wl[333].w[12:13]"  0.33246340556212228 0.66753659443787761;
	setAttr -s 2 ".wl[334].w[12:13]"  0.3 0.7;
	setAttr -s 2 ".wl[335].w[12:13]"  0.3 0.7;
	setAttr -s 3 ".wl[336].w[12:14]"  0.42200106395571124 0.46575644941978117 
		0.11224248662450757;
	setAttr -s 2 ".wl[337].w[12:13]"  0.44189084770647646 0.55810915229352354;
	setAttr -s 2 ".wl[338].w[12:13]"  0.3 0.7;
	setAttr -s 2 ".wl[339].w[12:13]"  0.3 0.7;
	setAttr -s 3 ".wl[340].w[12:14]"  0.22374023451397507 0.63181814944234649 
		0.14444161604367842;
	setAttr -s 4 ".wl[341].w";
	setAttr ".wl[341].w[12:14]" 0.34473711612050761 0.53391020728020366 0.11417650367971738;
	setAttr ".wl[341].w[22]" 0.0071761729195713997;
	setAttr -s 2 ".wl[342].w[12:13]"  0.3 0.7;
	setAttr ".wl[343].w[7]"  1;
	setAttr ".wl[344].w[7]"  1;
	setAttr ".wl[345].w[7]"  0.99999999999999989;
	setAttr ".wl[346].w[7]"  1;
	setAttr ".wl[347].w[7]"  1;
	setAttr ".wl[348].w[7]"  1;
	setAttr ".wl[349].w[7]"  1;
	setAttr ".wl[350].w[7]"  1;
	setAttr ".wl[351].w[7]"  1;
	setAttr ".wl[352].w[7]"  1;
	setAttr ".wl[353].w[7]"  1;
	setAttr ".wl[354].w[7]"  1;
	setAttr ".wl[355].w[7]"  1;
	setAttr ".wl[356].w[7]"  1;
	setAttr -s 2 ".wl[357].w[7:8]"  0.88635697023828974 0.11364302976171023;
	setAttr -s 2 ".wl[358].w[7:8]"  0.74108496732071605 0.25891503267928395;
	setAttr -s 2 ".wl[359].w[7:8]"  0.50648200377915797 0.49351799622084208;
	setAttr -s 2 ".wl[360].w[7:8]"  0.62530123589644815 0.37469876410355191;
	setAttr -s 2 ".wl[361].w[7:8]"  0.91720397659652564 0.082796023403474472;
	setAttr -s 2 ".wl[362].w[7:8]"  0.57200960861922423 0.42799039138077571;
	setAttr -s 2 ".wl[363].w[7:8]"  0.92816957555885338 0.071830424441146581;
	setAttr -s 2 ".wl[364].w[7:8]"  0.66683491449166377 0.33316508550833618;
	setAttr -s 2 ".wl[365].w[7:8]"  0.93672388006027218 0.063276119939727787;
	setAttr -s 2 ".wl[366].w[7:8]"  0.71871297489262043 0.28128702510737957;
	setAttr -s 2 ".wl[367].w[7:8]"  0.93432456404898701 0.065675435951012939;
	setAttr -s 2 ".wl[368].w[7:8]"  0.71042790168624692 0.28957209831375302;
	setAttr ".wl[369].w[7]"  1;
	setAttr -s 2 ".wl[370].w[7:8]"  0.70536349872388648 0.29463650127611374;
	setAttr -s 2 ".wl[371].w[7:8]"  0.60484989736665984 0.39515010263334027;
	setAttr -s 2 ".wl[372].w[7:8]"  0.50794014630705786 0.49205985369294203;
	setAttr -s 2 ".wl[373].w[7:8]"  0.56583079212593834 0.43416920787406166;
	setAttr -s 2 ".wl[374].w[7:8]"  0.66212360206789311 0.33787639793210689;
	setAttr -s 2 ".wl[375].w[7:8]"  0.71579259728414157 0.28420740271585837;
	setAttr -s 2 ".wl[376].w[7:8]"  0.71280376995169459 0.28719623004830536;
	setAttr -s 2 ".wl[377].w[7:8]"  0.7014899787878921 0.29851002121210812;
	setAttr -s 2 ".wl[378].w";
	setAttr ".wl[378].w[42]" 0.98861495803500221;
	setAttr ".wl[378].w[55]" 0.01138504196499778;
	setAttr -s 2 ".wl[379].w";
	setAttr ".wl[379].w[42]" 0.64446194148999325;
	setAttr ".wl[379].w[55]" 0.35553805851000669;
	setAttr ".wl[380].w[42]"  1;
	setAttr ".wl[381].w[42]"  1;
	setAttr -s 2 ".wl[382].w";
	setAttr ".wl[382].w[42]" 0.78040371767590511;
	setAttr ".wl[382].w[55]" 0.21959628232409473;
	setAttr -s 3 ".wl[383].w";
	setAttr ".wl[383].w[42]" 0.51415721337212195;
	setAttr ".wl[383].w[55:56]" 0.47671842312059287 0.0091243635072851791;
	setAttr -s 3 ".wl[384].w";
	setAttr ".wl[384].w[42]" 0.022559933252841911;
	setAttr ".wl[384].w[55:56]" 0.78036209137724033 0.19707797536991778;
	setAttr -s 2 ".wl[385].w";
	setAttr ".wl[385].w[42]" 0.60277322617835338;
	setAttr ".wl[385].w[55]" 0.39722677382164662;
	setAttr -s 2 ".wl[386].w";
	setAttr ".wl[386].w[42]" 0.96901824892151101;
	setAttr ".wl[386].w[52]" 0.030981751078489023;
	setAttr -s 3 ".wl[387].w";
	setAttr ".wl[387].w[42]" 0.34130177671023965;
	setAttr ".wl[387].w[49]" 0.17606607672437061;
	setAttr ".wl[387].w[52]" 0.48263214656538983;
	setAttr -s 2 ".wl[388].w";
	setAttr ".wl[388].w[42]" 0.25088863283435558;
	setAttr ".wl[388].w[52]" 0.74911136716564442;
	setAttr -s 2 ".wl[389].w";
	setAttr ".wl[389].w[42]" 0.40751124868365035;
	setAttr ".wl[389].w[52]" 0.5924887513163497;
	setAttr ".wl[390].w[42]"  1;
	setAttr -s 3 ".wl[391].w";
	setAttr ".wl[391].w[42:43]" 0.53400368068183091 0.42256999617663954;
	setAttr ".wl[391].w[46]" 0.043426323141529506;
	setAttr -s 2 ".wl[392].w";
	setAttr ".wl[392].w[42]" 0.46491235201942666;
	setAttr ".wl[392].w[46]" 0.5350876479805734;
	setAttr -s 3 ".wl[393].w[42:44]"  0.29773307723095321 0.66855944734691164 
		0.033707475422135189;
	setAttr -s 2 ".wl[394].w[44:45]"  0.0052772808274329178 0.99472271917796784;
	setAttr -s 2 ".wl[395].w[55:56]"  0.4707578840434703 0.5292421159565297;
	setAttr -s 2 ".wl[396].w[55:56]"  0.42867636426733008 0.57132363573266987;
	setAttr -s 2 ".wl[397].w[55:56]"  0.57250288617701228 0.42749711382298777;
	setAttr -s 3 ".wl[398].w";
	setAttr ".wl[398].w[42]" 0.0020625200892908173;
	setAttr ".wl[398].w[55:56]" 0.6809351842550021 0.31700229565570709;
	setAttr -s 2 ".wl[399].w[55:56]"  0.43682889740850644 0.56317110259149361;
	setAttr -s 3 ".wl[400].w";
	setAttr ".wl[400].w[42]" 0.00034847579158956332;
	setAttr ".wl[400].w[55:56]" 0.6445589925439813 0.35509253166442906;
	setAttr -s 2 ".wl[401].w[55:56]"  0.42712987626343696 0.57287012373656321;
	setAttr -s 3 ".wl[402].w";
	setAttr ".wl[402].w[42]" 0.021730524495992391;
	setAttr ".wl[402].w[55:56]" 0.70153950560459766 0.2767299698994099;
	setAttr ".wl[403].w[56]"  1;
	setAttr ".wl[404].w[56]"  1;
	setAttr -s 2 ".wl[405].w[55:56]"  0.0079984842767494273 0.99200151572325057;
	setAttr -s 2 ".wl[406].w[55:56]"  0.022730849359741245 0.97726915064025865;
	setAttr -s 2 ".wl[407].w[43:44]"  0.49999946850263066 0.50000053149736923;
	setAttr -s 2 ".wl[408].w[44:45]"  0.8509886218284437 0.14901137817155621;
	setAttr ".wl[409].w[44]"  1;
	setAttr -s 2 ".wl[410].w[43:44]"  0.46461645959936659 0.53538354040063341;
	setAttr -s 2 ".wl[411].w[43:44]"  0.48965457930492345 0.5103454206950766;
	setAttr -s 2 ".wl[412].w[43:44]"  0.0048509491643522456 0.99514905083564775;
	setAttr -s 3 ".wl[413].w[42:44]"  0.0053509160592922055 0.50792400552831229 
		0.48672507841239543;
	setAttr -s 2 ".wl[414].w[43:44]"  0.00063302730997820704 0.99936697269002184;
	setAttr -s 2 ".wl[415].w[47:48]"  0.55415934714081017 0.44584065285918983;
	setAttr -s 2 ".wl[416].w[47:48]"  0.99859628059691197 0.0014037194030879921;
	setAttr -s 3 ".wl[417].w";
	setAttr ".wl[417].w[42]" 0.32931453044193915;
	setAttr ".wl[417].w[46]" 0.11297529674287014;
	setAttr ".wl[417].w[49]" 0.55771017281519075;
	setAttr -s 2 ".wl[418].w[46:47]"  0.99919664297865074 0.00080335702134924983;
	setAttr -s 2 ".wl[419].w[46:47]"  0.96423102007972794 0.035768979920271983;
	setAttr -s 2 ".wl[420].w";
	setAttr ".wl[420].w[42]" 0.36657793793042737;
	setAttr ".wl[420].w[46]" 0.63342206206957252;
	setAttr -s 2 ".wl[421].w";
	setAttr ".wl[421].w[42]" 0.0060208773674084439;
	setAttr ".wl[421].w[46]" 0.99397912263259158;
	setAttr -s 2 ".wl[422].w[46:47]"  0.95110141994153452 0.048898580058465438;
	setAttr -s 2 ".wl[423].w[50:51]"  0.51906279416971268 0.48093720583028748;
	setAttr ".wl[424].w[51]"  1;
	setAttr ".wl[425].w[51]"  0.99999999999999989;
	setAttr -s 2 ".wl[426].w[50:51]"  0.51525552197820768 0.48474447802179227;
	setAttr ".wl[427].w[51]"  1;
	setAttr -s 2 ".wl[428].w[50:51]"  0.50126411554963857 0.49873588445036149;
	setAttr ".wl[429].w[51]"  1;
	setAttr -s 2 ".wl[430].w[50:51]"  0.50098884259004628 0.49901115740995372;
	setAttr -s 2 ".wl[431].w[49:50]"  0.50033392619872974 0.49966607380127032;
	setAttr -s 2 ".wl[432].w[49:50]"  0.433919675605136 0.566080324394864;
	setAttr -s 2 ".wl[433].w[49:50]"  0.0097818744771956696 0.99021812552280442;
	setAttr -s 2 ".wl[434].w[50:51]"  0.99425709496114179 0.0057429050388582452;
	setAttr -s 2 ".wl[435].w[49:50]"  0.43460434150356969 0.56539565849643025;
	setAttr -s 2 ".wl[436].w[50:51]"  0.95719272414353229 0.042807275856467791;
	setAttr -s 2 ".wl[437].w[49:50]"  0.50213853386777196 0.49786146613222798;
	setAttr -s 2 ".wl[438].w[50:51]"  0.9683212792835787 0.031678720716421305;
	setAttr -s 2 ".wl[439].w[49:50]"  0.97871568235279327 0.021284317647206709;
	setAttr -s 2 ".wl[440].w[49:50]"  0.96538332327783927 0.034616676722160761;
	setAttr -s 2 ".wl[441].w[49:50]"  0.99635768141694325 0.0036423185830568409;
	setAttr -s 2 ".wl[442].w[49:50]"  0.99470105965296363 0.0052989403470363964;
	setAttr -s 3 ".wl[443].w[52:54]"  0.005731790724941203 0.50817117617777319 
		0.48609703309728558;
	setAttr ".wl[444].w[54]"  1;
	setAttr ".wl[445].w[54]"  0.99999999999999989;
	setAttr -s 2 ".wl[446].w[53:54]"  0.48868391650420634 0.51131608349579361;
	setAttr -s 2 ".wl[447].w[53:54]"  0.00095483684763486207 0.99904516315236525;
	setAttr -s 2 ".wl[448].w[53:54]"  0.50492799482970374 0.4950720051702962;
	setAttr -s 2 ".wl[449].w[53:54]"  0.0011786216608485761 0.99882137833915152;
	setAttr -s 2 ".wl[450].w[53:54]"  0.50458033784338119 0.49541966215661876;
	setAttr -s 2 ".wl[451].w[52:53]"  0.51125989397959526 0.48874010602040485;
	setAttr -s 2 ".wl[452].w[52:53]"  0.53241773584192009 0.46758226415807996;
	setAttr -s 2 ".wl[453].w[52:53]"  0.31543346750755352 0.68456653249244637;
	setAttr -s 2 ".wl[454].w[52:53]"  0.27859366989123985 0.7214063301087601;
	setAttr -s 2 ".wl[455].w[52:53]"  0.54734051962381791 0.45265948037618214;
	setAttr -s 3 ".wl[456].w[52:54]"  0.26324272817047073 0.72282375717031244 
		0.013933514659216859;
	setAttr -s 2 ".wl[457].w[52:53]"  0.50248266826664256 0.49751733173335749;
	setAttr -s 3 ".wl[458].w[52:54]"  0.24825365301133587 0.75033816142701959 
		0.0014081855616446372;
	setAttr -s 2 ".wl[459].w";
	setAttr ".wl[459].w[42]" 0.0055945940015072054;
	setAttr ".wl[459].w[52]" 0.9944054059984927;
	setAttr -s 2 ".wl[460].w[52:53]"  0.98961195255002066 0.010388047449979333;
	setAttr -s 2 ".wl[461].w[52:53]"  0.98346176979108291 0.016538230208917114;
	setAttr -s 2 ".wl[462].w[52:53]"  0.99271205580073063 0.0072879441992693694;
	setAttr ".wl[463].w[42]"  1;
	setAttr -s 2 ".wl[464].w";
	setAttr ".wl[464].w[42]" 0.2810609550961779;
	setAttr ".wl[464].w[55]" 0.7189390449038221;
	setAttr -s 2 ".wl[465].w";
	setAttr ".wl[465].w[42]" 0.35985465530964622;
	setAttr ".wl[465].w[52]" 0.64014534469035378;
	setAttr -s 3 ".wl[466].w";
	setAttr ".wl[466].w[42]" 0.70763757262143556;
	setAttr ".wl[466].w[55:56]" 0.027449337327997807 0.26491309005056657;
	setAttr -s 2 ".wl[467].w";
	setAttr ".wl[467].w[42]" 0.61356422269991218;
	setAttr ".wl[467].w[46]" 0.38643577730008771;
	setAttr ".wl[468].w[42]"  1;
	setAttr -s 2 ".wl[469].w";
	setAttr ".wl[469].w[42]" 0.45829823436524092;
	setAttr ".wl[469].w[52]" 0.54170176563475914;
	setAttr -s 2 ".wl[470].w";
	setAttr ".wl[470].w[42]" 0.19614975958785485;
	setAttr ".wl[470].w[52]" 0.80385024041214526;
	setAttr -s 2 ".wl[471].w[46:47]"  0.51084553533518695 0.48915446466481316;
	setAttr -s 2 ".wl[472].w";
	setAttr ".wl[472].w[42]" 0.18841023739328058;
	setAttr ".wl[472].w[52]" 0.81158976260671944;
	setAttr -s 2 ".wl[473].w";
	setAttr ".wl[473].w[42]" 0.24545386765813032;
	setAttr ".wl[473].w[52]" 0.75454613234186962;
	setAttr -s 2 ".wl[474].w";
	setAttr ".wl[474].w[42]" 0.30087135999949649;
	setAttr ".wl[474].w[52]" 0.69912864000050368;
	setAttr -s 2 ".wl[475].w[44:45]"  0.44435544485098577 0.55564455514901423;
	setAttr -s 2 ".wl[476].w[44:45]"  0.4747698526346153 0.52523014736547491;
	setAttr -s 2 ".wl[477].w[44:45]"  0.49821106662803294 0.50178893337196706;
	setAttr -s 2 ".wl[478].w[44:45]"  0.52695001338898007 0.47304998661102005;
	setAttr ".wl[479].w[48]"  1;
	setAttr ".wl[480].w[48]"  1;
	setAttr ".wl[481].w[48]"  1;
	setAttr ".wl[482].w[48]"  1;
	setAttr ".wl[483].w[56]"  1;
	setAttr ".wl[484].w[56]"  1;
	setAttr -s 3 ".wl[485].w[40:42]"  0.041222913596055705 0.9337770864039443 
		0.025;
	setAttr -s 2 ".wl[486].w[41:42]"  0.9 0.1;
	setAttr -s 3 ".wl[487].w[40:42]"  0.090908855013549339 0.88409114498645069 
		0.025;
	setAttr -s 3 ".wl[488].w[40:42]"  0.027409354703018082 0.94759064529698189 
		0.025;
	setAttr -s 2 ".wl[489].w[41:42]"  0.9 0.1;
	setAttr -s 4 ".wl[490].w";
	setAttr ".wl[490].w[42]" 0.5;
	setAttr ".wl[490].w[46]" 0.13518039548556951;
	setAttr ".wl[490].w[49]" 0.14701252675478904;
	setAttr ".wl[490].w[56]" 0.21780707775964145;
	setAttr -s 2 ".wl[491].w[41:42]"  0.5 0.5;
	setAttr -s 2 ".wl[492].w[41:42]"  0.9 0.1;
	setAttr -s 2 ".wl[493].w[41:42]"  0.5 0.5;
	setAttr -s 3 ".wl[494].w[40:42]"  0.027679461473599079 0.94732053852640086 
		0.025;
	setAttr -s 2 ".wl[495].w[41:42]"  0.9 0.1;
	setAttr -s 2 ".wl[496].w[41:42]"  0.5 0.5;
	setAttr -s 3 ".wl[497].w[40:42]"  0.39000000000000007 0.585 0.025;
	setAttr -s 2 ".wl[498].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[499].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[500].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[501].w[23:24]"  0.5 0.5;
	setAttr -s 3 ".wl[502].w[22:24]"  0.39000000000000007 0.585 0.025;
	setAttr -s 3 ".wl[503].w[22:24]"  0.585 0.39000000000000007 0.025;
	setAttr -s 3 ".wl[504].w[22:24]"  0.03482372565194964 0.94017627434805029 
		0.025;
	setAttr -s 3 ".wl[505].w[22:24]"  0.027679461473599079 0.94732053852640086 
		0.025;
	setAttr -s 2 ".wl[506].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[507].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[508].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[509].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[510].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[511].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[512].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[513].w[23:24]"  0.5 0.5;
	setAttr -s 3 ".wl[514].w[22:24]"  0.090908855745216788 0.88409114425478319 
		0.025;
	setAttr -s 3 ".wl[515].w[22:24]"  0.09704652654847519 0.87795347345152475 
		0.025;
	setAttr -s 2 ".wl[516].w[23:24]"  0.9 0.1;
	setAttr -s 2 ".wl[517].w[23:24]"  0.9 0.1;
	setAttr -s 2 ".wl[518].w[23:24]"  0.9 0.1;
	setAttr -s 2 ".wl[519].w[23:24]"  0.9 0.1;
	setAttr -s 3 ".wl[520].w[22:24]"  0.027409354703018082 0.94759064529698189 
		0.025;
	setAttr -s 2 ".wl[521].w[23:24]"  0.9 0.1;
	setAttr -s 2 ".wl[522].w[23:24]"  0.9 0.1;
	setAttr -s 3 ".wl[523].w[22:24]"  0.034620072404314355 0.94037992759568567 
		0.025;
	setAttr -s 3 ".wl[524].w[22:24]"  0.041222913596055705 0.9337770864039443 
		0.025;
	setAttr -s 3 ".wl[525].w[22:24]"  0.057586248386241295 0.91741375161375871 
		0.025;
	setAttr -s 2 ".wl[526].w[24:25]"  0.80974637949436701 0.19025362050563302;
	setAttr -s 2 ".wl[527].w[25:26]"  0.5 0.5;
	setAttr -s 2 ".wl[528].w[25:26]"  0.4896545467841294 0.51034545321587055;
	setAttr -s 2 ".wl[529].w[24:25]"  0.30562381507580066 0.69437618492419928;
	setAttr ".wl[530].w[24]"  0.99999999999999989;
	setAttr ".wl[531].w[24]"  1;
	setAttr ".wl[532].w[24]"  1;
	setAttr ".wl[533].w[24]"  1;
	setAttr -s 3 ".wl[534].w";
	setAttr ".wl[534].w[24:25]" 0.53400456032430776 0.42257036625518074;
	setAttr ".wl[534].w[28]" 0.043425073420511419;
	setAttr -s 2 ".wl[535].w[24:25]"  0.93183518490676687 0.068164815093233092;
	setAttr ".wl[536].w[24]"  1;
	setAttr -s 2 ".wl[537].w[24:25]"  0.50999607606079866 0.49000392393920134;
	setAttr -s 3 ".wl[538].w[24:26]"  0.0053504156535645756 0.50792635440456713 
		0.48672322994186823;
	setAttr -s 2 ".wl[539].w[25:26]"  0.46461543660437277 0.53538456339562723;
	setAttr -s 3 ".wl[540].w[24:26]"  0.29773237942375247 0.66855595500168086 
		0.033711665574566653;
	setAttr -s 2 ".wl[541].w[24:25]"  0.24231142636761019 0.75768857363238973;
	setAttr -s 2 ".wl[542].w";
	setAttr ".wl[542].w[24]" 0.40470750951010381;
	setAttr ".wl[542].w[31]" 0.5952924904898963;
	setAttr -s 2 ".wl[543].w[31:32]"  0.97871608761265083 0.021283912387349078;
	setAttr -s 3 ".wl[544].w";
	setAttr ".wl[544].w[24]" 0.95140329798392709;
	setAttr ".wl[544].w[28]" 0.0058069668490294121;
	setAttr ".wl[544].w[31]" 0.042789735167043604;
	setAttr -s 2 ".wl[545].w[31:32]"  0.96538398904201317 0.034616010957986908;
	setAttr -s 2 ".wl[546].w";
	setAttr ".wl[546].w[24]" 0.94876664941347799;
	setAttr ".wl[546].w[34]" 0.051233350586522008;
	setAttr -s 2 ".wl[547].w";
	setAttr ".wl[547].w[24]" 0.19615048123338635;
	setAttr ".wl[547].w[34]" 0.8038495187666137;
	setAttr -s 2 ".wl[548].w";
	setAttr ".wl[548].w[24]" 0.35985387657026563;
	setAttr ".wl[548].w[34]" 0.64014612342973443;
	setAttr -s 2 ".wl[549].w";
	setAttr ".wl[549].w[24]" 0.18841145851472887;
	setAttr ".wl[549].w[34]" 0.81158854148527126;
	setAttr -s 2 ".wl[550].w";
	setAttr ".wl[550].w[24]" 0.30087076561430864;
	setAttr ".wl[550].w[34]" 0.69912923438569152;
	setAttr ".wl[551].w[24]"  1;
	setAttr ".wl[552].w[24]"  1;
	setAttr -s 3 ".wl[553].w";
	setAttr ".wl[553].w[24]" 0.51415722677562392;
	setAttr ".wl[553].w[37:38]" 0.47671836581400173 0.0091244074103743848;
	setAttr -s 2 ".wl[554].w";
	setAttr ".wl[554].w[24]" 0.28106096199204539;
	setAttr ".wl[554].w[37]" 0.71893903800795467;
	setAttr -s 2 ".wl[555].w";
	setAttr ".wl[555].w[24]" 0.64446240366043095;
	setAttr ".wl[555].w[37]" 0.35553759633956911;
	setAttr -s 2 ".wl[556].w";
	setAttr ".wl[556].w[24]" 0.78315751388010435;
	setAttr ".wl[556].w[37]" 0.21684248611989568;
	setAttr ".wl[557].w[24]"  1;
	setAttr ".wl[558].w[24]"  1;
	setAttr -s 2 ".wl[559].w";
	setAttr ".wl[559].w[24]" 0.45829256182130368;
	setAttr ".wl[559].w[34]" 0.54170743817869638;
	setAttr -s 2 ".wl[560].w[32:33]"  0.96832594812798356 0.031674051872016377;
	setAttr -s 2 ".wl[561].w[32:33]"  0.51525691908072002 0.48474308091927987;
	setAttr -s 2 ".wl[562].w[32:33]"  0.51905958530638563 0.48094041469361432;
	setAttr -s 2 ".wl[563].w[32:33]"  0.99425782812370866 0.0057421718762913554;
	setAttr -s 3 ".wl[564].w";
	setAttr ".wl[564].w[24]" 0.34130168682657924;
	setAttr ".wl[564].w[31]" 0.17606619240078081;
	setAttr ".wl[564].w[34]" 0.48263212077263995;
	setAttr -s 2 ".wl[565].w[31:32]"  0.99470103449490221 0.0052989655050977901;
	setAttr -s 2 ".wl[566].w[31:32]"  0.99635757896226873 0.0036424210377312021;
	setAttr -s 3 ".wl[567].w";
	setAttr ".wl[567].w[24]" 0.3293147303173431;
	setAttr ".wl[567].w[28]" 0.11297431031173265;
	setAttr ".wl[567].w[31]" 0.5577109593709243;
	setAttr -s 2 ".wl[568].w[31:32]"  0.0097847037573305998 0.99021529624266935;
	setAttr -s 2 ".wl[569].w[32:33]"  0.50098979603494209 0.49901020396505791;
	setAttr -s 2 ".wl[570].w[32:33]"  0.50126414967451605 0.49873585032548384;
	setAttr -s 2 ".wl[571].w[32:33]"  0.95718954220281616 0.042810457797183808;
	setAttr ".wl[572].w[33]"  1;
	setAttr ".wl[573].w[33]"  1;
	setAttr ".wl[574].w[33]"  1;
	setAttr ".wl[575].w[33]"  1;
	setAttr ".wl[576].w[38]"  1;
	setAttr ".wl[577].w[38]"  1;
	setAttr ".wl[578].w[38]"  1;
	setAttr ".wl[579].w[38]"  1;
	setAttr -s 3 ".wl[580].w";
	setAttr ".wl[580].w[24]" 0.022559045277778578;
	setAttr ".wl[580].w[37:38]" 0.78036244911679697 0.19707850560542453;
	setAttr -s 3 ".wl[581].w";
	setAttr ".wl[581].w[24]" 0.70763759615125521;
	setAttr ".wl[581].w[37:38]" 0.027448985585705257 0.2649134182630396;
	setAttr -s 2 ".wl[582].w";
	setAttr ".wl[582].w[24]" 0.60277306464163205;
	setAttr ".wl[582].w[37]" 0.39722693535836789;
	setAttr -s 2 ".wl[583].w[37:38]"  0.0079983484592291813 0.9920016515407708;
	setAttr ".wl[584].w[38]"  1;
	setAttr ".wl[585].w[38]"  1;
	setAttr -s 2 ".wl[586].w[37:38]"  0.022732291955008428 0.97726770804499163;
	setAttr -s 2 ".wl[587].w";
	setAttr ".wl[587].w[24]" 0.98861468812966102;
	setAttr ".wl[587].w[37]" 0.011385311870338966;
	setAttr -s 2 ".wl[588].w[37:38]"  0.57250282507289385 0.42749717492710615;
	setAttr -s 3 ".wl[589].w";
	setAttr ".wl[589].w[24]" 0.0020629652018015975;
	setAttr ".wl[589].w[37:38]" 0.68093475615453491 0.31700227864366359;
	setAttr ".wl[590].w[38]"  1;
	setAttr ".wl[591].w[38]"  1;
	setAttr -s 3 ".wl[592].w";
	setAttr ".wl[592].w[24]" 0.00034853547081660973;
	setAttr ".wl[592].w[37:38]" 0.64455919581322052 0.35509226871596283;
	setAttr -s 3 ".wl[593].w";
	setAttr ".wl[593].w[24]" 0.021730514270906453;
	setAttr ".wl[593].w[37:38]" 0.70153972439716872 0.27672976133192478;
	setAttr -s 2 ".wl[594].w[34:35]"  0.98961294000560351 0.010387059994396489;
	setAttr -s 2 ".wl[595].w";
	setAttr ".wl[595].w[24]" 0.0055982404168226779;
	setAttr ".wl[595].w[34]" 0.99440175958317734;
	setAttr -s 3 ".wl[596].w[34:36]"  0.24825178772926143 0.75033627634096467 
		0.0014119359297740655;
	setAttr -s 2 ".wl[597].w[35:36]"  0.48868917280435076 0.51131082719564946;
	setAttr -s 3 ".wl[598].w[34:36]"  0.0057297730333546858 0.50817524111719736 
		0.48609498590106887;
	setAttr -s 2 ".wl[599].w[34:35]"  0.2785906919497198 0.72140930805028014;
	setAttr -s 2 ".wl[600].w";
	setAttr ".wl[600].w[24]" 0.25088962202781229;
	setAttr ".wl[600].w[34]" 0.74911037797218771;
	setAttr -s 2 ".wl[601].w[34:35]"  0.99271205843573196 0.0072879415642680429;
	setAttr -s 2 ".wl[602].w[34:35]"  0.98346244248786174 0.016537557512138167;
	setAttr -s 2 ".wl[603].w[34:35]"  0.31543450700092579 0.68456549299907421;
	setAttr -s 2 ".wl[604].w[35:36]"  0.50458010650667162 0.49541989349332838;
	setAttr -s 2 ".wl[605].w[35:36]"  0.50492781056877545 0.49507218943122461;
	setAttr -s 3 ".wl[606].w[34:36]"  0.26324269870637174 0.72282388488872518 
		0.013933416404903068;
	setAttr -s 2 ".wl[607].w";
	setAttr ".wl[607].w[24]" 0.24545368857140992;
	setAttr ".wl[607].w[34]" 0.75454631142858997;
	setAttr -s 2 ".wl[608].w";
	setAttr ".wl[608].w[24]" 0.40751009637827085;
	setAttr ".wl[608].w[34]" 0.59248990362172915;
	setAttr ".wl[609].w[36]"  0.99999999999999989;
	setAttr -s 2 ".wl[610].w[35:36]"  0.00095474073001617602 0.99904525926998367;
	setAttr -s 2 ".wl[611].w[35:36]"  0.0011784675214004962 0.99882153247859962;
	setAttr ".wl[612].w[36]"  1;
	setAttr -s 2 ".wl[613].w[28:29]"  0.51084598527930891 0.48915401472069114;
	setAttr -s 2 ".wl[614].w[28:29]"  0.50711239839265421 0.4928876016073459;
	setAttr -s 2 ".wl[615].w[28:29]"  0.96423007307042974 0.035769926929570262;
	setAttr -s 2 ".wl[616].w[28:29]"  0.95110336078903435 0.048896639210965792;
	setAttr -s 2 ".wl[617].w[29:30]"  0.99859654809270515 0.0014034519072946804;
	setAttr -s 2 ".wl[618].w[28:29]"  0.00067901985651703384 0.99932098014348292;
	setAttr -s 2 ".wl[619].w[29:30]"  0.55416322780722882 0.44583677219277107;
	setAttr ".wl[620].w[30]"  1;
	setAttr ".wl[621].w[30]"  1;
	setAttr -s 2 ".wl[622].w[29:30]"  0.5831233318650545 0.41687666813494545;
	setAttr -s 2 ".wl[623].w[28:29]"  0.99919969612898507 0.00080030387101487973;
	setAttr -s 2 ".wl[624].w[28:29]"  0.5 0.5;
	setAttr -s 2 ".wl[625].w[28:29]"  0.5 0.5;
	setAttr -s 2 ".wl[626].w";
	setAttr ".wl[626].w[24]" 0.0060200361478653305;
	setAttr ".wl[626].w[28]" 0.99397996385213472;
	setAttr -s 2 ".wl[627].w[29:30]"  0.58382504453164019 0.41617495546835975;
	setAttr ".wl[628].w[30]"  1;
	setAttr ".wl[629].w[30]"  1;
	setAttr -s 2 ".wl[630].w[29:30]"  0.59166326122524926 0.40833673877475063;
	setAttr -s 2 ".wl[631].w[29:30]"  0.99318849669640152 0.0068115033035984702;
	setAttr -s 2 ".wl[632].w[25:26]"  0.00063310480132638589 0.99936689519867372;
	setAttr -s 2 ".wl[633].w[26:27]"  0.52695074803475161 0.47304925196524839;
	setAttr -s 2 ".wl[634].w[26:27]"  0.47476978343374887 0.52523021656625113;
	setAttr ".wl[635].w[26]"  1;
	setAttr -s 2 ".wl[636].w[26:27]"  0.0052722151227922124 0.99472778487720781;
	setAttr ".wl[637].w[27]"  1;
	setAttr -s 2 ".wl[638].w[26:27]"  0.85099234918121514 0.14900765081878475;
	setAttr -s 2 ".wl[639].w[26:27]"  0.44436024425644471 0.55563975574355529;
	setAttr -s 2 ".wl[640].w[26:27]"  0.49821099866502144 0.5017890013349785;
	setAttr -s 2 ".wl[641].w[25:26]"  0.0048537943525673085 0.99514620564743272;
	setAttr ".wl[642].w[27]"  1;
	setAttr -s 2 ".wl[643].w[26:27]"  0.020195359111476158 0.97980464088852381;
	setAttr ".wl[644].w[27]"  1;
	setAttr ".wl[645].w[27]"  1;
	setAttr ".wl[646].w[27]"  1;
	setAttr -s 2 ".wl[647].w[24:25]"  0.47669821377093508 0.52330178622906498;
	setAttr -s 2 ".wl[648].w";
	setAttr ".wl[648].w[24]" 0.96901951002444842;
	setAttr ".wl[648].w[34]" 0.030980489975551626;
	setAttr -s 3 ".wl[649].w";
	setAttr ".wl[649].w[24]" 0.61356701035203165;
	setAttr ".wl[649].w[28]" 0.38643297142295596;
	setAttr ".wl[649].w[34]" 1.8225012359607338e-008;
	setAttr -s 2 ".wl[650].w";
	setAttr ".wl[650].w[24]" 0.46491206266370239;
	setAttr ".wl[650].w[28]" 0.53508793733629756;
	setAttr -s 2 ".wl[651].w";
	setAttr ".wl[651].w[24]" 0.456381927001968;
	setAttr ".wl[651].w[28]" 0.543618072998032;
	setAttr -s 2 ".wl[652].w";
	setAttr ".wl[652].w[24]" 0.36658079401862159;
	setAttr ".wl[652].w[28]" 0.63341920598137846;
	setAttr -s 2 ".wl[653].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[654].w[23:24]"  0.4 0.6;
	setAttr -s 2 ".wl[655].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[656].w";
	setAttr ".wl[656].w[24]" 0.50774290902932784;
	setAttr ".wl[656].w[28]" 0.49225709097067211;
	setAttr -s 2 ".wl[657].w";
	setAttr ".wl[657].w[24]" 0.72108043355295615;
	setAttr ".wl[657].w[28]" 0.27891956644704402;
	setAttr ".wl[658].w[24]"  1;
	setAttr -s 2 ".wl[659].w[34:35]"  0.51126073560444241 0.48873926439555759;
	setAttr -s 2 ".wl[660].w[34:35]"  0.53241883761095088 0.46758116238904923;
	setAttr -s 2 ".wl[661].w[34:35]"  0.54734088423888594 0.45265911576111406;
	setAttr -s 2 ".wl[662].w[34:35]"  0.50248273982632197 0.49751726017367814;
	setAttr -s 2 ".wl[663].w[31:32]"  0.500337365634138 0.499662634365862;
	setAttr -s 2 ".wl[664].w[31:32]"  0.43392002714941408 0.56607997285058598;
	setAttr -s 2 ".wl[665].w[31:32]"  0.43460504412677203 0.56539495587322786;
	setAttr -s 2 ".wl[666].w[31:32]"  0.5021390802445993 0.49786091975540059;
	setAttr -s 2 ".wl[667].w[37:38]"  0.47075434575406083 0.52924565424593917;
	setAttr -s 2 ".wl[668].w[37:38]"  0.42867609236391624 0.57132390763608387;
	setAttr -s 2 ".wl[669].w[37:38]"  0.4368286624455488 0.56317133755445115;
	setAttr -s 2 ".wl[670].w[37:38]"  0.42713265175322584 0.57286734824677432;
	setAttr ".wl[671].w[3]"  1;
	setAttr ".wl[672].w[3]"  1;
	setAttr ".wl[673].w[3]"  0.99999999999999989;
	setAttr ".wl[674].w[3]"  1;
	setAttr ".wl[675].w[3]"  1;
	setAttr ".wl[676].w[3]"  1;
	setAttr ".wl[677].w[3]"  1;
	setAttr ".wl[678].w[3]"  0.99999999999999989;
	setAttr ".wl[679].w[3]"  1;
	setAttr ".wl[680].w[3]"  1;
	setAttr ".wl[681].w[3]"  1;
	setAttr ".wl[682].w[3]"  1;
	setAttr ".wl[683].w[3]"  1;
	setAttr ".wl[684].w[3]"  1;
	setAttr -s 2 ".wl[685].w[3:4]"  0.70148998306723087 0.29851001693276924;
	setAttr -s 2 ".wl[686].w[3:4]"  0.60484989164783232 0.39515010835216774;
	setAttr -s 2 ".wl[687].w[3:4]"  0.62530120734759886 0.37469879265240119;
	setAttr -s 2 ".wl[688].w[3:4]"  0.70536347265834465 0.29463652734165552;
	setAttr ".wl[689].w[3]"  1;
	setAttr -s 2 ".wl[690].w[3:4]"  0.88635697680385572 0.11364302319614433;
	setAttr ".wl[691].w[3]"  1;
	setAttr ".wl[692].w[3]"  1;
	setAttr -s 2 ".wl[693].w[3:4]"  0.71280376780814181 0.28719623219185825;
	setAttr -s 2 ".wl[694].w[3:4]"  0.71042790185600746 0.28957209814399254;
	setAttr -s 2 ".wl[695].w[3:4]"  0.93432446997959862 0.065675530020401413;
	setAttr ".wl[696].w[3]"  1;
	setAttr -s 2 ".wl[697].w[3:4]"  0.71579259721112432 0.28420740278887563;
	setAttr -s 2 ".wl[698].w[3:4]"  0.71871298100818526 0.28128701899181474;
	setAttr -s 2 ".wl[699].w[3:4]"  0.9367238840904295 0.063276115909570593;
	setAttr ".wl[700].w[3]"  1;
	setAttr -s 2 ".wl[701].w[3:4]"  0.66683490728641259 0.33316509271358735;
	setAttr -s 2 ".wl[702].w[3:4]"  0.66212360315598673 0.33787639684401327;
	setAttr -s 2 ".wl[703].w[3:4]"  0.92816955472260343 0.07183044527739646;
	setAttr ".wl[704].w[3]"  1;
	setAttr -s 2 ".wl[705].w[3:4]"  0.56583075446203879 0.43416924553796121;
	setAttr -s 2 ".wl[706].w[3:4]"  0.57200954225873613 0.42799045774126376;
	setAttr -s 2 ".wl[707].w[3:4]"  0.91720394940476557 0.082796050595234386;
	setAttr ".wl[708].w[3]"  1;
	setAttr -s 2 ".wl[709].w[3:4]"  0.50794014442622881 0.49205985557377124;
	setAttr -s 2 ".wl[710].w[3:4]"  0.50648200726191728 0.49351799273808272;
	setAttr -s 2 ".wl[711].w[3:4]"  0.74108498424341496 0.25891501575658504;
	setAttr ".wl[712].w[3]"  1;
	setAttr -s 2 ".wl[713].w[12:13]"  0.38926366970587323 0.61073633029412677;
	setAttr -s 3 ".wl[714].w";
	setAttr ".wl[714].w[12:13]" 0.3958094153390585 0.57662252561884542;
	setAttr ".wl[714].w[22]" 0.027568059042096138;
	setAttr -s 4 ".wl[715].w";
	setAttr ".wl[715].w[12:13]" 0.39592290382452722 0.47504598802313885;
	setAttr ".wl[715].w[21:22]" 0.05984527749962007 0.069185830652713776;
	setAttr -s 3 ".wl[716].w";
	setAttr ".wl[716].w[12:13]" 0.44163741346038621 0.5511946316104408;
	setAttr ".wl[716].w[22]" 0.0071679549291729927;
	setAttr -s 2 ".wl[717].w[12:13]"  0.3 0.7;
	setAttr -s 2 ".wl[718].w[12:13]"  0.3 0.69999999999999984;
	setAttr -s 3 ".wl[719].w";
	setAttr ".wl[719].w[12:13]" 0.29784714812412871 0.69497667895629989;
	setAttr ".wl[719].w[22]" 0.0071761729195713997;
	setAttr -s 4 ".wl[720].w";
	setAttr ".wl[720].w[12:13]" 0.32751004919049959 0.41364594791683212;
	setAttr ".wl[720].w[21:22]" 0.13495855759590392 0.12388544529676437;
	setAttr -s 2 ".wl[721].w[12:13]"  0.3 0.7;
	setAttr -s 3 ".wl[722].w[12:14]"  0.42199947858534304 0.46575772647905445 
		0.11224279493560256;
	setAttr -s 3 ".wl[723].w";
	setAttr ".wl[723].w[12:13]" 0.3282161575377523 0.65903007736349706;
	setAttr ".wl[723].w[22]" 0.012753765098750591;
	setAttr -s 4 ".wl[724].w";
	setAttr ".wl[724].w[12:13]" 0.28700500462733364 0.35737554379438063;
	setAttr ".wl[724].w[21:22]" 0.16294885295214903 0.19267059862613678;
	setAttr -s 2 ".wl[725].w[12:13]"  0.30000543781405131 0.69999456218594869;
	setAttr -s 3 ".wl[726].w[12:14]"  0.45352425382850525 0.54647444912111476 
		1.2970503798954031e-006;
	setAttr -s 2 ".wl[727].w[0:1]"  0.87192206422696916 0.12807793577303073;
	setAttr -s 2 ".wl[728].w[0:1]"  0.69994849655764013 0.30005150344235992;
	setAttr ".wl[729].w[0]"  1;
	setAttr -s 2 ".wl[730].w[0:1]"  0.14203565396229981 0.85796434603770022;
	setAttr -s 2 ".wl[731].w[0:1]"  0.84327292267358778 0.15672707732641225;
	setAttr -s 2 ".wl[732].w[0:1]"  0.98029962049986019 0.019700379500139722;
	setAttr -s 2 ".wl[733].w[12:13]"  0.4347285493104015 0.5652714506895985;
	setAttr -s 2 ".wl[734].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[735].w[22:23]"  0.49999986077939212 0.50000013922060793;
	setAttr -s 3 ".wl[736].w[22:24]"  0.097499999999999989 0.8775 0.025;
	setAttr -s 3 ".wl[737].w[22:24]"  0.097499999999999989 0.8775 0.025;
	setAttr -s 2 ".wl[738].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[739].w[22:24]"  0.38999999999999996 0.585 0.025;
	setAttr -s 3 ".wl[740].w[22:24]"  0.093972602114081372 0.88102739788591866 
		0.025;
	setAttr -s 2 ".wl[741].w[22:23]"  0.49999988028063452 0.50000011971936542;
	setAttr -s 2 ".wl[742].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[743].w[22:24]"  0.048749999999999995 0.92625 0.025;
	setAttr -s 3 ".wl[744].w[22:24]"  0.585 0.38999999999999996 0.025;
	setAttr -s 3 ".wl[745].w[22:24]"  0.031860533265714489 0.9431394667342855 
		0.025;
	setAttr -s 3 ".wl[746].w[22:24]"  0.0053346122615039338 0.96966538773849609 
		0.025;
	setAttr -s 2 ".wl[747].w[23:24]"  0.9 0.1;
	setAttr -s 2 ".wl[748].w[23:24]"  0.9 0.1;
	setAttr -s 2 ".wl[749].w[23:24]"  0.7 0.30000000000000004;
	setAttr -s 2 ".wl[750].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[751].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[752].w[22:24]"  0.585 0.38999999999999996 0.025;
	setAttr ".wl[753].w[0]"  1;
	setAttr -s 3 ".wl[754].w";
	setAttr ".wl[754].w[0:1]" 0.83081657709299639 0.069183422907003592;
	setAttr ".wl[754].w[11]" 0.1;
	setAttr ".wl[755].w[0]"  1;
	setAttr -s 2 ".wl[756].w[0:1]"  0.9742527186087907 0.025747281391209322;
	setAttr ".wl[757].w[0]"  1;
	setAttr -s 4 ".wl[758].w";
	setAttr ".wl[758].w[12:13]" 0.37181739244863737 0.4506579392764043;
	setAttr ".wl[758].w[21:22]" 0.082840760348875692 0.094683907926082611;
	setAttr ".wl[759].w[3]"  1;
	setAttr ".wl[760].w[3]"  1;
	setAttr ".wl[761].w[3]"  1;
	setAttr ".wl[762].w[3]"  1;
	setAttr -s 2 ".wl[763].w[2:3]"  0.011144916988818645 0.9888550830111813;
	setAttr ".wl[764].w[3]"  1;
	setAttr ".wl[765].w[3]"  1;
	setAttr ".wl[766].w[3]"  1;
	setAttr -s 2 ".wl[767].w[24:25]"  0.81570278213384761 0.18429721786620412;
	setAttr -s 2 ".wl[768].w";
	setAttr ".wl[768].w[24]" 0.77894877251533789;
	setAttr ".wl[768].w[28]" 0.22105122748466199;
	setAttr ".wl[769].w[24]"  1;
	setAttr -s 2 ".wl[770].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[771].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[772].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[773].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[774].w";
	setAttr ".wl[774].w[24]" 0.99847532374297798;
	setAttr ".wl[774].w[28]" 0.0015246762570220683;
	setAttr -s 2 ".wl[775].w";
	setAttr ".wl[775].w[24]" 0.99865712783469363;
	setAttr ".wl[775].w[28]" 0.0013428721653062133;
	setAttr ".wl[776].w[24]"  1;
	setAttr -s 2 ".wl[777].w";
	setAttr ".wl[777].w[24]" 0.99919559987317785;
	setAttr ".wl[777].w[28]" 0.00080440012682221726;
	setAttr ".wl[778].w[24]"  1;
	setAttr -s 2 ".wl[779].w[28:29]"  0.024951436803530323 0.97504856319646971;
	setAttr ".wl[780].w[27]"  1;
	setAttr ".wl[781].w[4]"  1;
	setAttr ".wl[782].w[4]"  1;
	setAttr ".wl[783].w[4]"  1;
	setAttr ".wl[784].w[4]"  1;
	setAttr -s 2 ".wl[785].w[4:5]"  0.33406586532524002 0.66593413467475993;
	setAttr -s 2 ".wl[786].w[4:5]"  0.00159404834883577 0.9984059516511643;
	setAttr -s 2 ".wl[787].w[4:5]"  0.011242099866159296 0.98875790013384079;
	setAttr -s 2 ".wl[788].w[3:4]"  0.46784919837395844 0.53215080162604156;
	setAttr -s 2 ".wl[789].w[3:4]"  0.4329836853219291 0.56701631467807101;
	setAttr -s 2 ".wl[790].w[3:4]"  0.38875253165929141 0.61124746834070864;
	setAttr -s 2 ".wl[791].w[3:4]"  0.45467367866962338 0.54532632133037673;
	setAttr -s 2 ".wl[792].w[3:4]"  0.55802828183875219 0.44197171816124792;
	setAttr -s 2 ".wl[793].w[3:4]"  0.49830925364881373 0.50169074635118638;
	setAttr -s 2 ".wl[794].w[3:4]"  0.46872423945785263 0.53127576054214742;
	setAttr -s 2 ".wl[795].w[3:4]"  0.46821501760202439 0.53178498239797567;
	setAttr -s 2 ".wl[796].w[3:4]"  0.48308847944726185 0.51691152055273815;
	setAttr -s 2 ".wl[797].w[3:4]"  0.46175239551299807 0.53824760448700193;
	setAttr -s 2 ".wl[798].w[3:4]"  0.41048838310470681 0.58951161689529319;
	setAttr -s 2 ".wl[799].w[3:4]"  0.38484548469640323 0.61515451530359677;
	setAttr -s 2 ".wl[800].w[3:4]"  0.30251724668417018 0.69748275331582987;
	setAttr -s 2 ".wl[801].w[3:4]"  0.34997051047233824 0.65002948952766193;
	setAttr -s 2 ".wl[802].w[3:4]"  0.28411512802270117 0.71588487197729878;
	setAttr -s 2 ".wl[803].w[3:4]"  0.39303519917218338 0.60696480082781679;
	setAttr -s 2 ".wl[804].w[3:4]"  0.26695139215607544 0.73304860784392456;
	setAttr -s 2 ".wl[805].w[3:4]"  0.26233666878290396 0.73766333121709604;
	setAttr -s 2 ".wl[806].w[3:4]"  0.33785525010999862 0.66214474989000127;
	setAttr -s 2 ".wl[807].w[3:4]"  0.40557040640963327 0.59442959359036673;
	setAttr -s 2 ".wl[808].w[3:4]"  0.32348725427469088 0.67651274572530906;
	setAttr -s 2 ".wl[809].w[3:4]"  0.4791770308616749 0.52082296913832526;
	setAttr -s 2 ".wl[810].w[3:4]"  0.46703778702980031 0.53296221297019986;
	setAttr -s 2 ".wl[811].w[3:4]"  0.39748345627778209 0.60251654372221797;
	setAttr ".wl[812].w[4]"  1;
	setAttr ".wl[813].w[4]"  1;
	setAttr -s 3 ".wl[814].w[3:5]"  0.00017182908610229426 0.32798112353029235 
		0.67184704738360534;
	setAttr -s 2 ".wl[815].w[4:5]"  0.011593916857387539 0.98840608314261247;
	setAttr -s 2 ".wl[816].w[4:5]"  0.010060914686632314 0.98993908531336772;
	setAttr ".wl[817].w[5]"  1;
	setAttr ".wl[818].w[5]"  1;
	setAttr ".wl[819].w[5]"  1;
	setAttr ".wl[820].w[5]"  1;
	setAttr ".wl[821].w[5]"  1;
	setAttr ".wl[822].w[5]"  1;
	setAttr ".wl[823].w[5]"  1;
	setAttr ".wl[824].w[5]"  1;
	setAttr ".wl[825].w[5]"  1;
	setAttr ".wl[826].w[5]"  1;
	setAttr -s 2 ".wl[827].w[4:5]"  0.012137787143171767 0.98786221285682818;
	setAttr -s 2 ".wl[828].w[4:5]"  0.4797389382090817 0.52026106179091824;
	setAttr -s 2 ".wl[829].w[4:5]"  0.464699987598931 0.53530001240106917;
	setAttr ".wl[830].w[4]"  1;
	setAttr ".wl[831].w[4]"  1;
	setAttr ".wl[832].w[4]"  1;
	setAttr ".wl[833].w[4]"  1;
	setAttr -s 2 ".wl[834].w[4:5]"  0.5090923098351865 0.49090769016481345;
	setAttr ".wl[835].w[4]"  1;
	setAttr ".wl[836].w[4]"  1;
	setAttr ".wl[837].w[4]"  1;
	setAttr ".wl[838].w[4]"  1;
	setAttr ".wl[839].w[4]"  1;
	setAttr -s 3 ".wl[840].w[3:5]"  0.0041978127547132975 0.41972104667480931 
		0.57608114057047743;
	setAttr -s 3 ".wl[841].w[3:5]"  0.010374575869184447 0.52345465774043176 
		0.46617076639038374;
	setAttr ".wl[842].w[4]"  1;
	setAttr -s 2 ".wl[843].w[4:5]"  0.016468945790151616 0.98353105420984832;
	setAttr ".wl[844].w[5]"  1;
	setAttr ".wl[845].w[5]"  1;
	setAttr ".wl[846].w[5]"  1;
	setAttr -s 2 ".wl[847].w[4:5]"  0.0071504947309311787 0.9928495052690689;
	setAttr ".wl[848].w[5]"  1;
	setAttr ".wl[849].w[5]"  1;
	setAttr -s 2 ".wl[850].w[4:5]"  0.043854609676285961 0.95614539032371404;
	setAttr -s 2 ".wl[851].w[4:5]"  0.03042202795489506 0.96957797204510499;
	setAttr ".wl[852].w[5]"  0.99999999999999989;
	setAttr -s 3 ".wl[853].w[3:5]"  0.0016700739243426415 0.48792517300115901 
		0.51040475307449829;
	setAttr -s 3 ".wl[854].w[3:5]"  0.0061914958771018842 0.46396427506719501 
		0.52984422905570316;
	setAttr ".wl[855].w[2]"  1;
	setAttr -s 2 ".wl[856].w[2:3]"  0.9121347584428674 0.087865241557132712;
	setAttr -s 2 ".wl[857].w[2:3]"  0.5 0.5;
	setAttr -s 2 ".wl[858].w[2:3]"  0.7470798827327304 0.2529201172672696;
	setAttr -s 2 ".wl[859].w[2:3]"  0.85490591718446318 0.14509408281553673;
	setAttr -s 2 ".wl[860].w[2:3]"  0.5 0.5;
	setAttr -s 2 ".wl[861].w[2:3]"  0.64602096895010364 0.35397903104989642;
	setAttr -s 2 ".wl[862].w[2:3]"  0.97401675909836616 0.025983240901633887;
	setAttr -s 2 ".wl[863].w[2:3]"  0.85489642795367671 0.14510357204632318;
	setAttr -s 2 ".wl[864].w[2:3]"  0.66842775740139648 0.33157224259860346;
	setAttr -s 2 ".wl[865].w[2:3]"  0.81313531206556111 0.18686468793443889;
	setAttr -s 2 ".wl[866].w[2:3]"  0.7 0.3;
	setAttr -s 2 ".wl[867].w[2:3]"  0.92256505031619152 0.077434949683808346;
	setAttr -s 2 ".wl[868].w[2:3]"  0.75 0.25;
	setAttr -s 2 ".wl[869].w[2:3]"  0.50043248901746951 0.49956751098253049;
	setAttr -s 2 ".wl[870].w[2:3]"  0.53284237823494529 0.46715762176505471;
	setAttr -s 2 ".wl[871].w[2:3]"  0.63133589276937641 0.36866410723062359;
	setAttr -s 2 ".wl[872].w[2:3]"  0.54902101790860469 0.4509789820913952;
	setAttr -s 2 ".wl[873].w[2:3]"  0.70048481825272413 0.29951518174727587;
	setAttr -s 2 ".wl[874].w[2:3]"  0.61411865359284157 0.38588134640715843;
	setAttr -s 2 ".wl[875].w[2:3]"  0.52422185812850508 0.47577814187149492;
	setAttr -s 2 ".wl[876].w[2:3]"  0.039320975772671803 0.9606790242273282;
	setAttr -s 2 ".wl[877].w[2:3]"  0.072499173919036541 0.9275008260809634;
	setAttr -s 2 ".wl[878].w[2:3]"  0.19374056582178195 0.80625943417821799;
	setAttr ".wl[879].w[3]"  1;
	setAttr -s 2 ".wl[880].w[2:3]"  0.094350383903134591 0.90564961609686534;
	setAttr -s 2 ".wl[881].w[2:3]"  0.070951073898490466 0.92904892610150935;
	setAttr ".wl[882].w[3]"  1;
	setAttr -s 2 ".wl[883].w[2:3]"  0.11360697146714302 0.88639302853285684;
	setAttr ".wl[884].w[2]"  1;
	setAttr ".wl[885].w[2]"  1;
	setAttr ".wl[886].w[2]"  1;
	setAttr ".wl[887].w[2]"  1;
	setAttr ".wl[888].w[2]"  1;
	setAttr ".wl[889].w[2]"  1;
	setAttr ".wl[890].w[2]"  1;
	setAttr -s 2 ".wl[891].w[2:3]"  0.99884693834141458 0.0011530616585855424;
	setAttr ".wl[892].w[2]"  1;
	setAttr ".wl[893].w[2]"  1;
	setAttr ".wl[894].w[2]"  1;
	setAttr -s 2 ".wl[895].w[2:3]"  0.89182105764641917 0.1081789423535808;
	setAttr ".wl[896].w[2]"  1;
	setAttr ".wl[897].w[2]"  1;
	setAttr -s 2 ".wl[898].w[0:1]"  0.012176835038709897 0.98782316496129008;
	setAttr -s 2 ".wl[899].w[0:1]"  0.03054461915466428 0.96945538084533567;
	setAttr ".wl[900].w[1]"  1;
	setAttr ".wl[901].w[1]"  1;
	setAttr ".wl[902].w[1]"  1;
	setAttr ".wl[903].w[1]"  1;
	setAttr -s 2 ".wl[904].w[0:1]"  0.14231579217785287 0.85768420782214716;
	setAttr -s 2 ".wl[905].w[0:1]"  0.34938711066571071 0.65061288933428929;
	setAttr -s 2 ".wl[906].w[0:1]"  0.34555001002325075 0.6544499899767493;
	setAttr -s 2 ".wl[907].w[1:2]"  0.54169755494544269 0.45830244505455719;
	setAttr -s 3 ".wl[908].w[0:2]"  0.52756031254386626 0.42740374765635897 
		0.045035939799774806;
	setAttr -s 2 ".wl[909].w[0:1]"  0.35035091418666781 0.64964908581333214;
	setAttr -s 2 ".wl[910].w[1:2]"  0.7875271760662671 0.21247282393373287;
	setAttr -s 3 ".wl[911].w";
	setAttr ".wl[911].w[0:1]" 0.52756113596795495 0.4274031380015581;
	setAttr ".wl[911].w[6]" 0.045035726030486944;
	setAttr -s 2 ".wl[912].w";
	setAttr ".wl[912].w[1]" 0.54169755030392674;
	setAttr ".wl[912].w[6]" 0.45830244972662626;
	setAttr -s 2 ".wl[913].w";
	setAttr ".wl[913].w[1]" 0.68098725769155544;
	setAttr ".wl[913].w[6]" 0.31901274230844467;
	setAttr -s 2 ".wl[914].w[0:1]"  0.69128445861471333 0.30871554138528662;
	setAttr -s 2 ".wl[915].w[0:1]"  0.53678392109199669 0.46321607890800326;
	setAttr -s 2 ".wl[916].w[0:1]"  0.69128446223981799 0.30871553776018196;
	setAttr -s 2 ".wl[917].w[1:2]"  0.25021070778085763 0.74978929221914226;
	setAttr -s 3 ".wl[918].w[0:2]"  0.66502086888649115 0.29585741306400359 
		0.039121718049505226;
	setAttr -s 3 ".wl[919].w";
	setAttr ".wl[919].w[0:1]" 0.66502024920752068 0.29446459553865484;
	setAttr ".wl[919].w[6]" 0.040515155253824563;
	setAttr -s 2 ".wl[920].w";
	setAttr ".wl[920].w[1]" 0.25021071127450417;
	setAttr ".wl[920].w[6]" 0.74978928872549588;
	setAttr -s 2 ".wl[921].w[0:1]"  0.72555358375986523 0.27444641624013477;
	setAttr -s 2 ".wl[922].w[0:1]"  0.72555358375986523 0.27444641624013483;
	setAttr -s 2 ".wl[923].w[1:2]"  0.36920318388792039 0.63079681611207961;
	setAttr -s 3 ".wl[924].w";
	setAttr ".wl[924].w[1:2]" 0.80100142180919653 0.18998578190803531;
	setAttr ".wl[924].w[6]" 0.0090127962827682498;
	setAttr -s 2 ".wl[925].w[0:1]"  0.22131574636235274 0.77868425363764737;
	setAttr -s 3 ".wl[926].w[0:2]"  0.39442378142266088 0.50557621857733914 
		0.1;
	setAttr -s 2 ".wl[927].w[0:1]"  0.22132623278406433 0.77867376721593573;
	setAttr -s 3 ".wl[928].w";
	setAttr ".wl[928].w[1:2]" 0.80100142194328583 0.010014219432857343;
	setAttr ".wl[928].w[6]" 0.18898435862385693;
	setAttr -s 2 ".wl[929].w";
	setAttr ".wl[929].w[1]" 0.36920287275852903;
	setAttr ".wl[929].w[6]" 0.63079712724147097;
	setAttr -s 3 ".wl[930].w";
	setAttr ".wl[930].w[0:1]" 0.34177215189873428 0.55822784810126569;
	setAttr ".wl[930].w[6]" 0.1;
	setAttr -s 2 ".wl[931].w[0:1]"  0.17200162632397509 0.82799837367602491;
	setAttr -s 2 ".wl[932].w[0:1]"  0.69912433230065396 0.30087566769934604;
	setAttr -s 2 ".wl[933].w[0:1]"  0.14922386961943943 0.85077613038056066;
	setAttr -s 2 ".wl[934].w[0:1]"  0.46319263540524869 0.53680736459475131;
	setAttr -s 3 ".wl[935].w";
	setAttr ".wl[935].w[1:2]" 0.81 0.1;
	setAttr ".wl[935].w[6]" 0.09;
	setAttr -s 2 ".wl[936].w[0:1]"  0.036108743386226543 0.9638912566137734;
	setAttr -s 2 ".wl[937].w[0:1]"  0.014567045651145355 0.98543295434885469;
	setAttr -s 2 ".wl[938].w[1:2]"  0.35 0.65;
	setAttr -s 2 ".wl[939].w[1:2]"  0.15000000000000002 0.85;
	setAttr -s 2 ".wl[940].w";
	setAttr ".wl[940].w[1]" 0.15000000000000002;
	setAttr ".wl[940].w[6]" 0.85;
	setAttr -s 2 ".wl[941].w";
	setAttr ".wl[941].w[1]" 0.35;
	setAttr ".wl[941].w[6]" 0.65;
	setAttr -s 2 ".wl[942].w[6:7]"  0.89182100032502243 0.10817899967497768;
	setAttr -s 2 ".wl[943].w[6:7]"  0.81313529644729632 0.18686470355270382;
	setAttr -s 2 ".wl[944].w[6:7]"  0.92256506253533699 0.077434937464663095;
	setAttr ".wl[945].w[6]"  1;
	setAttr ".wl[946].w[6]"  1;
	setAttr ".wl[947].w[6]"  1.0000000000000002;
	setAttr -s 2 ".wl[948].w[6:7]"  0.99884693800853275 0.0011530619914671592;
	setAttr ".wl[949].w[6]"  1;
	setAttr -s 2 ".wl[950].w[6:7]"  0.97401674760605128 0.025983252393948719;
	setAttr ".wl[951].w[6]"  1;
	setAttr -s 2 ".wl[952].w[6:7]"  0.85489646332631031 0.14510353667368966;
	setAttr ".wl[953].w[6]"  1;
	setAttr ".wl[954].w[6]"  1;
	setAttr -s 2 ".wl[955].w[6:7]"  0.8549060436876923 0.1450939563123077;
	setAttr ".wl[956].w[6]"  1;
	setAttr ".wl[957].w[6]"  1;
	setAttr ".wl[958].w[6]"  1;
	setAttr ".wl[959].w[6]"  1;
	setAttr ".wl[960].w[6]"  1;
	setAttr -s 2 ".wl[961].w[6:7]"  0.93604643009824418 0.063953569901755797;
	setAttr ".wl[962].w[6]"  1;
	setAttr -s 3 ".wl[963].w";
	setAttr ".wl[963].w[12:13]" 0.29815681166946872 0.69569922722876087;
	setAttr ".wl[963].w[22]" 0.006143961101770401;
	setAttr -s 4 ".wl[964].w";
	setAttr ".wl[964].w[11:12]" 0.49046443796796479 0.32674518122219176;
	setAttr ".wl[964].w[21:22]" 0.068725530896723175 0.11406484991312027;
	setAttr -s 3 ".wl[965].w";
	setAttr ".wl[965].w[11:12]" 0.68915922282378317 0.29591075057049154;
	setAttr ".wl[965].w[22]" 0.014930026605725288;
	setAttr -s 2 ".wl[966].w[11:12]"  0.62294451250886218 0.37705548749113782;
	setAttr -s 2 ".wl[967].w[6:7]"  0.53284238033074971 0.46715761966925029;
	setAttr -s 2 ".wl[968].w[6:7]"  0.094350545466245342 0.90564945453375467;
	setAttr -s 2 ".wl[969].w[6:7]"  0.039320776019419398 0.96067922398058059;
	setAttr -s 2 ".wl[970].w[6:7]"  0.52422191687182385 0.4757780831281761;
	setAttr ".wl[971].w[7]"  1;
	setAttr -s 2 ".wl[972].w[6:7]"  0.70048480756184373 0.2995151924381565;
	setAttr ".wl[973].w[7]"  1;
	setAttr -s 2 ".wl[974].w[6:7]"  0.6 0.4;
	setAttr -s 2 ".wl[975].w[6:7]"  0.070951074627521774 0.9290489253724783;
	setAttr -s 2 ".wl[976].w[6:7]"  0.7 0.3;
	setAttr -s 2 ".wl[977].w[6:7]"  0.11360692235895101 0.88639307764104891;
	setAttr -s 2 ".wl[978].w[6:7]"  0.50043248898301318 0.49956751101698676;
	setAttr -s 2 ".wl[979].w[6:7]"  0.19374054271816335 0.80625945728183668;
	setAttr -s 2 ".wl[980].w[6:7]"  0.072499219716797472 0.92750078028320249;
	setAttr -s 2 ".wl[981].w[6:7]"  0.5 0.5;
	setAttr -s 2 ".wl[982].w[6:7]"  0.5 0.5;
	setAttr -s 2 ".wl[983].w[6:7]"  0.5 0.5;
	setAttr -s 2 ".wl[984].w[6:7]"  0.64602097307942785 0.35397902692057226;
	setAttr -s 2 ".wl[985].w[6:7]"  0.8 0.2;
	setAttr -s 2 ".wl[986].w[6:7]"  0.6 0.4;
	setAttr -s 2 ".wl[987].w[6:7]"  0.6 0.4;
	setAttr -s 2 ".wl[988].w[6:7]"  0.6 0.4;
	setAttr -s 4 ".wl[989].w";
	setAttr ".wl[989].w[12:13]" 0.00062957363848965353 0.00078334789918013735;
	setAttr ".wl[989].w[21:22]" 0.48626018227168688 0.51232689619064331;
	setAttr -s 4 ".wl[990].w";
	setAttr ".wl[990].w[12:13]" 0.45396158458335695 0.00043646668892611482;
	setAttr ".wl[990].w[21:22]" 0.23768632077132651 0.30791562795639038;
	setAttr -s 2 ".wl[991].w[39:40]"  0.53868963949408033 0.46131036050591961;
	setAttr -s 3 ".wl[992].w";
	setAttr ".wl[992].w[12]" 0.090196132083826611;
	setAttr ".wl[992].w[39:40]" 0.54894503560185792 0.36085883231431537;
	setAttr -s 2 ".wl[993].w";
	setAttr ".wl[993].w[0]" 0.52;
	setAttr ".wl[993].w[11]" 0.48;
	setAttr -s 2 ".wl[994].w";
	setAttr ".wl[994].w[0]" 0.52;
	setAttr ".wl[994].w[11]" 0.48;
	setAttr -s 5 ".wl[995].w";
	setAttr ".wl[995].w[0]" 0.45556777829263495;
	setAttr ".wl[995].w[11:12]" 0.33109647091106442 0.17386856322236283;
	setAttr ".wl[995].w[21:22]" 0.023367704799203304 0.016099482774734497;
	setAttr -s 3 ".wl[996].w";
	setAttr ".wl[996].w[0]" 0.20601160885646258;
	setAttr ".wl[996].w[11]" 0.79307475805169658;
	setAttr ".wl[996].w[22]" 0.00091363309184089303;
	setAttr -s 2 ".wl[997].w";
	setAttr ".wl[997].w[0]" 0.4;
	setAttr ".wl[997].w[11]" 0.6;
	setAttr -s 2 ".wl[998].w";
	setAttr ".wl[998].w[0]" 0.52;
	setAttr ".wl[998].w[11]" 0.48;
	setAttr -s 2 ".wl[999].w";
	setAttr ".wl[999].w[0]" 0.5;
	setAttr ".wl[999].w[11]" 0.5;
	setAttr -s 2 ".wl[1000].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1001].w";
	setAttr ".wl[1001].w[0]" 0.52;
	setAttr ".wl[1001].w[11]" 0.48;
	setAttr -s 2 ".wl[1002].w[11:12]"  0.8 0.2;
	setAttr -s 5 ".wl[1003].w";
	setAttr ".wl[1003].w[11:12]" 0.22248761861074712 0.76780605092444643;
	setAttr ".wl[1003].w[21:22]" 0.0084577438557213499 0.00091363309184089303;
	setAttr ".wl[1003].w[39]" 0.00033495351724410055;
	setAttr -s 4 ".wl[1004].w";
	setAttr ".wl[1004].w[11:12]" 0.24329213146443215 0.63839463303709809;
	setAttr ".wl[1004].w[21:22]" 0.085956079221766873 0.032357156276702881;
	setAttr -s 3 ".wl[1005].w";
	setAttr ".wl[1005].w[11:12]" 0.2642994616367571 0.69215816999983903;
	setAttr ".wl[1005].w[39]" 0.043542368363403992;
	setAttr -s 3 ".wl[1006].w";
	setAttr ".wl[1006].w[12]" 0.94652987135312117;
	setAttr ".wl[1006].w[21:22]" 0.042156566365449087 0.011313562281429768;
	setAttr -s 3 ".wl[1007].w";
	setAttr ".wl[1007].w[12]" 0.92869329186566274;
	setAttr ".wl[1007].w[21:22]" 0.055309868283196312 0.015996839851140976;
	setAttr -s 4 ".wl[1008].w";
	setAttr ".wl[1008].w[12:13]" 0.71278612859288515 2.0092059939828576e-005;
	setAttr ".wl[1008].w[21:22]" 0.1833261814691958 0.10386759787797928;
	setAttr -s 4 ".wl[1009].w";
	setAttr ".wl[1009].w[12:13]" 0.71171036151914646 0.042945046848516133;
	setAttr ".wl[1009].w[21:22]" 0.16513457645461055 0.080210015177726746;
	setAttr -s 2 ".wl[1010].w";
	setAttr ".wl[1010].w[12]" 0.70067749210612695;
	setAttr ".wl[1010].w[39]" 0.29932250789387305;
	setAttr -s 3 ".wl[1011].w";
	setAttr ".wl[1011].w[12:13]" 0.76074531158424941 0.045148145323495231;
	setAttr ".wl[1011].w[39]" 0.19410654309225531;
	setAttr -s 4 ".wl[1012].w";
	setAttr ".wl[1012].w[11:12]" 0.040356986721072539 0.2016341499312912;
	setAttr ".wl[1012].w[21:22]" 0.30213760914193249 0.45587125420570374;
	setAttr -s 4 ".wl[1013].w";
	setAttr ".wl[1013].w[12:13]" 0.44427477765054513 5.3637789731444853e-005;
	setAttr ".wl[1013].w[21:22]" 0.24304710025911008 0.3126244843006134;
	setAttr -s 3 ".wl[1014].w";
	setAttr ".wl[1014].w[12]" 0.0097161788152082887;
	setAttr ".wl[1014].w[39:40]" 0.58484041526468267 0.40544340592010908;
	setAttr -s 3 ".wl[1015].w";
	setAttr ".wl[1015].w[12]" 0.4;
	setAttr ".wl[1015].w[39:40]" 0.29254583773702653 0.30745416226297345;
	setAttr -s 4 ".wl[1016].w";
	setAttr ".wl[1016].w[12:13]" 0.14058796608285704 0.00055087064293759993;
	setAttr ".wl[1016].w[21:22]" 0.43806876243726078 0.42079240083694458;
	setAttr -s 3 ".wl[1017].w";
	setAttr ".wl[1017].w[12]" 0.034143074108248885;
	setAttr ".wl[1017].w[39:40]" 0.58678446387210004 0.37907246201965106;
	setAttr -s 4 ".wl[1018].w";
	setAttr ".wl[1018].w[11:12]" 0.17142475896431184 0.56691905224640793;
	setAttr ".wl[1018].w[21:22]" 0.13024432343569597 0.13141186535358429;
	setAttr -s 4 ".wl[1019].w";
	setAttr ".wl[1019].w[11:12]" 0.19738631610724958 0.66744923320904059;
	setAttr ".wl[1019].w[39:40]" 0.10644907829794524 0.028715372385764586;
	setAttr -s 3 ".wl[1020].w";
	setAttr ".wl[1020].w[0]" 0.79947776035405693;
	setAttr ".wl[1020].w[11]" 0.19986944008851426;
	setAttr ".wl[1020].w[22]" 0.00065279955742880702;
	setAttr -s 4 ".wl[1021].w";
	setAttr ".wl[1021].w[0]" 0.42422708688993765;
	setAttr ".wl[1021].w[11]" 0.51139655650422788;
	setAttr ".wl[1021].w[21:22]" 0.024315571938104196 0.040060784667730331;
	setAttr -s 2 ".wl[1022].w";
	setAttr ".wl[1022].w[0]" 0.8;
	setAttr ".wl[1022].w[11]" 0.2;
	setAttr -s 2 ".wl[1023].w";
	setAttr ".wl[1023].w[0]" 0.44908764909365606;
	setAttr ".wl[1023].w[11]" 0.55091235090634405;
	setAttr -s 4 ".wl[1024].w";
	setAttr ".wl[1024].w[11:12]" 0.15600842010957067 0.35016047596619138;
	setAttr ".wl[1024].w[21:22]" 0.16694689381452726 0.32688421010971069;
	setAttr -s 3 ".wl[1025].w";
	setAttr ".wl[1025].w[12]" 0.4;
	setAttr ".wl[1025].w[39:40]" 0.20099644661336971 0.39900355338663024;
	setAttr -s 4 ".wl[1026].w";
	setAttr ".wl[1026].w[11:12]" 0.25432015891720261 0.59217773169206178;
	setAttr ".wl[1026].w[39:40]" 0.13292383319930837 0.020578276191427186;
	setAttr -s 2 ".wl[1027].w";
	setAttr ".wl[1027].w[0]" 0.8;
	setAttr ".wl[1027].w[11]" 0.2;
	setAttr -s 3 ".wl[1028].w";
	setAttr ".wl[1028].w[0]" 0.55939086515428382;
	setAttr ".wl[1028].w[11]" 0.43995633528828737;
	setAttr ".wl[1028].w[22]" 0.00065279955742880702;
	setAttr -s 2 ".wl[1029].w";
	setAttr ".wl[1029].w[0]" 0.55799775801595353;
	setAttr ".wl[1029].w[11]" 0.44200224198404653;
	setAttr -s 2 ".wl[1030].w";
	setAttr ".wl[1030].w[0]" 0.93665945728816324;
	setAttr ".wl[1030].w[11]" 0.063340542711836659;
	setAttr -s 5 ".wl[1031].w";
	setAttr ".wl[1031].w[0]" 0.081822659734865147;
	setAttr ".wl[1031].w[11:12]" 0.40517794230116611 0.25754049493295794;
	setAttr ".wl[1031].w[21:22]" 0.12007926078285711 0.13537964224815369;
	setAttr -s 4 ".wl[1032].w";
	setAttr ".wl[1032].w[0]" 0.099535982263831743;
	setAttr ".wl[1032].w[11:12]" 0.50313360050672695 0.31044122791239193;
	setAttr ".wl[1032].w[39]" 0.086889189317049351;
	setAttr -s 2 ".wl[1033].w";
	setAttr ".wl[1033].w[0]" 0.97549169462308816;
	setAttr ".wl[1033].w[11]" 0.024508305376911881;
	setAttr -s 2 ".wl[1034].w";
	setAttr ".wl[1034].w[0]" 0.506;
	setAttr ".wl[1034].w[11]" 0.494;
	setAttr -s 2 ".wl[1035].w";
	setAttr ".wl[1035].w[0]" 0.65;
	setAttr ".wl[1035].w[11]" 0.35;
	setAttr -s 3 ".wl[1036].w";
	setAttr ".wl[1036].w[0:1]" 0.69876514424733704 0.0012348557526629163;
	setAttr ".wl[1036].w[11]" 0.3;
	setAttr -s 3 ".wl[1037].w";
	setAttr ".wl[1037].w[0]" 0.33899485071951702;
	setAttr ".wl[1037].w[11:12]" 0.46100514928048297 0.2;
	setAttr -s 3 ".wl[1038].w";
	setAttr ".wl[1038].w[0]" 0.39853291383582184;
	setAttr ".wl[1038].w[11:12]" 0.56214854241184087 0.039318543752337301;
	setAttr -s 2 ".wl[1039].w";
	setAttr ".wl[1039].w[0]" 0.9;
	setAttr ".wl[1039].w[11]" 0.1;
	setAttr -s 2 ".wl[1040].w";
	setAttr ".wl[1040].w[0]" 0.74839609161892384;
	setAttr ".wl[1040].w[11]" 0.25160390838107616;
	setAttr -s 3 ".wl[1041].w";
	setAttr ".wl[1041].w[0]" 0.35414127684507374;
	setAttr ".wl[1041].w[11:12]" 0.59346681260194778 0.052391910552978517;
	setAttr -s 3 ".wl[1042].w";
	setAttr ".wl[1042].w[12]" 0.4;
	setAttr ".wl[1042].w[39:40]" 0.089086649850093569 0.51091335014990646;
	setAttr -s 2 ".wl[1043].w[1:2]"  0.05 0.95;
	setAttr ".wl[1044].w[2]"  0.99999999999999989;
	setAttr ".wl[1045].w[6]"  1;
	setAttr -s 2 ".wl[1046].w";
	setAttr ".wl[1046].w[1]" 0.099999999999999978;
	setAttr ".wl[1046].w[6]" 0.9;
	setAttr ".wl[1047].w[2]"  1;
	setAttr ".wl[1048].w[6]"  1;
	setAttr -s 3 ".wl[1049].w";
	setAttr ".wl[1049].w[12]" 0.3435088961899731;
	setAttr ".wl[1049].w[21:22]" 0.32415056530828734 0.3323405385017395;
	setAttr -s 3 ".wl[1050].w";
	setAttr ".wl[1050].w[12]" 0.5;
	setAttr ".wl[1050].w[39:40]" 0.31382181006259335 0.18617818993740665;
	setAttr -s 3 ".wl[1051].w";
	setAttr ".wl[1051].w[12]" 0.23536504268957092;
	setAttr ".wl[1051].w[21:22]" 0.26273327111887024 0.50190168619155884;
	setAttr -s 3 ".wl[1052].w";
	setAttr ".wl[1052].w[12]" 0.02941260838875592;
	setAttr ".wl[1052].w[39:40]" 0.52603440708666938 0.44455298452457465;
	setAttr -s 3 ".wl[1053].w";
	setAttr ".wl[1053].w[1:2]" 0.70646730586968454 0.15;
	setAttr ".wl[1053].w[6]" 0.14353269413031539;
	setAttr -s 2 ".wl[1054].w[1:2]"  0.73482595412916807 0.2651740458799301;
	setAttr -s 2 ".wl[1055].w[0:1]"  0.20754603605579056 0.79245396394420942;
	setAttr -s 2 ".wl[1056].w";
	setAttr ".wl[1056].w[1]" 0.73482595412916807;
	setAttr ".wl[1056].w[6]" 0.2651740458799301;
	setAttr -s 2 ".wl[1057].w[1:2]"  0.65044657419005325 0.34955342580994675;
	setAttr -s 2 ".wl[1058].w[1:2]"  0.6322612867323425 0.3677387132676575;
	setAttr -s 2 ".wl[1059].w";
	setAttr ".wl[1059].w[1]" 0.6504465596652651;
	setAttr ".wl[1059].w[6]" 0.3495534403347349;
	setAttr -s 2 ".wl[1060].w";
	setAttr ".wl[1060].w[1]" 0.63045584152052769;
	setAttr ".wl[1060].w[6]" 0.36954415847947225;
	setAttr -s 2 ".wl[1061].w[1:2]"  0.35 0.65;
	setAttr -s 2 ".wl[1062].w[1:2]"  0.074880594018517432 0.92511940598148246;
	setAttr -s 2 ".wl[1063].w";
	setAttr ".wl[1063].w[1]" 0.35;
	setAttr ".wl[1063].w[6]" 0.65;
	setAttr -s 2 ".wl[1064].w";
	setAttr ".wl[1064].w[1]" 0.075476247014601205;
	setAttr ".wl[1064].w[6]" 0.92452375298539891;
	setAttr -s 2 ".wl[1065].w";
	setAttr ".wl[1065].w[1]" 0.68031550454802103;
	setAttr ".wl[1065].w[6]" 0.31968449545197902;
	setAttr -s 2 ".wl[1066].w";
	setAttr ".wl[1066].w[1]" 0.35;
	setAttr ".wl[1066].w[6]" 0.65;
	setAttr -s 2 ".wl[1067].w[1:2]"  0.014913732336430454 0.98508626766356955;
	setAttr -s 2 ".wl[1068].w";
	setAttr ".wl[1068].w[1]" 0.014913737986510463;
	setAttr ".wl[1068].w[6]" 0.9850862620134897;
	setAttr -s 2 ".wl[1069].w[1:2]"  0.15000000000000002 0.85;
	setAttr -s 2 ".wl[1070].w";
	setAttr ".wl[1070].w[1]" 0.15000000000000002;
	setAttr ".wl[1070].w[6]" 0.85;
	setAttr -s 2 ".wl[1071].w[1:2]"  0.24745347627772923 0.75254652372227082;
	setAttr -s 2 ".wl[1072].w";
	setAttr ".wl[1072].w[1]" 0.2474534808295315;
	setAttr ".wl[1072].w[6]" 0.75254651917046855;
	setAttr -s 2 ".wl[1073].w";
	setAttr ".wl[1073].w[1]" 0.15000000000000002;
	setAttr ".wl[1073].w[6]" 0.85;
	setAttr ".wl[1074].w[2]"  1;
	setAttr ".wl[1075].w[6]"  1;
	setAttr ".wl[1076].w[2]"  0.99999999999999989;
	setAttr ".wl[1077].w[6]"  1;
	setAttr ".wl[1078].w[2]"  1;
	setAttr ".wl[1079].w[6]"  1;
	setAttr ".wl[1080].w[2]"  1;
	setAttr ".wl[1081].w[6]"  1;
	setAttr -s 2 ".wl[1082].w[1:2]"  0.4 0.6;
	setAttr -s 2 ".wl[1083].w";
	setAttr ".wl[1083].w[1]" 0.4;
	setAttr ".wl[1083].w[6]" 0.6;
	setAttr -s 3 ".wl[1084].w";
	setAttr ".wl[1084].w[1:2]" 0.80012195121951224 0.1;
	setAttr ".wl[1084].w[6]" 0.09987804878048781;
	setAttr -s 2 ".wl[1085].w[1:2]"  0.5 0.5;
	setAttr -s 2 ".wl[1086].w";
	setAttr ".wl[1086].w[1]" 0.5;
	setAttr ".wl[1086].w[6]" 0.5;
	setAttr -s 3 ".wl[1087].w";
	setAttr ".wl[1087].w[1:2]" 0.81 0.1;
	setAttr ".wl[1087].w[6]" 0.089999999999999955;
	setAttr -s 2 ".wl[1088].w[1:2]"  0.8 0.2;
	setAttr -s 2 ".wl[1089].w[1:2]"  0.0023420269001682439 0.99765797309983173;
	setAttr -s 2 ".wl[1090].w";
	setAttr ".wl[1090].w[1]" 0.1;
	setAttr ".wl[1090].w[6]" 0.9;
	setAttr -s 2 ".wl[1091].w";
	setAttr ".wl[1091].w[1]" 0.8;
	setAttr ".wl[1091].w[6]" 0.2;
	setAttr -s 3 ".wl[1092].w";
	setAttr ".wl[1092].w[1:2]" 0.89107171610005242 0.045741486512033774;
	setAttr ".wl[1092].w[6]" 0.063186797387913898;
	setAttr -s 3 ".wl[1093].w";
	setAttr ".wl[1093].w[1:2]" 0.84980958999514455 0.14421999733770291;
	setAttr ".wl[1093].w[6]" 0.0059704126671526114;
	setAttr -s 2 ".wl[1094].w[1:2]"  0.05 0.95;
	setAttr -s 3 ".wl[1095].w";
	setAttr ".wl[1095].w[1:2]" 0.84980958999514455 0.0066337918523917908;
	setAttr ".wl[1095].w[6]" 0.14355661815246373;
	setAttr -s 2 ".wl[1096].w";
	setAttr ".wl[1096].w[1]" 0.099999999999999978;
	setAttr ".wl[1096].w[6]" 0.9;
	setAttr -s 3 ".wl[1097].w";
	setAttr ".wl[1097].w[1:2]" 0.81 0.1;
	setAttr ".wl[1097].w[6]" 0.09;
	setAttr -s 4 ".wl[1098].w";
	setAttr ".wl[1098].w[11:12]" 0.3265749508570418 0.21074077606274083;
	setAttr ".wl[1098].w[21:22]" 0.28789268370090676 0.17479158937931061;
	setAttr -s 4 ".wl[1099].w";
	setAttr ".wl[1099].w[11:12]" 0.25602382874649887 0.35;
	setAttr ".wl[1099].w[39:40]" 0.27136453513913872 0.12261163611436242;
	setAttr -s 5 ".wl[1100].w";
	setAttr ".wl[1100].w[0]" 0.02778133211350034;
	setAttr ".wl[1100].w[11:12]" 0.61888717767511836 0.26107895819349647;
	setAttr ".wl[1100].w[21:22]" 0.033952547234950683 0.058299984782934189;
	setAttr -s 3 ".wl[1101].w";
	setAttr ".wl[1101].w[0]" 0.030605086726209769;
	setAttr ".wl[1101].w[11:12]" 0.71897642749692947 0.25041848577686066;
	setAttr -s 4 ".wl[1102].w";
	setAttr ".wl[1102].w[11:12]" 0.20436744769068368 0.31233981120203264;
	setAttr ".wl[1102].w[21:22]" 0.2550635313265906 0.22822920978069305;
	setAttr -s 3 ".wl[1103].w";
	setAttr ".wl[1103].w[11:12]" 0.23788350989883633 0.4;
	setAttr ".wl[1103].w[40]" 0.36211649010116365;
	setAttr -s 2 ".wl[1104].w[11:12]"  0.74427909299826456 0.25572090700173555;
	setAttr ".wl[1105].w[0]"  1;
	setAttr -s 2 ".wl[1106].w[0:1]"  0.99904183651279521 0.00095816348720477982;
	setAttr ".wl[1107].w[0]"  1;
	setAttr ".wl[1108].w[0]"  1;
	setAttr -s 2 ".wl[1109].w[0:1]"  0.98071965316129994 0.019280346838699978;
	setAttr -s 2 ".wl[1110].w[0:1]"  0.98071965316130005 0.019280346838699978;
	setAttr -s 2 ".wl[1111].w[1:2]"  0.80654 0.19345999999999994;
	setAttr -s 2 ".wl[1112].w[1:2]"  0.35 0.65;
	setAttr -s 2 ".wl[1113].w[1:2]"  0.15000000000000002 0.85;
	setAttr -s 2 ".wl[1114].w[0:1]"  0.9552288771223858 0.044771122877614304;
	setAttr -s 2 ".wl[1115].w[0:1]"  0.84436405421771821 0.15563594578228182;
	setAttr -s 3 ".wl[1116].w";
	setAttr ".wl[1116].w[0:1]" 0.76445204163951574 0.035547958360484348;
	setAttr ".wl[1116].w[11]" 0.2;
	setAttr -s 3 ".wl[1117].w";
	setAttr ".wl[1117].w[0:1]" 0.76751516761631111 0.13248483238368888;
	setAttr ".wl[1117].w[11]" 0.1;
	setAttr ".wl[1118].w[8]"  1;
	setAttr ".wl[1119].w[8]"  1;
	setAttr ".wl[1120].w[8]"  1;
	setAttr ".wl[1121].w[8]"  1;
	setAttr -s 3 ".wl[1122].w[7:9]"  0.0015798629565496405 0.48783546504173247 
		0.51058467200171787;
	setAttr -s 3 ".wl[1123].w[7:9]"  0.010050021501485103 0.52436876682100408 
		0.46558121167751082;
	setAttr ".wl[1124].w[9]"  1;
	setAttr ".wl[1125].w[9]"  1;
	setAttr -s 2 ".wl[1126].w[8:9]"  0.031218567548239245 0.96878143245176074;
	setAttr -s 3 ".wl[1127].w[7:9]"  0.0060988760795822536 0.5200943318118918 
		0.47380679210852594;
	setAttr -s 2 ".wl[1128].w[8:9]"  0.46470012256328547 0.53529987743671459;
	setAttr -s 3 ".wl[1129].w[7:9]"  0.0041977962071111843 0.41972102719371951 
		0.57608117659916924;
	setAttr -s 2 ".wl[1130].w[8:9]"  0.0071504354138431335 0.99284956458615681;
	setAttr -s 2 ".wl[1131].w[8:9]"  0.012137789749336624 0.98786221025066334;
	setAttr -s 3 ".wl[1132].w[7:9]"  0.0061915613196533281 0.46396427935046208 
		0.52984415932988449;
	setAttr -s 2 ".wl[1133].w[8:9]"  0.016468870034177911 0.98353112996582215;
	setAttr -s 2 ".wl[1134].w[7:8]"  0.26237551758884314 0.73762448241115686;
	setAttr -s 2 ".wl[1135].w[7:8]"  0.26695105797312524 0.73304894202687476;
	setAttr -s 2 ".wl[1136].w[7:8]"  0.30251722631732514 0.69748277368267475;
	setAttr -s 2 ".wl[1137].w[7:8]"  0.32348725311506193 0.67651274688493812;
	setAttr ".wl[1138].w[8]"  1;
	setAttr ".wl[1139].w[8]"  0.99999999999999989;
	setAttr ".wl[1140].w[8]"  1;
	setAttr ".wl[1141].w[8]"  1;
	setAttr ".wl[1142].w[8]"  1;
	setAttr ".wl[1143].w[8]"  0.99999999999999989;
	setAttr ".wl[1144].w[8]"  1;
	setAttr ".wl[1145].w[8]"  1;
	setAttr ".wl[1146].w[8]"  1;
	setAttr ".wl[1147].w[8]"  1;
	setAttr -s 2 ".wl[1148].w[8:9]"  0.010060914277392937 0.98993908572260703;
	setAttr -s 3 ".wl[1149].w[7:9]"  0.00016110779323343387 0.32778353506869423 
		0.67205535713807241;
	setAttr ".wl[1150].w[9]"  1;
	setAttr ".wl[1151].w[9]"  1;
	setAttr ".wl[1152].w[9]"  1;
	setAttr ".wl[1153].w[9]"  1;
	setAttr ".wl[1154].w[9]"  1;
	setAttr ".wl[1155].w[9]"  1;
	setAttr ".wl[1156].w[9]"  1;
	setAttr ".wl[1157].w[9]"  1;
	setAttr ".wl[1158].w[9]"  1;
	setAttr ".wl[1159].w[9]"  1;
	setAttr -s 2 ".wl[1160].w[7:8]"  0.45467367779288692 0.54532632220711308;
	setAttr -s 2 ".wl[1161].w[7:8]"  0.38875254803987747 0.61124745196012253;
	setAttr -s 2 ".wl[1162].w[7:8]"  0.46703778694159492 0.53296221305840519;
	setAttr -s 2 ".wl[1163].w[7:8]"  0.46821501744499511 0.53178498255500484;
	setAttr -s 2 ".wl[1164].w[7:8]"  0.498031874287645 0.50196812571235494;
	setAttr -s 2 ".wl[1165].w[7:8]"  0.39748343989683022 0.6025165601031699;
	setAttr -s 2 ".wl[1166].w[7:8]"  0.46175239567482967 0.53824760432517038;
	setAttr -s 2 ".wl[1167].w[7:8]"  0.2842152179259762 0.71578478207402385;
	setAttr -s 2 ".wl[1168].w[8:9]"  0.0015940422672161647 0.99840595773278384;
	setAttr ".wl[1169].w[9]"  1;
	setAttr ".wl[1170].w[9]"  1;
	setAttr -s 2 ".wl[1171].w[8:9]"  0.011242092072440814 0.98875790792755913;
	setAttr ".wl[1172].w[9]"  1;
	setAttr ".wl[1173].w[9]"  0.99999999999999989;
	setAttr -s 2 ".wl[1174].w[8:9]"  0.3340658011236266 0.66593419887637317;
	setAttr -s 2 ".wl[1175].w[8:9]"  0.47973894038315251 0.52026105961684743;
	setAttr ".wl[1176].w[8]"  1;
	setAttr ".wl[1177].w[8]"  1;
	setAttr -s 2 ".wl[1178].w[8:9]"  0.011593914676867661 0.98840608532313234;
	setAttr ".wl[1179].w[9]"  1;
	setAttr -s 2 ".wl[1180].w[7:8]"  0.3848454163728845 0.61515458362711561;
	setAttr -s 2 ".wl[1181].w[7:8]"  0.41035304214999696 0.58964695785000321;
	setAttr -s 2 ".wl[1182].w[7:8]"  0.4830884792690141 0.5169115207309859;
	setAttr -s 2 ".wl[1183].w[7:8]"  0.47917701873908242 0.52082298126091753;
	setAttr -s 2 ".wl[1184].w[7:8]"  0.55799148560363876 0.4420085143963613;
	setAttr -s 2 ".wl[1185].w[7:8]"  0.46784919593309943 0.53215080406690063;
	setAttr -s 2 ".wl[1186].w[7:8]"  0.39303518455957293 0.60696481544042702;
	setAttr -s 2 ".wl[1187].w[7:8]"  0.33787605288286382 0.66212394711713618;
	setAttr -s 2 ".wl[1188].w[7:8]"  0.40533736964529454 0.59466263035470546;
	setAttr -s 2 ".wl[1189].w[7:8]"  0.34997056312998093 0.65002943687001913;
	setAttr -s 2 ".wl[1190].w[7:8]"  0.43298368725903214 0.56701631274096798;
	setAttr -s 2 ".wl[1191].w[7:8]"  0.46872423940591962 0.53127576059408044;
	setAttr ".wl[1192].w[14]"  1;
	setAttr ".wl[1193].w[14]"  1;
	setAttr ".wl[1194].w[14]"  1;
	setAttr ".wl[1195].w[14]"  1;
	setAttr ".wl[1196].w[14]"  1;
	setAttr -s 2 ".wl[1197].w";
	setAttr ".wl[1197].w[14]" 0.97615710294368319;
	setAttr ".wl[1197].w[19]" 0.023842897056316755;
	setAttr ".wl[1198].w[14]"  1;
	setAttr ".wl[1199].w[14]"  1;
	setAttr ".wl[1200].w[14]"  1;
	setAttr ".wl[1201].w[14]"  1;
	setAttr -s 2 ".wl[1202].w";
	setAttr ".wl[1202].w[14]" 0.43522845631623142;
	setAttr ".wl[1202].w[18]" 0.56477154368376858;
	setAttr -s 2 ".wl[1203].w";
	setAttr ".wl[1203].w[14]" 0.78692926071866143;
	setAttr ".wl[1203].w[18]" 0.21307073928133852;
	setAttr -s 3 ".wl[1204].w";
	setAttr ".wl[1204].w[14]" 0.82764147786265374;
	setAttr ".wl[1204].w[19:20]" 0.10407043159963303 0.068288090537713161;
	setAttr ".wl[1205].w[14]"  1;
	setAttr -s 2 ".wl[1206].w[13:14]"  0.460148052082334 0.539851947917666;
	setAttr -s 2 ".wl[1207].w[13:14]"  0.27901408385946275 0.7209859161405372;
	setAttr -s 2 ".wl[1208].w[13:14]"  0.27901253294915829 0.72098746705084182;
	setAttr -s 2 ".wl[1209].w[13:14]"  0.46014804143137295 0.53985195856862711;
	setAttr -s 3 ".wl[1210].w[12:14]"  1.9166607776858324e-023 0.4051535701747454 
		0.59484642982525449;
	setAttr -s 3 ".wl[1211].w[12:14]"  0.20536184414832495 0.64251423726456314 
		0.15212391858711743;
	setAttr -s 3 ".wl[1212].w[12:14]"  0.20537322485394433 0.6425243935616427 
		0.15210238158441292;
	setAttr -s 3 ".wl[1213].w[12:14]"  0.10005672597191927 0.72627744375948555 
		0.17366583026859506;
	setAttr ".wl[1214].w[14]"  1;
	setAttr ".wl[1215].w[14]"  0.99999999999999978;
	setAttr ".wl[1216].w[14]"  0.99999999999999989;
	setAttr ".wl[1217].w[14]"  1;
	setAttr ".wl[1218].w[14]"  1;
	setAttr ".wl[1219].w[14]"  1;
	setAttr ".wl[1220].w[14]"  1;
	setAttr ".wl[1221].w[14]"  1;
	setAttr ".wl[1222].w[14]"  1;
	setAttr -s 3 ".wl[1223].w";
	setAttr ".wl[1223].w[14]" 0.99999719910450235;
	setAttr ".wl[1223].w[17:18]" 2.4072915528663009e-007 2.5601663423967059e-006;
	setAttr ".wl[1224].w[14]"  1;
	setAttr ".wl[1225].w[14]"  1;
	setAttr -s 3 ".wl[1226].w";
	setAttr ".wl[1226].w[14]" 0.99998988416108681;
	setAttr ".wl[1226].w[17:18]" 8.6942813774873677e-007 9.2464107754071695e-006;
	setAttr ".wl[1227].w[14]"  1;
	setAttr ".wl[1228].w[14]"  1;
	setAttr ".wl[1229].w[14]"  0.99999999999999978;
	setAttr ".wl[1230].w[14]"  1;
	setAttr ".wl[1231].w[14]"  1;
	setAttr ".wl[1232].w[14]"  0.99999999999999989;
	setAttr ".wl[1233].w[14]"  1;
	setAttr ".wl[1234].w[14]"  1;
	setAttr ".wl[1235].w[14]"  1;
	setAttr -s 2 ".wl[1236].w[13:14]"  0.23290747831042097 0.76709252168957898;
	setAttr -s 2 ".wl[1237].w[13:14]"  0.23290747831042097 0.76709252168957898;
	setAttr -s 3 ".wl[1238].w";
	setAttr ".wl[1238].w[14]" 0.58276126587693178;
	setAttr ".wl[1238].w[17:18]" 0.013089652563177472 0.40414908155989088;
	setAttr ".wl[1239].w[14]"  1;
	setAttr ".wl[1240].w[14]"  1;
	setAttr ".wl[1241].w[14]"  1;
	setAttr ".wl[1242].w[14]"  1;
	setAttr ".wl[1243].w[14]"  1;
	setAttr ".wl[1244].w[14]"  1;
	setAttr ".wl[1245].w[14]"  1;
	setAttr ".wl[1246].w[14]"  0.99999999999999989;
	setAttr ".wl[1247].w[14]"  0.99999999999999989;
	setAttr ".wl[1248].w[14]"  1;
	setAttr ".wl[1249].w[14]"  0.99999999999999989;
	setAttr ".wl[1250].w[14]"  1;
	setAttr ".wl[1251].w[14]"  1;
	setAttr ".wl[1252].w[14]"  0.99999999999999989;
	setAttr ".wl[1253].w[14]"  1;
	setAttr ".wl[1254].w[14]"  1;
	setAttr ".wl[1255].w[14]"  1;
	setAttr ".wl[1256].w[14]"  1;
	setAttr ".wl[1257].w[14]"  1.0000000000000002;
	setAttr ".wl[1258].w[14]"  1;
	setAttr ".wl[1259].w[14]"  1;
	setAttr ".wl[1260].w[14]"  1;
	setAttr ".wl[1261].w[14]"  1;
	setAttr ".wl[1262].w[14]"  1;
	setAttr ".wl[1263].w[14]"  0.99999999999999989;
	setAttr ".wl[1264].w[14]"  1;
	setAttr ".wl[1265].w[14]"  0.99999999999999989;
	setAttr -s 2 ".wl[1266].w[13:14]"  0.30000000082295891 0.69999999917704103;
	setAttr -s 2 ".wl[1267].w[13:14]"  0.24580000000000005 0.7542;
	setAttr -s 2 ".wl[1268].w[13:14]"  0.24580000000000005 0.7542;
	setAttr ".wl[1269].w[14]"  1;
	setAttr ".wl[1270].w[14]"  1;
	setAttr ".wl[1271].w[14]"  1;
	setAttr ".wl[1272].w[14]"  1;
	setAttr ".wl[1273].w[14]"  1;
	setAttr ".wl[1274].w[14]"  1;
	setAttr ".wl[1275].w[14]"  1;
	setAttr ".wl[1276].w[14]"  0.99999999999999989;
	setAttr ".wl[1277].w[14]"  1;
	setAttr ".wl[1278].w[14]"  1;
	setAttr ".wl[1279].w[14]"  1;
	setAttr ".wl[1280].w[14]"  1;
	setAttr ".wl[1281].w[14]"  1;
	setAttr ".wl[1282].w[14]"  1;
	setAttr ".wl[1283].w[14]"  1;
	setAttr ".wl[1284].w[14]"  1;
	setAttr ".wl[1285].w[14]"  0.99999999999999989;
	setAttr ".wl[1286].w[14]"  1;
	setAttr ".wl[1287].w[14]"  0.99999999999999989;
	setAttr ".wl[1288].w[14]"  1;
	setAttr ".wl[1289].w[14]"  0.99999999999999989;
	setAttr ".wl[1290].w[14]"  1;
	setAttr ".wl[1291].w[14]"  1;
	setAttr ".wl[1292].w[14]"  1;
	setAttr ".wl[1293].w[14]"  1;
	setAttr ".wl[1294].w[14]"  1;
	setAttr ".wl[1295].w[14]"  1;
	setAttr ".wl[1296].w[14]"  1;
	setAttr ".wl[1297].w[14]"  0.99999999999999989;
	setAttr ".wl[1298].w[14]"  0.99999999999999978;
	setAttr ".wl[1299].w[14]"  1;
	setAttr ".wl[1300].w[14]"  1;
	setAttr ".wl[1301].w[14]"  1;
	setAttr ".wl[1302].w[14]"  1;
	setAttr ".wl[1303].w[14]"  1;
	setAttr ".wl[1304].w[14]"  1;
	setAttr ".wl[1305].w[14]"  1;
	setAttr ".wl[1306].w[14]"  1;
	setAttr ".wl[1307].w[14]"  1;
	setAttr ".wl[1308].w[14]"  1;
	setAttr ".wl[1309].w[14]"  1;
	setAttr ".wl[1310].w[14]"  1;
	setAttr ".wl[1311].w[14]"  1;
	setAttr ".wl[1312].w[14]"  1;
	setAttr ".wl[1313].w[14]"  0.99999999999999989;
	setAttr ".wl[1314].w[14]"  1;
	setAttr ".wl[1315].w[14]"  1;
	setAttr ".wl[1316].w[14]"  1;
	setAttr ".wl[1317].w[14]"  1;
	setAttr ".wl[1318].w[14]"  1;
	setAttr ".wl[1319].w[14]"  1;
	setAttr ".wl[1320].w[14]"  1;
	setAttr ".wl[1321].w[14]"  1;
	setAttr ".wl[1322].w[14]"  1;
	setAttr ".wl[1323].w[14]"  1;
	setAttr ".wl[1324].w[14]"  1;
	setAttr ".wl[1325].w[14]"  1;
	setAttr ".wl[1326].w[14]"  1;
	setAttr ".wl[1327].w[14]"  1;
	setAttr ".wl[1328].w[14]"  1;
	setAttr ".wl[1329].w[14]"  0.99999999999999989;
	setAttr ".wl[1330].w[14]"  1;
	setAttr ".wl[1331].w[14]"  1;
	setAttr ".wl[1332].w[14]"  1;
	setAttr ".wl[1333].w[14]"  1;
	setAttr ".wl[1334].w[14]"  1;
	setAttr ".wl[1335].w[14]"  1;
	setAttr ".wl[1336].w[14]"  1;
	setAttr ".wl[1337].w[14]"  1;
	setAttr ".wl[1338].w[14]"  1;
	setAttr ".wl[1339].w[14]"  0.99999999999999989;
	setAttr ".wl[1340].w[14]"  1;
	setAttr ".wl[1341].w[14]"  0.99999999999999989;
	setAttr -s 3 ".wl[1342].w";
	setAttr ".wl[1342].w[14]" 0.66405966380650638;
	setAttr ".wl[1342].w[17:18]" 0.028873134833290143 0.30706720136020349;
	setAttr -s 3 ".wl[1343].w";
	setAttr ".wl[1343].w[14]" 0.1970586235074471;
	setAttr ".wl[1343].w[17:18]" 0.52429947628473339 0.27864190020781937;
	setAttr ".wl[1344].w[14]"  1;
	setAttr ".wl[1345].w[14]"  1;
	setAttr ".wl[1346].w[14]"  0.99999999999999989;
	setAttr -s 2 ".wl[1347].w";
	setAttr ".wl[1347].w[14]" 0.99942703582984393;
	setAttr ".wl[1347].w[17]" 0.00057296417015607122;
	setAttr ".wl[1348].w[14]"  1;
	setAttr -s 2 ".wl[1349].w";
	setAttr ".wl[1349].w[14]" 0.62011959235529346;
	setAttr ".wl[1349].w[17]" 0.37988040764470649;
	setAttr -s 2 ".wl[1350].w";
	setAttr ".wl[1350].w[14]" 0.93690809304007583;
	setAttr ".wl[1350].w[17]" 0.06309190695992406;
	setAttr ".wl[1351].w[14]"  1;
	setAttr -s 2 ".wl[1352].w";
	setAttr ".wl[1352].w[14]" 0.98837387018128309;
	setAttr ".wl[1352].w[19]" 0.011626129818716826;
	setAttr ".wl[1353].w[14]"  1;
	setAttr ".wl[1354].w[14]"  1;
	setAttr ".wl[1355].w[14]"  1;
	setAttr ".wl[1356].w[14]"  1;
	setAttr ".wl[1357].w[14]"  0.99999999999999989;
	setAttr ".wl[1358].w[14]"  1;
	setAttr ".wl[1359].w[14]"  1;
	setAttr ".wl[1360].w[14]"  1;
	setAttr ".wl[1361].w[14]"  1;
	setAttr ".wl[1362].w[14]"  1;
	setAttr ".wl[1363].w[14]"  0.99999999999999978;
	setAttr -s 2 ".wl[1364].w";
	setAttr ".wl[1364].w[14]" 0.99946116104500748;
	setAttr ".wl[1364].w[17]" 0.00053883895499246979;
	setAttr ".wl[1365].w[14]"  0.99999999999999989;
	setAttr ".wl[1366].w[14]"  1;
	setAttr ".wl[1367].w[14]"  1;
	setAttr ".wl[1368].w[14]"  1.0000000000145519;
	setAttr -s 2 ".wl[1369].w";
	setAttr ".wl[1369].w[14]" 0.99930711267266659;
	setAttr ".wl[1369].w[17]" 0.00069288732733343021;
	setAttr ".wl[1370].w[14]"  1;
	setAttr ".wl[1371].w[14]"  1;
	setAttr ".wl[1372].w[16]"  1;
	setAttr ".wl[1373].w[16]"  1;
	setAttr ".wl[1374].w[16]"  1;
	setAttr ".wl[1375].w[16]"  1;
	setAttr ".wl[1376].w[16]"  1;
	setAttr -s 2 ".wl[1377].w[15:16]"  0.42196171076316535 0.57803828923683453;
	setAttr -s 2 ".wl[1378].w[15:16]"  0.27288524089798205 0.72711475910201795;
	setAttr -s 2 ".wl[1379].w[15:16]"  0.34273802288133676 0.65726197711866319;
	setAttr -s 2 ".wl[1380].w[15:16]"  0.25246114698781841 0.7475388530121817;
	setAttr -s 2 ".wl[1381].w[14:15]"  0.10616717276167817 0.89383282723832169;
	setAttr -s 2 ".wl[1382].w[14:15]"  0.15640122127018088 0.84359877872981914;
	setAttr -s 3 ".wl[1383].w[14:16]"  0.12586953281925553 0.80583532647320999 
		0.06829514070753448;
	setAttr -s 2 ".wl[1384].w[14:15]"  0.10645095467281435 0.89354904532718571;
	setAttr ".wl[1385].w[14]"  1;
	setAttr -s 2 ".wl[1386].w[14:15]"  1 7.5013990297102243e-021;
	setAttr ".wl[1387].w[14]"  1;
	setAttr ".wl[1388].w[14]"  1;
	setAttr -s 2 ".wl[1389].w[39:40]"  0.3 0.7;
	setAttr -s 2 ".wl[1390].w[39:40]"  0.1 0.9;
	setAttr -s 2 ".wl[1391].w[39:40]"  0.3 0.7;
	setAttr -s 2 ".wl[1392].w[39:40]"  0.1 0.9;
	setAttr -s 2 ".wl[1393].w[39:40]"  0.18571315755212081 0.81428684244787919;
	setAttr -s 2 ".wl[1394].w[39:40]"  0.17399879520007785 0.82600120479992212;
	setAttr -s 2 ".wl[1395].w[39:40]"  0.1 0.9;
	setAttr -s 2 ".wl[1396].w[39:40]"  0.1 0.9;
	setAttr -s 2 ".wl[1397].w[39:40]"  0.3 0.7;
	setAttr ".wl[1398].w[40]"  1;
	setAttr ".wl[1399].w[40]"  1;
	setAttr ".wl[1400].w[40]"  1;
	setAttr ".wl[1401].w[40]"  1;
	setAttr -s 2 ".wl[1402].w[39:40]"  0.30000000000000004 0.7;
	setAttr ".wl[1403].w[40]"  1;
	setAttr ".wl[1404].w[40]"  1;
	setAttr ".wl[1405].w[40]"  1;
	setAttr -s 3 ".wl[1406].w";
	setAttr ".wl[1406].w[12]" 0.04002372430030101;
	setAttr ".wl[1406].w[21:22]" 0.074811955230796409 0.88516432046890259;
	setAttr -s 3 ".wl[1407].w";
	setAttr ".wl[1407].w[12]" 0.14006442263313199;
	setAttr ".wl[1407].w[21:22]" 0.2620665387850199 0.59786903858184814;
	setAttr -s 3 ".wl[1408].w";
	setAttr ".wl[1408].w[12]" 0.00088681565822422437;
	setAttr ".wl[1408].w[21:22]" 0.19694809330402552 0.80216509103775024;
	setAttr -s 3 ".wl[1409].w";
	setAttr ".wl[1409].w[12]" 0.31555234787786557;
	setAttr ".wl[1409].w[21:22]" 0.14204860331689759 0.54239904880523682;
	setAttr -s 4 ".wl[1410].w";
	setAttr ".wl[1410].w[12:13]" 0.16591352997915434 0.00050240486738711506;
	setAttr ".wl[1410].w[21:22]" 0.24680727215037629 0.58677679300308228;
	setAttr -s 4 ".wl[1411].w";
	setAttr ".wl[1411].w[12:13]" 0.11392026399927234 0.25645182941571359;
	setAttr ".wl[1411].w[21:22]" 0.27503322889832277 0.35459467768669128;
	setAttr -s 3 ".wl[1412].w";
	setAttr ".wl[1412].w[12]" 0.072772281807380948;
	setAttr ".wl[1412].w[21:22]" 0.30642233546404241 0.62080538272857666;
	setAttr -s 4 ".wl[1413].w";
	setAttr ".wl[1413].w[12:13]" 0.3522529570921169 0.076446229137926647;
	setAttr ".wl[1413].w[21:22]" 0.28477997503246866 0.28652083873748779;
	setAttr -s 4 ".wl[1414].w";
	setAttr ".wl[1414].w[12:13]" 0.17484907334836888 0.1729423471671091;
	setAttr ".wl[1414].w[21:22]" 0.24882747024685115 0.4033811092376709;
	setAttr -s 4 ".wl[1415].w";
	setAttr ".wl[1415].w[12:13]" 0.39223706487089394 0.1388606093528777;
	setAttr ".wl[1415].w[21:22]" 0.29500383749307102 0.17389848828315735;
	setAttr -s 3 ".wl[1416].w";
	setAttr ".wl[1416].w[12]" 0.46670898100290542;
	setAttr ".wl[1416].w[21:22]" 0.26101005772199382 0.27228096127510071;
	setAttr -s 4 ".wl[1417].w";
	setAttr ".wl[1417].w[12:13]" 0.3218783975810553 0.0002077673420022511;
	setAttr ".wl[1417].w[21:22]" 0.35081856492008212 0.32709527015686035;
	setAttr -s 4 ".wl[1418].w";
	setAttr ".wl[1418].w[12:13]" 0.26485358660319885 0.00043371924074404433;
	setAttr ".wl[1418].w[21:22]" 0.28225544499147576 0.4524572491645813;
	setAttr -s 4 ".wl[1419].w";
	setAttr ".wl[1419].w[12:13]" 0.32655965722536628 7.2698347467433814e-005;
	setAttr ".wl[1419].w[21:22]" 0.19675382482801465 0.47661381959915161;
	setAttr -s 4 ".wl[1420].w";
	setAttr ".wl[1420].w[12:13]" 0.00029170158615943252 0.00028789143491349993;
	setAttr ".wl[1420].w[21:22]" 0.36597949520906015 0.63344091176986694;
	setAttr -s 5 ".wl[1421].w";
	setAttr ".wl[1421].w[12]" 0.13348871681664173;
	setAttr ".wl[1421].w[16]" 0.00016190437554558466;
	setAttr ".wl[1421].w[18]" 0.00016615686698812714;
	setAttr ".wl[1421].w[21:22]" 0.13512277663213926 0.7310604453086853;
	setAttr -s 4 ".wl[1422].w";
	setAttr ".wl[1422].w[18]" 0.00017588544349786398;
	setAttr ".wl[1422].w[21:23]" 0.15038861825524646 0.84930354356765747 0.00013195273359819555;
	setAttr -s 5 ".wl[1423].w";
	setAttr ".wl[1423].w[12]" 0.10065470780951692;
	setAttr ".wl[1423].w[16]" 0.00031102428026447154;
	setAttr ".wl[1423].w[21:23]" 0.26890129639798077 0.62981647253036499 0.00031649898187287062;
	setAttr -s 5 ".wl[1424].w";
	setAttr ".wl[1424].w[12]" 0.0013967247700268496;
	setAttr ".wl[1424].w[16]" 0.048010606581359055;
	setAttr ".wl[1424].w[21:23]" 0.16895277950088947 0.73278450965881348 0.048855379488911187;
	setAttr -s 4 ".wl[1425].w";
	setAttr ".wl[1425].w[18]" 0.019206599553425453;
	setAttr ".wl[1425].w[21:23]" 0.10100202566133713 0.86117303371429443 0.018618341070942977;
	setAttr -s 4 ".wl[1426].w";
	setAttr ".wl[1426].w[18]" 0.00019981441021794035;
	setAttr ".wl[1426].w[21:23]" 0.26445037175799047 0.73515611886978149 0.00019369496201008708;
	setAttr -s 4 ".wl[1427].w";
	setAttr ".wl[1427].w[18]" 0.013780880715292935;
	setAttr ".wl[1427].w[21:23]" 0.048096890824938274 0.91258955001831055 0.025532678441458241;
	setAttr -s 5 ".wl[1428].w";
	setAttr ".wl[1428].w[12]" 4.2809711516537328e-005;
	setAttr ".wl[1428].w[18]" 2.9831308946473049e-005;
	setAttr ".wl[1428].w[21:23]" 0.13244110281649568 0.86743098497390747 5.5271189133830552e-005;
	setAttr -s 4 ".wl[1429].w";
	setAttr ".wl[1429].w[18]" 2.5787927824981295e-006;
	setAttr ".wl[1429].w[21:23]" 0.055579248003951309 0.94441485404968262 3.3191535835806328e-006;
	setAttr -s 4 ".wl[1430].w";
	setAttr ".wl[1430].w[18]" 0.0086227266624439476;
	setAttr ".wl[1430].w[21:23]" 0.062694354693128729 0.90705454349517822 0.021628375149249101;
	setAttr -s 5 ".wl[1431].w";
	setAttr ".wl[1431].w[16]" 0.00021713336422915882;
	setAttr ".wl[1431].w[18]" 0.00042267125536322179;
	setAttr ".wl[1431].w[21:23]" 0.11875329316753527 0.88034969568252563 0.00025720653034671306;
	setAttr -s 4 ".wl[1432].w";
	setAttr ".wl[1432].w[18]" 0.00062012022456001061;
	setAttr ".wl[1432].w[21:23]" 0.079764231731499782 0.91895103454589844 0.00066461349804177567;
	setAttr -s 5 ".wl[1433].w";
	setAttr ".wl[1433].w[12]" 0.0010160282313857931;
	setAttr ".wl[1433].w[16]" 0.0441459613646678;
	setAttr ".wl[1433].w[18]" 0.045305474949729654;
	setAttr ".wl[1433].w[21:22]" 0.051815796276512893 0.85771673917770386;
	setAttr -s 5 ".wl[1434].w";
	setAttr ".wl[1434].w[12]" 0.00072607835741928005;
	setAttr ".wl[1434].w[18]" 0.0253795447627536;
	setAttr ".wl[1434].w[21:23]" 0.049635599509197666 0.90521854162216187 0.019040235748467582;
	setAttr -s 4 ".wl[1435].w";
	setAttr ".wl[1435].w[18]" 0.025689262396059495;
	setAttr ".wl[1435].w[21:23]" 0.10348636726813415 0.79338759183883667 0.077436778496969685;
	setAttr -s 3 ".wl[1436].w[21:23]"  0.11121638294986659 0.87152912406838934 
		0.017254492981744154;
	setAttr -s 2 ".wl[1437].w[21:22]"  0.23403847217559814 0.76596152782440186;
	setAttr -s 4 ".wl[1438].w";
	setAttr ".wl[1438].w[11:12]" 0.022328791856385546 0.087553696538793635;
	setAttr ".wl[1438].w[21:22]" 0.230059132431542 0.66005837917327881;
	setAttr -s 2 ".wl[1439].w[21:22]"  0.18524169921875 0.81475830078125;
	setAttr -s 4 ".wl[1440].w[21:24]"  0.092680603935156383 0.88738149404525757 
		0.016005924720079404 0.0039319772995066475;
	setAttr -s 4 ".wl[1441].w";
	setAttr ".wl[1441].w[18]" 0.016529201397493327;
	setAttr ".wl[1441].w[21:23]" 0.092932621414127614 0.83710199594497681 0.05343618124340227;
	setAttr -s 2 ".wl[1442].w[21:22]"  0.28943836688995361 0.71056163311004639;
	setAttr -s 2 ".wl[1443].w[21:22]"  0.31138509511947632 0.68861490488052368;
	setAttr -s 4 ".wl[1444].w";
	setAttr ".wl[1444].w[18]" 0.014737545599645475;
	setAttr ".wl[1444].w[21:23]" 0.11306901860356645 0.82290607690811157 0.04928735888867649;
	setAttr -s 3 ".wl[1445].w[21:23]"  0.082172094977650087 0.90426358490006664 
		0.013564320122283299;
	setAttr -s 2 ".wl[1446].w[21:22]"  0.16637128591537476 0.83362871408462524;
	setAttr -s 2 ".wl[1447].w[22:23]"  0.8298565149307251 0.1701434850692749;
	setAttr -s 2 ".wl[1448].w[22:23]"  0.8395613431930542 0.1604386568069458;
	setAttr -s 2 ".wl[1449].w[22:23]"  0.84846466779708862 0.15153533220291138;
	setAttr -s 2 ".wl[1450].w[22:23]"  0.84216761589050293 0.15783238410949707;
	setAttr -s 2 ".wl[1451].w[22:23]"  0.8486369252204895 0.1513630747795105;
	setAttr -s 2 ".wl[1452].w[22:23]"  0.83590239286422729 0.16409760713577271;
	setAttr -s 2 ".wl[1453].w[21:22]"  0.045520246028900146 0.95447975397109985;
	setAttr -s 4 ".wl[1454].w";
	setAttr ".wl[1454].w[18]" 8.5388467393273508e-007;
	setAttr ".wl[1454].w[21:23]" 0.063968427864369057 0.93602913618087769 1.5820700793283606e-006;
	setAttr -s 2 ".wl[1455].w[21:22]"  0.054928243160247803 0.9450717568397522;
	setAttr -s 3 ".wl[1456].w[21:23]"  0.045598432332646752 0.92992195018601409 
		0.024479617481339148;
	setAttr -s 3 ".wl[1457].w[21:23]"  0.014210030425761194 0.92619565240752011 
		0.059594317166718723;
	setAttr -s 3 ".wl[1458].w[21:23]"  0.011268105109189399 0.93184175914033496 
		0.056890135750475593;
	setAttr -s 4 ".wl[1459].w";
	setAttr ".wl[1459].w[18]" 0.0077790530990317344;
	setAttr ".wl[1459].w[21:23]" 0.07154869902805619 0.89261901378631592 0.028053234086596157;
	setAttr -s 4 ".wl[1460].w";
	setAttr ".wl[1460].w[18]" 0.00040208339212918505;
	setAttr ".wl[1460].w[21:23]" 0.040225602225402904 0.9588547945022583 0.00051751988020961096;
	setAttr ".wl[1461].w[22]"  1;
	setAttr ".wl[1462].w[22]"  1;
	setAttr ".wl[1463].w[22]"  1;
	setAttr ".wl[1464].w[22]"  1;
	setAttr ".wl[1465].w[22]"  1;
	setAttr -s 5 ".wl[1466].w";
	setAttr ".wl[1466].w[12]" 0.021119861651473976;
	setAttr ".wl[1466].w[16]" 0.019890352968164691;
	setAttr ".wl[1466].w[18]" 0.020118083543822923;
	setAttr ".wl[1466].w[21:22]" 0.023353775620412923 0.91551792621612549;
	setAttr -s 5 ".wl[1467].w";
	setAttr ".wl[1467].w[12]" 0.017309825505989116;
	setAttr ".wl[1467].w[16]" 0.017428490278228334;
	setAttr ".wl[1467].w[18]" 0.015480801944941249;
	setAttr ".wl[1467].w[21:22]" 0.019497157009153068 0.93028372526168823;
	setAttr -s 5 ".wl[1468].w";
	setAttr ".wl[1468].w[12]" 0.014127572166677808;
	setAttr ".wl[1468].w[16]" 0.012997781058197848;
	setAttr ".wl[1468].w[18]" 0.014205430676435579;
	setAttr ".wl[1468].w[21:22]" 0.016332299175165824 0.94233691692352295;
	setAttr -s 5 ".wl[1469].w";
	setAttr ".wl[1469].w[12]" 0.0044526154066972731;
	setAttr ".wl[1469].w[16]" 0.0042420163304477364;
	setAttr ".wl[1469].w[18]" 0.004131016812884622;
	setAttr ".wl[1469].w[21:22]" 0.0051986172161141691 0.9819757342338562;
	setAttr -s 3 ".wl[1470].w";
	setAttr ".wl[1470].w[12]" 0.10689515711174766;
	setAttr ".wl[1470].w[21:22]" 0.081427088962928856 0.81167775392532349;
	setAttr -s 3 ".wl[1471].w";
	setAttr ".wl[1471].w[12]" 0.027246593516176622;
	setAttr ".wl[1471].w[21:22]" 0.21879041286199721 0.75396299362182617;
	setAttr -s 3 ".wl[1472].w";
	setAttr ".wl[1472].w[12]" 0.07432698032936641;
	setAttr ".wl[1472].w[21:22]" 0.065692853712229049 0.85998016595840454;
	setAttr -s 3 ".wl[1473].w";
	setAttr ".wl[1473].w[12]" 0.00023955491428578375;
	setAttr ".wl[1473].w[21:22]" 0.095768745866964211 0.90399169921875;
	setAttr -s 4 ".wl[1474].w";
	setAttr ".wl[1474].w[18]" 0.01418666717159517;
	setAttr ".wl[1474].w[21:23]" 0.020405766431482616 0.94714796543121338 0.018259600965708833;
	setAttr -s 2 ".wl[1475].w[21:22]"  0.23843002319335938 0.76156997680664063;
	setAttr ".wl[1476].w[14]"  1;
	setAttr ".wl[1477].w[14]"  1;
	setAttr -s 2 ".wl[1478].w[19:20]"  0.9 0.099999999999999978;
	setAttr -s 2 ".wl[1479].w[19:20]"  0.9 0.099999999999999978;
	setAttr ".wl[1480].w[14]"  1;
	setAttr ".wl[1481].w[14]"  1;
	setAttr ".wl[1482].w[14]"  1;
	setAttr ".wl[1483].w[14]"  1;
	setAttr -s 2 ".wl[1484].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[1485].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[1486].w[19:20]"  0.5 0.5;
	setAttr ".wl[1487].w[20]"  1;
	setAttr ".wl[1488].w[20]"  1;
	setAttr -s 2 ".wl[1489].w[17:18]"  0.50007177048033635 0.4999282295196637;
	setAttr ".wl[1490].w[18]"  1;
	setAttr -s 2 ".wl[1491].w[17:18]"  0.50001325607299807 0.49998674392700199;
	setAttr ".wl[1492].w[18]"  1;
	setAttr -s 2 ".wl[1493].w[17:18]"  0.5 0.5;
	setAttr -s 3 ".wl[1494].w";
	setAttr ".wl[1494].w[14]" 0.0016975375084187906;
	setAttr ".wl[1494].w[17:18]" 0.8985061295873531 0.099796332904228111;
	setAttr -s 3 ".wl[1495].w";
	setAttr ".wl[1495].w[14]" 0.00031357241245942976;
	setAttr ".wl[1495].w[17:18]" 0.89971732694031026 0.09996910064723033;
	setAttr -s 2 ".wl[1496].w";
	setAttr ".wl[1496].w[14]" 0.83398123570553306;
	setAttr ".wl[1496].w[17]" 0.16601876429446683;
	setAttr -s 3 ".wl[1497].w";
	setAttr ".wl[1497].w[14]" 0.94454361591195068;
	setAttr ".wl[1497].w[17:18]" 0.048533352916363133 0.006923031171686066;
	setAttr ".wl[1498].w[14]"  1;
	setAttr ".wl[1499].w[14]"  1;
	setAttr ".wl[1500].w[14]"  1;
	setAttr ".wl[1501].w[14]"  1;
	setAttr ".wl[1502].w[14]"  1;
	setAttr ".wl[1503].w[14]"  1;
	setAttr ".wl[1504].w[14]"  1;
	setAttr ".wl[1505].w[14]"  1;
	setAttr ".wl[1506].w[14]"  1;
	setAttr ".wl[1507].w[14]"  1;
	setAttr ".wl[1508].w[14]"  0.99999999999999989;
	setAttr ".wl[1509].w[14]"  1;
	setAttr ".wl[1510].w[14]"  1;
	setAttr ".wl[1511].w[14]"  1;
	setAttr ".wl[1512].w[14]"  1;
	setAttr ".wl[1513].w[14]"  0.99999999999999989;
	setAttr ".wl[1514].w[14]"  0.99999999999999989;
	setAttr ".wl[1515].w[14]"  1;
	setAttr ".wl[1516].w[14]"  1;
	setAttr ".wl[1517].w[14]"  1;
	setAttr ".wl[1518].w[14]"  1;
	setAttr ".wl[1519].w[14]"  1;
	setAttr ".wl[1520].w[14]"  1;
	setAttr -s 3 ".wl[1521].w";
	setAttr ".wl[1521].w[14]" 0.99999460263150597;
	setAttr ".wl[1521].w[17:18]" 4.7235750084034993e-006 6.7379348550941206e-007;
	setAttr ".wl[1522].w[14]"  1;
	setAttr ".wl[1523].w[14]"  1;
	setAttr -s 2 ".wl[1524].w[13:14]"  0.11900940862659309 0.88099059137340696;
	setAttr ".wl[1525].w[14]"  1;
	setAttr ".wl[1526].w[14]"  1;
	setAttr ".wl[1527].w[14]"  1;
	setAttr ".wl[1528].w[14]"  1;
	setAttr ".wl[1529].w[14]"  1;
	setAttr ".wl[1530].w[14]"  0.99999999999999989;
	setAttr ".wl[1531].w[14]"  1;
	setAttr ".wl[1532].w[14]"  1;
	setAttr ".wl[1533].w[14]"  1;
	setAttr ".wl[1534].w[14]"  1;
	setAttr -s 2 ".wl[1535].w[13:14]"  0.081238880722231566 0.91876111927776838;
	setAttr -s 2 ".wl[1536].w[13:14]"  0.26046905652929375 0.73953094347070625;
	setAttr ".wl[1537].w[3]"  1;
	setAttr ".wl[1538].w[7]"  1;
	setAttr ".wl[1539].w[3]"  1;
	setAttr ".wl[1540].w[3]"  1;
	setAttr ".wl[1541].w[3]"  1;
	setAttr ".wl[1542].w[7]"  1;
	setAttr ".wl[1543].w[7]"  1;
	setAttr ".wl[1544].w[7]"  1;
	setAttr ".wl[1545].w[3]"  1;
	setAttr ".wl[1546].w[3]"  1;
	setAttr ".wl[1547].w[3]"  1;
	setAttr ".wl[1548].w[7]"  1;
	setAttr ".wl[1549].w[7]"  1;
	setAttr ".wl[1550].w[7]"  1;
	setAttr ".wl[1551].w[3]"  1;
	setAttr ".wl[1552].w[7]"  0.99999999999999989;
	setAttr ".wl[1553].w[3]"  1;
	setAttr ".wl[1554].w[7]"  1;
	setAttr ".wl[1555].w[3]"  1;
	setAttr ".wl[1556].w[3]"  1;
	setAttr ".wl[1557].w[3]"  1;
	setAttr ".wl[1558].w[7]"  1;
	setAttr ".wl[1559].w[7]"  1;
	setAttr ".wl[1560].w[7]"  1;
	setAttr ".wl[1561].w[3]"  1;
	setAttr ".wl[1562].w[3]"  1;
	setAttr ".wl[1563].w[3]"  1;
	setAttr ".wl[1564].w[7]"  1;
	setAttr ".wl[1565].w[7]"  1;
	setAttr ".wl[1566].w[7]"  1;
	setAttr ".wl[1567].w[3]"  1;
	setAttr ".wl[1568].w[7]"  1;
	setAttr ".wl[1569].w[3]"  1;
	setAttr ".wl[1570].w[7]"  1;
	setAttr ".wl[1571].w[3]"  1;
	setAttr ".wl[1572].w[3]"  1;
	setAttr ".wl[1573].w[3]"  1;
	setAttr ".wl[1574].w[3]"  1;
	setAttr ".wl[1575].w[7]"  1;
	setAttr ".wl[1576].w[7]"  1;
	setAttr ".wl[1577].w[7]"  1;
	setAttr ".wl[1578].w[7]"  1;
	setAttr ".wl[1579].w[3]"  1;
	setAttr ".wl[1580].w[3]"  1;
	setAttr ".wl[1581].w[7]"  1;
	setAttr ".wl[1582].w[7]"  1;
	setAttr ".wl[1583].w[3]"  1;
	setAttr ".wl[1584].w[7]"  1;
	setAttr ".wl[1585].w[3]"  1;
	setAttr ".wl[1586].w[7]"  1;
	setAttr ".wl[1587].w[3]"  1;
	setAttr ".wl[1588].w[7]"  1;
	setAttr ".wl[1589].w[3]"  1;
	setAttr ".wl[1590].w[7]"  1;
	setAttr ".wl[1591].w[3]"  1;
	setAttr ".wl[1592].w[7]"  1;
	setAttr ".wl[1593].w[3]"  1;
	setAttr ".wl[1594].w[3]"  1;
	setAttr ".wl[1595].w[7]"  1;
	setAttr ".wl[1596].w[7]"  1;
	setAttr ".wl[1597].w[3]"  1;
	setAttr ".wl[1598].w[7]"  1;
	setAttr ".wl[1599].w[3]"  1;
	setAttr ".wl[1600].w[3]"  1;
	setAttr ".wl[1601].w[7]"  1;
	setAttr ".wl[1602].w[7]"  1;
	setAttr ".wl[1603].w[3]"  1;
	setAttr ".wl[1604].w[3]"  1;
	setAttr ".wl[1605].w[7]"  1;
	setAttr ".wl[1606].w[7]"  1;
	setAttr ".wl[1607].w[3]"  1;
	setAttr ".wl[1608].w[7]"  1;
	setAttr ".wl[1609].w[3]"  1;
	setAttr ".wl[1610].w[7]"  1;
	setAttr ".wl[1611].w[3]"  1;
	setAttr ".wl[1612].w[7]"  1;
	setAttr ".wl[1613].w[3]"  1;
	setAttr ".wl[1614].w[3]"  1;
	setAttr ".wl[1615].w[7]"  1;
	setAttr ".wl[1616].w[7]"  1;
	setAttr ".wl[1617].w[3]"  1;
	setAttr ".wl[1618].w[3]"  1;
	setAttr ".wl[1619].w[7]"  1;
	setAttr ".wl[1620].w[7]"  1;
	setAttr ".wl[1621].w[3]"  1;
	setAttr ".wl[1622].w[7]"  1;
	setAttr ".wl[1623].w[3]"  1;
	setAttr ".wl[1624].w[7]"  1;
	setAttr ".wl[1625].w[3]"  1;
	setAttr ".wl[1626].w[3]"  1;
	setAttr ".wl[1627].w[7]"  1;
	setAttr ".wl[1628].w[7]"  1;
	setAttr ".wl[1629].w[3]"  1;
	setAttr ".wl[1630].w[3]"  1;
	setAttr ".wl[1631].w[7]"  1;
	setAttr ".wl[1632].w[7]"  1;
	setAttr ".wl[1633].w[3]"  1;
	setAttr ".wl[1634].w[7]"  1;
	setAttr ".wl[1635].w[3]"  1;
	setAttr ".wl[1636].w[7]"  1;
	setAttr ".wl[1637].w[3]"  1;
	setAttr ".wl[1638].w[7]"  1;
	setAttr ".wl[1639].w[3]"  1;
	setAttr ".wl[1640].w[7]"  1;
	setAttr ".wl[1641].w[3]"  1;
	setAttr ".wl[1642].w[3]"  1;
	setAttr ".wl[1643].w[7]"  1;
	setAttr ".wl[1644].w[7]"  1;
	setAttr ".wl[1645].w[3]"  1;
	setAttr ".wl[1646].w[3]"  1;
	setAttr ".wl[1647].w[7]"  1;
	setAttr ".wl[1648].w[7]"  1;
	setAttr ".wl[1649].w[3]"  1;
	setAttr ".wl[1650].w[7]"  1;
	setAttr ".wl[1651].w[3]"  1;
	setAttr ".wl[1652].w[7]"  1;
	setAttr ".wl[1653].w[3]"  1;
	setAttr ".wl[1654].w[7]"  1;
	setAttr ".wl[1655].w[3]"  1;
	setAttr ".wl[1656].w[7]"  1;
	setAttr ".wl[1657].w[3]"  1;
	setAttr ".wl[1658].w[3]"  1;
	setAttr ".wl[1659].w[7]"  1;
	setAttr ".wl[1660].w[7]"  1;
	setAttr ".wl[1661].w[3]"  1;
	setAttr ".wl[1662].w[3]"  1;
	setAttr ".wl[1663].w[7]"  1;
	setAttr ".wl[1664].w[7]"  1;
	setAttr ".wl[1665].w[3]"  1;
	setAttr ".wl[1666].w[7]"  1;
	setAttr ".wl[1667].w[3]"  1;
	setAttr ".wl[1668].w[7]"  1;
	setAttr ".wl[1669].w[3]"  1;
	setAttr ".wl[1670].w[7]"  1;
	setAttr ".wl[1671].w[3]"  1;
	setAttr ".wl[1672].w[7]"  1;
	setAttr ".wl[1673].w[3]"  1;
	setAttr ".wl[1674].w[3]"  1;
	setAttr ".wl[1675].w[3]"  1;
	setAttr ".wl[1676].w[7]"  1;
	setAttr ".wl[1677].w[7]"  1;
	setAttr ".wl[1678].w[7]"  1;
	setAttr ".wl[1679].w[3]"  1;
	setAttr ".wl[1680].w[3]"  1;
	setAttr ".wl[1681].w[3]"  1;
	setAttr ".wl[1682].w[7]"  1;
	setAttr ".wl[1683].w[7]"  1;
	setAttr ".wl[1684].w[7]"  1;
	setAttr ".wl[1685].w[3]"  1;
	setAttr ".wl[1686].w[7]"  1;
	setAttr ".wl[1687].w[3]"  1;
	setAttr ".wl[1688].w[7]"  1;
	setAttr ".wl[1689].w[3]"  1;
	setAttr ".wl[1690].w[7]"  1;
	setAttr ".wl[1691].w[3]"  1;
	setAttr ".wl[1692].w[7]"  1;
	setAttr ".wl[1693].w[3]"  1;
	setAttr ".wl[1694].w[7]"  1;
	setAttr ".wl[1695].w[3]"  1;
	setAttr ".wl[1696].w[7]"  1;
	setAttr ".wl[1697].w[3]"  1;
	setAttr ".wl[1698].w[3]"  1;
	setAttr ".wl[1699].w[7]"  1;
	setAttr ".wl[1700].w[7]"  1;
	setAttr ".wl[1701].w[3]"  1;
	setAttr ".wl[1702].w[7]"  1;
	setAttr ".wl[1703].w[3]"  1;
	setAttr ".wl[1704].w[7]"  1;
	setAttr ".wl[1705].w[3]"  1;
	setAttr ".wl[1706].w[7]"  1;
	setAttr ".wl[1707].w[3]"  1;
	setAttr ".wl[1708].w[3]"  1;
	setAttr ".wl[1709].w[7]"  1;
	setAttr ".wl[1710].w[7]"  1;
	setAttr ".wl[1711].w[3]"  1;
	setAttr ".wl[1712].w[7]"  1;
	setAttr ".wl[1713].w[3]"  1;
	setAttr ".wl[1714].w[7]"  1;
	setAttr ".wl[1715].w[3]"  1;
	setAttr ".wl[1716].w[7]"  1;
	setAttr ".wl[1717].w[3]"  1;
	setAttr ".wl[1718].w[3]"  1;
	setAttr ".wl[1719].w[7]"  1;
	setAttr ".wl[1720].w[7]"  1;
	setAttr ".wl[1721].w[3]"  1;
	setAttr ".wl[1722].w[3]"  1;
	setAttr ".wl[1723].w[7]"  1;
	setAttr ".wl[1724].w[7]"  1;
	setAttr ".wl[1725].w[3]"  1;
	setAttr ".wl[1726].w[7]"  1;
	setAttr ".wl[1727].w[3]"  1;
	setAttr ".wl[1728].w[7]"  1;
	setAttr ".wl[1729].w[3]"  1;
	setAttr ".wl[1730].w[7]"  1;
	setAttr ".wl[1731].w[3]"  1;
	setAttr ".wl[1732].w[3]"  1;
	setAttr ".wl[1733].w[7]"  1;
	setAttr ".wl[1734].w[7]"  1;
	setAttr ".wl[1735].w[3]"  1;
	setAttr ".wl[1736].w[7]"  1;
	setAttr ".wl[1737].w[3]"  1;
	setAttr ".wl[1738].w[7]"  1;
	setAttr ".wl[1739].w[3]"  1;
	setAttr ".wl[1740].w[7]"  1;
	setAttr ".wl[1741].w[3]"  1;
	setAttr ".wl[1742].w[7]"  1;
	setAttr ".wl[1743].w[3]"  1;
	setAttr ".wl[1744].w[7]"  1;
	setAttr ".wl[1745].w[3]"  1;
	setAttr ".wl[1746].w[7]"  1;
	setAttr ".wl[1747].w[3]"  1;
	setAttr ".wl[1748].w[7]"  1;
	setAttr -s 2 ".wl[1749].w[21:22]"  0.47005319595336914 0.52994680404663086;
	setAttr -s 4 ".wl[1750].w";
	setAttr ".wl[1750].w[11:12]" 0.038299283141422787 0.19232371235345;
	setAttr ".wl[1750].w[21:22]" 0.26613743245693999 0.50323957204818726;
	setAttr -s 3 ".wl[1751].w";
	setAttr ".wl[1751].w[12]" 0.14321896791603839;
	setAttr ".wl[1751].w[21:22]" 0.28617629051062787 0.57060474157333374;
	setAttr -s 2 ".wl[1752].w[21:22]"  0.35533177852630615 0.64466822147369385;
	setAttr -s 2 ".wl[1753].w[21:22]"  0.30182790756225586 0.69817209243774414;
	setAttr -s 2 ".wl[1754].w[21:22]"  0.2663920521736145 0.7336079478263855;
	setAttr -s 5 ".wl[1755].w";
	setAttr ".wl[1755].w[14:15]" 0.012804537425040891 0.0079894752909800693;
	setAttr ".wl[1755].w[17:18]" 0.014397100816389915 0.014160817085875257;
	setAttr ".wl[1755].w[22]" 0.95064806938171387;
	setAttr -s 5 ".wl[1756].w";
	setAttr ".wl[1756].w[14:15]" 0.0084613064527329935 0.0052201605067384435;
	setAttr ".wl[1756].w[17:18]" 0.0092926799739260491 0.009182737736860078;
	setAttr ".wl[1756].w[22]" 0.96784311532974243;
	setAttr -s 5 ".wl[1757].w";
	setAttr ".wl[1757].w[14:15]" 0.012463936164110994 0.0082903321049265562;
	setAttr ".wl[1757].w[17:18]" 0.01311441855505458 0.012697322697392245;
	setAttr ".wl[1757].w[22]" 0.95343399047851563;
	setAttr -s 5 ".wl[1758].w";
	setAttr ".wl[1758].w[14:15]" 0.0042352188110033702 0.0027598251459534234;
	setAttr ".wl[1758].w[17:18]" 0.0045150834459181995 0.0044388834461240286;
	setAttr ".wl[1758].w[22]" 0.98405098915100098;
	setAttr ".wl[1759].w[22]"  1;
	setAttr ".wl[1760].w[22]"  1;
	setAttr ".wl[1761].w[22]"  1;
	setAttr ".wl[1762].w[22]"  1;
	setAttr ".wl[1763].w[22]"  1;
	setAttr -s 2 ".wl[1764].w[21:22]"  0.62036147713661194 0.37963852286338806;
	setAttr -s 4 ".wl[1765].w";
	setAttr ".wl[1765].w[11:12]" 0.0067573969634874848 0.25224359850020228;
	setAttr ".wl[1765].w[21:22]" 0.3354706081682835 0.40552839636802673;
	setAttr -s 3 ".wl[1766].w";
	setAttr ".wl[1766].w[12]" 0.23473924846794758;
	setAttr ".wl[1766].w[21:22]" 0.27929412870261516 0.48596662282943726;
	setAttr -s 4 ".wl[1767].w";
	setAttr ".wl[1767].w[11:12]" 9.2786199737528962e-006 0.0018342534558149434;
	setAttr ".wl[1767].w[21:22]" 0.42284438992509632 0.57531207799911499;
	setAttr -s 3 ".wl[1768].w";
	setAttr ".wl[1768].w[12]" 0.0010563207720907493;
	setAttr ".wl[1768].w[21:22]" 0.3460156844998209 0.65292799472808838;
	setAttr -s 3 ".wl[1769].w";
	setAttr ".wl[1769].w[12]" 9.8756696922457724e-007;
	setAttr ".wl[1769].w[21:22]" 0.28592350669954081 0.71407550573348999;
	setAttr ".wl[1770].w[22]"  1;
	setAttr ".wl[1771].w[22]"  1;
	setAttr ".wl[1772].w[22]"  1;
	setAttr ".wl[1773].w[22]"  1;
	setAttr ".wl[1774].w[22]"  1;
	setAttr ".wl[1775].w[22]"  0.99999999999999989;
	setAttr ".wl[1776].w[14]"  1;
	setAttr ".wl[1777].w[14]"  1;
	setAttr ".wl[1778].w[14]"  1;
	setAttr ".wl[1779].w[14]"  1;
	setAttr ".wl[1780].w[14]"  1;
	setAttr ".wl[1781].w[14]"  1;
	setAttr ".wl[1782].w[14]"  1;
	setAttr ".wl[1783].w[14]"  1;
	setAttr ".wl[1784].w[14]"  1;
	setAttr ".wl[1785].w[14]"  1;
	setAttr -s 2 ".wl[1786].w";
	setAttr ".wl[1786].w[0]" 0.52;
	setAttr ".wl[1786].w[11]" 0.48;
	setAttr -s 2 ".wl[1787].w";
	setAttr ".wl[1787].w[0]" 0.52;
	setAttr ".wl[1787].w[11]" 0.48;
	setAttr -s 2 ".wl[1788].w";
	setAttr ".wl[1788].w[0]" 0.52;
	setAttr ".wl[1788].w[11]" 0.48;
	setAttr -s 2 ".wl[1789].w";
	setAttr ".wl[1789].w[0]" 0.52;
	setAttr ".wl[1789].w[11]" 0.48;
	setAttr -s 2 ".wl[1790].w";
	setAttr ".wl[1790].w[0]" 0.52;
	setAttr ".wl[1790].w[11]" 0.48;
	setAttr -s 2 ".wl[1791].w";
	setAttr ".wl[1791].w[0]" 0.52;
	setAttr ".wl[1791].w[11]" 0.48;
	setAttr -s 2 ".wl[1792].w";
	setAttr ".wl[1792].w[0]" 0.52;
	setAttr ".wl[1792].w[11]" 0.48;
	setAttr -s 2 ".wl[1793].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1794].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1795].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1796].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1797].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1798].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1799].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1800].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1801].w[11:12]"  0.8 0.2;
	setAttr -s 2 ".wl[1802].w[11:12]"  0.8 0.2;
	setAttr -s 3 ".wl[1803].w";
	setAttr ".wl[1803].w[11:12]" 0.19893156371437046 0.7815954891296848;
	setAttr ".wl[1803].w[39]" 0.019472947155944827;
	setAttr -s 3 ".wl[1804].w";
	setAttr ".wl[1804].w[11:12]" 0.24890732338924124 0.69638788325585155;
	setAttr ".wl[1804].w[39]" 0.054704793354907293;
	setAttr -s 3 ".wl[1805].w";
	setAttr ".wl[1805].w[11:12]" 0.27712952339525121 0.62737234608839465;
	setAttr ".wl[1805].w[39]" 0.095498130516354043;
	setAttr -s 3 ".wl[1806].w";
	setAttr ".wl[1806].w[11:12]" 0.28602694679819174 0.61960736691189233;
	setAttr ".wl[1806].w[39]" 0.094365686289915804;
	setAttr -s 3 ".wl[1807].w";
	setAttr ".wl[1807].w[11:12]" 0.26586752828602678 0.69051004948442796;
	setAttr ".wl[1807].w[39]" 0.043622422229545335;
	setAttr -s 3 ".wl[1808].w";
	setAttr ".wl[1808].w[11:12]" 0.20537326988716798 0.77494106912968852;
	setAttr ".wl[1808].w[39]" 0.019685660983143515;
	setAttr -s 4 ".wl[1809].w";
	setAttr ".wl[1809].w[0]" 0.0026723301569645287;
	setAttr ".wl[1809].w[11:12]" 0.25497595675792817 0.66278221141011318;
	setAttr ".wl[1809].w[39]" 0.079569501674994242;
	setAttr -s 4 ".wl[1810].w";
	setAttr ".wl[1810].w[0]" 0.045;
	setAttr ".wl[1810].w[11:12]" 0.29027870336813461 0.5652546654715126;
	setAttr ".wl[1810].w[39]" 0.099466631160352773;
	setAttr -s 4 ".wl[1811].w";
	setAttr ".wl[1811].w[0]" 0.044555041902682849;
	setAttr ".wl[1811].w[11:12]" 0.26178331065067661 0.5816791747303931;
	setAttr ".wl[1811].w[39]" 0.11198247271624738;
	setAttr -s 4 ".wl[1812].w";
	setAttr ".wl[1812].w[0]" 0.01768624372323303;
	setAttr ".wl[1812].w[11:12]" 0.26582637638041062 0.63052440836947521;
	setAttr ".wl[1812].w[39]" 0.085962971526881177;
	setAttr -s 2 ".wl[1813].w";
	setAttr ".wl[1813].w[0]" 0.86079999999999979;
	setAttr ".wl[1813].w[11]" 0.13920000000000021;
	setAttr -s 2 ".wl[1814].w";
	setAttr ".wl[1814].w[0]" 0.61080000000000012;
	setAttr ".wl[1814].w[11]" 0.38919999999999988;
	setAttr -s 2 ".wl[1815].w";
	setAttr ".wl[1815].w[0]" 0.62580000000000013;
	setAttr ".wl[1815].w[11]" 0.37419999999999987;
	setAttr -s 2 ".wl[1816].w";
	setAttr ".wl[1816].w[0]" 0.52;
	setAttr ".wl[1816].w[11]" 0.48;
	setAttr -s 2 ".wl[1817].w";
	setAttr ".wl[1817].w[0]" 0.52;
	setAttr ".wl[1817].w[11]" 0.48;
	setAttr -s 2 ".wl[1818].w";
	setAttr ".wl[1818].w[0]" 0.52;
	setAttr ".wl[1818].w[11]" 0.48;
	setAttr -s 57 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-016 -2.2204460492503131e-016 1 0
		 1 4.9303806576313238e-032 -2.2204460492503131e-016 0 4.9303806576313238e-032 1 2.2204460492503131e-016 0
		 -42.443413265848903 -1.5629999999999973 9.8309521771559779e-015 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-016 -2.2204460492503131e-016 1 0
		 1 4.9303806576313238e-032 -2.2204460492503131e-016 0 4.9303806576313238e-032 1 2.2204460492503131e-016 0
		 -42.443413265848925 -1.5629999999999999 9.8309521771559826e-015 1;
	setAttr ".pm[2]" -type "matrix" -0.14344841520534551 -0.98202473439751237 0.12267833226193668 0
		 -0.98960848953353264 0.14357230046565872 -0.00787603848126729 0 -0.0098787457824655849 -0.12253332432646202 -0.99241523306087531 0
		 35.222535362671401 -8.625309540538062 2.2895355504914066 1;
	setAttr ".pm[3]" -type "matrix" -0.11417888494502053 -0.98202473439751237 0.15030170745606017 0
		 -0.96878260787216364 0.14357230046565872 0.20210703407701558 0 -0.22005326836273323 -0.12253332432646201 -0.96776140836075131 0
		 18.237703494637813 -8.6253095405380691 -1.6136689753543627 1;
	setAttr ".pm[4]" -type "matrix" -0.18815360902669864 -0.98202473489457054 -0.015021300392335898 0
		 -0.69293410662471855 0.14357227708539552 -0.70656162160756886 0 0.69601763144796214 -0.12253334773751584 -0.70749207444734374 0
		 2.8965794274807397 -8.6253094899498208 1.5287991459326733 1;
	setAttr ".pm[5]" -type "matrix" -0.12539974418984137 -0.99203482324456604 0.011908552693135732 0
		 -0.01105430071425614 -0.010605422892017258 -0.99988265683579103 0 0.99204470997674343 -0.12551667010960854 -0.0096363339374753408 0
		 -3.6156928772147134 -8.7302171640470991 -0.0043452524179325366 1;
	setAttr ".pm[6]" -type "matrix" 0.14344080376906573 0.98202523301724365 0.12268324063013816 0
		 -0.98960959270728144 0.14356470984499312 0.0078757925855652312 0 -0.0098787567948740718 -0.12253822181079221 0.99241462824750648 0
		 35.22253549882452 -8.6254942680349949 -2.2896019279985262 1;
	setAttr ".pm[7]" -type "matrix" 0.11417040586064071 0.98202523301724343 0.15030489061585642 0
		 -0.96878363375454013 0.14356470984499303 -0.20210750865930469 0 -0.22005315129523717 -0.12253822181079216 0.96776081487136267 0
		 18.237717699540458 -8.625494268035002 1.6136041430448977 1;
	setAttr ".pm[8]" -type "matrix" 0.18815168828469467 0.98202523301724365 -0.015012791647362293 0
		 -0.6929338766282549 0.14356470984499309 0.7065633847773749 0 0.69601837965427116 -0.12253822181079224 0.70749049419684218 0
		 2.8965299500477402 -8.6254942680350126 -1.5288506550747394 1;
	setAttr ".pm[9]" -type "matrix" 0.12540448007104332 0.99203409389888486 0.011919434564161328 0
		 -0.01105454634849273 -0.010616313408577757 0.99988253854872333 0 0.99204410858748626 -0.12552151382065474 0.0096351536070163266 0
		 -3.6157664652451356 -8.7303983797844875 0.0042796649472280875 1;
	setAttr ".pm[10]" -type "matrix" 2.2204460492503131e-016 -2.2204460492503131e-016 1 0
		 1 4.9303806576313238e-032 -2.2204460492503131e-016 0 4.9303806576313238e-032 1 2.2204460492503131e-016 0
		 -42.443413265848903 -1.5629999999999973 9.8309521771559779e-015 1;
	setAttr ".pm[11]" -type "matrix" 2.9410358754422209e-016 -1.1004859356777159e-016 1 0
		 0.91006980612847399 0.41445500114401068 -2.2204460492503136e-016 0 -0.41445500114401068 0.91006980612847399 2.2204460492503136e-016 0
		 -44.168102382058535 -21.832051551966334 9.8309521771559795e-015 1;
	setAttr ".pm[12]" -type "matrix" 2.4753428048985548e-016 -1.9322109909374812e-016 1 0
		 0.99249288162712934 0.12230241174840756 -2.2204460492503141e-016 0 -0.12230241174840756 0.99249288162712934 2.2204460492503131e-016 0
		 -53.741151642327729 -5.9832288452254305 9.830952177155981e-015 1;
	setAttr ".pm[13]" -type "matrix" 9.1941142448845378e-017 -3.0025728880050122e-016 1 0
		 0.88315235441492523 -0.4690862595422875 -2.2204460492503136e-016 0 0.4690862595422875 0.88315235441492523 2.2204460492503136e-016 0
		 -51.262906505188411 28.590499832476745 9.830952177155981e-015 1;
	setAttr ".pm[14]" -type "matrix" 9.1941142448845378e-017 -3.0025728880050122e-016 1 0
		 0.88315235441492523 -0.4690862595422875 -2.2204460492503136e-016 0 0.4690862595422875 0.88315235441492523 2.2204460492503136e-016 0
		 -53.380907864188408 28.590499832476745 9.8309521771559826e-015 1;
	setAttr ".pm[15]" -type "matrix" 5.6801776279671835e-018 3.1396711409754557e-016 1 0
		 -0.69420046610379249 0.71978171195250373 -2.2204460492503131e-016 0 -0.71978171195250373 -0.69420046610379249 2.2204460492503126e-016 0
		 41.226068870544438 -47.638311267981827 9.8309521771559779e-015 1;
	setAttr ".pm[16]" -type "matrix" 5.6801776279671835e-018 3.1396711409754557e-016 1 0
		 -0.69420046610379249 0.71978171195250373 -2.2204460492503131e-016 0 -0.71978171195250373 -0.69420046610379249 2.2204460492503126e-016 0
		 37.350360422544441 -47.63831126798182 9.8309521771559795e-015 1;
	setAttr ".pm[17]" -type "matrix" 0.099717993915552902 -0.99501573941795407 -1.6275960427640035e-016 0
		 -0.99501573938084864 -0.099717993911834363 8.6361264096059392e-006 0 -8.5930817052106028e-006 -8.6117720032569327e-007 -0.99999999996270872 0
		 63.553941423837081 3.4877400906659268 1.6846038101965526 1;
	setAttr ".pm[18]" -type "matrix" 0.099717993915552902 -0.99501573941795407 -1.6275960427640035e-016 0
		 -0.99501573938084864 -0.099717993911834363 8.6361264096059392e-006 0 -8.5930817052106028e-006 -8.6117720032569327e-007 -0.99999999996270872 0
		 60.391941423837082 3.4877400906659264 1.6846038101965521 1;
	setAttr ".pm[19]" -type "matrix" -0.099719182637120513 0.99501562028652835 -2.5013380564276011e-016 0
		 -0.99501562024942303 -0.099719182633401793 -8.6361264095762287e-006 0 -8.5930806762881061e-006 -8.6118746660128345e-007 0.99999999996270883 0
		 63.553925763538338 3.4879307015510754 -1.6846038101965557 1;
	setAttr ".pm[20]" -type "matrix" -0.099719182637120513 0.99501562028652835 -2.5013380564276011e-016 0
		 -0.99501562024942303 -0.099719182633401793 -8.6361264095762287e-006 0 -8.5930806762881061e-006 -8.6118746660128345e-007 0.99999999996270883 0
		 60.391925763538339 3.487930701551075 -1.6846038101965555 1;
	setAttr ".pm[21]" -type "matrix" -0.99598959486545358 -0.089469139482559537 -6.0059898779742596e-011 0
		 -0.089469139479223664 0.99598959482831173 8.636121014475916e-006 0 -7.7260649654075693e-007 8.6014920439361465e-006 -0.9999999999627085 0
		 2.0236768085075432 -56.059117965716567 -1.6663751551076651 1;
	setAttr ".pm[22]" -type "matrix" -0.82641966472747841 -0.56111722617498416 0.046669007289781204 0
		 -0.56022416851336498 0.82773634596918433 0.031645261495066947 0 -0.056386334915746683 7.1605907048036249e-006 -0.99840902499100781 0
		 26.157672225254316 -49.520723787962943 -3.1461891169501537 1;
	setAttr ".pm[23]" -type "matrix" -0.8239799817785799 -0.56111722617498416 -0.078768319252493058 0
		 -0.55857163386149888 0.82773634596918444 -0.053387933157950929 0 0.095156289720997697 7.1605907048105621e-006 -0.99546234508155007 0
		 17.25841685739417 -49.520723787962922 -0.54408169701412823 1;
	setAttr ".pm[24]" -type "matrix" -0.82397970376510732 -0.56111680983058709 -0.07877419316445472 0
		 -0.55857001728491507 0.82773662823723393 -0.053400468769335255 0 0.095168185721361639 3.4694469519536136e-017 -0.99546120789637205 0
		 6.8785993333992241 -49.52073577017778 -0.54364887786346094 1;
	setAttr ".pm[25]" -type "matrix" -0.63390623597231732 -0.76812033957108627 0.090299656326078517 0
		 -0.75657844541220964 0.64009507152756406 0.13366882712048675 0 -0.16047410985281274 0.016414729462340929 -0.98690354985866047 0
		 15.256939354849049 -46.690824941246312 -6.2091422525124189 1;
	setAttr ".pm[26]" -type "matrix" -0.45935375324086108 -0.88365157242599091 0.090299656326078503 0
		 -0.87365819272257916 0.46781834823460056 0.13366882712048678 0 -0.16036050533797416 -0.01748975712021229 -0.98690354985866036 0
		 23.60790252720793 -42.699509508416156 -6.2091422525124216 1;
	setAttr ".pm[27]" -type "matrix" -0.25391677974048138 -0.9630016827781851 0.090299656326078531 0
		 -0.95511295819864417 0.26437072783738469 0.13366882712048681 0 -0.15259589131840612 -0.052305613743800557 -0.98690354985866047 0
		 31.467545573389348 -36.680510882207678 -6.2091422525124216 1;
	setAttr ".pm[28]" -type "matrix" -0.59374213539344645 -0.8044095298324585 -0.01988931806659203 0
		 -0.80463074615472097 0.59334915656808873 0.022497571923094338 0 -0.0062959511535013481 0.029361313231216254 -0.99954903545769636 0
		 18.444585176233492 -45.674188677739643 -3.4270841012245445 1;
	setAttr ".pm[29]" -type "matrix" -0.37453617627306107 -0.92699895776079511 -0.019889318066592027 0
		 -0.92711525743250633 0.37410046604277636 0.022497571923094335 0 -0.013414622566871851 0.026865844802969469 -0.99954903545769602 0
		 27.526415234423784 -40.078724588280565 -3.4270841012245432 1;
	setAttr ".pm[30]" -type "matrix" -0.086411613379630478 -0.99606096605578098 -0.01988931806659203 0
		 -0.99604446395901514 0.085960950868190666 0.022497571923094338 0 -0.020699248530502611 0.02175469663914795 -0.99954903545769624 0
		 36.810925019088032 -30.63563616096835 -3.4270841012245441 1;
	setAttr ".pm[31]" -type "matrix" -0.59697967295573084 -0.79853701337692851 -0.077161572979842649 0
		 -0.79929836733083159 0.60027281093231555 -0.028189935042196853 0 0.068828700844454679 0.044846301101333134 -0.99661999740000851 0
		 18.153413015195692 -45.920264879565387 -1.6826430335170459 1;
	setAttr ".pm[32]" -type "matrix" -0.42773704108810484 -0.90060375045658625 -0.077161572979842663 0
		 -0.90199972619363478 0.4308152986012963 -0.028189935042196849 0 0.058630347327986672 0.057541838297073156 -0.99661999740000839 0
		 25.090744662568678 -41.792151670579123 -1.6826430335170453 1;
	setAttr ".pm[33]" -type "matrix" -0.26485055424727799 -0.96119731354711258 -0.077161572979842677 0
		 -0.96310661508353013 0.26763963746923125 -0.028189935042196843 0 0.047747585250507943 0.066848701447016504 -0.99661999740000862 0
		 30.706792023154627 -37.022427049585708 -1.6826430335170459 1;
	setAttr ".pm[34]" -type "matrix" -0.62183077353059568 -0.77036602934932208 -0.14093498470890845 0
		 -0.75675915564877028 0.63739002642911691 -0.14508457723152143 0 0.20159878331007058 0.016435785142449557 -0.97933027908599368 0
		 15.902481106757863 -46.469537838107662 2.6229145243031948 1;
	setAttr ".pm[35]" -type "matrix" -0.39408067004800845 -0.90820578922379325 -0.1409349847089085 0
		 -0.89923670934756095 0.41270304822150372 -0.14508457723152146 0 0.18993095075917188 0.069558884472557772 -0.97933027908599379 0
		 25.736736033181558 -41.097622039754505 2.6229145243031948 1;
	setAttr ".pm[36]" -type "matrix" -0.39408067004800845 -0.90820578922379325 -0.1409349847089085 0
		 -0.89923670934756095 0.41270304822150372 -0.14508457723152146 0 0.18993095075917188 0.069558884472557772 -0.97933027908599379 0
		 24.697736033181553 -41.097622039754505 2.6229145243031935 1;
	setAttr ".pm[37]" -type "matrix" -0.44684559037494698 -0.89414006128270329 0.02902704207852632 0
		 -0.82728936314141455 0.40065321610680682 -0.39378840772214829 0 0.34047221325027072 -0.19997637668643667 -0.91874268474466025 0
		 24.908929747252582 -37.666966824443115 18.243264212231519 1;
	setAttr ".pm[38]" -type "matrix" -0.42410694479363248 -0.89414006128270362 0.14369011861354403 0
		 -0.90102010133161659 0.40065321610680699 -0.16625215012033928 0 0.091082799551696431 -0.19997637668643675 -0.97555798002640004 0
		 26.997788893545575 -37.666966824443136 11.652781160399545 1;
	setAttr ".pm[39]" -type "matrix" 0.99598970076656834 0.089467960560868892 -4.0820751777583097e-016 0
		 -0.089467960557532852 0.99598970072943238 -8.6354524145252198e-006 0 -7.7259631561350924e-007 8.6008216664536864e-006 0.99999999996271438 0
		 2.0236104530873815 -56.059120362158637 1.6663751174756221 1;
	setAttr ".pm[40]" -type "matrix" 0.82641969238185509 0.56111735892990022 0.046666921379206354 0
		 -0.56022437856778751 0.8277362559751763 -0.031643896765937075 0 -0.056383842559865227 7.1924017313110233e-006 0.99840916574641536 0
		 26.157689658841647 -49.52072265999918 3.1461259376918695 1;
	setAttr ".pm[41]" -type "matrix" 0.8239800645868971 0.56111735892990022 -0.07876650729152529 0
		 -0.55857188651670298 0.82773625597517664 0.053386685019475424 0 0.095154089541879094 7.1924017313040861e-006 0.99546255539408701 0
		 17.258421981477991 -49.520722659999187 0.54409783603807582 1;
	setAttr ".pm[42]" -type "matrix" 0.82397970376510699 0.56111680983058698 -0.078774193164454734 0
		 -0.55857001728491529 0.82773662823723404 0.053400468769335269 0 0.095168185721361626 7.63278329429795e-017 0.99546120789637227 0
		 6.8785976038808077 -49.520735770175307 0.54364900125532911 1;
	setAttr ".pm[43]" -type "matrix" 0.63390683216557742 0.76812034021669573 0.090295465440900849 0
		 -0.75657946062010462 0.64009508414297678 -0.13366302040499828 0 -0.16046696825700116 0.016414207302897409 0.98690471976631255 0
		 15.256899038287456 -46.690830970706578 6.2093446102205601 1;
	setAttr ".pm[44]" -type "matrix" 0.45935433613038634 0.88365169767223428 0.090295465440900849 0
		 -0.87365918814327914 0.46781814837502766 -0.13366302040499828 0 -0.16035341234609882 -0.017488775026796601 0.98690471976631255 0
		 23.608295460186461 -42.699431689078466 6.2093446102205601 1;
	setAttr ".pm[45]" -type "matrix" 0.25391732085231228 0.96300193306763082 0.090295465440900849 0
		 -0.95511388535710229 0.26437031409421319 -0.13366302040499828 0 -0.15258918758956572 -0.052303096789121609 0.98690471976631255 0
		 31.468152385025991 -36.680294407145091 6.2093446102205601 1;
	setAttr ".pm[46]" -type "matrix" 0.59374203685733673 0.80440945655781482 -0.019895222262169127 0
		 -0.80463085408272872 0.59334905732132437 -0.022496329344904399 0 -0.0062914486884239314 0.029365326088031961 0.99954894592368215 0
		 18.444585646157293 -45.674175645496369 3.4273278560026652 1;
	setAttr ".pm[47]" -type "matrix" 0.3745360991072289 0.92699886224130734 -0.019895222262169138 0
		 -0.92711533722122208 0.37410034303002931 -0.022496329344904403 0 -0.013411262234396029 0.026870853133754147 0.99954894592368249 0
		 27.526412441670967 -40.07871185012042 3.4273278560026652 1;
	setAttr ".pm[48]" -type "matrix" 0.086411567596366368 0.99606085211530959 -0.019895222262169134 0
		 -0.99604450419245061 0.08596080987299301 -0.022496329344904399 0 -0.020697503558492942 0.021760469877778233 0.99954894592368237 0
		 36.810918615750126 -30.635624800622566 3.4273278560026657 1;
	setAttr ".pm[49]" -type "matrix" 0.59697924387259504 0.79853630213192273 -0.077172252544415412 0
		 -0.7992981307183169 0.60027312279700829 0.028190003152402044 0 0.068835169902521934 0.044854790435386142 0.99661916856916255 0
		 18.15342509761204 -45.920257228615085 1.6825905488429183 1;
	setAttr ".pm[50]" -type "matrix" 0.42773676073029926 0.90060296854783384 -0.077172252544415371 0
		 -0.90199955574607427 0.43081565101113956 0.02819000315240203 0 0.058635014742344407 0.057551436877601198 0.99661916856916222 0
		 25.090754998427315 -41.792141786660274 1.6825905488429174 1;
	setAttr ".pm[51]" -type "matrix" 0.26485041392576752 0.96119649483368896 -0.077172252544415385 0
		 -0.96310650842077816 0.26764001412308558 0.028190003152402037 0 0.04775051498033709 0.066858964691536099 0.99661916856916255 0
		 30.707130197329946 -37.022357383960205 1.682590548842918 1;
	setAttr ".pm[52]" -type "matrix" 0.62182969051504333 0.77036586029242005 -0.14094068713428418 0
		 -0.75675814843529798 0.63739019168334399 0.14508910476892961 0 0.20160590460287714 0.016437300319371385 0.97932878768443543 0
		 15.902502410615927 -46.46954470413008 -2.6226147789041536 1;
	setAttr ".pm[53]" -type "matrix" 0.39407967123308002 0.90820533770229694 -0.14094068713428426 0
		 -0.89923578258098869 0.41270347587984751 0.14508910476892961 0 0.18993741086680285 0.069562242385858716 0.97932878768443576 0
		 25.736758396336363 -41.097622980998153 -2.6226147789041532 1;
	setAttr ".pm[54]" -type "matrix" 0.39407967123308002 0.90820533770229694 -0.14094068713428426 0
		 -0.89923578258098869 0.41270347587984751 0.14508910476892961 0 0.18993741086680285 0.069562242385858716 0.97932878768443576 0
		 24.697870570336363 -41.097622980998153 -2.6226147789041523 1;
	setAttr ".pm[55]" -type "matrix" 0.44684672815676263 0.89413940612483644 0.029029708064676044 0
		 -0.827290929562427 0.40065431419809378 0.39378399964725674 0 0.34046691381003735 -0.1999771060126122 0.91874448987272239 0
		 24.908908522718239 -37.666937340930993 -18.243370200121827 1;
	setAttr ".pm[56]" -type "matrix" 0.42410735379848319 0.89413940612483633 0.14369298823802162 0
		 -0.90102047348439296 0.40065431419809372 0.16624748682736659 0 0.091077213483988784 -0.19997710601261223 0.97555835205015407 0
		 26.997795823904251 -37.666937340930978 -11.652889030153775 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 56 ".ma";
	setAttr -s 57 ".dpf[0:56]"  3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 
		3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3;
	setAttr -s 56 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 4;
	setAttr ".ucm" yes;
createNode groupId -n "groupId1549";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts37";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:1962]";
createNode tweak -n "tweak4";
createNode objectSet -n "skinCluster13Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster13GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster13GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet13";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId1551";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts39";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode animCurveTA -n "C_POSITION_rotateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -90 15 -90 18 -90;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "C_POSITION_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 15 0 18 0;
	setAttr -s 3 ".kit[0:2]"  10 2 2;
	setAttr -s 3 ".kot[0:2]"  10 2 2;
createNode animCurveTA -n "C_POSITION_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 15 0 18 0;
	setAttr -s 3 ".kit[0:2]"  10 2 2;
	setAttr -s 3 ".kot[0:2]"  10 2 2;
createNode animCurveTU -n "C_POSITION_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 15 1 18 1;
	setAttr -s 3 ".kit[0:2]"  9 2 2;
	setAttr -s 3 ".kot[0:2]"  5 2 2;
createNode animCurveTL -n "C_POSITION_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 15 0 18 0;
	setAttr -s 3 ".kit[0:2]"  10 2 2;
	setAttr -s 3 ".kot[0:2]"  10 2 2;
createNode animCurveTL -n "C_POSITION_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 15 0 18 0;
	setAttr -s 3 ".kit[0:2]"  10 2 2;
	setAttr -s 3 ".kot[0:2]"  10 2 2;
createNode animCurveTL -n "C_POSITION_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 15 0 18 0;
	setAttr -s 3 ".kit[0:2]"  10 2 2;
	setAttr -s 3 ".kot[0:2]"  10 2 2;
createNode animCurveTU -n "C_POSITION_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 15 1 18 1;
	setAttr -s 3 ".kit[0:2]"  10 2 2;
	setAttr -s 3 ".kot[0:2]"  10 2 2;
createNode animCurveTU -n "C_POSITION_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 15 1 18 1;
	setAttr -s 3 ".kit[0:2]"  10 2 2;
	setAttr -s 3 ".kot[0:2]"  10 2 2;
createNode animCurveTU -n "C_POSITION_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 15 1 18 1;
	setAttr -s 3 ".kit[0:2]"  10 2 2;
	setAttr -s 3 ".kot[0:2]"  10 2 2;
createNode animCurveTU -n "C_flyCTR_visibility";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 4 1 15 1 18 1 20 1;
	setAttr -s 5 ".kit[2:4]"  2 2 9;
	setAttr -s 5 ".kot[2:4]"  2 2 5;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 0;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_flyCTR_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 20 0;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_flyCTR_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 20 0;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_flyCTR_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 20 0;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "C_flyCTR_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 20 0;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "C_flyCTR_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 20 0;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "C_flyCTR_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 20 0;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "C_flyCTR_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 4 1 15 1 18 1 20 1;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "C_flyCTR_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 4 1 15 1 18 1 20 1;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "C_flyCTR_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 4 1 15 1 18 1 20 1;
	setAttr -s 5 ".kit[2:4]"  2 2 10;
	setAttr -s 5 ".kot[2:4]"  2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_pelvisCTR_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -6.7381076000000002 4 -6.5179811000000001 
		7 -8.5644603999999998 10 -9.1938118000000006 15 -12.234591 18 -12.234591 21 -9.7243025000000003 
		24 -6.7381076000000002;
	setAttr -s 8 ".kit[1:7]"  3 1 1 2 2 1 3;
	setAttr -s 8 ".kot[1:7]"  3 1 1 2 2 1 3;
	setAttr -s 8 ".kix[0:7]"  0.46025103330612183 1 0.15737113356590271 
		0.28672784566879272 0.054728366434574127 1 0.025815842673182487 1;
	setAttr -s 8 ".kiy[0:7]"  0.88778883218765259 0 -0.98753952980041504 
		-0.95801204442977905 -0.99850130081176758 0 0.99966675043106079 0;
	setAttr -s 8 ".kox[0:7]"  0.46025103330612183 1 0.15737113356590271 
		0.28672793507575989 1 0.039804473519325256 0.025815846398472786 1;
	setAttr -s 8 ".koy[0:7]"  0.88778883218765259 0 -0.98753952980041504 
		-0.95801204442977905 0 0.99920749664306641 0.99966675043106079 0;
createNode animCurveTL -n "C_pelvisCTR_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.0028318135 4 -0.20338811000000001 7 
		-9.3657783999999999 10 -10.528092 15 -11.967875 18 -11.967875 21 -4.0607905000000004 
		24 0.0028318135;
	setAttr -s 8 ".kit[4:7]"  2 2 10 3;
	setAttr -s 8 ".kot[4:7]"  2 2 10 3;
	setAttr -s 8 ".kix[0:7]"  1 1 0.055470742285251617 0.09761868417263031 
		0.11499030888080597 1 0.016705118119716644 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 -0.99846029281616211 -0.99522393941879272 
		-0.99336659908294678 0 0.99986046552658081 0;
	setAttr -s 8 ".kox[0:7]"  1 1 0.055470742285251617 0.09761868417263031 
		1 0.012645870447158813 0.016705118119716644 1;
	setAttr -s 8 ".koy[0:7]"  0 0 -0.99846029281616211 -0.99522393941879272 
		0 0.9999200701713562 0.99986046552658081 0;
createNode animCurveTL -n "C_pelvisCTR_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.7975235999999999 4 -3.1897586000000002 
		7 6.1694865999999999 10 10.91512 15 9.5832113999999997 18 9.5832113999999997 21 1.4672782 
		24 -3.7975235999999999;
	setAttr -s 8 ".kit[4:7]"  2 2 10 3;
	setAttr -s 8 ".kot[4:7]"  2 2 10 3;
	setAttr -s 8 ".kix[0:7]"  1 0.031035525724291801 0.015316939912736416 
		1 0.1241653636097908 1 0.014945192262530327 1;
	setAttr -s 8 ".kiy[0:7]"  0 0.99951833486557007 0.99988263845443726 
		0 -0.99226152896881104 0 -0.99988836050033569 0;
	setAttr -s 8 ".kox[0:7]"  1 0.031035525724291801 0.015316939912736416 
		1 1 0.012320501729846001 0.014945192262530327 1;
	setAttr -s 8 ".koy[0:7]"  0 0.99951833486557007 0.99988263845443726 
		0 0 -0.99992412328720093 -0.99988836050033569 0;
createNode animCurveTA -n "C_pelvisCTR_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 -3.6087921000000001 7 0.43066136999999999 
		10 18.923781 15 31.590176000000003 18 31.590176000000003 24 0;
	setAttr -s 7 ".kit[1:6]"  3 1 1 2 2 3;
	setAttr -s 7 ".kot[1:6]"  3 1 1 2 2 3;
	setAttr -s 7 ".kix[0:6]"  0.93926632404327393 1 0.31198954582214355 
		0.44155216217041016 0.60199522972106934 1 1;
	setAttr -s 7 ".kiy[0:6]"  -0.34318923950195313 0 0.95008552074432373 
		0.89723563194274902 0.79849964380264282 0 0;
	setAttr -s 7 ".kox[0:6]"  0.93926632404327393 1 0.31198954582214355 
		0.44155162572860718 1 0.34100228548049927 1;
	setAttr -s 7 ".koy[0:6]"  -0.34318923950195313 0 0.95008552074432373 
		0.89723587036132813 0 -0.94006246328353882 0;
createNode animCurveTA -n "C_pelvisCTR_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 -15.845816999999998 7 88.684678 10 
		90 15 102.66744 18 102.66744 24 0;
	setAttr -s 7 ".kit[1:6]"  3 1 2 3 1 3;
	setAttr -s 7 ".kot[1:6]"  3 1 2 3 1 3;
	setAttr -s 7 ".kix[0:6]"  1 1 0.52432912588119507 0.97464728355407715 
		1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0.85151565074920654 0.22374682128429413 
		0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 0.52432912588119507 0.6019635796546936 
		1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0.85151565074920654 0.79852354526519775 
		0 0 0;
createNode animCurveTA -n "C_pelvisCTR_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 5.8054747999999998 7 -14.825205 10 
		0 15 13.299857000000001 18 13.299857000000001 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 1 2 2 3;
	setAttr -s 7 ".kot[0:6]"  1 3 3 1 2 2 3;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.39097344875335693 0.58323425054550171 
		1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.92040199041366577 0.81230407953262329 
		0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 0.39097344875335693 1 0.65273576974868774 
		1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0.92040199041366577 0 -0.75758570432662964 
		0;
createNode animCurveTU -n "C_pelvisCTR_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 4 1 7 1 10 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[4:6]"  2 2 10;
	setAttr -s 7 ".kot[4:6]"  2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_pelvisCTR_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 4 1 7 1 10 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[4:6]"  2 2 10;
	setAttr -s 7 ".kot[4:6]"  2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_pelvisCTR_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 4 1 7 1 10 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[4:6]"  2 2 10;
	setAttr -s 7 ".kot[4:6]"  2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_pelvisCTR_visibility";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 4 1 7 1 10 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[4:6]"  2 2 9;
	setAttr -s 7 ".kot[4:6]"  2 2 5;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 0;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "R_handCTR_twist";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 6 0 9 -29.5 15 -64.1 18 -64.1 19 -52.078600581258598 
		21 -1.2625 24 0;
	setAttr -s 8 ".kit[1:7]"  10 9 1 1 10 1 10;
	setAttr -s 8 ".kot[1:7]"  10 9 1 1 10 1 10;
	setAttr -s 8 ".kix[0:7]"  1 1 0.004680135753005743 1 1 0.0015914037358015776 
		0.012998451478779316 0.078960627317428589;
	setAttr -s 8 ".kiy[0:7]"  0 0 -0.99998897314071655 0 0 0.99999874830245972 
		0.9999154806137085 0.99687778949737549;
	setAttr -s 8 ".kox[0:7]"  1 1 0.004680135753005743 1 1 0.0015914037358015776 
		0.012998451478779316 0.078960627317428589;
	setAttr -s 8 ".koy[0:7]"  0 0 -0.99998897314071655 0 0 0.99999874830245972 
		0.9999154806137085 0.99687778949737549;
createNode animCurveTA -n "R_handCTR_rotateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  1 41.584565 6 15.872 7 23.288 8 169.386 
		9 -14.977000000000002 10 -4.137 11 0.23301385697978916 12 4.6847305611742582 15 14.225016438176105 
		18 14.225016438176105 19 -15.539716357345188 20 8.89 21 -0.83138405999220866 24 41.584565;
	setAttr -s 14 ".kit[0:13]"  1 10 10 10 1 10 10 10 
		2 2 10 10 10 3;
	setAttr -s 14 ".kot[0:13]"  1 10 10 10 1 10 10 10 
		2 2 10 10 10 3;
	setAttr -s 14 ".kix[0:13]"  1 0.53079158067703247 0.024874195456504822 
		0.099329136312007904 0.93673938512802124 0.24356865882873535 0.39734193682670593 
		0.47921156883239746 0.51485288143157959 1 0.5821453332901001 0.25135967135429382 
		0.22753198444843292 1;
	setAttr -s 14 ".kiy[0:13]"  0 -0.84750241041183472 0.99969059228897095 
		-0.99505466222763062 -0.35002779960632324 0.96988368034362793 0.91767066717147827 
		0.87769943475723267 0.85727852582931519 0 -0.81308472156524658 0.96789371967315674 
		0.9737706184387207 0;
	setAttr -s 14 ".kox[0:13]"  1 0.53079158067703247 0.024874195456504822 
		0.099329136312007904 0.93673974275588989 0.24356865882873535 0.39734193682670593 
		0.47921156883239746 1 0.064033441245555878 0.5821453332901001 0.25135967135429382 
		0.22753198444843292 1;
	setAttr -s 14 ".koy[0:13]"  0 -0.84750241041183472 0.99969059228897095 
		-0.99505466222763062 -0.3500266969203949 0.96988368034362793 0.91767066717147827 
		0.87769943475723267 0 -0.9979478120803833 -0.81308472156524658 0.96789371967315674 
		0.9737706184387207 0;
createNode animCurveTA -n "R_handCTR_rotateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  1 7.3114039000000002 6 0 7 15.011999999999999 
		8 85.86 9 128.374 10 182.528 11 192.66354101978132 12 191.38121113781673 15 194.87257832807774 
		18 194.87257832807774 19 229.575 20 195.016 21 147.63423014284356 24 7.3114039000000002;
	setAttr -s 14 ".kit[0:13]"  1 10 10 10 1 1 10 10 
		1 1 10 10 1 3;
	setAttr -s 14 ".kot[0:13]"  1 10 10 10 1 1 10 10 
		1 1 10 10 1 3;
	setAttr -s 14 ".kix[0:13]"  1 0.83000016212463379 0.044443804770708084 
		0.033675771206617355 0.033953629434108734 0.060034055262804031 1 1 1 1 0.99929583072662354 
		0.046565037220716476 0.036756526678800583 1;
	setAttr -s 14 ".kiy[0:13]"  0 0.55776321887969971 0.99901193380355835 
		0.99943286180496216 0.99942344427108765 0.99819636344909668 0 0 0 0 0.037521269172430038 
		-0.99891531467437744 -0.9993242621421814 0;
	setAttr -s 14 ".kox[0:13]"  1 0.83000016212463379 0.044443804770708084 
		0.033675771206617355 0.033953629434108734 0.060034055262804031 1 1 1 1 0.99929583072662354 
		0.046565037220716476 0.036756526678800583 1;
	setAttr -s 14 ".koy[0:13]"  0 0.55776321887969971 0.99901193380355835 
		0.99943286180496216 0.99942344427108765 0.99819636344909668 0 0 0 0 0.037521269172430038 
		-0.99891531467437744 -0.9993242621421814 0;
createNode animCurveTA -n "R_handCTR_rotateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  1 -23.856215 6 0 7 8.088 8 171.076 9 -0.859 
		10 11.56 11 10.789299915758756 12 18.305024198110569 15 16.333151450921456 18 16.333151450921456 
		19 46.614 20 44.498 21 24.722390963398002 24 -23.856215;
	setAttr -s 14 ".kit[0:13]"  1 10 10 10 10 1 10 10 
		2 2 10 10 1 3;
	setAttr -s 14 ".kot[0:13]"  1 10 10 10 10 1 10 10 
		2 2 10 10 1 3;
	setAttr -s 14 ".kix[0:13]"  1 0.33765596151351929 0.022322053089737892 
		0.39264151453971863 0.023938814178109169 0.28819286823272705 1 1 0.94556832313537598 
		1 1 1 0.10756271332502365 1;
	setAttr -s 14 ".kiy[0:13]"  0 0.94126957654953003 0.99975085258483887 
		-0.91969162225723267 -0.99971342086791992 0.95757240056991577 0 0 -0.3254237174987793 
		0 0 0 -0.99419832229614258 0;
	setAttr -s 14 ".kox[0:13]"  1 0.33765596151351929 0.022322053089737892 
		0.39264151453971863 0.023938814178109169 0.28819286823272705 1 1 1 0.062946394085884094 
		1 1 0.10756271332502365 1;
	setAttr -s 14 ".koy[0:13]"  0 0.94126957654953003 0.99975085258483887 
		-0.91969162225723267 -0.99971342086791992 0.95757240056991577 0 0 0 0.99801695346832275 
		0 0 -0.99419832229614258 0;
createNode animCurveTL -n "R_handCTR_translateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  1 -1.8198262000000001 6 -1.413 7 -9.5004456091847942 
		8 -13.386661463429103 9 -26.589439747556721 10 -28.028988568326113 15 -35.970293794743135 
		18 -35.970293794743135 19 -27.289560181924493 20 -19.654 21 -13.449 24 -1.8198262000000001;
	setAttr -s 12 ".kit[0:11]"  1 1 10 10 1 10 3 1 
		10 10 1 3;
	setAttr -s 12 ".kot[0:11]"  1 1 10 10 1 10 3 1 
		10 10 1 3;
	setAttr -s 12 ".kix[0:11]"  0.064375013113021851 0.088109195232391357 
		0.0055676912888884544 0.0039011172484606504 0.070002011954784393 0.021315176039934158 
		1 1 0.0040858611464500427 0.0048167044296860695 0.0060774986632168293 1;
	setAttr -s 12 ".kiy[0:11]"  0.99792581796646118 0.99611079692840576 
		-0.99998456239700317 -0.99999237060546875 -0.99754679203033447 -0.99977284669876099 
		0 0 0.99999165534973145 0.9999883770942688 0.99998152256011963 0;
	setAttr -s 12 ".kox[0:11]"  0.064375057816505432 0.088109530508518219 
		0.0055676912888884544 0.0039011172484606504 0.070002011954784393 0.021315176039934158 
		1 1 0.0040858611464500427 0.0048167044296860695 0.0060775033198297024 1;
	setAttr -s 12 ".koy[0:11]"  0.99792575836181641 0.99611079692840576 
		-0.99998456239700317 -0.99999237060546875 -0.99754679203033447 -0.99977284669876099 
		0 0 0.99999165534973145 0.9999883770942688 0.9999815821647644 0;
createNode animCurveTL -n "R_handCTR_translateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  1 -4.3031072999999997 6 1.533 7 -3.1051066294404612 
		8 -0.77378201736251939 9 -4.1421270827918049 10 -6.5806601218991467 15 -3.3222890152774847 
		18 -3.3222890152774847 19 -6.9747046210327266 20 -12.373 21 -10.629 24 -4.3031072999999997;
	setAttr -s 12 ".kit[0:11]"  1 1 10 10 1 10 2 2 
		10 10 10 3;
	setAttr -s 12 ".kot[0:11]"  1 1 10 10 1 10 2 2 
		10 10 10 3;
	setAttr -s 12 ".kix[0:11]"  1 0.012762059457600117 0.028888234868645668 
		0.06415431946516037 0.0083526670932769775 0.23700033128261566 0.051083516329526901 
		1 0.0073657035827636719 0.018240334466099739 0.01652006059885025 1;
	setAttr -s 12 ".kiy[0:11]"  0 0.99991858005523682 -0.99958264827728271 
		-0.99794000387191772 -0.99996507167816162 0.97150957584381104 0.99869436025619507 
		0 -0.99997282028198242 -0.99983364343643188 0.99986350536346436 0;
	setAttr -s 12 ".kox[0:11]"  1 0.012762059457600117 0.028888234868645668 
		0.06415431946516037 0.0083526670932769775 0.23700033128261566 1 0.0091259917244315147 
		0.0073657035827636719 0.018240334466099739 0.01652006059885025 1;
	setAttr -s 12 ".koy[0:11]"  0 0.99991858005523682 -0.99958264827728271 
		-0.99794000387191772 -0.99996507167816162 0.97150957584381104 0 -0.999958336353302 
		-0.99997282028198242 -0.99983364343643188 0.99986350536346436 0;
createNode animCurveTL -n "R_handCTR_translateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  1 -1.235205 6 1.607 7 -16.268997445016605 
		8 -21.047371480751217 9 -20.560906257913928 10 -15.890694510656756 15 -12.675661976641845 
		18 -12.675661976641845 19 -17.976271224133122 20 -19.896 21 -21.532 24 -1.235205;
	setAttr -s 12 ".kit[0:11]"  1 1 10 10 9 10 2 2 
		10 10 1 3;
	setAttr -s 12 ".kot[0:11]"  1 1 10 10 9 10 2 2 
		10 10 1 3;
	setAttr -s 12 ".kix[0:11]"  1 0.068876221776008606 0.0029427604749798775 
		0.015531232580542564 0.012927141971886158 0.02535567432641983 0.051770288497209549 
		1 0.0092327836900949478 0.018745791167020798 0.040186159312725067 1;
	setAttr -s 12 ".kiy[0:11]"  0 -0.99762523174285889 -0.99999570846557617 
		-0.99987936019897461 0.9999164342880249 0.99967849254608154 0.99865901470184326 0 
		-0.9999573826789856 -0.99982422590255737 0.99919217824935913 0;
	setAttr -s 12 ".kox[0:11]"  1 0.068876147270202637 0.0029427604749798775 
		0.015531232580542564 0.012927141971886158 0.02535567432641983 1 0.0062884548678994179 
		0.0092327836900949478 0.018745791167020798 0.040186155587434769 1;
	setAttr -s 12 ".koy[0:11]"  0 -0.99762523174285889 -0.99999570846557617 
		-0.99987936019897461 0.9999164342880249 0.99967849254608154 0 -0.99998021125793457 
		-0.9999573826789856 -0.99982422590255737 0.99919217824935913 0;
createNode animCurveTU -n "R_handCTR_scaleX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 6 1 9 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  1 10 9 2 2 10 10;
	setAttr -s 7 ".kot[0:6]"  1 10 9 2 2 10 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "R_handCTR_scaleY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 6 1 9 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  1 10 9 2 2 10 10;
	setAttr -s 7 ".kot[0:6]"  1 10 9 2 2 10 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "R_handCTR_scaleZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 6 1 9 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  1 10 9 2 2 10 10;
	setAttr -s 7 ".kot[0:6]"  1 10 9 2 2 10 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "L_handCTR_twist";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 41.442857 5 41.442857 8 41.442857 15 
		12.057905 18 12.057905 24 41.442857;
	setAttr -s 6 ".kit[3:5]"  2 2 10;
	setAttr -s 6 ".kot[3:5]"  2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.0079403212293982506 1 0.0068060467019677162;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 -0.99996846914291382 0 0.99997681379318237;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 0.0068060467019677162 0.0068060471676290035;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0.99997681379318237 0.99997687339782715;
createNode animCurveTL -n "L_handCTR_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.5646884999999999 5 -0.98034231000000005 
		8 -0.81942448999999995 12 -7.2869755999999999 15 -14.740853 18 -14.740853 24 -4.5646884999999999;
	setAttr -s 7 ".kit[4:6]"  2 2 3;
	setAttr -s 7 ".kot[4:6]"  2 2 3;
	setAttr -s 7 ".kix[0:6]"  0.31092718243598938 0.073473863303661346 
		0.045423101633787155 0.018586300313472748 0.013414627872407436 1 1;
	setAttr -s 7 ".kiy[0:6]"  0.95043373107910156 0.99729716777801514 
		-0.99896782636642456 -0.99982720613479614 -0.99990999698638916 0 0;
	setAttr -s 7 ".kox[0:6]"  0.31092718243598938 0.07347385585308075 
		0.045423097908496857 0.018586300313472748 1 0.019649973139166832 1;
	setAttr -s 7 ".koy[0:6]"  0.95043373107910156 0.99729716777801514 
		-0.99896782636642456 -0.99982720613479614 0 0.99980688095092773 0;
createNode animCurveTL -n "L_handCTR_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.3228628999999996 5 3.9705848000000001 
		8 4.0802528000000002 12 0.91898696999999996 15 -1.5527918999999999 18 -1.5527918999999999 
		24 -5.3228628999999996;
	setAttr -s 7 ".kit[3:6]"  10 2 2 3;
	setAttr -s 7 ".kot[3:6]"  10 2 2 3;
	setAttr -s 7 ".kix[0:6]"  0.16076177358627319 0.14350852370262146 
		0.16076177358627319 0.041386749595403671 0.040423624217510223 1 1;
	setAttr -s 7 ".kiy[0:6]"  -0.98699325323104858 0.98964911699295044 
		-0.98699325323104858 -0.99914318323135376 -0.99918264150619507 0 0;
	setAttr -s 7 ".kox[0:6]"  0.16076177358627319 0.14350850880146027 
		0.16076177358627319 0.041386749595403671 1 0.05297490581870079 1;
	setAttr -s 7 ".koy[0:6]"  -0.98699325323104858 0.98964911699295044 
		-0.98699325323104858 -0.99914318323135376 0 -0.99859583377838135 0;
createNode animCurveTL -n "L_handCTR_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.0274957999999996 5 5.91535 8 5.9010084000000003 
		12 -8.1604744999999994 15 -14.414565 18 -14.414565 24 4.0274957999999996;
	setAttr -s 7 ".kit[3:6]"  10 2 2 3;
	setAttr -s 7 ".kot[3:6]"  10 2 2 3;
	setAttr -s 7 ".kix[0:6]"  0.35091507434844971 0.35091507434844971 
		0.35091507434844971 0.011484683491289616 0.015987491235136986 1 1;
	setAttr -s 7 ".kiy[0:6]"  0.93640726804733276 0.93640726804733276 
		0.93640726804733276 -0.99993401765823364 -0.99987214803695679 0 0;
	setAttr -s 7 ".kox[0:6]"  0.35091507434844971 0.35091507434844971 
		0.35091507434844971 0.011484683491289616 1 0.010844136588275433 1;
	setAttr -s 7 ".koy[0:6]"  0.93640726804733276 0.93640726804733276 
		0.93640726804733276 -0.99993401765823364 0 0.99994117021560669 0;
createNode animCurveTA -n "L_handCTR_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 36.239627 5 39.757061 8 22.572453 15 
		-31.717261 18 -31.717261 24 36.239627;
	setAttr -s 6 ".kit[3:5]"  2 2 3;
	setAttr -s 6 ".kot[3:5]"  2 2 3;
	setAttr -s 6 ".kix[0:5]"  0.66021060943603516 0.66021060943603516 
		0.21566078066825867 0.23910997807979584 1 1;
	setAttr -s 6 ".kiy[0:5]"  -0.75108057260513306 -0.75108057260513306 
		-0.97646838426589966 -0.97099250555038452 0 0;
	setAttr -s 6 ".kox[0:5]"  0.66021060943603516 0.66021060943603516 
		0.21566078066825867 1 0.16627652943134308 1;
	setAttr -s 6 ".koy[0:5]"  -0.75108057260513306 -0.75108057260513306 
		-0.97646838426589966 0 0.98607921600341797 0;
createNode animCurveTA -n "L_handCTR_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -71.639339 5 -60.79010199999999 8 -50.472533 
		15 53.946497 18 53.946497 24 -71.639339;
	setAttr -s 6 ".kit[3:5]"  2 2 3;
	setAttr -s 6 ".kot[3:5]"  2 2 3;
	setAttr -s 6 ".kix[0:5]"  0.80460518598556519 0.58944714069366455 
		0.40189367532730103 0.12699571251869202 1 1;
	setAttr -s 6 ".kiy[0:5]"  0.59381020069122314 0.80780696868896484 
		0.91568630933761597 0.99190330505371094 0 0;
	setAttr -s 6 ".kox[0:5]"  0.80460518598556519 0.589447021484375 0.40189367532730103 
		1 0.090868107974529266 1;
	setAttr -s 6 ".koy[0:5]"  0.59381020069122314 0.80780708789825439 
		0.91568630933761597 0 -0.99586290121078491 0;
createNode animCurveTA -n "L_handCTR_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -55.914884 5 -16.342945 8 4.1592748000000004 
		15 -7.1644986000000008 18 -7.1644986000000008 24 -55.914884;
	setAttr -s 6 ".kit[3:5]"  2 2 3;
	setAttr -s 6 ".kot[3:5]"  2 2 3;
	setAttr -s 6 ".kix[0:5]"  0.66390281915664673 0.19621200859546661 
		0.66390281915664673 0.76306170225143433 1 1;
	setAttr -s 6 ".kiy[0:5]"  0.74781882762908936 0.98056149482727051 
		0.74781882762908936 -0.64632570743560791 0 0;
	setAttr -s 6 ".kox[0:5]"  0.66390281915664673 0.19621200859546661 
		0.66390281915664673 1 0.2288212776184082 1;
	setAttr -s 6 ".koy[0:5]"  0.74781882762908936 0.98056149482727051 
		0.74781882762908936 0 -0.97346842288970947 0;
createNode animCurveTU -n "L_handCTR_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 8 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[3:5]"  2 2 10;
	setAttr -s 6 ".kot[3:5]"  2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_handCTR_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 8 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[3:5]"  2 2 10;
	setAttr -s 6 ".kot[3:5]"  2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_handCTR_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 8 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[3:5]"  2 2 10;
	setAttr -s 6 ".kot[3:5]"  2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "C_hipCTR_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 3 0 5 0 8 0 10 0 15 0 18 0 24 0;
	setAttr -s 8 ".kit[2:7]"  10 1 1 2 2 10;
	setAttr -s 8 ".kot[2:7]"  10 1 1 2 2 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "C_hipCTR_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 3 0 5 0 8 0 10 0 15 0 18 0 24 0;
	setAttr -s 8 ".kit[2:7]"  10 1 1 2 2 10;
	setAttr -s 8 ".kot[2:7]"  10 1 1 2 2 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "C_hipCTR_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 3 0 5 0 8 0 10 0 15 0 18 0 24 0;
	setAttr -s 8 ".kit[2:7]"  10 1 1 2 2 10;
	setAttr -s 8 ".kot[2:7]"  10 1 1 2 2 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "C_hipCTR_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kot[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "C_hipCTR_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kot[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "C_hipCTR_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kot[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_hipCTR_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 3 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kot[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_hipCTR_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 3 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kot[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_hipCTR_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 3 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kot[2:6]"  10 1 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_hipCTR_visibility";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 3 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[2:6]"  9 1 2 2 9;
	setAttr -s 7 ".kot[2:6]"  5 1 2 2 5;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 0 1 1 1 0;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "L_legCTR_twist";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 4 0 7 18.5 15 18.5 18 18.5 19 18.5 
		21 0 24 0;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_legCTR_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 8.5104273999999993 4 8.5104273999999993 
		7 12.806508 15 12.806508 18 12.806508 19 12.806508 21 8.5104273999999993 24 8.5104273999999993;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_legCTR_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 4 0 7 0 15 0 18 0 19 0 21 0 24 0;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_legCTR_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 9.7818088000000003 4 9.7818088000000003 
		7 -7.121658 15 -7.121658 18 -7.121658 19 -7.121658 21 9.7818088000000003 24 9.7818088000000003;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 0.014639479108154774 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 -0.99989283084869385 0 0 0 0 0 0;
createNode animCurveTA -n "L_legCTR_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 4 0 7 0 15 0 18 0 19 0 21 0 24 0;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_legCTR_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -4.0312628000000004 4 -4.0312628000000004 
		7 85.045782 15 85.045782 18 85.045782 19 85.045782 21 -4.0312628000000004 24 -4.0312628000000004;
	setAttr -s 8 ".kit[6:7]"  10 10;
	setAttr -s 8 ".kot[6:7]"  10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_legCTR_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 4 0 7 0 15 0 18 0 19 0 21 0 24 0;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_legCTR_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1 4 1 7 1 15 1 18 1 19 1 21 1 24 1;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_legCTR_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1 4 1 7 1 15 1 18 1 19 1 21 1 24 1;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_legCTR_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1 4 1 7 1 15 1 18 1 19 1 21 1 24 1;
	setAttr -s 8 ".kit[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kot[3:7]"  2 2 1 10 10;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_legCTR_twist";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 38.2 6 38.2 15 38.2 18 38.2 24 38.2;
	setAttr -s 5 ".kit[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "R_legCTR_translateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -10.301319 6 -10.301319 9 -5.9762123999999996 
		15 -5.9762123999999996 18 -5.9762123999999996 21 -5.9762123999999996 24 -10.301319;
	setAttr -s 7 ".kit[0:6]"  1 10 10 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  1 10 10 2 2 1 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 0.023114645853638649;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 -0.9997328519821167;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 0.023114645853638649;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 -0.9997328519821167;
createNode animCurveTL -n "R_legCTR_translateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -6.1177416999999998 6 -6.1177416999999998 
		9 -6.7297308999999998 15 -6.7297308999999998 18 -6.7297308999999998 21 -6.7297308999999998 
		24 -6.1177416999999998;
	setAttr -s 7 ".kit[0:6]"  1 10 10 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  1 10 10 2 2 1 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 0.16126292943954468;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0.98691153526306152;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 0.16126292943954468;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0.98691153526306152;
createNode animCurveTL -n "R_legCTR_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 6 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "R_legCTR_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 6 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "R_legCTR_rotateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -37.815816 6 -37.815816 9 18.206721 15 
		18.206721 18 18.206721 21 18.206721 24 -37.815816;
	setAttr -s 7 ".kit[0:6]"  1 10 10 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  1 10 10 2 2 1 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 0.10174205154180527;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 -0.99481081962585449;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 0.10174205154180527;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 -0.99481081962585449;
createNode animCurveTA -n "R_legCTR_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 6 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "R_legCTR_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 6 1 15 1 18 1 24 1;
	setAttr -s 5 ".kit[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "R_legCTR_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 6 1 15 1 18 1 24 1;
	setAttr -s 5 ".kit[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "R_legCTR_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 1 6 1 15 1 18 1 24 1;
	setAttr -s 5 ".kit[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 10 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "R_foot_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 6 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "R_foot_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 6.4401305999999998 5 6.4401305999999998 
		6 6.4401305999999998 15 6.4401305999999998 18 6.4401305999999998 24 6.4401305999999998;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "R_foot_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "R_foot_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "R_foot_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "R_foot_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "R_foot_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 6 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 6 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 6 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_foot_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 6.4401305999999998 15 6.4401305999999998 
		18 6.4401305999999998 24 6.4401305999999998;
	setAttr -s 4 ".kit[0:3]"  10 2 2 10;
	setAttr -s 4 ".kot[0:3]"  10 2 2 10;
createNode animCurveTL -n "L_foot_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 15 0 18 0 24 0;
	setAttr -s 4 ".kit[0:3]"  10 2 2 10;
	setAttr -s 4 ".kot[0:3]"  10 2 2 10;
createNode animCurveTL -n "L_foot_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 15 0 18 0 24 0;
	setAttr -s 4 ".kit[0:3]"  10 2 2 10;
	setAttr -s 4 ".kot[0:3]"  10 2 2 10;
createNode animCurveTA -n "L_foot_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_foot_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_foot_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_low_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_low_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_low_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_low_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "C_spine_low_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_spine_low_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_spine_low_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "C_spine_low_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 5.6847593999999999 4 -15.624297000000002 
		7 0 10 0 15 2.918253 18 2.918253 24 5.6847593999999999;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.95634007453918457 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0.29225632548332214 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.97207248210906982 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0.2346808910369873 0;
createNode animCurveTA -n "C_spine_low_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 4 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "C_spine_low_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 0 7 0 10 7.8961534000000011 15 7.8961534000000011 
		18 7.8961534000000011 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 0.82343810796737671;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 -0.5674060583114624;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.82343810796737671 0.82343816757202148;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 -0.5674060583114624 -0.56740611791610718;
createNode animCurveTU -n "C_spine_mid_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_mid_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_mid_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_mid_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "C_spine_mid_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 6.8010460999999998 4 6.8010460999999998 
		7 6.8010371000000003 10 6.8010460999999998 15 6.8010460999999998 18 6.8010460999999998 
		24 6.8010460999999998;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "C_spine_mid_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 0 7 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "C_spine_mid_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 0 7 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "C_spine_mid_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.9264608999999999 4 0 7 0 10 10 15 12.96115 
		18 12.96115 24 1.9264608999999999;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.95513278245925903 1 0.7203218936920166;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0.29617798328399658 0 -0.69363993406295776;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.7203218936920166 0.7203218936920166;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 -0.69363993406295776 -0.69363993406295776;
createNode animCurveTA -n "C_spine_mid_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.0011713502000000001 4 0 7 0 10 -10.486872 
		15 -10.486872 18 -10.486872 24 -0.0011713502000000001;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 0.73774796724319458;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0.67507630586624146;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.73774796724319458 0.73774796724319458;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0.67507630586624146 0.67507630586624146;
createNode animCurveTA -n "C_spine_mid_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.034837799000000003 4 0 7 0 10 29.888347000000003 
		15 29.888347000000003 18 29.888347000000003 24 0.034837799000000003;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 0.3583533763885498;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 -0.93358594179153442;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.3583533763885498 0.35835340619087219;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 -0.93358594179153442 -0.9335860013961792;
createNode animCurveTU -n "C_spine_up_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_up_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_up_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_spine_up_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "C_spine_up_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 5.301807 4 5.301807 7 5.302 10 5.301807 
		15 5.301807 18 5.301807 24 5.301807;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  0.47656446695327759 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  -0.8791394829750061 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.47656446695327759 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  -0.8791394829750061 0 0 0 0 0 0;
createNode animCurveTL -n "C_spine_up_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 0 7 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  0.82130438089370728 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  -0.57049030065536499 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.82130438089370728 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  -0.57049030065536499 0 0 0 0 0 0;
createNode animCurveTL -n "C_spine_up_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 0 7 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "C_spine_up_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 16.17864 7 0 10 12.193323 15 26.294923 
		18 26.294923 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.56071126461029053 1 0.39950528740882874;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0.82801139354705811 0 -0.91673088073730469;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.39950528740882874 0.39950528740882874;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 -0.91673088073730469 -0.91673088073730469;
createNode animCurveTA -n "C_spine_up_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 0 7 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "C_spine_up_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 4 0 7 0 10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  0.9802398681640625 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0.19781258702278137 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.9802398681640625 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0.1978125125169754 0 0 0 0 0 0;
createNode animCurveTA -n "C_neck_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 20.992596 4 49.770181 7 0 10 0 15 -18.934689 
		18 -18.934689 24 20.992596;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.45030248165130615 1 0.27586403489112854;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.8928760290145874 0 0.9611966609954834;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.27586403489112854 0.27586403489112854;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0.9611966609954834 0.9611966609954834;
createNode animCurveTA -n "C_neck_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.1042613000000001 4 -5.1774639000000002 
		7 0 10 0 15 10.169795 18 10.169795 24 -7.1042613000000001;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.684517502784729 1 0.55279898643493652;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0.7289963960647583 0 -0.83331453800201416;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.55279898643493652 0.5527990460395813;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 -0.83331453800201416 -0.83331465721130371;
createNode animCurveTA -n "C_neck_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.7169990999999998 4 -17.287486 7 0 
		10 0 15 -17.797682 18 -17.797682 24 -2.7169990999999998;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.47279155254364014 1 0.60501080751419067;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.88117426633834839 0 0.79621726274490356;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.60501080751419067 0.60501080751419067;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0.79621726274490356 0.79621726274490356;
createNode animCurveTU -n "C_neck_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_neck_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_neck_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_neck_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "C_neck_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 4.6498 4 4.6498 15 4.6498 18 4.6498 24 
		4.6498;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_neck_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_neck_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "C_head_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 31.444572000000004 4 37.723287 7 0 10 
		3.4786176000000002 15 -26.023867 18 -26.023867 24 31.444572000000004;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.30794802308082581 1 0.19554948806762695;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.95140326023101807 0 0.98069393634796143;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.19554948806762695 0.19554945826530457;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0.98069393634796143 0.98069381713867188;
createNode animCurveTA -n "C_head_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -13.481229 4 -17.084321 7 0 10 8.940886 
		15 1.9007598999999999 18 1.9007598999999999 24 -13.481229;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.80490273237228394 1 0.5974164605140686;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.59340673685073853 0 -0.80193114280700684;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.5974164605140686 0.5974164605140686;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 -0.80193114280700684 -0.80193114280700684;
createNode animCurveTA -n "C_head_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.1254911000000001 4 9.8173332000000002 
		7 0 10 -25.8739 15 -36.531433000000007 18 -36.531433000000007 24 2.1254911000000001;
	setAttr -s 7 ".kit[1:6]"  3 3 1 1 1 10;
	setAttr -s 7 ".kot[1:6]"  3 3 1 1 1 10;
	setAttr -s 7 ".kix[0:6]"  0.89262503385543823 1 1 0.43981477618217468 
		0.8588903546333313 0.8588903546333313 0.28420808911323547;
	setAttr -s 7 ".kiy[0:6]"  -0.45079997181892395 0 0 -0.89808851480484009 
		0.51215940713882446 0.51215940713882446 0.95876264572143555;
	setAttr -s 7 ".kox[0:6]"  0.89262503385543823 1 1 0.43981477618217468 
		0.8588903546333313 0.8588903546333313 0.28420808911323547;
	setAttr -s 7 ".koy[0:6]"  -0.45079997181892395 0 0 -0.89808851480484009 
		0.51215940713882446 0.51215940713882446 0.95876264572143555;
createNode animCurveTU -n "C_head_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_head_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_head_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "C_head_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "C_head_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 2.1180013999999998 4 2.1180013999999998 
		15 2.1180013999999998 18 2.1180013999999998 24 2.1180013999999998;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_head_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "C_head_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 15 0 18 0 24 0;
	setAttr -s 5 ".kit[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kot[0:4]"  1 3 2 2 10;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "C_hair_a_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 13 0.55636306000000002 15 
		0.47525492000000003 18 0.47525492000000003 24 1;
	setAttr -s 7 ".kit[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kot[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.63497895002365112 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.7725294828414917 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.35614630579948425 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0.93443018198013306 0;
createNode animCurveTU -n "C_hair_a_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 13 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kot[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_hair_a_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 13 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kot[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_hair_a_visibility";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 13 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[1:6]"  9 3 1 2 2 1;
	setAttr -s 7 ".kot[1:6]"  5 3 1 2 2 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "C_hair_a_translateX";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.58336507000000004 5 0.98202392000000005 
		7 1.2671823 10 0.58336507000000004 13 1.3536261999999999 15 1.5649405000000001 18 
		1.5649405000000001 24 0.58336507000000004;
	setAttr -s 8 ".kit[0:7]"  1 9 9 9 9 2 2 9;
	setAttr -s 8 ".kot[0:7]"  1 9 9 9 9 2 2 9;
	setAttr -s 8 ".kix[0:7]"  1 0.28071564435958862 0.38571706414222717 
		0.91792821884155273 0.16739910840988159 0.30086803436279297 1 0.19965186715126038;
	setAttr -s 8 ".kiy[0:7]"  0 0.95979094505310059 -0.92261707782745361 
		0.39674645662307739 0.98588919639587402 0.95366579294204712 0 -0.97986686229705811;
	setAttr -s 8 ".kox[0:7]"  1 0.28071564435958862 0.38571706414222717 
		0.91792821884155273 0.16739910840988159 1 0.19965186715126038 0.19965186715126038;
	setAttr -s 8 ".koy[0:7]"  0 0.95979094505310059 -0.92261707782745361 
		0.39674645662307739 0.98588919639587402 0 -0.97986686229705811 -0.97986686229705811;
createNode animCurveTL -n "C_hair_a_translateY";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.9188019000000001 5 -3.4929798000000001 
		7 -3.1883916999999999 10 -3.9188019000000001 13 -3.7864670999999999 15 -4.483144 
		18 -4.483144 24 -3.9188019000000001;
	setAttr -s 8 ".kit[0:7]"  1 9 9 9 9 2 2 9;
	setAttr -s 8 ".kot[0:7]"  1 9 9 9 9 2 2 9;
	setAttr -s 8 ".kix[0:7]"  1 0.26409709453582764 0.36447641253471375 
		0.31714320182800293 0.28323549032211304 0.095257222652435303 1 0.33403831720352173;
	setAttr -s 8 ".kiy[0:7]"  0 0.96449607610702515 -0.93121260404586792 
		-0.94837766885757446 -0.95905035734176636 -0.99545270204544067 0 0.94255948066711426;
	setAttr -s 8 ".kox[0:7]"  1 0.26409709453582764 0.36447641253471375 
		0.31714320182800293 0.28323549032211304 1 0.33403831720352173 0.33403831720352173;
	setAttr -s 8 ".koy[0:7]"  0 0.96449607610702515 -0.93121260404586792 
		-0.94837766885757446 -0.95905035734176636 0 0.94255948066711426 0.94255948066711426;
createNode animCurveTL -n "C_hair_a_translateZ";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0.31875720000000002 7 0.54676237000000005 
		10 0 13 -0.29817305 15 -0.066124986999999996 18 -0.066124986999999996 24 0;
	setAttr -s 8 ".kit[0:7]"  1 9 9 9 9 2 2 9;
	setAttr -s 8 ".kot[0:7]"  1 9 9 9 9 2 2 9;
	setAttr -s 8 ".kix[0:7]"  1 0.34352850914001465 0.46334940195083618 
		0.23033957183361053 0.92951482534408569 0.27612701058387756 1 0.94945192337036133;
	setAttr -s 8 ".kiy[0:7]"  0 0.93914228677749634 -0.88617569208145142 
		-0.97311031818389893 -0.36878493428230286 0.96112114191055298 0 0.31391251087188721;
	setAttr -s 8 ".kox[0:7]"  1 0.34352850914001465 0.46334940195083618 
		0.23033957183361053 0.92951482534408569 1 0.94945192337036133 0.94945192337036133;
	setAttr -s 8 ".koy[0:7]"  0 0.93914228677749634 -0.88617569208145142 
		-0.97311031818389893 -0.36878493428230286 0 0.31391251087188721 0.31391251087188721;
createNode animCurveTA -n "C_hair_a_rotateX";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 5 0 7 5.8238618000000004 10 0 13 0.26226562000000003 
		15 -0.24571382 18 -0.24571382 20 0 24 0;
	setAttr -s 9 ".kit[0:8]"  1 9 9 9 9 2 2 9 
		9;
	setAttr -s 9 ".kot[0:8]"  1 9 9 9 9 2 2 9 
		9;
	setAttr -s 9 ".kix[0:8]"  1 0.89147400856018066 1 0.89964020252227783 
		0.99966907501220703 0.99127256870269775 1 0.99977022409439087 1;
	setAttr -s 9 ".kiy[0:8]"  0 0.45307189226150513 0 -0.43663209676742554 
		-0.025722576305270195 -0.13182806968688965 0 0.021437650546431541 0;
	setAttr -s 9 ".kox[0:8]"  1 0.89147400856018066 1 0.89964020252227783 
		0.99966907501220703 1 0.99793738126754761 0.99977022409439087 1;
	setAttr -s 9 ".koy[0:8]"  0 0.45307189226150513 0 -0.43663209676742554 
		-0.025722576305270195 0 0.064195044338703156 0.021437650546431541 0;
createNode animCurveTA -n "C_hair_a_rotateY";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 5 -23.560833 7 28.669443 10 0 13 0 
		15 5.2416955999999999 18 5.2416955999999999 20 8.7893001999999996 24 0;
	setAttr -s 9 ".kit[0:8]"  1 9 9 9 9 2 2 9 
		3;
	setAttr -s 9 ".kot[0:8]"  1 9 9 9 9 2 2 9 
		3;
	setAttr -s 9 ".kix[0:8]"  1 0.37114995718002319 0.37562426924705505 
		0.37114995718002319 0.87661945819854736 0.58893543481826782 1 0.90937769412994385 
		1;
	setAttr -s 9 ".kiy[0:8]"  0 0.92857295274734497 0.92677199840545654 
		-0.92857295274734497 0.48118439316749573 0.80818003416061401 0 -0.41597139835357666 
		0;
	setAttr -s 9 ".kox[0:8]"  1 0.37114995718002319 0.37562426924705505 
		0.37114995718002319 0.87661945819854736 1 0.73272430896759033 0.90937769412994385 
		1;
	setAttr -s 9 ".koy[0:8]"  0 0.92857295274734497 0.92677199840545654 
		-0.92857295274734497 0.48118439316749573 0 0.68052554130554199 -0.41597139835357666 
		0;
createNode animCurveTA -n "C_hair_a_rotateZ";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 5 3.3424676 7 12.002499 10 27.119170000000004 
		13 46.032650000000004 15 56.797685 18 56.797685 20 0 24 0;
	setAttr -s 9 ".kit[0:8]"  1 9 9 9 9 2 2 1 
		9;
	setAttr -s 9 ".kot[0:8]"  1 9 9 9 9 2 2 1 
		9;
	setAttr -s 9 ".kix[0:8]"  1 0.69054591655731201 0.37268954515457153 
		0.31912803649902344 0.30629327893257141 0.3343995213508606 1 0.60908287763595581 
		1;
	setAttr -s 9 ".kiy[0:8]"  0 0.72328853607177734 0.92795610427856445 
		0.94771158695220947 0.95193719863891602 0.94243139028549194 0 -0.7931065559387207 
		0;
	setAttr -s 9 ".kox[0:8]"  1 0.69054591655731201 0.37268954515457153 
		0.31912803649902344 0.30629327893257141 1 0.067099742591381073 0.60908287763595581 
		1;
	setAttr -s 9 ".koy[0:8]"  0 0.72328853607177734 0.92795610427856445 
		0.94771158695220947 0.95193719863891602 0 -0.99774628877639771 -0.7931065559387207 
		0;
createNode animCurveTU -n "C_hair_b_visibility";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 13 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[1:6]"  9 3 1 2 2 1;
	setAttr -s 7 ".kot[1:6]"  5 3 1 2 2 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "C_hair_b_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.8757084000000002 5 3.8757084000000002 
		7 3.8757084000000002 10 3.8757084000000002 13 3.8757084000000002 15 3.8757084000000002 
		18 3.8757084000000002 24 3.8757084000000002;
	setAttr -s 8 ".kit[5:7]"  2 2 1;
	setAttr -s 8 ".kot[5:7]"  2 2 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "C_hair_b_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 0 10 0 13 0 15 0 18 0 24 0;
	setAttr -s 8 ".kit[5:7]"  2 2 1;
	setAttr -s 8 ".kot[5:7]"  2 2 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "C_hair_b_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 0 10 0 13 0 15 0 18 0 24 0;
	setAttr -s 8 ".kit[5:7]"  2 2 1;
	setAttr -s 8 ".kot[5:7]"  2 2 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "C_hair_b_rotateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 5 4.2259146000000003 7 0 10 0 13 0 
		15 0 18 0 20 -5.7381384999999998 24 0;
	setAttr -s 9 ".kit[0:8]"  1 10 10 3 1 2 2 10 
		1;
	setAttr -s 9 ".kot[0:8]"  1 10 10 3 1 2 2 10 
		1;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 0.55412685871124268 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 -0.83243221044540405 0 0;
createNode animCurveTA -n "C_hair_b_rotateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 5 -20.372661 7 0 10 0 13 0 15 0 18 
		0 20 15.613133999999999 24 0;
	setAttr -s 9 ".kit[0:8]"  1 10 10 3 1 2 2 10 
		1;
	setAttr -s 9 ".kot[0:8]"  1 10 10 3 1 2 2 10 
		1;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 0.23763942718505859 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0.97135341167449951 0 0;
createNode animCurveTA -n "C_hair_b_rotateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 5 -0.0058749208999999995 7 -6.5205292999999998 
		10 -15.849742 13 14.068309 15 -0.14381125 18 -0.14381125 20 4.5654532000000003 24 
		0;
	setAttr -s 9 ".kit[0:8]"  1 10 10 3 1 2 2 10 
		1;
	setAttr -s 9 ".kot[0:8]"  1 10 10 3 1 2 2 10 
		1;
	setAttr -s 9 ".kix[0:8]"  1 1 0.51620292663574219 1 1 0.25955390930175781 
		1 0.99992132186889648 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 -0.85646629333496094 0 0 -0.96572864055633545 
		0 0.012548912316560745 0;
	setAttr -s 9 ".kox[0:8]"  1 1 0.51620292663574219 1 1 1 0.62994086742401123 
		0.99992132186889648 1;
	setAttr -s 9 ".koy[0:8]"  0 0 -0.85646629333496094 0 0 0 0.77664309740066528 
		0.012548912316560745 0;
createNode animCurveTU -n "C_hair_b_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 13 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kot[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_hair_b_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 13 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kot[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "C_hair_b_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 13 1 15 1 18 1 24 1;
	setAttr -s 7 ".kit[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kot[1:6]"  10 3 1 2 2 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "R_hair_a_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_hair_a_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_hair_a_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_hair_a_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "R_hair_a_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.5647707 5 3.9634295000000002 7 4.2485879000000004 
		10 3.5647707 15 3.5647707 18 3.5647707 24 3.5647707;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.28071564435958862 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.95979094505310059 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.28071564435958862 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.95979094505310059 0 0 0 0 0;
createNode animCurveTL -n "R_hair_a_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.25192123999999999 5 0.67774336000000002 
		7 0.98233143000000001 10 0.25192123999999999 15 0.25192123999999999 18 0.25192123999999999 
		24 0.25192123999999999;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.26409709453582764 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.96449607610702515 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.26409709453582764 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.96449607610702515 0 0 0 0 0;
createNode animCurveTL -n "R_hair_a_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.8671153 5 -2.5483581000000002 7 -2.3203529000000001 
		10 -2.8671153 15 -2.8671153 18 -2.8671153 24 -2.8671153;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.34352850914001465 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.93914228677749634 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.34352850914001465 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.93914228677749634 0 0 0 0 0;
createNode animCurveTA -n "R_hair_a_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "R_hair_a_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "R_hair_a_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_hair_b_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "R_hair_b_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.162 5 3.042211 7 2.9565267 10 3.162 
		15 3.162 18 3.162 24 3.162;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.69749832153320313 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.7165864109992981 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.69749832153320313 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 -0.7165864109992981 0 0 0 0 0;
createNode animCurveTL -n "R_hair_b_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 -0.33235888000000002 7 -0.57009325 
		10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.33103948831558228 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.94361686706542969 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.33103948831558228 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 -0.94361686706542969 0 0 0 0 0;
createNode animCurveTL -n "R_hair_b_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 -0.56306988000000002 7 -0.96583048000000005 
		10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.20277382433414459 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.97922563552856445 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.20277382433414459 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 -0.97922563552856445 0 0 0 0 0;
createNode animCurveTA -n "R_hair_b_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "R_hair_b_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "R_hair_b_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_hair_b_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_hair_b_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_hair_b_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_hair_a_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_hair_a_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_hair_a_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_hair_a_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_hair_a_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.5647707 5 3.9634295000000002 7 4.2485879000000004 
		10 3.5647707 15 3.5647707 18 3.5647707 24 3.5647707;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.28071564435958862 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.95979094505310059 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.28071564435958862 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.95979094505310059 0 0 0 0 0;
createNode animCurveTL -n "L_hair_a_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.25192123999999999 5 0.67774336000000002 
		7 0.98233143000000001 10 0.25192123999999999 15 0.25192123999999999 18 0.25192123999999999 
		24 0.25192123999999999;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.26409709453582764 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.96449607610702515 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.26409709453582764 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.96449607610702515 0 0 0 0 0;
createNode animCurveTL -n "L_hair_a_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.867 5 3.1857571999999998 7 3.4137624 
		10 2.867 15 2.867 18 2.867 24 2.867;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.34352850914001465 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.93914228677749634 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.34352850914001465 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.93914228677749634 0 0 0 0 0;
createNode animCurveTA -n "L_hair_a_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_hair_a_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_hair_a_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_hair_b_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_hair_b_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.162 5 2.978639 7 2.847482 10 3.162 
		15 3.162 18 3.162 24 3.162;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.53659319877624512 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.8438410758972168 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.53659319877624512 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 -0.8438410758972168 0 0 0 0 0;
createNode animCurveTL -n "L_hair_b_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0.30197776999999998 7 0.51798071000000001 
		10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.36019724607467651 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.93287616968154907 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.36019724607467651 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.93287616968154907 0 0 0 0 0;
createNode animCurveTL -n "L_hair_b_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0.56306988000000002 7 0.96583048000000005 
		10 0 15 0 18 0 24 0;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.20277382433414459 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.97922563552856445 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.20277382433414459 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.97922563552856445 0 0 0 0 0;
createNode animCurveTA -n "L_hair_b_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_hair_b_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_hair_b_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_hair_b_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_hair_b_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_hair_b_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_shou_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1.0989986 10 1.3 15 1.3 18 1.3 24 
		1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 0.70710676908493042 1 1 1 0.55470019578933716;
	setAttr -s 6 ".kiy[0:5]"  0 0.70710676908493042 0 0 0 -0.8320503830909729;
	setAttr -s 6 ".kox[0:5]"  1 0.70710676908493042 1 1 0.55470019578933716 
		0.55470019578933716;
	setAttr -s 6 ".koy[0:5]"  0 0.70710676908493042 0 0 -0.8320503830909729 
		-0.8320503830909729;
createNode animCurveTU -n "R_shou_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_shou_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_shou_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "R_shou_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.0574460999999999 5 2.4119552999999998 
		7 2.6655337000000001 10 2.0574460999999999 15 2.0574460999999999 18 2.0574460999999999 
		24 2.0574460999999999;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.31243497133255005 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.94993919134140015 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.31243497133255005 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.94993919134140015 0 0 0 0 0;
createNode animCurveTL -n "R_shou_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.78579944999999995 5 -0.33573566999999999 
		7 -0.013807669 10 -0.78579944999999995 15 -0.78579944999999995 18 -0.78579944999999995 
		24 -0.78579944999999995;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.25079059600830078 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.96804136037826538 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.25079059600830078 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.96804136037826538 0 0 0 0 0;
createNode animCurveTL -n "R_shou_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3 5 -2.6629165000000001 7 -2.4218025999999999 
		10 -3 15 -3 18 -3 24 -3;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.32689854502677917 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.94505947828292847 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.32689854502677917 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.94505947828292847 0 0 0 0 0;
createNode animCurveTA -n "R_shou_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "R_shou_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 50.596807 15 50.596807 18 50.596807 
		24 0;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.22088569402694702;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.97529977560043335;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 0.22088569402694702 0.22088569402694702;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 -0.97529977560043335 -0.97529977560043335;
createNode animCurveTA -n "R_shou_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -0.56588205000000003 5 0 10 -0.56588205000000003 
		15 -0.56588205000000003 18 -0.56588205000000003 24 -0.56588205000000003;
	setAttr -s 6 ".kit[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 3 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  0.92360007762908936 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  -0.38335734605789185 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.92360007762908936 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.38335734605789185 0 0 0 0 0;
createNode animCurveTA -n "R_little_up_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -85.317764 5 -85.317764 7 -36.05721 10 
		17.858095 15 17.858095 18 17.858095 21 -85.317764 24 -85.317764;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.083010837435722351 1;
	setAttr -s 8 ".kiy[6:7]"  -0.99654865264892578 0;
	setAttr -s 8 ".kox[6:7]"  0.083010837435722351 1;
	setAttr -s 8 ".koy[6:7]"  -0.99654865264892578 0;
createNode animCurveTA -n "R_little_up_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 0.31514469000000001 10 0 15 0 
		18 0 21 0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTA -n "R_little_up_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 13.828577 10 0 15 0 18 0 21 0 
		24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTU -n "R_little_up_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_up_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_up_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_up_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_little_up_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.116683 5 4.116683 10 4.116683 15 4.116683 
		18 4.116683 21 4.116683 24 4.116683;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_little_up_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.15835379999999999 5 -0.15835379999999999 
		10 -0.15835379999999999 15 -0.15835379999999999 18 -0.15835379999999999 21 -0.15835379999999999 
		24 -0.15835379999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_little_up_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.1186517 5 2.1186517 10 2.1186517 15 
		2.1186517 18 2.1186517 21 2.1186517 24 2.1186517;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_little_mid_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_little_mid_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -2.9081804 5 -2.9081804 7 0 10 0 15 0 
		18 0 21 -2.9081804 24 -2.9081804;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.9472387433052063 1;
	setAttr -s 8 ".kiy[6:7]"  -0.32052862644195557 0;
	setAttr -s 8 ".kox[6:7]"  0.9472387433052063 1;
	setAttr -s 8 ".koy[6:7]"  -0.32052871584892273 0;
createNode animCurveTA -n "R_little_mid_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -37.217672 5 -37.217672 7 -45.099847 
		10 18.006491 15 18.006491 18 18.006491 21 -37.217672 24 -37.217672;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.15377582609653473 1;
	setAttr -s 8 ".kiy[6:7]"  -0.98810577392578125 0;
	setAttr -s 8 ".kox[6:7]"  0.15377582609653473 1;
	setAttr -s 8 ".koy[6:7]"  -0.98810577392578125 0;
createNode animCurveTU -n "R_little_mid_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_mid_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_mid_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_mid_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_little_mid_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.0954408 5 1.0954408 10 1.0954408 15 
		1.0954408 18 1.0954408 21 1.0954408 24 1.0954408;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_little_mid_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_little_mid_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_little_low_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.8718779000000001 5 1.8718779000000001 
		7 -36.280444 10 5.0543518000000009 15 5.0543518000000009 18 5.0543518000000009 21 
		1.8718779000000001 24 1.8718779000000001;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.93777090311050415 1;
	setAttr -s 8 ".kiy[6:7]"  -0.34725436568260193 0;
	setAttr -s 8 ".kox[6:7]"  0.93777090311050415 1;
	setAttr -s 8 ".koy[6:7]"  -0.34725436568260193 0;
createNode animCurveTA -n "R_little_low_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_little_low_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_low_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_little_low_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.97124661999999995 5 0.97124661999999995 
		10 0.97124661999999995 15 0.97124661999999995 18 0.97124661999999995 21 0.97124661999999995 
		24 0.97124661999999995;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_little_low_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_little_low_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_low_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_low_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_little_low_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_ring_up_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.14599793999999999 5 0.14599793999999999 
		7 0 10 0 15 0 18 0 21 0.14599793999999999 24 0.14599793999999999;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTA -n "R_ring_up_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -1.7239914000000001 5 -1.7239914000000001 
		7 0 10 0 15 0 18 0 21 -1.7239914000000001 24 -1.7239914000000001;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTA -n "R_ring_up_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -65.049578 5 -65.049578 7 -57.132639 
		10 12.666955000000002 15 12.666955000000002 18 12.666955000000002 21 -65.049578 24 
		-65.049578;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.10991602391004562 1;
	setAttr -s 8 ".kiy[6:7]"  -0.99394088983535767 0;
	setAttr -s 8 ".kox[6:7]"  0.10991602391004562 1;
	setAttr -s 8 ".koy[6:7]"  -0.99394088983535767 0;
createNode animCurveTU -n "R_ring_up_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_up_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_up_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_up_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_up_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.5268911999999997 5 4.5268911999999997 
		10 4.5268911999999997 15 4.5268911999999997 18 4.5268911999999997 21 4.5268911999999997 
		24 4.5268911999999997;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_up_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.23100726999999999 5 -0.23100726999999999 
		10 -0.23100726999999999 15 -0.23100726999999999 18 -0.23100726999999999 21 -0.23100726999999999 
		24 -0.23100726999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_up_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.2088558 5 1.2088558 10 1.2088558 15 
		1.2088558 18 1.2088558 21 1.2088558 24 1.2088558;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_ring_mid_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.2376591000000001 5 -3.2376591000000001 
		7 0 10 0 15 0 18 0 21 -3.2376591000000001 24 -3.2376591000000001;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.93579930067062378 1;
	setAttr -s 8 ".kiy[6:7]"  -0.35253310203552246 0;
	setAttr -s 8 ".kox[6:7]"  0.93579930067062378 1;
	setAttr -s 8 ".koy[6:7]"  -0.35253310203552246 0;
createNode animCurveTA -n "R_ring_mid_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.13528309999999999 5 0.13528309999999999 
		7 0 10 0 15 0 18 0 21 0.13528309999999999 24 0.13528309999999999;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTA -n "R_ring_mid_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -74.667836 5 -74.667836 7 -73.083427 
		10 -7.7552621000000013 15 -7.7552621000000013 18 -7.7552621000000013 21 -74.667836 
		24 -74.667836;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.1273951530456543 1;
	setAttr -s 8 ".kiy[6:7]"  -0.9918520450592041 0;
	setAttr -s 8 ".kox[6:7]"  0.1273951530456543 1;
	setAttr -s 8 ".koy[6:7]"  -0.9918520450592041 0;
createNode animCurveTU -n "R_ring_mid_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_mid_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_mid_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_mid_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_mid_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.7749664000000001 5 1.7749664000000001 
		10 1.7749664000000001 15 1.7749664000000001 18 1.7749664000000001 21 1.7749664000000001 
		24 1.7749664000000001;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_mid_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_mid_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_ring_low_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_ring_low_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 13.238946 5 13.238946 7 -57.132639 10 
		-79.116731 15 -79.116731 18 -79.116731 21 13.238946 24 13.238946;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kix[4:7]"  1 1 0.092657037079334259 1;
	setAttr -s 8 ".kiy[4:7]"  0 0 0.99569809436798096 0;
	setAttr -s 8 ".kox[4:7]"  1 1 0.092657037079334259 1;
	setAttr -s 8 ".koy[4:7]"  0 0 0.99569809436798096 0;
createNode animCurveTA -n "R_ring_low_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_low_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_low_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.3103469000000001 5 1.3103469000000001 
		10 1.3103469000000001 15 1.3103469000000001 18 1.3103469000000001 21 1.3103469000000001 
		24 1.3103469000000001;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_low_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_ring_low_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_low_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_low_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_ring_low_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_mid_up_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.9994383000000004 5 3.9994383000000004 
		7 0 10 0 15 0 18 0 21 3.9994383000000004 24 3.9994383000000004;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.90663802623748779 1;
	setAttr -s 8 ".kiy[6:7]"  0.42190924286842346 0;
	setAttr -s 8 ".kox[6:7]"  0.90663802623748779 1;
	setAttr -s 8 ".koy[6:7]"  0.42190924286842346 0;
createNode animCurveTA -n "R_mid_up_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -6.6831588000000002 5 -6.6831588000000002 
		7 0 10 0 15 0 18 0 21 -6.6831588000000002 24 -6.6831588000000002;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.78941220045089722 1;
	setAttr -s 8 ".kiy[6:7]"  -0.61386340856552124 0;
	setAttr -s 8 ".kox[6:7]"  0.78941220045089722 1;
	setAttr -s 8 ".koy[6:7]"  -0.61386340856552124 0;
createNode animCurveTA -n "R_mid_up_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -24.2685 5 -24.2685 7 -57.132639 10 -25.154807000000005 
		15 -25.154807000000005 18 -25.154807000000005 21 -24.2685 24 -24.2685;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTU -n "R_mid_up_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_up_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_up_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_up_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_up_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.7127924999999999 5 4.7127924999999999 
		10 4.7127924999999999 15 4.7127924999999999 18 4.7127924999999999 21 4.7127924999999999 
		24 4.7127924999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_up_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.16572756 5 -0.16572756 10 -0.16572756 
		15 -0.16572756 18 -0.16572756 21 -0.16572756 24 -0.16572756;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_up_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.12032635999999999 5 0.12032635999999999 
		10 0.12032635999999999 15 0.12032635999999999 18 0.12032635999999999 21 0.12032635999999999 
		24 0.12032635999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_mid_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_mid_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_mid_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_mid_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_mid_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.7992489 5 1.7992489 10 1.7992489 15 
		1.7992489 18 1.7992489 21 1.7992489 24 1.7992489;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_mid_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_mid_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_mid_mid_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_mid_mid_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_mid_mid_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -72.604679 5 -72.604679 7 -57.132639 
		10 -87.453636 15 -87.453636 18 -87.453636 21 -72.604679 24 -72.604679;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kix[4:7]"  1 1 0.5009315013885498 1;
	setAttr -s 8 ".kiy[4:7]"  0 0 0.86548691987991333 0;
	setAttr -s 8 ".kox[4:7]"  1 1 0.5009315013885498 1;
	setAttr -s 8 ".koy[4:7]"  0 0 0.86548691987991333 0;
createNode animCurveTA -n "R_mid_low_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -51.477835 5 -51.477835 7 -57.132639 
		10 -27.642567 15 -27.642567 18 -27.642567 21 -51.477835 24 -51.477835;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.33919718861579895 1;
	setAttr -s 8 ".kiy[6:7]"  -0.94071531295776367 0;
	setAttr -s 8 ".kox[6:7]"  0.33919718861579895 1;
	setAttr -s 8 ".koy[6:7]"  -0.94071531295776367 0;
createNode animCurveTA -n "R_mid_low_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_mid_low_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_low_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_low_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.2793348 5 1.2793348 10 1.2793348 15 
		1.2793348 18 1.2793348 21 1.2793348 24 1.2793348;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_low_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_mid_low_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_low_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_low_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_mid_low_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index_up_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index_up_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index_up_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index_up_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_index_up_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.4562872000000002 5 4.4562872000000002 
		10 4.4562872000000002 15 4.4562872000000002 18 4.4562872000000002 21 4.4562872000000002 
		24 4.4562872000000002;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_index_up_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.39650271999999998 5 -0.39650271999999998 
		10 -0.39650271999999998 15 -0.39650271999999998 18 -0.39650271999999998 21 -0.39650271999999998 
		24 -0.39650271999999998;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_index_up_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.90234608999999999 5 -0.90234608999999999 
		10 -0.90234608999999999 15 -0.90234608999999999 18 -0.90234608999999999 21 -0.90234608999999999 
		24 -0.90234608999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_index_up_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.713579 5 -3.713579 7 0 10 0 15 0 18 
		0 21 -3.713579 24 -3.713579;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.91797000169754028 1;
	setAttr -s 8 ".kiy[6:7]"  -0.39664977788925171 0;
	setAttr -s 8 ".kox[6:7]"  0.91797000169754028 1;
	setAttr -s 8 ".koy[6:7]"  -0.39664986729621887 0;
createNode animCurveTA -n "R_index_up_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -13.453781 5 -13.453781 7 0 10 0 15 0 
		18 0 21 -13.453781 24 -13.453781;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.53834015130996704 1;
	setAttr -s 8 ".kiy[6:7]"  -0.8427276611328125 0;
	setAttr -s 8 ".kox[6:7]"  0.53834015130996704 1;
	setAttr -s 8 ".koy[6:7]"  -0.8427276611328125 0;
createNode animCurveTA -n "R_index_up_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 11.907224 5 11.907224 7 -62.492203 10 
		-72.154333 15 -72.154333 18 -72.154333 21 11.907224 24 11.907224;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kix[4:7]"  1 1 0.10170882940292358 1;
	setAttr -s 8 ".kiy[4:7]"  0 0 0.99481421709060669 0;
	setAttr -s 8 ".kox[4:7]"  1 1 0.10170882940292358 1;
	setAttr -s 8 ".koy[4:7]"  0 0 0.99481421709060669 0;
createNode animCurveTA -n "R_index_mid_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -40.166467 5 -40.166467 7 -84.856228 
		10 -64.03229 15 -64.03229 18 -64.03229 21 -40.166467 24 -40.166467;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kix[4:7]"  1 1 0.33881276845932007 1;
	setAttr -s 8 ".kiy[4:7]"  0 0 0.94085383415222168 0;
	setAttr -s 8 ".kox[4:7]"  1 1 0.33881276845932007 1;
	setAttr -s 8 ".koy[4:7]"  0 0 0.94085383415222168 0;
createNode animCurveTA -n "R_index_mid_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_index_mid_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index_mid_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index_mid_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index_mid_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index_mid_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_index_mid_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.0469344999999999 5 2.0469344999999999 
		10 2.0469344999999999 15 2.0469344999999999 18 2.0469344999999999 21 2.0469344999999999 
		24 2.0469344999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_index_mid_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_index_mid_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_index3_low_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -80.674101 5 -80.674101 7 -6.2718125999999996 
		10 -27.482209 15 -27.482209 18 -27.482209 21 -80.674101 24 -80.674101;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.15950421988964081 1;
	setAttr -s 8 ".kiy[6:7]"  -0.98719727993011475 0;
	setAttr -s 8 ".kox[6:7]"  0.15950421988964081 1;
	setAttr -s 8 ".koy[6:7]"  -0.98719727993011475 0;
createNode animCurveTA -n "R_index3_low_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 0 10 -2.0501936999999999 15 -2.0501936999999999 
		18 -2.0501936999999999 21 0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTA -n "R_index3_low_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 0 10 6.82155 15 6.82155 18 6.82155 
		21 0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.78326123952865601 1;
	setAttr -s 8 ".kiy[6:7]"  -0.6216927170753479 0;
	setAttr -s 8 ".kox[6:7]"  0.78326123952865601 1;
	setAttr -s 8 ".koy[6:7]"  -0.6216927170753479 0;
createNode animCurveTU -n "R_index3_low_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_index3_low_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.039 5 1.039 10 1.039 15 1.039 18 1.039 
		21 1.039 24 1.039;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_index3_low_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_index3_low_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index3_low_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index3_low_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_index3_low_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_thumb_up_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_thumb_up_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_thumb_up_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_thumb_up_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_thumb_up_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.0133296999999999 5 2.0133296999999999 
		10 2.0133296999999999 15 2.0133296999999999 18 2.0133296999999999 21 2.0133296999999999 
		24 2.0133296999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_thumb_up_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.0756935999999999 5 -1.0756935999999999 
		10 -1.0756935999999999 15 -1.0756935999999999 18 -1.0756935999999999 21 -1.0756935999999999 
		24 -1.0756935999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_thumb_up_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.7597509 5 -1.7597509 10 -1.7597509 
		15 -1.7597509 18 -1.7597509 21 -1.7597509 24 -1.7597509;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_thumb_up_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 -64.130352 10 9.8419238999999994 
		15 9.8419238999999994 18 9.8419238999999994 21 0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.65775370597839355 1;
	setAttr -s 8 ".kiy[6:7]"  -0.75323301553726196 0;
	setAttr -s 8 ".kox[6:7]"  0.65775370597839355 1;
	setAttr -s 8 ".koy[6:7]"  -0.75323301553726196 0;
createNode animCurveTA -n "R_thumb_up_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 18.895779000000005 10 5.63233 
		15 5.63233 18 5.63233 21 0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  0.83639252185821533 1;
	setAttr -s 8 ".kiy[6:7]"  -0.54813098907470703 0;
	setAttr -s 8 ".kox[6:7]"  0.83639252185821533 1;
	setAttr -s 8 ".koy[6:7]"  -0.54813098907470703 0;
createNode animCurveTA -n "R_thumb_up_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 -33.736513 10 0 15 0 18 0 21 
		0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTU -n "R_thumb_mid_visibility";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 9;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 5 5;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
createNode animCurveTL -n "R_thumb_mid_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.8470298999999999 5 1.8470298999999999 
		10 1.8470298999999999 15 1.8470298999999999 18 1.8470298999999999 21 1.8470298999999999 
		24 1.8470298999999999;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_thumb_mid_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTL -n "R_thumb_mid_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 0 10 0 15 0 18 0 21 0 24 0;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "R_thumb_mid_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 4.1210376000000002 10 0 15 0 
		18 0 21 0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTA -n "R_thumb_mid_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 -61.301684000000009 10 -52.921592000000004 
		15 -52.921592000000004 18 -52.921592000000004 21 0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 1 1 1 10;
	setAttr -s 8 ".kix[4:7]"  1 1 0.16029804944992065 1;
	setAttr -s 8 ".kiy[4:7]"  0 0 0.98706865310668945 0;
	setAttr -s 8 ".kox[4:7]"  1 1 0.16029804944992065 1;
	setAttr -s 8 ".koy[4:7]"  0 0 0.98706865310668945 0;
createNode animCurveTA -n "R_thumb_mid_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 5 0 7 -9.2534676000000005 10 0 15 0 
		18 0 21 0 24 0;
	setAttr -s 8 ".kit[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kot[0:7]"  10 3 3 3 2 2 1 10;
	setAttr -s 8 ".kix[6:7]"  1 1;
	setAttr -s 8 ".kiy[6:7]"  0 0;
	setAttr -s 8 ".kox[6:7]"  1 1;
	setAttr -s 8 ".koy[6:7]"  0 0;
createNode animCurveTU -n "R_thumb_mid_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_thumb_mid_scaleY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTU -n "R_thumb_mid_scaleZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 10 1 15 1 18 1 21 1 24 1;
	setAttr -s 7 ".kit[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kot[0:6]"  10 3 3 2 2 1 10;
	setAttr -s 7 ".kix[5:6]"  1 1;
	setAttr -s 7 ".kiy[5:6]"  0 0;
	setAttr -s 7 ".kox[5:6]"  1 1;
	setAttr -s 7 ".koy[5:6]"  0 0;
createNode animCurveTA -n "weapons_jnt_R_rotateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 173.24526 24 173.24526;
createNode animCurveTA -n "weapons_jnt_R_rotateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 56.603876 24 56.603876;
createNode animCurveTA -n "weapons_jnt_R_rotateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -155.76083 24 -155.76083;
createNode animCurveTL -n "weapons_jnt_R_translateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.956314 24 2.956314;
createNode animCurveTL -n "weapons_jnt_R_translateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.4716944000000001 24 -1.4716944000000001;
createNode animCurveTL -n "weapons_jnt_R_translateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.27514306999999999 24 0.27514306999999999;
createNode animCurveTU -n "L_shou_scaleX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 5 1 7 1 10 1.1260247000000001 15 1.1260247000000001 
		18 1.1260247000000001 24 1;
	setAttr -s 7 ".kit[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 3 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 0.84604442119598389;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 -0.53311246633529663;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 0.84604442119598389 0.84604442119598389;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 -0.53311246633529663 -0.53311246633529663;
createNode animCurveTU -n "L_shou_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_shou_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_shou_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_shou_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.0574460999999999 5 2.4119552999999998 
		7 2.6655337000000001 10 2.0574460999999999 15 2.0574460999999999 18 2.0574460999999999 
		24 2.0574460999999999;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.31243497133255005 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.94993919134140015 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.31243497133255005 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.94993919134140015 0 0 0 0 0;
createNode animCurveTL -n "L_shou_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.78579944999999995 5 -0.33573566999999999 
		7 -0.013807669 10 -0.78579944999999995 15 -0.78579944999999995 18 -0.78579944999999995 
		24 -0.78579944999999995;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.25079059600830078 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.96804136037826538 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.25079059600830078 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.96804136037826538 0 0 0 0 0;
createNode animCurveTL -n "L_shou_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3 5 3.3370834999999999 7 3.5781974000000001 
		10 3 15 3 18 3 24 3;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.32689854502677917 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.94505947828292847 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.32689854502677917 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.94505947828292847 0 0 0 0 0;
createNode animCurveTA -n "L_shou_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 -1.5592798000000001 10 0 15 0 18 
		0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  0.85596925020217896 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  -0.51702672243118286 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.85596925020217896 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.51702672243118286 0 0 0 0 0;
createNode animCurveTA -n "L_shou_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 5 1.3278437999999999 7 0 10 25.48032 
		15 25.48032 18 25.48032 24 0;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  0.71202588081359863 1 1 1 1 1 0.41015666723251343;
	setAttr -s 7 ".kiy[0:6]"  0.70215320587158203 0 0 0 0 0 -0.91201508045196533;
	setAttr -s 7 ".kox[0:6]"  0.71202588081359863 1 1 1 1 0.41015666723251343 
		0.41015666723251343;
	setAttr -s 7 ".koy[0:6]"  0.70215320587158203 0 0 0 0 -0.91201508045196533 
		-0.91201508045196533;
createNode animCurveTA -n "L_shou_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 10.715597 5 4.468507 7 0 10 19.781011 
		15 19.781011 18 19.781011 24 10.715597;
	setAttr -s 7 ".kit[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kot[0:6]"  1 10 3 3 2 2 10;
	setAttr -s 7 ".kix[0:6]"  1 0.73040676116943359 1 1 1 1 0.78425949811935425;
	setAttr -s 7 ".kiy[0:6]"  0 -0.68301230669021606 0 0 0 0 -0.62043291330337524;
	setAttr -s 7 ".kox[0:6]"  1 0.73040676116943359 1 1 1 0.78425949811935425 
		0.78425955772399902;
	setAttr -s 7 ".koy[0:6]"  0 -0.68301230669021606 0 0 0 -0.62043291330337524 
		-0.62043297290802002;
createNode animCurveTU -n "L_little_up_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_little_up_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_little_up_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_little_up_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_up_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 4.116683 5 4.116683 10 4.116683 15 4.116683 
		18 4.116683 24 4.116683;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_up_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -0.15835379999999999 5 -0.15835379999999999 
		10 -0.15835379999999999 15 -0.15835379999999999 18 -0.15835379999999999 24 -0.15835379999999999;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_up_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -2.119 5 -2.119 10 -2.119 15 -2.119 18 
		-2.119 24 -2.119;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_little_up_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0.41677086000000002 10 1.2629594 
		15 1.2629594 18 1.2629594 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 0.99398118257522583 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 -0.10955064743757248 0;
createNode animCurveTA -n "L_little_up_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -4.7681481000000003 5 -4.7120715000000004 
		10 -4.5982165999999998 15 -4.5982165999999998 18 -4.5982165999999998 24 -4.7681481000000003;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 0.9998900294303894 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 -0.014827691949903965 0;
createNode animCurveTA -n "L_little_up_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.865503 10 -80.16775 
		15 -80.16775 18 -80.16775 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.74530619382858276 1 1 1 0.59755653142929077;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66672229766845703 0 0 0 0.8018268346786499;
	setAttr -s 6 ".kox[0:5]"  1 0.74530619382858276 1 1 1 0.59755653142929077;
	setAttr -s 6 ".koy[0:5]"  0 -0.66672229766845703 0 0 0 0.8018268346786499;
createNode animCurveTU -n "L_little_mid_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_little_mid_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_little_mid_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_little_mid_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_mid_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.095 5 1.095 10 1.095 15 1.095 18 1.095 
		24 1.095;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_mid_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_mid_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_little_mid_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_little_mid_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_little_mid_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.84877 10 -80.117042 
		15 -80.117042 18 -80.117042 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_little_low_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_low_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0.971 5 0.971 10 0.971 15 0.971 18 0.971 
		24 0.971;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_low_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_little_low_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_little_low_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_little_low_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_little_low_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.84877 10 -80.117042 
		15 -80.117042 18 -80.117042 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_little_low_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_little_low_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_little_low_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_up_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_up_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_up_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_up_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_up_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 4.5268911999999997 5 4.5268911999999997 
		10 4.5268911999999997 15 4.5268911999999997 18 4.5268911999999997 24 4.5268911999999997;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_up_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -0.23100726999999999 5 -0.23100726999999999 
		10 -0.23100726999999999 15 -0.23100726999999999 18 -0.23100726999999999 24 -0.23100726999999999;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_up_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -1.209 5 -1.209 10 -1.209 15 -1.209 18 
		-1.209 24 -1.209;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_up_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_up_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_up_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -72.61236 5 -77.669763 10 -87.938035 
		15 -87.938035 18 -87.938035 24 -72.61236;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_ring_mid_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_mid_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_mid_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_mid_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_mid_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.7749664000000001 5 1.7749664000000001 
		10 1.7749664000000001 15 1.7749664000000001 18 1.7749664000000001 24 1.7749664000000001;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_mid_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_mid_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_mid_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_mid_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_mid_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.84877 10 -80.117042 
		15 -80.117042 18 -80.117042 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_ring_low_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_low_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.3103469000000001 5 1.3103469000000001 
		10 1.3103469000000001 15 1.3103469000000001 18 1.3103469000000001 24 1.3103469000000001;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_low_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ring_low_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_low_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_low_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_ring_low_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.84877 10 -80.117042 
		15 -80.117042 18 -80.117042 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_ring_low_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_low_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_ring_low_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_up_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_up_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_up_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_up_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_up_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 4.7127924999999999 5 4.7127924999999999 
		10 4.7127924999999999 15 4.7127924999999999 18 4.7127924999999999 24 4.7127924999999999;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_up_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -0.16572756 5 -0.16572756 10 -0.16572756 
		15 -0.16572756 18 -0.16572756 24 -0.16572756;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_up_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -0.12 5 -0.12 10 -0.12 15 -0.12 18 -0.12 
		24 -0.12;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_up_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_up_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_up_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.84877 10 -80.117042 
		15 -80.117042 18 -80.117042 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_mid_mid_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_mid_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_mid_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_mid_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_mid_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.7992489 5 1.7992489 10 1.7992489 15 
		1.7992489 18 1.7992489 24 1.7992489;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_mid_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_mid_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_mid_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_mid_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_mid_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.84877 10 -80.117042 
		15 -80.117042 18 -80.117042 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_mid_low_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_low_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.279 5 1.279 10 1.279 15 1.279 18 1.279 
		24 1.279;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_low_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_mid_low_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_low_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_low_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_mid_low_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.84877 10 -80.117042 
		15 -80.117042 18 -80.117042 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_mid_low_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_low_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_mid_low_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_up_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_up_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_up_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_up_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_up_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 4.4562872000000002 5 4.4562872000000002 
		10 4.4562872000000002 15 4.4562872000000002 18 4.4562872000000002 24 4.4562872000000002;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_up_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -0.39650271999999998 5 -0.39650271999999998 
		10 -0.39650271999999998 15 -0.39650271999999998 18 -0.39650271999999998 24 -0.39650271999999998;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_up_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0.902 5 0.902 10 0.902 15 0.902 18 0.902 
		24 0.902;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_up_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_up_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_up_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -73.887428 5 -78.944831 10 -89.213103 
		15 -89.213103 18 -89.213103 24 -73.887428;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_index_mid_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_mid_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_mid_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_mid_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_mid_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 2.0469344999999999 5 2.0469344999999999 
		10 2.0469344999999999 15 2.0469344999999999 18 2.0469344999999999 24 2.0469344999999999;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_mid_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_mid_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_mid_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_mid_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_mid_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -68.424088 5 -73.481491 10 -83.749763 
		15 -83.749763 18 -83.749763 24 -68.424088;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_index_low_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_low_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.0388877999999999 5 1.0388877999999999 
		10 1.0388877999999999 15 1.0388877999999999 18 1.0388877999999999 24 1.0388877999999999;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_low_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_index_low_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_low_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_low_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_index_low_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -64.791367000000008 5 -69.84877 10 -80.117042 
		15 -80.117042 18 -80.117042 24 -64.791367000000008;
	setAttr -s 6 ".kit[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kot[1:5]"  10 3 1 1 10;
	setAttr -s 6 ".kix[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".kiy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
	setAttr -s 6 ".kox[0:5]"  1 0.7463994026184082 1 1 1 0.59882539510726929;
	setAttr -s 6 ".koy[0:5]"  0 -0.66549831628799438 0 0 0 0.80087965726852417;
createNode animCurveTU -n "L_index_low_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_low_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_index_low_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_thumb_up_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_thumb_up_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_thumb_up_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_thumb_up_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_thumb_up_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 2.0133296999999999 5 2.0133296999999999 
		10 2.0133296999999999 15 2.0133296999999999 18 2.0133296999999999 24 2.0133296999999999;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_thumb_up_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -1.0756935999999999 5 -1.0756935999999999 
		10 -1.0756935999999999 15 -1.0756935999999999 18 -1.0756935999999999 24 -1.0756935999999999;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_thumb_up_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.76 5 1.76 10 1.76 15 1.76 18 1.76 24 
		1.76;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_thumb_up_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 -1.8682642999999999 10 -5.661485 
		15 -5.661485 18 -5.661485 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.89654803276062012;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0.44294652342796326;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 0.89654803276062012 0.89654803276062012;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0.44294652342796326 0.44294652342796326;
createNode animCurveTA -n "L_thumb_up_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -11.225684 5 -9.5773434999999996 10 -6.2306442000000004 
		15 -6.2306442000000004 18 -6.2306442000000004 24 -11.225684;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.91669529676437378;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.3995870053768158;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 0.91669529676437378 0.91669529676437378;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 -0.3995870053768158 -0.3995870053768158;
createNode animCurveTA -n "L_thumb_up_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0.84118546999999999 10 2.549082 15 
		2.549082 18 2.549082 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 0.97614008188247681 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 -0.2171417623758316 0;
createNode animCurveTU -n "L_thumb_mid_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 9 3 2 2 9;
	setAttr -s 6 ".kot[0:5]"  1 5 3 2 2 5;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0 1 1 1 0;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_thumb_mid_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.8470298999999999 5 1.8470298999999999 
		10 1.8470298999999999 15 1.8470298999999999 18 1.8470298999999999 24 1.8470298999999999;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_thumb_mid_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_thumb_mid_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_thumb_mid_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "L_thumb_mid_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 47.65143 5 55.256302 10 70.696812 15 
		70.696812 18 70.696812 24 47.65143;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 0.59787636995315552 1 1 1 0.44523760676383972;
	setAttr -s 6 ".kiy[0:5]"  0 0.80158835649490356 0 0 0 -0.89541250467300415;
	setAttr -s 6 ".kox[0:5]"  1 0.59787636995315552 1 1 0.44523760676383972 
		0.44523760676383972;
	setAttr -s 6 ".koy[0:5]"  0 0.80158835649490356 0 0 -0.89541250467300415 
		-0.89541250467300415;
createNode animCurveTA -n "L_thumb_mid_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 10 0 15 0 18 0 24 0;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_thumb_mid_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_thumb_mid_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "L_thumb_mid_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 10 1 15 1 18 1 24 1;
	setAttr -s 6 ".kit[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kot[0:5]"  1 10 3 2 2 10;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "R_arm_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 4 1 5 1.2 10 1 15 1 18 1;
	setAttr -s 6 ".kit[4:5]"  2 2;
	setAttr -s 6 ".kot[4:5]"  2 2;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.91914504766464233 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 -0.39391931891441345 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 0.91914504766464233 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 -0.39391931891441345 0 0;
createNode animCurveTU -n "weapons_jnt_R_visibility";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 24 1;
createNode animCurveTU -n "weapons_jnt_R_scaleX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 24 1;
createNode animCurveTU -n "weapons_jnt_R_scaleY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 24 1;
createNode animCurveTU -n "weapons_jnt_R_scaleZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 24 1;
createNode animCurveTU -n "R_thigh_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTL -n "R_thigh_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -6.3305052449908956 18 -6.3305052449908956;
createNode animCurveTL -n "R_thigh_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 0.00023657825694622936 18 0.00023657825694622936;
createNode animCurveTL -n "R_thigh_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -3.6985268363424657 18 -3.6985268363424657;
createNode animCurveTA -n "R_thigh_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 42.789544145929817 18 42.789544145929817;
createNode animCurveTA -n "R_thigh_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -11.966385761655285 18 -11.966385761655285;
createNode animCurveTA -n "R_thigh_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -6.7529007596743025 18 -6.7529007596743025;
createNode animCurveTU -n "R_thigh_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_thigh_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_thigh_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_calf_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTL -n "R_calf_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 17.05729979350297 18 17.05729979350297;
createNode animCurveTL -n "R_calf_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 8.8817841970012523e-015 18 0;
createNode animCurveTL -n "R_calf_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 0 18 0;
createNode animCurveTA -n "R_calf_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 2.1371834367228594e-014 18 0;
createNode animCurveTA -n "R_calf_rotateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -37.026323014841402 18 -37.026323014841402;
createNode animCurveTA -n "R_calf_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 7.576639599183518e-015 18 0;
createNode animCurveTU -n "R_calf_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_calf_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_calf_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_thigh_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTL -n "L_thigh_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -6.3305052449908956 18 -6.3305052449908956;
createNode animCurveTL -n "L_thigh_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 0.00023657825694622936 18 0.00023657825694622936;
createNode animCurveTL -n "L_thigh_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 3.699 18 3.699;
createNode animCurveTA -n "L_thigh_rotateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 82.945621682309351 18 82.945621682309351;
createNode animCurveTA -n "L_thigh_rotateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -75.817605876990044 18 -75.817605876990044;
createNode animCurveTA -n "L_thigh_rotateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -98.361376352797294 18 -98.361376352797294;
createNode animCurveTU -n "L_thigh_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_thigh_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_thigh_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_calf_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTL -n "L_calf_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 17.05729979350297 18 17.05729979350297;
createNode animCurveTL -n "L_calf_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 8.8817841970012523e-015 18 0;
createNode animCurveTL -n "L_calf_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 0 18 0;
createNode animCurveTA -n "L_calf_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -4.3131362368040565e-014 18 0;
createNode animCurveTA -n "L_calf_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 74.724318356035155 18 74.724318356035155;
createNode animCurveTA -n "L_calf_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1.5290722428302325e-014 18 0;
createNode animCurveTU -n "L_calf_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_calf_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_calf_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_arm_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTL -n "R_arm_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 3.0350587520020733 18 3.0350587520020733;
createNode animCurveTL -n "R_arm_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 0 18 0;
createNode animCurveTL -n "R_arm_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -4.1054715183008739e-011 18 0;
createNode animCurveTA -n "R_arm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 60.160479297540327 18 60.160479297540327;
createNode animCurveTA -n "R_arm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 20.873252095667816 18 20.873252095667816;
createNode animCurveTA -n "R_arm_rotateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 112.92036340482913 18 112.92036340482913;
createNode animCurveTU -n "R_arm_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_arm_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_forearm_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTL -n "R_forearm_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 9.1797314974452568 18 9.1797314974452568;
createNode animCurveTL -n "R_forearm_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 0 18 0;
createNode animCurveTL -n "R_forearm_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -4.4408920985006262e-016 18 0;
createNode animCurveTA -n "R_forearm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -3.3858483302492019e-013 18 0;
createNode animCurveTA -n "R_forearm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 70.658887835556868 18 70.658887835556868;
createNode animCurveTA -n "R_forearm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 2.214212537646978e-012 18 0;
createNode animCurveTU -n "R_forearm_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_forearm_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_forearm_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_arm_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTL -n "L_arm_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 3.0350587520020733 18 3.0350587520020733;
createNode animCurveTL -n "L_arm_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 0 18 0;
createNode animCurveTL -n "L_arm_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -4.1054715183008739e-011 18 0;
createNode animCurveTA -n "L_arm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 17.534878983804568 18 17.534878983804568;
createNode animCurveTA -n "L_arm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -4.0041653424086636 18 -4.0041653424086636;
createNode animCurveTA -n "L_arm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -29.127413079533536 18 -29.127413079533536;
createNode animCurveTU -n "L_arm_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_arm_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_arm_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_forearm_visibility";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTL -n "L_forearm_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 9.1797314974452568 18 9.1797314974452568;
createNode animCurveTL -n "L_forearm_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 0 18 0;
createNode animCurveTL -n "L_forearm_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -4.4408920985006262e-016 18 0;
createNode animCurveTA -n "L_forearm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 3.5407894675248884e-014 18 0;
createNode animCurveTA -n "L_forearm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 -7.3892336995717933 18 -7.3892336995717933;
createNode animCurveTA -n "L_forearm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 2.3155379885504009e-013 18 0;
createNode animCurveTU -n "L_forearm_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_forearm_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_forearm_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_handCTR_ikFkBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_handCTR_ikFkBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "L_legCTR_ikFkBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
createNode animCurveTU -n "R_legCTR_ikFkBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  15 1 18 1;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 24;
	setAttr ".unw" 24;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
	setAttr -k on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :lambert1;
	setAttr ".c" -type "float3" 1 0.84363753 0.82499999 ;
	setAttr ".miic" -type "float3" 97.409096 97.409096 97.409096 ;
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr ".mcfr" 15;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :defaultObjectSet;
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr ".hwfr" 15;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -k on ".fir";
	setAttr -k on ".aap";
	setAttr -k on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
connectAttr "C_POSITION_rotateY.o" "C_POSITION.ry";
connectAttr "C_POSITION_rotateX.o" "C_POSITION.rx";
connectAttr "C_POSITION_rotateZ.o" "C_POSITION.rz";
connectAttr "C_POSITION_visibility.o" "C_POSITION.v";
connectAttr "C_POSITION_translateX.o" "C_POSITION.tx";
connectAttr "C_POSITION_translateY.o" "C_POSITION.ty";
connectAttr "C_POSITION_translateZ.o" "C_POSITION.tz";
connectAttr "C_POSITION_scaleX.o" "C_POSITION.sx";
connectAttr "C_POSITION_scaleY.o" "C_POSITION.sy";
connectAttr "C_POSITION_scaleZ.o" "C_POSITION.sz";
connectAttr "C_flyCTR_visibility.o" "C_flyCTR.v";
connectAttr "C_flyCTR_translateX.o" "C_flyCTR.tx";
connectAttr "C_flyCTR_translateY.o" "C_flyCTR.ty";
connectAttr "C_flyCTR_translateZ.o" "C_flyCTR.tz";
connectAttr "C_flyCTR_rotateX.o" "C_flyCTR.rx";
connectAttr "C_flyCTR_rotateY.o" "C_flyCTR.ry";
connectAttr "C_flyCTR_rotateZ.o" "C_flyCTR.rz";
connectAttr "C_flyCTR_scaleX.o" "C_flyCTR.sx";
connectAttr "C_flyCTR_scaleY.o" "C_flyCTR.sy";
connectAttr "C_flyCTR_scaleZ.o" "C_flyCTR.sz";
connectAttr "C_pelvisCTR_translateY.o" "C_pelvisCTR.ty";
connectAttr "C_pelvisCTR_translateZ.o" "C_pelvisCTR.tz";
connectAttr "C_pelvisCTR_translateX.o" "C_pelvisCTR.tx";
connectAttr "C_pelvisCTR_rotateX.o" "C_pelvisCTR.rx";
connectAttr "C_pelvisCTR_rotateY.o" "C_pelvisCTR.ry";
connectAttr "C_pelvisCTR_rotateZ.o" "C_pelvisCTR.rz";
connectAttr "C_pelvisCTR_scaleX.o" "C_pelvisCTR.sx";
connectAttr "C_pelvisCTR_scaleY.o" "C_pelvisCTR.sy";
connectAttr "C_pelvisCTR_scaleZ.o" "C_pelvisCTR.sz";
connectAttr "C_pelvisCTR_visibility.o" "C_pelvisCTR.v";
connectAttr "R_handCTR_twist.o" "R_handCTR.twist";
connectAttr "R_handCTR_ikFkBlend.o" "R_handCTR.ikFkBlend";
connectAttr "R_handCTR_translateX.o" "R_handCTR.tx";
connectAttr "R_handCTR_translateY.o" "R_handCTR.ty";
connectAttr "R_handCTR_translateZ.o" "R_handCTR.tz";
connectAttr "R_handCTR_rotateX.o" "R_handCTR.rx";
connectAttr "R_handCTR_rotateY.o" "R_handCTR.ry";
connectAttr "R_handCTR_rotateZ.o" "R_handCTR.rz";
connectAttr "R_handCTR_scaleX.o" "R_handCTR.sx";
connectAttr "R_handCTR_scaleY.o" "R_handCTR.sy";
connectAttr "R_handCTR_scaleZ.o" "R_handCTR.sz";
connectAttr "L_handCTR_twist.o" "L_handCTR.twist";
connectAttr "L_handCTR_ikFkBlend.o" "L_handCTR.ikFkBlend";
connectAttr "L_handCTR_translateX.o" "L_handCTR.tx";
connectAttr "L_handCTR_translateY.o" "L_handCTR.ty";
connectAttr "L_handCTR_translateZ.o" "L_handCTR.tz";
connectAttr "L_handCTR_rotateX.o" "L_handCTR.rx";
connectAttr "L_handCTR_rotateY.o" "L_handCTR.ry";
connectAttr "L_handCTR_rotateZ.o" "L_handCTR.rz";
connectAttr "L_handCTR_scaleX.o" "L_handCTR.sx";
connectAttr "L_handCTR_scaleY.o" "L_handCTR.sy";
connectAttr "L_handCTR_scaleZ.o" "L_handCTR.sz";
connectAttr "C_hipCTR_translateX.o" "C_hipCTR.tx";
connectAttr "C_hipCTR_translateY.o" "C_hipCTR.ty";
connectAttr "C_hipCTR_translateZ.o" "C_hipCTR.tz";
connectAttr "C_hipCTR_rotateX.o" "C_hipCTR.rx";
connectAttr "C_hipCTR_rotateY.o" "C_hipCTR.ry";
connectAttr "C_hipCTR_rotateZ.o" "C_hipCTR.rz";
connectAttr "C_hipCTR_scaleX.o" "C_hipCTR.sx";
connectAttr "C_hipCTR_scaleY.o" "C_hipCTR.sy";
connectAttr "C_hipCTR_scaleZ.o" "C_hipCTR.sz";
connectAttr "C_hipCTR_visibility.o" "C_hipCTR.v";
connectAttr "L_legCTR_twist.o" "L_legCTR.twist";
connectAttr "L_legCTR_ikFkBlend.o" "L_legCTR.ikFkBlend";
connectAttr "L_legCTR_translateX.o" "L_legCTR.tx";
connectAttr "L_legCTR_translateY.o" "L_legCTR.ty";
connectAttr "L_legCTR_translateZ.o" "L_legCTR.tz";
connectAttr "L_legCTR_rotateX.o" "L_legCTR.rx";
connectAttr "L_legCTR_rotateY.o" "L_legCTR.ry";
connectAttr "L_legCTR_rotateZ.o" "L_legCTR.rz";
connectAttr "L_legCTR_scaleX.o" "L_legCTR.sx";
connectAttr "L_legCTR_scaleY.o" "L_legCTR.sy";
connectAttr "L_legCTR_scaleZ.o" "L_legCTR.sz";
connectAttr "R_legCTR_twist.o" "R_legCTR.twist";
connectAttr "R_legCTR_ikFkBlend.o" "R_legCTR.ikFkBlend";
connectAttr "R_legCTR_translateX.o" "R_legCTR.tx";
connectAttr "R_legCTR_translateZ.o" "R_legCTR.tz";
connectAttr "R_legCTR_translateY.o" "R_legCTR.ty";
connectAttr "R_legCTR_rotateX.o" "R_legCTR.rx";
connectAttr "R_legCTR_rotateY.o" "R_legCTR.ry";
connectAttr "R_legCTR_rotateZ.o" "R_legCTR.rz";
connectAttr "R_legCTR_scaleX.o" "R_legCTR.sx";
connectAttr "R_legCTR_scaleY.o" "R_legCTR.sy";
connectAttr "R_legCTR_scaleZ.o" "R_legCTR.sz";
connectAttr "R_arm.msg" "ikHandle1.hsj";
connectAttr "effector1.hp" "ikHandle1.hee";
connectAttr "ikRPsolver.msg" "ikHandle1.hsv";
connectAttr "ikHandle1_pointConstraint1.ctx" "ikHandle1.tx";
connectAttr "ikHandle1_pointConstraint1.cty" "ikHandle1.ty";
connectAttr "ikHandle1_pointConstraint1.ctz" "ikHandle1.tz";
connectAttr "ikHandle1_orientConstraint1.crx" "ikHandle1.rx";
connectAttr "ikHandle1_orientConstraint1.cry" "ikHandle1.ry";
connectAttr "ikHandle1_orientConstraint1.crz" "ikHandle1.rz";
connectAttr "ikHandle1_poleVectorConstraint1.ctx" "ikHandle1.pvx";
connectAttr "ikHandle1_poleVectorConstraint1.cty" "ikHandle1.pvy";
connectAttr "ikHandle1_poleVectorConstraint1.ctz" "ikHandle1.pvz";
connectAttr "unitConversion1.o" "ikHandle1.twi";
connectAttr "ikHandle1_scaleConstraint1.csx" "ikHandle1.sx";
connectAttr "ikHandle1_scaleConstraint1.csy" "ikHandle1.sy";
connectAttr "ikHandle1_scaleConstraint1.csz" "ikHandle1.sz";
connectAttr "R_handCTR.ikFkBlend" "ikHandle1.ikb";
connectAttr "ikHandle1.pim" "ikHandle1_pointConstraint1.cpim";
connectAttr "ikHandle1.rp" "ikHandle1_pointConstraint1.crp";
connectAttr "ikHandle1.rpt" "ikHandle1_pointConstraint1.crt";
connectAttr "R_handCTR.t" "ikHandle1_pointConstraint1.tg[0].tt";
connectAttr "R_handCTR.rp" "ikHandle1_pointConstraint1.tg[0].trp";
connectAttr "R_handCTR.rpt" "ikHandle1_pointConstraint1.tg[0].trt";
connectAttr "R_handCTR.pm" "ikHandle1_pointConstraint1.tg[0].tpm";
connectAttr "ikHandle1_pointConstraint1.w0" "ikHandle1_pointConstraint1.tg[0].tw"
		;
connectAttr "ikHandle1.ro" "ikHandle1_orientConstraint1.cro";
connectAttr "ikHandle1.pim" "ikHandle1_orientConstraint1.cpim";
connectAttr "R_handCTR.r" "ikHandle1_orientConstraint1.tg[0].tr";
connectAttr "R_handCTR.ro" "ikHandle1_orientConstraint1.tg[0].tro";
connectAttr "R_handCTR.pm" "ikHandle1_orientConstraint1.tg[0].tpm";
connectAttr "ikHandle1_orientConstraint1.w0" "ikHandle1_orientConstraint1.tg[0].tw"
		;
connectAttr "ikHandle1.pim" "ikHandle1_poleVectorConstraint1.cpim";
connectAttr "R_arm.pm" "ikHandle1_poleVectorConstraint1.ps";
connectAttr "R_arm.t" "ikHandle1_poleVectorConstraint1.crp";
connectAttr "R_arm.t" "ikHandle1_poleVectorConstraint1.tg[0].tt";
connectAttr "R_arm.rp" "ikHandle1_poleVectorConstraint1.tg[0].trp";
connectAttr "R_arm.rpt" "ikHandle1_poleVectorConstraint1.tg[0].trt";
connectAttr "R_arm.pm" "ikHandle1_poleVectorConstraint1.tg[0].tpm";
connectAttr "ikHandle1_poleVectorConstraint1.w0" "ikHandle1_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "ikHandle1.pim" "ikHandle1_scaleConstraint1.cpim";
connectAttr "R_handCTR.s" "ikHandle1_scaleConstraint1.tg[0].ts";
connectAttr "R_handCTR.pm" "ikHandle1_scaleConstraint1.tg[0].tpm";
connectAttr "ikHandle1_scaleConstraint1.w0" "ikHandle1_scaleConstraint1.tg[0].tw"
		;
connectAttr "L_arm.msg" "ikHandle2.hsj";
connectAttr "effector2.hp" "ikHandle2.hee";
connectAttr "ikRPsolver.msg" "ikHandle2.hsv";
connectAttr "ikHandle2_pointConstraint1.ctx" "ikHandle2.tx";
connectAttr "ikHandle2_pointConstraint1.cty" "ikHandle2.ty";
connectAttr "ikHandle2_pointConstraint1.ctz" "ikHandle2.tz";
connectAttr "ikHandle2_orientConstraint1.crx" "ikHandle2.rx";
connectAttr "ikHandle2_orientConstraint1.cry" "ikHandle2.ry";
connectAttr "ikHandle2_orientConstraint1.crz" "ikHandle2.rz";
connectAttr "ikHandle2_poleVectorConstraint1.ctx" "ikHandle2.pvx";
connectAttr "ikHandle2_poleVectorConstraint1.cty" "ikHandle2.pvy";
connectAttr "ikHandle2_poleVectorConstraint1.ctz" "ikHandle2.pvz";
connectAttr "unitConversion2.o" "ikHandle2.twi";
connectAttr "ikHandle2_scaleConstraint1.csx" "ikHandle2.sx";
connectAttr "ikHandle2_scaleConstraint1.csy" "ikHandle2.sy";
connectAttr "ikHandle2_scaleConstraint1.csz" "ikHandle2.sz";
connectAttr "L_handCTR.ikFkBlend" "ikHandle2.ikb";
connectAttr "ikHandle2.pim" "ikHandle2_pointConstraint1.cpim";
connectAttr "ikHandle2.rp" "ikHandle2_pointConstraint1.crp";
connectAttr "ikHandle2.rpt" "ikHandle2_pointConstraint1.crt";
connectAttr "L_handCTR.t" "ikHandle2_pointConstraint1.tg[0].tt";
connectAttr "L_handCTR.rp" "ikHandle2_pointConstraint1.tg[0].trp";
connectAttr "L_handCTR.rpt" "ikHandle2_pointConstraint1.tg[0].trt";
connectAttr "L_handCTR.pm" "ikHandle2_pointConstraint1.tg[0].tpm";
connectAttr "ikHandle2_pointConstraint1.w0" "ikHandle2_pointConstraint1.tg[0].tw"
		;
connectAttr "ikHandle2.ro" "ikHandle2_orientConstraint1.cro";
connectAttr "ikHandle2.pim" "ikHandle2_orientConstraint1.cpim";
connectAttr "L_handCTR.r" "ikHandle2_orientConstraint1.tg[0].tr";
connectAttr "L_handCTR.ro" "ikHandle2_orientConstraint1.tg[0].tro";
connectAttr "L_handCTR.pm" "ikHandle2_orientConstraint1.tg[0].tpm";
connectAttr "ikHandle2_orientConstraint1.w0" "ikHandle2_orientConstraint1.tg[0].tw"
		;
connectAttr "ikHandle2.pim" "ikHandle2_poleVectorConstraint1.cpim";
connectAttr "L_arm.pm" "ikHandle2_poleVectorConstraint1.ps";
connectAttr "L_arm.t" "ikHandle2_poleVectorConstraint1.crp";
connectAttr "L_arm.t" "ikHandle2_poleVectorConstraint1.tg[0].tt";
connectAttr "L_arm.rp" "ikHandle2_poleVectorConstraint1.tg[0].trp";
connectAttr "L_arm.rpt" "ikHandle2_poleVectorConstraint1.tg[0].trt";
connectAttr "L_arm.pm" "ikHandle2_poleVectorConstraint1.tg[0].tpm";
connectAttr "ikHandle2_poleVectorConstraint1.w0" "ikHandle2_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "ikHandle2.pim" "ikHandle2_scaleConstraint1.cpim";
connectAttr "L_handCTR.s" "ikHandle2_scaleConstraint1.tg[0].ts";
connectAttr "L_handCTR.pm" "ikHandle2_scaleConstraint1.tg[0].tpm";
connectAttr "ikHandle2_scaleConstraint1.w0" "ikHandle2_scaleConstraint1.tg[0].tw"
		;
connectAttr "R_thigh.msg" "ikHandle3.hsj";
connectAttr "effector3.hp" "ikHandle3.hee";
connectAttr "ikRPsolver.msg" "ikHandle3.hsv";
connectAttr "ikHandle3_pointConstraint1.ctx" "ikHandle3.tx";
connectAttr "ikHandle3_pointConstraint1.cty" "ikHandle3.ty";
connectAttr "ikHandle3_pointConstraint1.ctz" "ikHandle3.tz";
connectAttr "ikHandle3_orientConstraint1.crx" "ikHandle3.rx";
connectAttr "ikHandle3_orientConstraint1.cry" "ikHandle3.ry";
connectAttr "ikHandle3_orientConstraint1.crz" "ikHandle3.rz";
connectAttr "ikHandle3_poleVectorConstraint1.ctx" "ikHandle3.pvx";
connectAttr "ikHandle3_poleVectorConstraint1.cty" "ikHandle3.pvy";
connectAttr "ikHandle3_poleVectorConstraint1.ctz" "ikHandle3.pvz";
connectAttr "unitConversion3.o" "ikHandle3.twi";
connectAttr "ikHandle3_scaleConstraint1.csx" "ikHandle3.sx";
connectAttr "ikHandle3_scaleConstraint1.csy" "ikHandle3.sy";
connectAttr "ikHandle3_scaleConstraint1.csz" "ikHandle3.sz";
connectAttr "R_legCTR.ikFkBlend" "ikHandle3.ikb";
connectAttr "ikHandle3.pim" "ikHandle3_pointConstraint1.cpim";
connectAttr "ikHandle3.rp" "ikHandle3_pointConstraint1.crp";
connectAttr "ikHandle3.rpt" "ikHandle3_pointConstraint1.crt";
connectAttr "R_legCTR.t" "ikHandle3_pointConstraint1.tg[0].tt";
connectAttr "R_legCTR.rp" "ikHandle3_pointConstraint1.tg[0].trp";
connectAttr "R_legCTR.rpt" "ikHandle3_pointConstraint1.tg[0].trt";
connectAttr "R_legCTR.pm" "ikHandle3_pointConstraint1.tg[0].tpm";
connectAttr "ikHandle3_pointConstraint1.w0" "ikHandle3_pointConstraint1.tg[0].tw"
		;
connectAttr "ikHandle3.ro" "ikHandle3_orientConstraint1.cro";
connectAttr "ikHandle3.pim" "ikHandle3_orientConstraint1.cpim";
connectAttr "R_legCTR.r" "ikHandle3_orientConstraint1.tg[0].tr";
connectAttr "R_legCTR.ro" "ikHandle3_orientConstraint1.tg[0].tro";
connectAttr "R_legCTR.pm" "ikHandle3_orientConstraint1.tg[0].tpm";
connectAttr "ikHandle3_orientConstraint1.w0" "ikHandle3_orientConstraint1.tg[0].tw"
		;
connectAttr "ikHandle3.pim" "ikHandle3_poleVectorConstraint1.cpim";
connectAttr "R_thigh.pm" "ikHandle3_poleVectorConstraint1.ps";
connectAttr "R_thigh.t" "ikHandle3_poleVectorConstraint1.crp";
connectAttr "R_thigh.t" "ikHandle3_poleVectorConstraint1.tg[0].tt";
connectAttr "R_thigh.rp" "ikHandle3_poleVectorConstraint1.tg[0].trp";
connectAttr "R_thigh.rpt" "ikHandle3_poleVectorConstraint1.tg[0].trt";
connectAttr "R_thigh.pm" "ikHandle3_poleVectorConstraint1.tg[0].tpm";
connectAttr "ikHandle3_poleVectorConstraint1.w0" "ikHandle3_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "ikHandle3.pim" "ikHandle3_scaleConstraint1.cpim";
connectAttr "R_legCTR.s" "ikHandle3_scaleConstraint1.tg[0].ts";
connectAttr "R_legCTR.pm" "ikHandle3_scaleConstraint1.tg[0].tpm";
connectAttr "ikHandle3_scaleConstraint1.w0" "ikHandle3_scaleConstraint1.tg[0].tw"
		;
connectAttr "L_thigh.msg" "ikHandle4.hsj";
connectAttr "effector4.hp" "ikHandle4.hee";
connectAttr "ikRPsolver.msg" "ikHandle4.hsv";
connectAttr "ikHandle4_pointConstraint1.ctx" "ikHandle4.tx";
connectAttr "ikHandle4_pointConstraint1.cty" "ikHandle4.ty";
connectAttr "ikHandle4_pointConstraint1.ctz" "ikHandle4.tz";
connectAttr "ikHandle4_orientConstraint1.crx" "ikHandle4.rx";
connectAttr "ikHandle4_orientConstraint1.cry" "ikHandle4.ry";
connectAttr "ikHandle4_orientConstraint1.crz" "ikHandle4.rz";
connectAttr "ikHandle4_poleVectorConstraint1.ctx" "ikHandle4.pvx";
connectAttr "ikHandle4_poleVectorConstraint1.cty" "ikHandle4.pvy";
connectAttr "ikHandle4_poleVectorConstraint1.ctz" "ikHandle4.pvz";
connectAttr "unitConversion4.o" "ikHandle4.twi";
connectAttr "ikHandle4_scaleConstraint1.csx" "ikHandle4.sx";
connectAttr "ikHandle4_scaleConstraint1.csy" "ikHandle4.sy";
connectAttr "ikHandle4_scaleConstraint1.csz" "ikHandle4.sz";
connectAttr "L_legCTR.ikFkBlend" "ikHandle4.ikb";
connectAttr "ikHandle4.pim" "ikHandle4_pointConstraint1.cpim";
connectAttr "ikHandle4.rp" "ikHandle4_pointConstraint1.crp";
connectAttr "ikHandle4.rpt" "ikHandle4_pointConstraint1.crt";
connectAttr "L_legCTR.t" "ikHandle4_pointConstraint1.tg[0].tt";
connectAttr "L_legCTR.rp" "ikHandle4_pointConstraint1.tg[0].trp";
connectAttr "L_legCTR.rpt" "ikHandle4_pointConstraint1.tg[0].trt";
connectAttr "L_legCTR.pm" "ikHandle4_pointConstraint1.tg[0].tpm";
connectAttr "ikHandle4_pointConstraint1.w0" "ikHandle4_pointConstraint1.tg[0].tw"
		;
connectAttr "ikHandle4.ro" "ikHandle4_orientConstraint1.cro";
connectAttr "ikHandle4.pim" "ikHandle4_orientConstraint1.cpim";
connectAttr "L_legCTR.r" "ikHandle4_orientConstraint1.tg[0].tr";
connectAttr "L_legCTR.ro" "ikHandle4_orientConstraint1.tg[0].tro";
connectAttr "L_legCTR.pm" "ikHandle4_orientConstraint1.tg[0].tpm";
connectAttr "ikHandle4_orientConstraint1.w0" "ikHandle4_orientConstraint1.tg[0].tw"
		;
connectAttr "ikHandle4.pim" "ikHandle4_poleVectorConstraint1.cpim";
connectAttr "L_thigh.pm" "ikHandle4_poleVectorConstraint1.ps";
connectAttr "L_thigh.t" "ikHandle4_poleVectorConstraint1.crp";
connectAttr "L_thigh.t" "ikHandle4_poleVectorConstraint1.tg[0].tt";
connectAttr "L_thigh.rp" "ikHandle4_poleVectorConstraint1.tg[0].trp";
connectAttr "L_thigh.rpt" "ikHandle4_poleVectorConstraint1.tg[0].trt";
connectAttr "L_thigh.pm" "ikHandle4_poleVectorConstraint1.tg[0].tpm";
connectAttr "ikHandle4_poleVectorConstraint1.w0" "ikHandle4_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "ikHandle4.pim" "ikHandle4_scaleConstraint1.cpim";
connectAttr "L_legCTR.s" "ikHandle4_scaleConstraint1.tg[0].ts";
connectAttr "L_legCTR.pm" "ikHandle4_scaleConstraint1.tg[0].tpm";
connectAttr "ikHandle4_scaleConstraint1.w0" "ikHandle4_scaleConstraint1.tg[0].tw"
		;
connectAttr "C_root_pointConstraint1.ctx" "C_root.tx";
connectAttr "C_root_pointConstraint1.cty" "C_root.ty";
connectAttr "C_root_pointConstraint1.ctz" "C_root.tz";
connectAttr "C_root_orientConstraint1.crx" "C_root.rx";
connectAttr "C_root_orientConstraint1.cry" "C_root.ry";
connectAttr "C_root_orientConstraint1.crz" "C_root.rz";
connectAttr "C_root_scaleConstraint1.csx" "C_root.sx";
connectAttr "C_root_scaleConstraint1.csy" "C_root.sy";
connectAttr "C_root_scaleConstraint1.csz" "C_root.sz";
connectAttr "C_root.s" "C_hip.is";
connectAttr "C_hip_pointConstraint1.ctx" "C_hip.tx";
connectAttr "C_hip_pointConstraint1.cty" "C_hip.ty";
connectAttr "C_hip_pointConstraint1.ctz" "C_hip.tz";
connectAttr "C_hip_orientConstraint1.crx" "C_hip.rx";
connectAttr "C_hip_orientConstraint1.cry" "C_hip.ry";
connectAttr "C_hip_orientConstraint1.crz" "C_hip.rz";
connectAttr "C_hip_scaleConstraint1.csx" "C_hip.sx";
connectAttr "C_hip_scaleConstraint1.csy" "C_hip.sy";
connectAttr "C_hip_scaleConstraint1.csz" "C_hip.sz";
connectAttr "R_thigh_translateX.o" "R_thigh.tx";
connectAttr "R_thigh_translateY.o" "R_thigh.ty";
connectAttr "R_thigh_translateZ.o" "R_thigh.tz";
connectAttr "C_hip.s" "R_thigh.is";
connectAttr "R_thigh_scaleX.o" "R_thigh.sx";
connectAttr "R_thigh_scaleY.o" "R_thigh.sy";
connectAttr "R_thigh_scaleZ.o" "R_thigh.sz";
connectAttr "R_thigh_visibility.o" "R_thigh.v";
connectAttr "R_thigh_rotateX.o" "R_thigh.rx";
connectAttr "R_thigh_rotateY.o" "R_thigh.ry";
connectAttr "R_thigh_rotateZ.o" "R_thigh.rz";
connectAttr "R_thigh.s" "R_calf.is";
connectAttr "R_calf_scaleX.o" "R_calf.sx";
connectAttr "R_calf_scaleY.o" "R_calf.sy";
connectAttr "R_calf_scaleZ.o" "R_calf.sz";
connectAttr "R_calf_visibility.o" "R_calf.v";
connectAttr "R_calf_translateX.o" "R_calf.tx";
connectAttr "R_calf_translateY.o" "R_calf.ty";
connectAttr "R_calf_translateZ.o" "R_calf.tz";
connectAttr "R_calf_rotateX.o" "R_calf.rx";
connectAttr "R_calf_rotateY.o" "R_calf.ry";
connectAttr "R_calf_rotateZ.o" "R_calf.rz";
connectAttr "R_calf.s" "R_heel.is";
connectAttr "R_heel_orientConstraint1.crx" "R_heel.rx";
connectAttr "R_heel_orientConstraint1.cry" "R_heel.ry";
connectAttr "R_heel_orientConstraint1.crz" "R_heel.rz";
connectAttr "R_heel.s" "R_foot.is";
connectAttr "R_foot_visibility.o" "R_foot.v";
connectAttr "R_foot_translateX.o" "R_foot.tx";
connectAttr "R_foot_translateY.o" "R_foot.ty";
connectAttr "R_foot_translateZ.o" "R_foot.tz";
connectAttr "R_foot_rotateX.o" "R_foot.rx";
connectAttr "R_foot_rotateY.o" "R_foot.ry";
connectAttr "R_foot_rotateZ.o" "R_foot.rz";
connectAttr "R_foot_scaleX.o" "R_foot.sx";
connectAttr "R_foot_scaleY.o" "R_foot.sy";
connectAttr "R_foot_scaleZ.o" "R_foot.sz";
connectAttr "R_heel.ro" "R_heel_orientConstraint1.cro";
connectAttr "R_heel.pim" "R_heel_orientConstraint1.cpim";
connectAttr "R_heel.jo" "R_heel_orientConstraint1.cjo";
connectAttr "R_legCTR.r" "R_heel_orientConstraint1.tg[0].tr";
connectAttr "R_legCTR.ro" "R_heel_orientConstraint1.tg[0].tro";
connectAttr "R_legCTR.pm" "R_heel_orientConstraint1.tg[0].tpm";
connectAttr "R_heel_orientConstraint1.w0" "R_heel_orientConstraint1.tg[0].tw";
connectAttr "R_heel.tx" "effector3.tx";
connectAttr "R_heel.ty" "effector3.ty";
connectAttr "R_heel.tz" "effector3.tz";
connectAttr "L_thigh_translateX.o" "L_thigh.tx";
connectAttr "L_thigh_translateY.o" "L_thigh.ty";
connectAttr "L_thigh_translateZ.o" "L_thigh.tz";
connectAttr "C_hip.s" "L_thigh.is";
connectAttr "L_thigh_scaleX.o" "L_thigh.sx";
connectAttr "L_thigh_scaleY.o" "L_thigh.sy";
connectAttr "L_thigh_scaleZ.o" "L_thigh.sz";
connectAttr "L_thigh_visibility.o" "L_thigh.v";
connectAttr "L_thigh_rotateX.o" "L_thigh.rx";
connectAttr "L_thigh_rotateY.o" "L_thigh.ry";
connectAttr "L_thigh_rotateZ.o" "L_thigh.rz";
connectAttr "L_thigh.s" "L_calf.is";
connectAttr "L_calf_scaleX.o" "L_calf.sx";
connectAttr "L_calf_scaleY.o" "L_calf.sy";
connectAttr "L_calf_scaleZ.o" "L_calf.sz";
connectAttr "L_calf_visibility.o" "L_calf.v";
connectAttr "L_calf_translateX.o" "L_calf.tx";
connectAttr "L_calf_translateY.o" "L_calf.ty";
connectAttr "L_calf_translateZ.o" "L_calf.tz";
connectAttr "L_calf_rotateX.o" "L_calf.rx";
connectAttr "L_calf_rotateY.o" "L_calf.ry";
connectAttr "L_calf_rotateZ.o" "L_calf.rz";
connectAttr "L_calf.s" "L_heel.is";
connectAttr "L_feet1_orientConstraint1.crx" "L_heel.rx";
connectAttr "L_feet1_orientConstraint1.cry" "L_heel.ry";
connectAttr "L_feet1_orientConstraint1.crz" "L_heel.rz";
connectAttr "L_heel.s" "L_foot.is";
connectAttr "L_foot_visibility.o" "L_foot.v";
connectAttr "L_foot_translateX.o" "L_foot.tx";
connectAttr "L_foot_translateY.o" "L_foot.ty";
connectAttr "L_foot_translateZ.o" "L_foot.tz";
connectAttr "L_foot_rotateX.o" "L_foot.rx";
connectAttr "L_foot_rotateY.o" "L_foot.ry";
connectAttr "L_foot_rotateZ.o" "L_foot.rz";
connectAttr "L_foot_scaleX.o" "L_foot.sx";
connectAttr "L_foot_scaleY.o" "L_foot.sy";
connectAttr "L_foot_scaleZ.o" "L_foot.sz";
connectAttr "L_heel.ro" "L_feet1_orientConstraint1.cro";
connectAttr "L_heel.pim" "L_feet1_orientConstraint1.cpim";
connectAttr "L_heel.jo" "L_feet1_orientConstraint1.cjo";
connectAttr "L_legCTR.r" "L_feet1_orientConstraint1.tg[0].tr";
connectAttr "L_legCTR.ro" "L_feet1_orientConstraint1.tg[0].tro";
connectAttr "L_legCTR.pm" "L_feet1_orientConstraint1.tg[0].tpm";
connectAttr "L_feet1_orientConstraint1.w0" "L_feet1_orientConstraint1.tg[0].tw";
connectAttr "L_heel.tx" "effector4.tx";
connectAttr "L_heel.ty" "effector4.ty";
connectAttr "L_heel.tz" "effector4.tz";
connectAttr "C_hip.pim" "C_hip_pointConstraint1.cpim";
connectAttr "C_hip.rp" "C_hip_pointConstraint1.crp";
connectAttr "C_hip.rpt" "C_hip_pointConstraint1.crt";
connectAttr "C_hipCTR.t" "C_hip_pointConstraint1.tg[0].tt";
connectAttr "C_hipCTR.rp" "C_hip_pointConstraint1.tg[0].trp";
connectAttr "C_hipCTR.rpt" "C_hip_pointConstraint1.tg[0].trt";
connectAttr "C_hipCTR.pm" "C_hip_pointConstraint1.tg[0].tpm";
connectAttr "C_hip_pointConstraint1.w0" "C_hip_pointConstraint1.tg[0].tw";
connectAttr "C_hip.ro" "C_hip_orientConstraint1.cro";
connectAttr "C_hip.pim" "C_hip_orientConstraint1.cpim";
connectAttr "C_hip.jo" "C_hip_orientConstraint1.cjo";
connectAttr "C_hipCTR.r" "C_hip_orientConstraint1.tg[0].tr";
connectAttr "C_hipCTR.ro" "C_hip_orientConstraint1.tg[0].tro";
connectAttr "C_hipCTR.pm" "C_hip_orientConstraint1.tg[0].tpm";
connectAttr "C_hip_orientConstraint1.w0" "C_hip_orientConstraint1.tg[0].tw";
connectAttr "C_hip.pim" "C_hip_scaleConstraint1.cpim";
connectAttr "C_hipCTR.s" "C_hip_scaleConstraint1.tg[0].ts";
connectAttr "C_hipCTR.pm" "C_hip_scaleConstraint1.tg[0].tpm";
connectAttr "C_hip_scaleConstraint1.w0" "C_hip_scaleConstraint1.tg[0].tw";
connectAttr "C_root.s" "C_spine_low.is";
connectAttr "C_spine_low_scaleX.o" "C_spine_low.sx";
connectAttr "C_spine_low_scaleY.o" "C_spine_low.sy";
connectAttr "C_spine_low_scaleZ.o" "C_spine_low.sz";
connectAttr "C_spine_low_visibility.o" "C_spine_low.v";
connectAttr "C_spine_low_translateX.o" "C_spine_low.tx";
connectAttr "C_spine_low_translateY.o" "C_spine_low.ty";
connectAttr "C_spine_low_translateZ.o" "C_spine_low.tz";
connectAttr "C_spine_low_rotateX.o" "C_spine_low.rx";
connectAttr "C_spine_low_rotateY.o" "C_spine_low.ry";
connectAttr "C_spine_low_rotateZ.o" "C_spine_low.rz";
connectAttr "C_spine_low.s" "C_spine_mid.is";
connectAttr "C_spine_mid_scaleX.o" "C_spine_mid.sx";
connectAttr "C_spine_mid_scaleY.o" "C_spine_mid.sy";
connectAttr "C_spine_mid_scaleZ.o" "C_spine_mid.sz";
connectAttr "C_spine_mid_visibility.o" "C_spine_mid.v";
connectAttr "C_spine_mid_translateX.o" "C_spine_mid.tx";
connectAttr "C_spine_mid_translateY.o" "C_spine_mid.ty";
connectAttr "C_spine_mid_translateZ.o" "C_spine_mid.tz";
connectAttr "C_spine_mid_rotateX.o" "C_spine_mid.rx";
connectAttr "C_spine_mid_rotateY.o" "C_spine_mid.ry";
connectAttr "C_spine_mid_rotateZ.o" "C_spine_mid.rz";
connectAttr "C_spine_mid.s" "C_spine_up.is";
connectAttr "C_spine_up_scaleX.o" "C_spine_up.sx";
connectAttr "C_spine_up_scaleY.o" "C_spine_up.sy";
connectAttr "C_spine_up_scaleZ.o" "C_spine_up.sz";
connectAttr "C_spine_up_visibility.o" "C_spine_up.v";
connectAttr "C_spine_up_translateX.o" "C_spine_up.tx";
connectAttr "C_spine_up_translateY.o" "C_spine_up.ty";
connectAttr "C_spine_up_translateZ.o" "C_spine_up.tz";
connectAttr "C_spine_up_rotateX.o" "C_spine_up.rx";
connectAttr "C_spine_up_rotateY.o" "C_spine_up.ry";
connectAttr "C_spine_up_rotateZ.o" "C_spine_up.rz";
connectAttr "C_neck_scaleX.o" "C_neck.sx";
connectAttr "C_neck_scaleY.o" "C_neck.sy";
connectAttr "C_neck_scaleZ.o" "C_neck.sz";
connectAttr "C_neck_rotateX.o" "C_neck.rx";
connectAttr "C_neck_rotateY.o" "C_neck.ry";
connectAttr "C_neck_rotateZ.o" "C_neck.rz";
connectAttr "C_neck_visibility.o" "C_neck.v";
connectAttr "C_neck_translateX.o" "C_neck.tx";
connectAttr "C_neck_translateY.o" "C_neck.ty";
connectAttr "C_neck_translateZ.o" "C_neck.tz";
connectAttr "C_neck.s" "C_head.is";
connectAttr "C_head_scaleX.o" "C_head.sx";
connectAttr "C_head_scaleY.o" "C_head.sy";
connectAttr "C_head_scaleZ.o" "C_head.sz";
connectAttr "C_head_rotateX.o" "C_head.rx";
connectAttr "C_head_rotateY.o" "C_head.ry";
connectAttr "C_head_rotateZ.o" "C_head.rz";
connectAttr "C_head_visibility.o" "C_head.v";
connectAttr "C_head_translateX.o" "C_head.tx";
connectAttr "C_head_translateY.o" "C_head.ty";
connectAttr "C_head_translateZ.o" "C_head.tz";
connectAttr "C_head.s" "C_hair_a.is";
connectAttr "C_hair_a_scaleX.o" "C_hair_a.sx";
connectAttr "C_hair_a_scaleY.o" "C_hair_a.sy";
connectAttr "C_hair_a_scaleZ.o" "C_hair_a.sz";
connectAttr "C_hair_a_visibility.o" "C_hair_a.v";
connectAttr "C_hair_a_translateX.o" "C_hair_a.tx";
connectAttr "C_hair_a_translateY.o" "C_hair_a.ty";
connectAttr "C_hair_a_translateZ.o" "C_hair_a.tz";
connectAttr "C_hair_a_rotateX.o" "C_hair_a.rx";
connectAttr "C_hair_a_rotateY.o" "C_hair_a.ry";
connectAttr "C_hair_a_rotateZ.o" "C_hair_a.rz";
connectAttr "C_hair_a.s" "C_hair_b.is";
connectAttr "C_hair_b_visibility.o" "C_hair_b.v";
connectAttr "C_hair_b_translateX.o" "C_hair_b.tx";
connectAttr "C_hair_b_translateY.o" "C_hair_b.ty";
connectAttr "C_hair_b_translateZ.o" "C_hair_b.tz";
connectAttr "C_hair_b_rotateX.o" "C_hair_b.rx";
connectAttr "C_hair_b_rotateY.o" "C_hair_b.ry";
connectAttr "C_hair_b_rotateZ.o" "C_hair_b.rz";
connectAttr "C_hair_b_scaleX.o" "C_hair_b.sx";
connectAttr "C_hair_b_scaleY.o" "C_hair_b.sy";
connectAttr "C_hair_b_scaleZ.o" "C_hair_b.sz";
connectAttr "C_head.s" "R_hair_a.is";
connectAttr "R_hair_a_scaleX.o" "R_hair_a.sx";
connectAttr "R_hair_a_scaleY.o" "R_hair_a.sy";
connectAttr "R_hair_a_scaleZ.o" "R_hair_a.sz";
connectAttr "R_hair_a_visibility.o" "R_hair_a.v";
connectAttr "R_hair_a_translateX.o" "R_hair_a.tx";
connectAttr "R_hair_a_translateY.o" "R_hair_a.ty";
connectAttr "R_hair_a_translateZ.o" "R_hair_a.tz";
connectAttr "R_hair_a_rotateX.o" "R_hair_a.rx";
connectAttr "R_hair_a_rotateY.o" "R_hair_a.ry";
connectAttr "R_hair_a_rotateZ.o" "R_hair_a.rz";
connectAttr "R_hair_a.s" "R_hair_b.is";
connectAttr "R_hair_b_visibility.o" "R_hair_b.v";
connectAttr "R_hair_b_translateX.o" "R_hair_b.tx";
connectAttr "R_hair_b_translateY.o" "R_hair_b.ty";
connectAttr "R_hair_b_translateZ.o" "R_hair_b.tz";
connectAttr "R_hair_b_rotateX.o" "R_hair_b.rx";
connectAttr "R_hair_b_rotateY.o" "R_hair_b.ry";
connectAttr "R_hair_b_rotateZ.o" "R_hair_b.rz";
connectAttr "R_hair_b_scaleX.o" "R_hair_b.sx";
connectAttr "R_hair_b_scaleY.o" "R_hair_b.sy";
connectAttr "R_hair_b_scaleZ.o" "R_hair_b.sz";
connectAttr "C_head.s" "L_hair_a.is";
connectAttr "L_hair_a_scaleX.o" "L_hair_a.sx";
connectAttr "L_hair_a_scaleY.o" "L_hair_a.sy";
connectAttr "L_hair_a_scaleZ.o" "L_hair_a.sz";
connectAttr "L_hair_a_visibility.o" "L_hair_a.v";
connectAttr "L_hair_a_translateX.o" "L_hair_a.tx";
connectAttr "L_hair_a_translateY.o" "L_hair_a.ty";
connectAttr "L_hair_a_translateZ.o" "L_hair_a.tz";
connectAttr "L_hair_a_rotateX.o" "L_hair_a.rx";
connectAttr "L_hair_a_rotateY.o" "L_hair_a.ry";
connectAttr "L_hair_a_rotateZ.o" "L_hair_a.rz";
connectAttr "L_hair_a.s" "L_hair_b.is";
connectAttr "L_hair_b_visibility.o" "L_hair_b.v";
connectAttr "L_hair_b_translateX.o" "L_hair_b.tx";
connectAttr "L_hair_b_translateY.o" "L_hair_b.ty";
connectAttr "L_hair_b_translateZ.o" "L_hair_b.tz";
connectAttr "L_hair_b_rotateX.o" "L_hair_b.rx";
connectAttr "L_hair_b_rotateY.o" "L_hair_b.ry";
connectAttr "L_hair_b_rotateZ.o" "L_hair_b.rz";
connectAttr "L_hair_b_scaleX.o" "L_hair_b.sx";
connectAttr "L_hair_b_scaleY.o" "L_hair_b.sy";
connectAttr "L_hair_b_scaleZ.o" "L_hair_b.sz";
connectAttr "C_spine_up.s" "R_shou.is";
connectAttr "R_shou_scaleX.o" "R_shou.sx";
connectAttr "R_shou_scaleY.o" "R_shou.sy";
connectAttr "R_shou_scaleZ.o" "R_shou.sz";
connectAttr "R_shou_visibility.o" "R_shou.v";
connectAttr "R_shou_translateX.o" "R_shou.tx";
connectAttr "R_shou_translateY.o" "R_shou.ty";
connectAttr "R_shou_translateZ.o" "R_shou.tz";
connectAttr "R_shou_rotateX.o" "R_shou.rx";
connectAttr "R_shou_rotateY.o" "R_shou.ry";
connectAttr "R_shou_rotateZ.o" "R_shou.rz";
connectAttr "R_arm_translateX.o" "R_arm.tx";
connectAttr "R_arm_translateY.o" "R_arm.ty";
connectAttr "R_arm_translateZ.o" "R_arm.tz";
connectAttr "R_shou.s" "R_arm.is";
connectAttr "R_arm_scaleX.o" "R_arm.sx";
connectAttr "R_arm_scaleY.o" "R_arm.sy";
connectAttr "R_arm_scaleZ.o" "R_arm.sz";
connectAttr "R_arm_visibility.o" "R_arm.v";
connectAttr "R_arm_rotateX.o" "R_arm.rx";
connectAttr "R_arm_rotateY.o" "R_arm.ry";
connectAttr "R_arm_rotateZ.o" "R_arm.rz";
connectAttr "R_arm.s" "R_forearm.is";
connectAttr "R_forearm_scaleX.o" "R_forearm.sx";
connectAttr "R_forearm_scaleY.o" "R_forearm.sy";
connectAttr "R_forearm_scaleZ.o" "R_forearm.sz";
connectAttr "R_forearm_visibility.o" "R_forearm.v";
connectAttr "R_forearm_translateX.o" "R_forearm.tx";
connectAttr "R_forearm_translateY.o" "R_forearm.ty";
connectAttr "R_forearm_translateZ.o" "R_forearm.tz";
connectAttr "R_forearm_rotateX.o" "R_forearm.rx";
connectAttr "R_forearm_rotateY.o" "R_forearm.ry";
connectAttr "R_forearm_rotateZ.o" "R_forearm.rz";
connectAttr "R_forearm.s" "R_hand.is";
connectAttr "R_hand_orientConstraint1.crx" "R_hand.rx";
connectAttr "R_hand_orientConstraint1.cry" "R_hand.ry";
connectAttr "R_hand_orientConstraint1.crz" "R_hand.rz";
connectAttr "R_hand.s" "R_little_up.is";
connectAttr "R_little_up_scaleX.o" "R_little_up.sx";
connectAttr "R_little_up_scaleY.o" "R_little_up.sy";
connectAttr "R_little_up_scaleZ.o" "R_little_up.sz";
connectAttr "R_little_up_rotateZ.o" "R_little_up.rz";
connectAttr "R_little_up_rotateX.o" "R_little_up.rx";
connectAttr "R_little_up_rotateY.o" "R_little_up.ry";
connectAttr "R_little_up_visibility.o" "R_little_up.v";
connectAttr "R_little_up_translateX.o" "R_little_up.tx";
connectAttr "R_little_up_translateY.o" "R_little_up.ty";
connectAttr "R_little_up_translateZ.o" "R_little_up.tz";
connectAttr "R_little_up.s" "R_little_mid.is";
connectAttr "R_little_mid_scaleX.o" "R_little_mid.sx";
connectAttr "R_little_mid_scaleY.o" "R_little_mid.sy";
connectAttr "R_little_mid_scaleZ.o" "R_little_mid.sz";
connectAttr "R_little_mid_rotateX.o" "R_little_mid.rx";
connectAttr "R_little_mid_rotateY.o" "R_little_mid.ry";
connectAttr "R_little_mid_rotateZ.o" "R_little_mid.rz";
connectAttr "R_little_mid_visibility.o" "R_little_mid.v";
connectAttr "R_little_mid_translateX.o" "R_little_mid.tx";
connectAttr "R_little_mid_translateY.o" "R_little_mid.ty";
connectAttr "R_little_mid_translateZ.o" "R_little_mid.tz";
connectAttr "R_little_mid.s" "R_little_low.is";
connectAttr "R_little_low_rotateZ.o" "R_little_low.rz";
connectAttr "R_little_low_rotateX.o" "R_little_low.rx";
connectAttr "R_little_low_rotateY.o" "R_little_low.ry";
connectAttr "R_little_low_visibility.o" "R_little_low.v";
connectAttr "R_little_low_translateX.o" "R_little_low.tx";
connectAttr "R_little_low_translateY.o" "R_little_low.ty";
connectAttr "R_little_low_translateZ.o" "R_little_low.tz";
connectAttr "R_little_low_scaleX.o" "R_little_low.sx";
connectAttr "R_little_low_scaleY.o" "R_little_low.sy";
connectAttr "R_little_low_scaleZ.o" "R_little_low.sz";
connectAttr "R_hand.s" "R_ring_up.is";
connectAttr "R_ring_up_scaleX.o" "R_ring_up.sx";
connectAttr "R_ring_up_scaleY.o" "R_ring_up.sy";
connectAttr "R_ring_up_scaleZ.o" "R_ring_up.sz";
connectAttr "R_ring_up_rotateX.o" "R_ring_up.rx";
connectAttr "R_ring_up_rotateY.o" "R_ring_up.ry";
connectAttr "R_ring_up_rotateZ.o" "R_ring_up.rz";
connectAttr "R_ring_up_visibility.o" "R_ring_up.v";
connectAttr "R_ring_up_translateX.o" "R_ring_up.tx";
connectAttr "R_ring_up_translateY.o" "R_ring_up.ty";
connectAttr "R_ring_up_translateZ.o" "R_ring_up.tz";
connectAttr "R_ring_up.s" "R_ring_mid.is";
connectAttr "R_ring_mid_scaleX.o" "R_ring_mid.sx";
connectAttr "R_ring_mid_scaleY.o" "R_ring_mid.sy";
connectAttr "R_ring_mid_scaleZ.o" "R_ring_mid.sz";
connectAttr "R_ring_mid_rotateX.o" "R_ring_mid.rx";
connectAttr "R_ring_mid_rotateY.o" "R_ring_mid.ry";
connectAttr "R_ring_mid_rotateZ.o" "R_ring_mid.rz";
connectAttr "R_ring_mid_visibility.o" "R_ring_mid.v";
connectAttr "R_ring_mid_translateX.o" "R_ring_mid.tx";
connectAttr "R_ring_mid_translateY.o" "R_ring_mid.ty";
connectAttr "R_ring_mid_translateZ.o" "R_ring_mid.tz";
connectAttr "R_ring_mid.s" "R_ring_low.is";
connectAttr "R_ring_low_rotateY.o" "R_ring_low.ry";
connectAttr "R_ring_low_rotateZ.o" "R_ring_low.rz";
connectAttr "R_ring_low_rotateX.o" "R_ring_low.rx";
connectAttr "R_ring_low_visibility.o" "R_ring_low.v";
connectAttr "R_ring_low_translateX.o" "R_ring_low.tx";
connectAttr "R_ring_low_translateY.o" "R_ring_low.ty";
connectAttr "R_ring_low_translateZ.o" "R_ring_low.tz";
connectAttr "R_ring_low_scaleX.o" "R_ring_low.sx";
connectAttr "R_ring_low_scaleY.o" "R_ring_low.sy";
connectAttr "R_ring_low_scaleZ.o" "R_ring_low.sz";
connectAttr "R_hand.s" "R_mid_up.is";
connectAttr "R_mid_up_scaleX.o" "R_mid_up.sx";
connectAttr "R_mid_up_scaleY.o" "R_mid_up.sy";
connectAttr "R_mid_up_scaleZ.o" "R_mid_up.sz";
connectAttr "R_mid_up_rotateX.o" "R_mid_up.rx";
connectAttr "R_mid_up_rotateY.o" "R_mid_up.ry";
connectAttr "R_mid_up_rotateZ.o" "R_mid_up.rz";
connectAttr "R_mid_up_visibility.o" "R_mid_up.v";
connectAttr "R_mid_up_translateX.o" "R_mid_up.tx";
connectAttr "R_mid_up_translateY.o" "R_mid_up.ty";
connectAttr "R_mid_up_translateZ.o" "R_mid_up.tz";
connectAttr "R_mid_up.s" "R_mid_mid.is";
connectAttr "R_mid_mid_scaleX.o" "R_mid_mid.sx";
connectAttr "R_mid_mid_scaleY.o" "R_mid_mid.sy";
connectAttr "R_mid_mid_scaleZ.o" "R_mid_mid.sz";
connectAttr "R_mid_mid_visibility.o" "R_mid_mid.v";
connectAttr "R_mid_mid_translateX.o" "R_mid_mid.tx";
connectAttr "R_mid_mid_translateY.o" "R_mid_mid.ty";
connectAttr "R_mid_mid_translateZ.o" "R_mid_mid.tz";
connectAttr "R_mid_mid_rotateX.o" "R_mid_mid.rx";
connectAttr "R_mid_mid_rotateY.o" "R_mid_mid.ry";
connectAttr "R_mid_mid_rotateZ.o" "R_mid_mid.rz";
connectAttr "R_mid_mid.s" "R_mid_low.is";
connectAttr "R_mid_low_rotateZ.o" "R_mid_low.rz";
connectAttr "R_mid_low_rotateX.o" "R_mid_low.rx";
connectAttr "R_mid_low_rotateY.o" "R_mid_low.ry";
connectAttr "R_mid_low_visibility.o" "R_mid_low.v";
connectAttr "R_mid_low_translateX.o" "R_mid_low.tx";
connectAttr "R_mid_low_translateY.o" "R_mid_low.ty";
connectAttr "R_mid_low_translateZ.o" "R_mid_low.tz";
connectAttr "R_mid_low_scaleX.o" "R_mid_low.sx";
connectAttr "R_mid_low_scaleY.o" "R_mid_low.sy";
connectAttr "R_mid_low_scaleZ.o" "R_mid_low.sz";
connectAttr "R_hand.s" "R_index_up.is";
connectAttr "R_index_up_scaleX.o" "R_index_up.sx";
connectAttr "R_index_up_scaleY.o" "R_index_up.sy";
connectAttr "R_index_up_scaleZ.o" "R_index_up.sz";
connectAttr "R_index_up_visibility.o" "R_index_up.v";
connectAttr "R_index_up_translateX.o" "R_index_up.tx";
connectAttr "R_index_up_translateY.o" "R_index_up.ty";
connectAttr "R_index_up_translateZ.o" "R_index_up.tz";
connectAttr "R_index_up_rotateX.o" "R_index_up.rx";
connectAttr "R_index_up_rotateY.o" "R_index_up.ry";
connectAttr "R_index_up_rotateZ.o" "R_index_up.rz";
connectAttr "R_index_up.s" "R_index_mid.is";
connectAttr "R_index_mid_scaleX.o" "R_index_mid.sx";
connectAttr "R_index_mid_scaleY.o" "R_index_mid.sy";
connectAttr "R_index_mid_scaleZ.o" "R_index_mid.sz";
connectAttr "R_index_mid_rotateZ.o" "R_index_mid.rz";
connectAttr "R_index_mid_rotateX.o" "R_index_mid.rx";
connectAttr "R_index_mid_rotateY.o" "R_index_mid.ry";
connectAttr "R_index_mid_visibility.o" "R_index_mid.v";
connectAttr "R_index_mid_translateX.o" "R_index_mid.tx";
connectAttr "R_index_mid_translateY.o" "R_index_mid.ty";
connectAttr "R_index_mid_translateZ.o" "R_index_mid.tz";
connectAttr "R_index_mid.s" "R_index3_low.is";
connectAttr "R_index3_low_rotateZ.o" "R_index3_low.rz";
connectAttr "R_index3_low_rotateX.o" "R_index3_low.rx";
connectAttr "R_index3_low_rotateY.o" "R_index3_low.ry";
connectAttr "R_index3_low_visibility.o" "R_index3_low.v";
connectAttr "R_index3_low_translateX.o" "R_index3_low.tx";
connectAttr "R_index3_low_translateY.o" "R_index3_low.ty";
connectAttr "R_index3_low_translateZ.o" "R_index3_low.tz";
connectAttr "R_index3_low_scaleX.o" "R_index3_low.sx";
connectAttr "R_index3_low_scaleY.o" "R_index3_low.sy";
connectAttr "R_index3_low_scaleZ.o" "R_index3_low.sz";
connectAttr "R_hand.s" "R_thumb_up.is";
connectAttr "R_thumb_up_scaleX.o" "R_thumb_up.sx";
connectAttr "R_thumb_up_scaleY.o" "R_thumb_up.sy";
connectAttr "R_thumb_up_scaleZ.o" "R_thumb_up.sz";
connectAttr "R_thumb_up_visibility.o" "R_thumb_up.v";
connectAttr "R_thumb_up_translateX.o" "R_thumb_up.tx";
connectAttr "R_thumb_up_translateY.o" "R_thumb_up.ty";
connectAttr "R_thumb_up_translateZ.o" "R_thumb_up.tz";
connectAttr "R_thumb_up_rotateX.o" "R_thumb_up.rx";
connectAttr "R_thumb_up_rotateY.o" "R_thumb_up.ry";
connectAttr "R_thumb_up_rotateZ.o" "R_thumb_up.rz";
connectAttr "R_thumb_up.s" "R_thumb_mid.is";
connectAttr "R_thumb_mid_visibility.o" "R_thumb_mid.v";
connectAttr "R_thumb_mid_translateX.o" "R_thumb_mid.tx";
connectAttr "R_thumb_mid_translateY.o" "R_thumb_mid.ty";
connectAttr "R_thumb_mid_translateZ.o" "R_thumb_mid.tz";
connectAttr "R_thumb_mid_rotateX.o" "R_thumb_mid.rx";
connectAttr "R_thumb_mid_rotateY.o" "R_thumb_mid.ry";
connectAttr "R_thumb_mid_rotateZ.o" "R_thumb_mid.rz";
connectAttr "R_thumb_mid_scaleX.o" "R_thumb_mid.sx";
connectAttr "R_thumb_mid_scaleY.o" "R_thumb_mid.sy";
connectAttr "R_thumb_mid_scaleZ.o" "R_thumb_mid.sz";
connectAttr "R_hand.ro" "R_hand_orientConstraint1.cro";
connectAttr "R_hand.pim" "R_hand_orientConstraint1.cpim";
connectAttr "R_hand.jo" "R_hand_orientConstraint1.cjo";
connectAttr "R_handCTR.r" "R_hand_orientConstraint1.tg[0].tr";
connectAttr "R_handCTR.ro" "R_hand_orientConstraint1.tg[0].tro";
connectAttr "R_handCTR.pm" "R_hand_orientConstraint1.tg[0].tpm";
connectAttr "R_hand_orientConstraint1.w0" "R_hand_orientConstraint1.tg[0].tw";
connectAttr "R_hand.s" "sword_jnt.is";
connectAttr "weapons_jnt_R_rotateX.o" "sword_jnt.rx";
connectAttr "weapons_jnt_R_rotateY.o" "sword_jnt.ry";
connectAttr "weapons_jnt_R_rotateZ.o" "sword_jnt.rz";
connectAttr "weapons_jnt_R_translateX.o" "sword_jnt.tx";
connectAttr "weapons_jnt_R_translateY.o" "sword_jnt.ty";
connectAttr "weapons_jnt_R_translateZ.o" "sword_jnt.tz";
connectAttr "weapons_jnt_R_visibility.o" "sword_jnt.v";
connectAttr "weapons_jnt_R_scaleX.o" "sword_jnt.sx";
connectAttr "weapons_jnt_R_scaleY.o" "sword_jnt.sy";
connectAttr "weapons_jnt_R_scaleZ.o" "sword_jnt.sz";
connectAttr "R_hand.tx" "effector1.tx";
connectAttr "R_hand.ty" "effector1.ty";
connectAttr "R_hand.tz" "effector1.tz";
connectAttr "C_spine_up.s" "L_shou.is";
connectAttr "L_shou_scaleX.o" "L_shou.sx";
connectAttr "L_shou_scaleY.o" "L_shou.sy";
connectAttr "L_shou_scaleZ.o" "L_shou.sz";
connectAttr "L_shou_visibility.o" "L_shou.v";
connectAttr "L_shou_translateX.o" "L_shou.tx";
connectAttr "L_shou_translateY.o" "L_shou.ty";
connectAttr "L_shou_translateZ.o" "L_shou.tz";
connectAttr "L_shou_rotateX.o" "L_shou.rx";
connectAttr "L_shou_rotateY.o" "L_shou.ry";
connectAttr "L_shou_rotateZ.o" "L_shou.rz";
connectAttr "L_arm_translateX.o" "L_arm.tx";
connectAttr "L_arm_translateY.o" "L_arm.ty";
connectAttr "L_arm_translateZ.o" "L_arm.tz";
connectAttr "L_shou.s" "L_arm.is";
connectAttr "L_arm_scaleX.o" "L_arm.sx";
connectAttr "L_arm_scaleY.o" "L_arm.sy";
connectAttr "L_arm_scaleZ.o" "L_arm.sz";
connectAttr "L_arm_visibility.o" "L_arm.v";
connectAttr "L_arm_rotateX.o" "L_arm.rx";
connectAttr "L_arm_rotateY.o" "L_arm.ry";
connectAttr "L_arm_rotateZ.o" "L_arm.rz";
connectAttr "L_arm.s" "L_forearm.is";
connectAttr "L_forearm_scaleX.o" "L_forearm.sx";
connectAttr "L_forearm_scaleY.o" "L_forearm.sy";
connectAttr "L_forearm_scaleZ.o" "L_forearm.sz";
connectAttr "L_forearm_visibility.o" "L_forearm.v";
connectAttr "L_forearm_translateX.o" "L_forearm.tx";
connectAttr "L_forearm_translateY.o" "L_forearm.ty";
connectAttr "L_forearm_translateZ.o" "L_forearm.tz";
connectAttr "L_forearm_rotateX.o" "L_forearm.rx";
connectAttr "L_forearm_rotateY.o" "L_forearm.ry";
connectAttr "L_forearm_rotateZ.o" "L_forearm.rz";
connectAttr "L_forearm.s" "L_hand.is";
connectAttr "L_hand_orientConstraint1.crx" "L_hand.rx";
connectAttr "L_hand_orientConstraint1.cry" "L_hand.ry";
connectAttr "L_hand_orientConstraint1.crz" "L_hand.rz";
connectAttr "L_hand.s" "L_little_up.is";
connectAttr "L_little_up_scaleX.o" "L_little_up.sx";
connectAttr "L_little_up_scaleY.o" "L_little_up.sy";
connectAttr "L_little_up_scaleZ.o" "L_little_up.sz";
connectAttr "L_little_up_visibility.o" "L_little_up.v";
connectAttr "L_little_up_translateX.o" "L_little_up.tx";
connectAttr "L_little_up_translateY.o" "L_little_up.ty";
connectAttr "L_little_up_translateZ.o" "L_little_up.tz";
connectAttr "L_little_up_rotateX.o" "L_little_up.rx";
connectAttr "L_little_up_rotateY.o" "L_little_up.ry";
connectAttr "L_little_up_rotateZ.o" "L_little_up.rz";
connectAttr "L_little_up.s" "L_little_mid.is";
connectAttr "L_little_mid_scaleX.o" "L_little_mid.sx";
connectAttr "L_little_mid_scaleY.o" "L_little_mid.sy";
connectAttr "L_little_mid_scaleZ.o" "L_little_mid.sz";
connectAttr "L_little_mid_visibility.o" "L_little_mid.v";
connectAttr "L_little_mid_translateX.o" "L_little_mid.tx";
connectAttr "L_little_mid_translateY.o" "L_little_mid.ty";
connectAttr "L_little_mid_translateZ.o" "L_little_mid.tz";
connectAttr "L_little_mid_rotateX.o" "L_little_mid.rx";
connectAttr "L_little_mid_rotateY.o" "L_little_mid.ry";
connectAttr "L_little_mid_rotateZ.o" "L_little_mid.rz";
connectAttr "L_little_mid.s" "L_little_low.is";
connectAttr "L_little_low_visibility.o" "L_little_low.v";
connectAttr "L_little_low_translateX.o" "L_little_low.tx";
connectAttr "L_little_low_translateY.o" "L_little_low.ty";
connectAttr "L_little_low_translateZ.o" "L_little_low.tz";
connectAttr "L_little_low_rotateX.o" "L_little_low.rx";
connectAttr "L_little_low_rotateY.o" "L_little_low.ry";
connectAttr "L_little_low_rotateZ.o" "L_little_low.rz";
connectAttr "L_little_low_scaleX.o" "L_little_low.sx";
connectAttr "L_little_low_scaleY.o" "L_little_low.sy";
connectAttr "L_little_low_scaleZ.o" "L_little_low.sz";
connectAttr "L_hand.s" "L_ring_up.is";
connectAttr "L_ring_up_scaleX.o" "L_ring_up.sx";
connectAttr "L_ring_up_scaleY.o" "L_ring_up.sy";
connectAttr "L_ring_up_scaleZ.o" "L_ring_up.sz";
connectAttr "L_ring_up_visibility.o" "L_ring_up.v";
connectAttr "L_ring_up_translateX.o" "L_ring_up.tx";
connectAttr "L_ring_up_translateY.o" "L_ring_up.ty";
connectAttr "L_ring_up_translateZ.o" "L_ring_up.tz";
connectAttr "L_ring_up_rotateX.o" "L_ring_up.rx";
connectAttr "L_ring_up_rotateY.o" "L_ring_up.ry";
connectAttr "L_ring_up_rotateZ.o" "L_ring_up.rz";
connectAttr "L_ring_up.s" "L_ring_mid.is";
connectAttr "L_ring_mid_scaleX.o" "L_ring_mid.sx";
connectAttr "L_ring_mid_scaleY.o" "L_ring_mid.sy";
connectAttr "L_ring_mid_scaleZ.o" "L_ring_mid.sz";
connectAttr "L_ring_mid_visibility.o" "L_ring_mid.v";
connectAttr "L_ring_mid_translateX.o" "L_ring_mid.tx";
connectAttr "L_ring_mid_translateY.o" "L_ring_mid.ty";
connectAttr "L_ring_mid_translateZ.o" "L_ring_mid.tz";
connectAttr "L_ring_mid_rotateX.o" "L_ring_mid.rx";
connectAttr "L_ring_mid_rotateY.o" "L_ring_mid.ry";
connectAttr "L_ring_mid_rotateZ.o" "L_ring_mid.rz";
connectAttr "L_ring_mid.s" "L_ring_low.is";
connectAttr "L_ring_low_visibility.o" "L_ring_low.v";
connectAttr "L_ring_low_translateX.o" "L_ring_low.tx";
connectAttr "L_ring_low_translateY.o" "L_ring_low.ty";
connectAttr "L_ring_low_translateZ.o" "L_ring_low.tz";
connectAttr "L_ring_low_rotateX.o" "L_ring_low.rx";
connectAttr "L_ring_low_rotateY.o" "L_ring_low.ry";
connectAttr "L_ring_low_rotateZ.o" "L_ring_low.rz";
connectAttr "L_ring_low_scaleX.o" "L_ring_low.sx";
connectAttr "L_ring_low_scaleY.o" "L_ring_low.sy";
connectAttr "L_ring_low_scaleZ.o" "L_ring_low.sz";
connectAttr "L_hand.s" "L_mid_up.is";
connectAttr "L_mid_up_scaleX.o" "L_mid_up.sx";
connectAttr "L_mid_up_scaleY.o" "L_mid_up.sy";
connectAttr "L_mid_up_scaleZ.o" "L_mid_up.sz";
connectAttr "L_mid_up_visibility.o" "L_mid_up.v";
connectAttr "L_mid_up_translateX.o" "L_mid_up.tx";
connectAttr "L_mid_up_translateY.o" "L_mid_up.ty";
connectAttr "L_mid_up_translateZ.o" "L_mid_up.tz";
connectAttr "L_mid_up_rotateX.o" "L_mid_up.rx";
connectAttr "L_mid_up_rotateY.o" "L_mid_up.ry";
connectAttr "L_mid_up_rotateZ.o" "L_mid_up.rz";
connectAttr "L_mid_up.s" "L_mid_mid.is";
connectAttr "L_mid_mid_scaleX.o" "L_mid_mid.sx";
connectAttr "L_mid_mid_scaleY.o" "L_mid_mid.sy";
connectAttr "L_mid_mid_scaleZ.o" "L_mid_mid.sz";
connectAttr "L_mid_mid_visibility.o" "L_mid_mid.v";
connectAttr "L_mid_mid_translateX.o" "L_mid_mid.tx";
connectAttr "L_mid_mid_translateY.o" "L_mid_mid.ty";
connectAttr "L_mid_mid_translateZ.o" "L_mid_mid.tz";
connectAttr "L_mid_mid_rotateX.o" "L_mid_mid.rx";
connectAttr "L_mid_mid_rotateY.o" "L_mid_mid.ry";
connectAttr "L_mid_mid_rotateZ.o" "L_mid_mid.rz";
connectAttr "L_mid_mid.s" "L_mid_low.is";
connectAttr "L_mid_low_visibility.o" "L_mid_low.v";
connectAttr "L_mid_low_translateX.o" "L_mid_low.tx";
connectAttr "L_mid_low_translateY.o" "L_mid_low.ty";
connectAttr "L_mid_low_translateZ.o" "L_mid_low.tz";
connectAttr "L_mid_low_rotateX.o" "L_mid_low.rx";
connectAttr "L_mid_low_rotateY.o" "L_mid_low.ry";
connectAttr "L_mid_low_rotateZ.o" "L_mid_low.rz";
connectAttr "L_mid_low_scaleX.o" "L_mid_low.sx";
connectAttr "L_mid_low_scaleY.o" "L_mid_low.sy";
connectAttr "L_mid_low_scaleZ.o" "L_mid_low.sz";
connectAttr "L_hand.s" "L_index_up.is";
connectAttr "L_index_up_scaleX.o" "L_index_up.sx";
connectAttr "L_index_up_scaleY.o" "L_index_up.sy";
connectAttr "L_index_up_scaleZ.o" "L_index_up.sz";
connectAttr "L_index_up_visibility.o" "L_index_up.v";
connectAttr "L_index_up_translateX.o" "L_index_up.tx";
connectAttr "L_index_up_translateY.o" "L_index_up.ty";
connectAttr "L_index_up_translateZ.o" "L_index_up.tz";
connectAttr "L_index_up_rotateX.o" "L_index_up.rx";
connectAttr "L_index_up_rotateY.o" "L_index_up.ry";
connectAttr "L_index_up_rotateZ.o" "L_index_up.rz";
connectAttr "L_index_up.s" "L_index_mid.is";
connectAttr "L_index_mid_scaleX.o" "L_index_mid.sx";
connectAttr "L_index_mid_scaleY.o" "L_index_mid.sy";
connectAttr "L_index_mid_scaleZ.o" "L_index_mid.sz";
connectAttr "L_index_mid_visibility.o" "L_index_mid.v";
connectAttr "L_index_mid_translateX.o" "L_index_mid.tx";
connectAttr "L_index_mid_translateY.o" "L_index_mid.ty";
connectAttr "L_index_mid_translateZ.o" "L_index_mid.tz";
connectAttr "L_index_mid_rotateX.o" "L_index_mid.rx";
connectAttr "L_index_mid_rotateY.o" "L_index_mid.ry";
connectAttr "L_index_mid_rotateZ.o" "L_index_mid.rz";
connectAttr "L_index_mid.s" "L_index_low.is";
connectAttr "L_index_low_visibility.o" "L_index_low.v";
connectAttr "L_index_low_translateX.o" "L_index_low.tx";
connectAttr "L_index_low_translateY.o" "L_index_low.ty";
connectAttr "L_index_low_translateZ.o" "L_index_low.tz";
connectAttr "L_index_low_rotateX.o" "L_index_low.rx";
connectAttr "L_index_low_rotateY.o" "L_index_low.ry";
connectAttr "L_index_low_rotateZ.o" "L_index_low.rz";
connectAttr "L_index_low_scaleX.o" "L_index_low.sx";
connectAttr "L_index_low_scaleY.o" "L_index_low.sy";
connectAttr "L_index_low_scaleZ.o" "L_index_low.sz";
connectAttr "L_hand.s" "L_thumb_up.is";
connectAttr "L_thumb_up_scaleX.o" "L_thumb_up.sx";
connectAttr "L_thumb_up_scaleY.o" "L_thumb_up.sy";
connectAttr "L_thumb_up_scaleZ.o" "L_thumb_up.sz";
connectAttr "L_thumb_up_visibility.o" "L_thumb_up.v";
connectAttr "L_thumb_up_translateX.o" "L_thumb_up.tx";
connectAttr "L_thumb_up_translateY.o" "L_thumb_up.ty";
connectAttr "L_thumb_up_translateZ.o" "L_thumb_up.tz";
connectAttr "L_thumb_up_rotateX.o" "L_thumb_up.rx";
connectAttr "L_thumb_up_rotateY.o" "L_thumb_up.ry";
connectAttr "L_thumb_up_rotateZ.o" "L_thumb_up.rz";
connectAttr "L_thumb_up.s" "L_thumb_mid.is";
connectAttr "L_thumb_mid_visibility.o" "L_thumb_mid.v";
connectAttr "L_thumb_mid_translateX.o" "L_thumb_mid.tx";
connectAttr "L_thumb_mid_translateY.o" "L_thumb_mid.ty";
connectAttr "L_thumb_mid_translateZ.o" "L_thumb_mid.tz";
connectAttr "L_thumb_mid_rotateX.o" "L_thumb_mid.rx";
connectAttr "L_thumb_mid_rotateY.o" "L_thumb_mid.ry";
connectAttr "L_thumb_mid_rotateZ.o" "L_thumb_mid.rz";
connectAttr "L_thumb_mid_scaleX.o" "L_thumb_mid.sx";
connectAttr "L_thumb_mid_scaleY.o" "L_thumb_mid.sy";
connectAttr "L_thumb_mid_scaleZ.o" "L_thumb_mid.sz";
connectAttr "L_hand.ro" "L_hand_orientConstraint1.cro";
connectAttr "L_hand.pim" "L_hand_orientConstraint1.cpim";
connectAttr "L_hand.jo" "L_hand_orientConstraint1.cjo";
connectAttr "L_handCTR.r" "L_hand_orientConstraint1.tg[0].tr";
connectAttr "L_handCTR.ro" "L_hand_orientConstraint1.tg[0].tro";
connectAttr "L_handCTR.pm" "L_hand_orientConstraint1.tg[0].tpm";
connectAttr "L_hand_orientConstraint1.w0" "L_hand_orientConstraint1.tg[0].tw";
connectAttr "L_hand.s" "sword_jnt_02.is";
connectAttr "L_hand.tx" "effector2.tx";
connectAttr "L_hand.ty" "effector2.ty";
connectAttr "L_hand.tz" "effector2.tz";
connectAttr "C_root.pim" "C_root_pointConstraint1.cpim";
connectAttr "C_root.rp" "C_root_pointConstraint1.crp";
connectAttr "C_root.rpt" "C_root_pointConstraint1.crt";
connectAttr "C_pelvisCTR.t" "C_root_pointConstraint1.tg[0].tt";
connectAttr "C_pelvisCTR.rp" "C_root_pointConstraint1.tg[0].trp";
connectAttr "C_pelvisCTR.rpt" "C_root_pointConstraint1.tg[0].trt";
connectAttr "C_pelvisCTR.pm" "C_root_pointConstraint1.tg[0].tpm";
connectAttr "C_root_pointConstraint1.w0" "C_root_pointConstraint1.tg[0].tw";
connectAttr "C_root.ro" "C_root_orientConstraint1.cro";
connectAttr "C_root.pim" "C_root_orientConstraint1.cpim";
connectAttr "C_root.jo" "C_root_orientConstraint1.cjo";
connectAttr "C_pelvisCTR.r" "C_root_orientConstraint1.tg[0].tr";
connectAttr "C_pelvisCTR.ro" "C_root_orientConstraint1.tg[0].tro";
connectAttr "C_pelvisCTR.pm" "C_root_orientConstraint1.tg[0].tpm";
connectAttr "C_root_orientConstraint1.w0" "C_root_orientConstraint1.tg[0].tw";
connectAttr "C_root.pim" "C_root_scaleConstraint1.cpim";
connectAttr "C_pelvisCTR.s" "C_root_scaleConstraint1.tg[0].ts";
connectAttr "C_pelvisCTR.pm" "C_root_scaleConstraint1.tg[0].tpm";
connectAttr "C_root_scaleConstraint1.w0" "C_root_scaleConstraint1.tg[0].tw";
connectAttr "groupId1549.id" "CHAR1_Body1Shape.iog.og[0].gid";
connectAttr "Char1_char1_body_d_tga.mwc" "CHAR1_Body1Shape.iog.og[0].gco";
connectAttr "skinCluster13GroupId.id" "CHAR1_Body1Shape.iog.og[7].gid";
connectAttr "skinCluster13Set.mwc" "CHAR1_Body1Shape.iog.og[7].gco";
connectAttr "groupId1551.id" "CHAR1_Body1Shape.iog.og[8].gid";
connectAttr "tweakSet13.mwc" "CHAR1_Body1Shape.iog.og[8].gco";
connectAttr "skinCluster1.og[0]" "CHAR1_Body1Shape.i";
connectAttr "tweak4.vl[0].vt[0]" "CHAR1_Body1Shape.twl";
connectAttr "R_thigh.iog" "FK_Control.dsm" -na;
connectAttr "R_calf.iog" "FK_Control.dsm" -na;
connectAttr "R_heel.iog" "FK_Control.dsm" -na;
connectAttr "R_foot.iog" "FK_Control.dsm" -na;
connectAttr "L_thigh.iog" "FK_Control.dsm" -na;
connectAttr "L_calf.iog" "FK_Control.dsm" -na;
connectAttr "L_heel.iog" "FK_Control.dsm" -na;
connectAttr "L_foot.iog" "FK_Control.dsm" -na;
connectAttr "C_spine_low.iog" "FK_Control.dsm" -na;
connectAttr "C_spine_mid.iog" "FK_Control.dsm" -na;
connectAttr "C_spine_up.iog" "FK_Control.dsm" -na;
connectAttr "C_neck.iog" "FK_Control.dsm" -na;
connectAttr "C_head.iog" "FK_Control.dsm" -na;
connectAttr "C_hair_a.iog" "FK_Control.dsm" -na;
connectAttr "C_hair_b.iog" "FK_Control.dsm" -na;
connectAttr "R_hair_a.iog" "FK_Control.dsm" -na;
connectAttr "R_hair_b.iog" "FK_Control.dsm" -na;
connectAttr "L_hair_a.iog" "FK_Control.dsm" -na;
connectAttr "L_hair_b.iog" "FK_Control.dsm" -na;
connectAttr "R_shou.iog" "FK_Control.dsm" -na;
connectAttr "R_arm.iog" "FK_Control.dsm" -na;
connectAttr "R_forearm.iog" "FK_Control.dsm" -na;
connectAttr "R_hand.iog" "FK_Control.dsm" -na;
connectAttr "R_little_up.iog" "FK_Control.dsm" -na;
connectAttr "R_little_mid.iog" "FK_Control.dsm" -na;
connectAttr "R_little_low.iog" "FK_Control.dsm" -na;
connectAttr "R_ring_up.iog" "FK_Control.dsm" -na;
connectAttr "R_ring_mid.iog" "FK_Control.dsm" -na;
connectAttr "R_ring_low.iog" "FK_Control.dsm" -na;
connectAttr "R_mid_up.iog" "FK_Control.dsm" -na;
connectAttr "R_mid_mid.iog" "FK_Control.dsm" -na;
connectAttr "R_mid_low.iog" "FK_Control.dsm" -na;
connectAttr "R_index_up.iog" "FK_Control.dsm" -na;
connectAttr "R_index_mid.iog" "FK_Control.dsm" -na;
connectAttr "R_index3_low.iog" "FK_Control.dsm" -na;
connectAttr "R_thumb_up.iog" "FK_Control.dsm" -na;
connectAttr "R_thumb_mid.iog" "FK_Control.dsm" -na;
connectAttr "L_shou.iog" "FK_Control.dsm" -na;
connectAttr "L_arm.iog" "FK_Control.dsm" -na;
connectAttr "L_forearm.iog" "FK_Control.dsm" -na;
connectAttr "L_hand.iog" "FK_Control.dsm" -na;
connectAttr "L_little_up.iog" "FK_Control.dsm" -na;
connectAttr "L_little_mid.iog" "FK_Control.dsm" -na;
connectAttr "L_little_low.iog" "FK_Control.dsm" -na;
connectAttr "L_ring_up.iog" "FK_Control.dsm" -na;
connectAttr "L_ring_mid.iog" "FK_Control.dsm" -na;
connectAttr "L_ring_low.iog" "FK_Control.dsm" -na;
connectAttr "L_mid_up.iog" "FK_Control.dsm" -na;
connectAttr "L_mid_mid.iog" "FK_Control.dsm" -na;
connectAttr "L_mid_low.iog" "FK_Control.dsm" -na;
connectAttr "L_index_up.iog" "FK_Control.dsm" -na;
connectAttr "L_index_mid.iog" "FK_Control.dsm" -na;
connectAttr "L_index_low.iog" "FK_Control.dsm" -na;
connectAttr "L_thumb_up.iog" "FK_Control.dsm" -na;
connectAttr "L_thumb_mid.iog" "FK_Control.dsm" -na;
connectAttr "C_POSITION.iog" "IK_Control.dsm" -na;
connectAttr "C_flyCTR.iog" "IK_Control.dsm" -na;
connectAttr "C_pelvisCTR.iog" "IK_Control.dsm" -na;
connectAttr "C_hipCTR.iog" "IK_Control.dsm" -na;
connectAttr "R_handCTR.iog" "IK_Control.dsm" -na;
connectAttr "L_handCTR.iog" "IK_Control.dsm" -na;
connectAttr "L_legCTR.iog" "IK_Control.dsm" -na;
connectAttr "R_legCTR.iog" "IK_Control.dsm" -na;
connectAttr "C_root.iog" "SkeSkinning.dsm" -na;
connectAttr "C_hip.iog" "SkeSkinning.dsm" -na;
connectAttr "R_thigh.iog" "SkeSkinning.dsm" -na;
connectAttr "R_calf.iog" "SkeSkinning.dsm" -na;
connectAttr "R_heel.iog" "SkeSkinning.dsm" -na;
connectAttr "R_foot.iog" "SkeSkinning.dsm" -na;
connectAttr "L_thigh.iog" "SkeSkinning.dsm" -na;
connectAttr "L_calf.iog" "SkeSkinning.dsm" -na;
connectAttr "L_heel.iog" "SkeSkinning.dsm" -na;
connectAttr "L_foot.iog" "SkeSkinning.dsm" -na;
connectAttr "C_spine_low.iog" "SkeSkinning.dsm" -na;
connectAttr "C_spine_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "C_spine_up.iog" "SkeSkinning.dsm" -na;
connectAttr "C_neck.iog" "SkeSkinning.dsm" -na;
connectAttr "C_head.iog" "SkeSkinning.dsm" -na;
connectAttr "C_hair_a.iog" "SkeSkinning.dsm" -na;
connectAttr "C_hair_b.iog" "SkeSkinning.dsm" -na;
connectAttr "R_hair_a.iog" "SkeSkinning.dsm" -na;
connectAttr "R_hair_b.iog" "SkeSkinning.dsm" -na;
connectAttr "L_hair_a.iog" "SkeSkinning.dsm" -na;
connectAttr "L_hair_b.iog" "SkeSkinning.dsm" -na;
connectAttr "R_shou.iog" "SkeSkinning.dsm" -na;
connectAttr "R_arm.iog" "SkeSkinning.dsm" -na;
connectAttr "R_forearm.iog" "SkeSkinning.dsm" -na;
connectAttr "R_hand.iog" "SkeSkinning.dsm" -na;
connectAttr "R_little_up.iog" "SkeSkinning.dsm" -na;
connectAttr "R_little_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "R_little_low.iog" "SkeSkinning.dsm" -na;
connectAttr "R_ring_up.iog" "SkeSkinning.dsm" -na;
connectAttr "R_ring_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "R_ring_low.iog" "SkeSkinning.dsm" -na;
connectAttr "R_mid_up.iog" "SkeSkinning.dsm" -na;
connectAttr "R_mid_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "R_mid_low.iog" "SkeSkinning.dsm" -na;
connectAttr "R_index_up.iog" "SkeSkinning.dsm" -na;
connectAttr "R_index_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "R_index3_low.iog" "SkeSkinning.dsm" -na;
connectAttr "R_thumb_up.iog" "SkeSkinning.dsm" -na;
connectAttr "R_thumb_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "L_shou.iog" "SkeSkinning.dsm" -na;
connectAttr "L_arm.iog" "SkeSkinning.dsm" -na;
connectAttr "L_forearm.iog" "SkeSkinning.dsm" -na;
connectAttr "L_hand.iog" "SkeSkinning.dsm" -na;
connectAttr "L_little_up.iog" "SkeSkinning.dsm" -na;
connectAttr "L_little_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "L_little_low.iog" "SkeSkinning.dsm" -na;
connectAttr "L_ring_up.iog" "SkeSkinning.dsm" -na;
connectAttr "L_ring_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "L_ring_low.iog" "SkeSkinning.dsm" -na;
connectAttr "L_mid_up.iog" "SkeSkinning.dsm" -na;
connectAttr "L_mid_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "L_mid_low.iog" "SkeSkinning.dsm" -na;
connectAttr "L_index_up.iog" "SkeSkinning.dsm" -na;
connectAttr "L_index_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "L_index_low.iog" "SkeSkinning.dsm" -na;
connectAttr "L_thumb_up.iog" "SkeSkinning.dsm" -na;
connectAttr "L_thumb_mid.iog" "SkeSkinning.dsm" -na;
connectAttr "R_handCTR.twist" "unitConversion1.i";
connectAttr "L_handCTR.twist" "unitConversion2.i";
connectAttr "R_legCTR.twist" "unitConversion3.i";
connectAttr "L_legCTR.twist" "unitConversion4.i";
connectAttr "character_1.msg" "bindPose1.m[0]";
connectAttr "IK_FK.msg" "bindPose1.m[1]";
connectAttr "FK_grp.msg" "bindPose1.m[2]";
connectAttr "C_root.msg" "bindPose1.m[3]";
connectAttr "C_hip.msg" "bindPose1.m[4]";
connectAttr "R_thigh.msg" "bindPose1.m[5]";
connectAttr "R_calf.msg" "bindPose1.m[6]";
connectAttr "R_heel.msg" "bindPose1.m[7]";
connectAttr "R_foot.msg" "bindPose1.m[8]";
connectAttr "L_thigh.msg" "bindPose1.m[9]";
connectAttr "L_calf.msg" "bindPose1.m[10]";
connectAttr "L_heel.msg" "bindPose1.m[11]";
connectAttr "L_foot.msg" "bindPose1.m[12]";
connectAttr "C_spine_low.msg" "bindPose1.m[13]";
connectAttr "C_spine_mid.msg" "bindPose1.m[14]";
connectAttr "C_spine_up.msg" "bindPose1.m[15]";
connectAttr "C_neck.msg" "bindPose1.m[16]";
connectAttr "C_head.msg" "bindPose1.m[17]";
connectAttr "C_hair_a.msg" "bindPose1.m[18]";
connectAttr "C_hair_b.msg" "bindPose1.m[19]";
connectAttr "R_shou.msg" "bindPose1.m[20]";
connectAttr "R_arm.msg" "bindPose1.m[21]";
connectAttr "R_forearm.msg" "bindPose1.m[22]";
connectAttr "R_hand.msg" "bindPose1.m[23]";
connectAttr "R_little_up.msg" "bindPose1.m[24]";
connectAttr "R_little_mid.msg" "bindPose1.m[25]";
connectAttr "R_little_low.msg" "bindPose1.m[26]";
connectAttr "R_ring_up.msg" "bindPose1.m[27]";
connectAttr "R_ring_mid.msg" "bindPose1.m[28]";
connectAttr "R_ring_low.msg" "bindPose1.m[29]";
connectAttr "R_mid_up.msg" "bindPose1.m[30]";
connectAttr "R_mid_mid.msg" "bindPose1.m[31]";
connectAttr "R_mid_low.msg" "bindPose1.m[32]";
connectAttr "R_index_up.msg" "bindPose1.m[33]";
connectAttr "R_index_mid.msg" "bindPose1.m[34]";
connectAttr "R_index3_low.msg" "bindPose1.m[35]";
connectAttr "R_thumb_up.msg" "bindPose1.m[36]";
connectAttr "R_thumb_mid.msg" "bindPose1.m[37]";
connectAttr "L_shou.msg" "bindPose1.m[38]";
connectAttr "L_arm.msg" "bindPose1.m[39]";
connectAttr "L_forearm.msg" "bindPose1.m[40]";
connectAttr "L_hand.msg" "bindPose1.m[41]";
connectAttr "L_little_up.msg" "bindPose1.m[42]";
connectAttr "L_little_mid.msg" "bindPose1.m[43]";
connectAttr "L_little_low.msg" "bindPose1.m[44]";
connectAttr "L_ring_up.msg" "bindPose1.m[45]";
connectAttr "L_ring_mid.msg" "bindPose1.m[46]";
connectAttr "L_ring_low.msg" "bindPose1.m[47]";
connectAttr "L_mid_up.msg" "bindPose1.m[48]";
connectAttr "L_mid_mid.msg" "bindPose1.m[49]";
connectAttr "L_mid_low.msg" "bindPose1.m[50]";
connectAttr "L_index_up.msg" "bindPose1.m[51]";
connectAttr "L_index_mid.msg" "bindPose1.m[52]";
connectAttr "L_thumb_up.msg" "bindPose1.m[53]";
connectAttr "L_index_low.msg" "bindPose1.m[54]";
connectAttr "L_thumb_mid.msg" "bindPose1.m[55]";
connectAttr "R_hair_a.msg" "bindPose1.m[56]";
connectAttr "L_hair_a.msg" "bindPose1.m[58]";
connectAttr "R_hair_b.msg" "bindPose1.m[59]";
connectAttr "L_hair_b.msg" "bindPose1.m[60]";
connectAttr "SETUP.msg" "bindPose1.m[61]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[61]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[4]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[6]" "bindPose1.p[7]";
connectAttr "bindPose1.m[7]" "bindPose1.p[8]";
connectAttr "bindPose1.m[4]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[10]" "bindPose1.p[11]";
connectAttr "bindPose1.m[11]" "bindPose1.p[12]";
connectAttr "bindPose1.m[3]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "bindPose1.m[14]" "bindPose1.p[15]";
connectAttr "bindPose1.m[15]" "bindPose1.p[16]";
connectAttr "bindPose1.m[16]" "bindPose1.p[17]";
connectAttr "bindPose1.m[17]" "bindPose1.p[18]";
connectAttr "bindPose1.m[18]" "bindPose1.p[19]";
connectAttr "bindPose1.m[15]" "bindPose1.p[20]";
connectAttr "bindPose1.m[20]" "bindPose1.p[21]";
connectAttr "bindPose1.m[21]" "bindPose1.p[22]";
connectAttr "bindPose1.m[22]" "bindPose1.p[23]";
connectAttr "bindPose1.m[23]" "bindPose1.p[24]";
connectAttr "bindPose1.m[24]" "bindPose1.p[25]";
connectAttr "bindPose1.m[25]" "bindPose1.p[26]";
connectAttr "bindPose1.m[23]" "bindPose1.p[27]";
connectAttr "bindPose1.m[27]" "bindPose1.p[28]";
connectAttr "bindPose1.m[28]" "bindPose1.p[29]";
connectAttr "bindPose1.m[23]" "bindPose1.p[30]";
connectAttr "bindPose1.m[30]" "bindPose1.p[31]";
connectAttr "bindPose1.m[31]" "bindPose1.p[32]";
connectAttr "bindPose1.m[23]" "bindPose1.p[33]";
connectAttr "bindPose1.m[33]" "bindPose1.p[34]";
connectAttr "bindPose1.m[34]" "bindPose1.p[35]";
connectAttr "bindPose1.m[23]" "bindPose1.p[36]";
connectAttr "bindPose1.m[36]" "bindPose1.p[37]";
connectAttr "bindPose1.m[15]" "bindPose1.p[38]";
connectAttr "bindPose1.m[38]" "bindPose1.p[39]";
connectAttr "bindPose1.m[39]" "bindPose1.p[40]";
connectAttr "bindPose1.m[40]" "bindPose1.p[41]";
connectAttr "bindPose1.m[41]" "bindPose1.p[42]";
connectAttr "bindPose1.m[42]" "bindPose1.p[43]";
connectAttr "bindPose1.m[43]" "bindPose1.p[44]";
connectAttr "bindPose1.m[41]" "bindPose1.p[45]";
connectAttr "bindPose1.m[45]" "bindPose1.p[46]";
connectAttr "bindPose1.m[46]" "bindPose1.p[47]";
connectAttr "bindPose1.m[41]" "bindPose1.p[48]";
connectAttr "bindPose1.m[48]" "bindPose1.p[49]";
connectAttr "bindPose1.m[49]" "bindPose1.p[50]";
connectAttr "bindPose1.m[41]" "bindPose1.p[51]";
connectAttr "bindPose1.m[51]" "bindPose1.p[52]";
connectAttr "bindPose1.m[41]" "bindPose1.p[53]";
connectAttr "bindPose1.m[52]" "bindPose1.p[54]";
connectAttr "bindPose1.m[53]" "bindPose1.p[55]";
connectAttr "bindPose1.m[17]" "bindPose1.p[56]";
connectAttr "bindPose1.m[17]" "bindPose1.p[58]";
connectAttr "bindPose1.m[56]" "bindPose1.p[59]";
connectAttr "bindPose1.m[58]" "bindPose1.p[60]";
connectAttr "bindPose1.m[0]" "bindPose1.p[61]";
connectAttr "C_root.bps" "bindPose1.wm[3]";
connectAttr "C_hip.bps" "bindPose1.wm[4]";
connectAttr "R_thigh.bps" "bindPose1.wm[5]";
connectAttr "R_calf.bps" "bindPose1.wm[6]";
connectAttr "R_heel.bps" "bindPose1.wm[7]";
connectAttr "R_foot.bps" "bindPose1.wm[8]";
connectAttr "L_thigh.bps" "bindPose1.wm[9]";
connectAttr "L_calf.bps" "bindPose1.wm[10]";
connectAttr "L_heel.bps" "bindPose1.wm[11]";
connectAttr "L_foot.bps" "bindPose1.wm[12]";
connectAttr "C_spine_low.bps" "bindPose1.wm[13]";
connectAttr "C_spine_mid.bps" "bindPose1.wm[14]";
connectAttr "C_spine_up.bps" "bindPose1.wm[15]";
connectAttr "C_neck.bps" "bindPose1.wm[16]";
connectAttr "C_head.bps" "bindPose1.wm[17]";
connectAttr "C_hair_a.bps" "bindPose1.wm[18]";
connectAttr "C_hair_b.bps" "bindPose1.wm[19]";
connectAttr "R_shou.bps" "bindPose1.wm[20]";
connectAttr "R_arm.bps" "bindPose1.wm[21]";
connectAttr "R_forearm.bps" "bindPose1.wm[22]";
connectAttr "R_hand.bps" "bindPose1.wm[23]";
connectAttr "R_little_up.bps" "bindPose1.wm[24]";
connectAttr "R_little_mid.bps" "bindPose1.wm[25]";
connectAttr "R_little_low.bps" "bindPose1.wm[26]";
connectAttr "R_ring_up.bps" "bindPose1.wm[27]";
connectAttr "R_ring_mid.bps" "bindPose1.wm[28]";
connectAttr "R_ring_low.bps" "bindPose1.wm[29]";
connectAttr "R_mid_up.bps" "bindPose1.wm[30]";
connectAttr "R_mid_mid.bps" "bindPose1.wm[31]";
connectAttr "R_mid_low.bps" "bindPose1.wm[32]";
connectAttr "R_index_up.bps" "bindPose1.wm[33]";
connectAttr "R_index_mid.bps" "bindPose1.wm[34]";
connectAttr "R_index3_low.bps" "bindPose1.wm[35]";
connectAttr "R_thumb_up.bps" "bindPose1.wm[36]";
connectAttr "R_thumb_mid.bps" "bindPose1.wm[37]";
connectAttr "L_shou.bps" "bindPose1.wm[38]";
connectAttr "L_arm.bps" "bindPose1.wm[39]";
connectAttr "L_forearm.bps" "bindPose1.wm[40]";
connectAttr "L_hand.bps" "bindPose1.wm[41]";
connectAttr "L_little_up.bps" "bindPose1.wm[42]";
connectAttr "L_little_mid.bps" "bindPose1.wm[43]";
connectAttr "L_little_low.bps" "bindPose1.wm[44]";
connectAttr "L_ring_up.bps" "bindPose1.wm[45]";
connectAttr "L_ring_mid.bps" "bindPose1.wm[46]";
connectAttr "L_ring_low.bps" "bindPose1.wm[47]";
connectAttr "L_mid_up.bps" "bindPose1.wm[48]";
connectAttr "L_mid_mid.bps" "bindPose1.wm[49]";
connectAttr "L_mid_low.bps" "bindPose1.wm[50]";
connectAttr "L_index_up.bps" "bindPose1.wm[51]";
connectAttr "L_index_mid.bps" "bindPose1.wm[52]";
connectAttr "L_thumb_up.bps" "bindPose1.wm[53]";
connectAttr "L_index_low.bps" "bindPose1.wm[54]";
connectAttr "L_thumb_mid.bps" "bindPose1.wm[55]";
connectAttr "R_hair_a.bps" "bindPose1.wm[56]";
connectAttr "L_hair_a.bps" "bindPose1.wm[58]";
connectAttr "R_hair_b.bps" "bindPose1.wm[59]";
connectAttr "L_hair_b.bps" "bindPose1.wm[60]";
connectAttr "sword_jnt.msg" "bindPose2.m[0]";
connectAttr "bindPose2.w" "bindPose2.p[0]";
connectAttr "sword_jnt.bps" "bindPose2.wm[0]";
connectAttr "char1_swordSG.msg" "char_character1_sword_rig_v2:sword_final:sword:materialInfo5.sg"
		;
connectAttr "char1_sword.msg" "char_character1_sword_rig_v2:sword_final:sword:materialInfo5.m"
		;
connectAttr "file2.msg" "char_character1_sword_rig_v2:sword_final:sword:materialInfo5.t"
		 -na;
connectAttr "char1_sword.oc" "char1_swordSG.ss";
connectAttr "file2.oc" "char1_sword.c";
connectAttr "place2dTexture2.c" "file2.c";
connectAttr "place2dTexture2.tf" "file2.tf";
connectAttr "place2dTexture2.rf" "file2.rf";
connectAttr "place2dTexture2.mu" "file2.mu";
connectAttr "place2dTexture2.mv" "file2.mv";
connectAttr "place2dTexture2.s" "file2.s";
connectAttr "place2dTexture2.wu" "file2.wu";
connectAttr "place2dTexture2.wv" "file2.wv";
connectAttr "place2dTexture2.re" "file2.re";
connectAttr "place2dTexture2.of" "file2.of";
connectAttr "place2dTexture2.r" "file2.ro";
connectAttr "place2dTexture2.n" "file2.n";
connectAttr "place2dTexture2.vt1" "file2.vt1";
connectAttr "place2dTexture2.vt2" "file2.vt2";
connectAttr "place2dTexture2.vt3" "file2.vt3";
connectAttr "place2dTexture2.vc1" "file2.vc1";
connectAttr "place2dTexture2.o" "file2.uv";
connectAttr "place2dTexture2.ofs" "file2.fs";
connectAttr "Char1_char1_body_d_tga1.oc" "Char1_char1_body_d_tga.ss";
connectAttr "groupId1549.msg" "Char1_char1_body_d_tga.gn" -na;
connectAttr "CHAR1_Body1Shape.iog.og[0]" "Char1_char1_body_d_tga.dsm" -na;
connectAttr "Char1_char1_body_d_tga.msg" "materialInfo1.sg";
connectAttr "Char1_char1_body_d_tga1.msg" "materialInfo1.m";
connectAttr "file1.msg" "materialInfo1.t" -na;
connectAttr "file1.oc" "Char1_char1_body_d_tga1.c";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
relationship "link" ":lightLinker1" ":defaultObjectSet.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Char1_char1_body_d_tga.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "char1_swordSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Char1_char1_body_d_tga.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "char1_swordSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "skinCluster13GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster13GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "C_root.wm" "skinCluster1.ma[0]";
connectAttr "C_hip.wm" "skinCluster1.ma[1]";
connectAttr "R_thigh.wm" "skinCluster1.ma[2]";
connectAttr "R_calf.wm" "skinCluster1.ma[3]";
connectAttr "R_heel.wm" "skinCluster1.ma[4]";
connectAttr "R_foot.wm" "skinCluster1.ma[5]";
connectAttr "L_thigh.wm" "skinCluster1.ma[6]";
connectAttr "L_calf.wm" "skinCluster1.ma[7]";
connectAttr "L_heel.wm" "skinCluster1.ma[8]";
connectAttr "L_foot.wm" "skinCluster1.ma[9]";
connectAttr "C_spine_mid.wm" "skinCluster1.ma[11]";
connectAttr "C_spine_up.wm" "skinCluster1.ma[12]";
connectAttr "C_neck.wm" "skinCluster1.ma[13]";
connectAttr "C_head.wm" "skinCluster1.ma[14]";
connectAttr "C_hair_a.wm" "skinCluster1.ma[15]";
connectAttr "C_hair_b.wm" "skinCluster1.ma[16]";
connectAttr "R_hair_a.wm" "skinCluster1.ma[17]";
connectAttr "R_hair_b.wm" "skinCluster1.ma[18]";
connectAttr "L_hair_a.wm" "skinCluster1.ma[19]";
connectAttr "L_hair_b.wm" "skinCluster1.ma[20]";
connectAttr "R_shou.wm" "skinCluster1.ma[21]";
connectAttr "R_arm.wm" "skinCluster1.ma[22]";
connectAttr "R_forearm.wm" "skinCluster1.ma[23]";
connectAttr "R_hand.wm" "skinCluster1.ma[24]";
connectAttr "R_little_up.wm" "skinCluster1.ma[25]";
connectAttr "R_little_mid.wm" "skinCluster1.ma[26]";
connectAttr "R_little_low.wm" "skinCluster1.ma[27]";
connectAttr "R_ring_up.wm" "skinCluster1.ma[28]";
connectAttr "R_ring_mid.wm" "skinCluster1.ma[29]";
connectAttr "R_ring_low.wm" "skinCluster1.ma[30]";
connectAttr "R_mid_up.wm" "skinCluster1.ma[31]";
connectAttr "R_mid_mid.wm" "skinCluster1.ma[32]";
connectAttr "R_mid_low.wm" "skinCluster1.ma[33]";
connectAttr "R_index_up.wm" "skinCluster1.ma[34]";
connectAttr "R_index_mid.wm" "skinCluster1.ma[35]";
connectAttr "R_index3_low.wm" "skinCluster1.ma[36]";
connectAttr "R_thumb_up.wm" "skinCluster1.ma[37]";
connectAttr "R_thumb_mid.wm" "skinCluster1.ma[38]";
connectAttr "L_shou.wm" "skinCluster1.ma[39]";
connectAttr "L_arm.wm" "skinCluster1.ma[40]";
connectAttr "L_forearm.wm" "skinCluster1.ma[41]";
connectAttr "L_hand.wm" "skinCluster1.ma[42]";
connectAttr "L_little_up.wm" "skinCluster1.ma[43]";
connectAttr "L_little_mid.wm" "skinCluster1.ma[44]";
connectAttr "L_little_low.wm" "skinCluster1.ma[45]";
connectAttr "L_ring_up.wm" "skinCluster1.ma[46]";
connectAttr "L_ring_mid.wm" "skinCluster1.ma[47]";
connectAttr "L_ring_low.wm" "skinCluster1.ma[48]";
connectAttr "L_mid_up.wm" "skinCluster1.ma[49]";
connectAttr "L_mid_mid.wm" "skinCluster1.ma[50]";
connectAttr "L_mid_low.wm" "skinCluster1.ma[51]";
connectAttr "L_index_up.wm" "skinCluster1.ma[52]";
connectAttr "L_index_mid.wm" "skinCluster1.ma[53]";
connectAttr "L_index_low.wm" "skinCluster1.ma[54]";
connectAttr "L_thumb_up.wm" "skinCluster1.ma[55]";
connectAttr "L_thumb_mid.wm" "skinCluster1.ma[56]";
connectAttr "C_root.liw" "skinCluster1.lw[0]";
connectAttr "C_hip.liw" "skinCluster1.lw[1]";
connectAttr "R_thigh.liw" "skinCluster1.lw[2]";
connectAttr "R_calf.liw" "skinCluster1.lw[3]";
connectAttr "R_heel.liw" "skinCluster1.lw[4]";
connectAttr "R_foot.liw" "skinCluster1.lw[5]";
connectAttr "L_thigh.liw" "skinCluster1.lw[6]";
connectAttr "L_calf.liw" "skinCluster1.lw[7]";
connectAttr "L_heel.liw" "skinCluster1.lw[8]";
connectAttr "L_foot.liw" "skinCluster1.lw[9]";
connectAttr "C_spine_mid.liw" "skinCluster1.lw[11]";
connectAttr "C_spine_up.liw" "skinCluster1.lw[12]";
connectAttr "C_neck.liw" "skinCluster1.lw[13]";
connectAttr "C_head.liw" "skinCluster1.lw[14]";
connectAttr "C_hair_a.liw" "skinCluster1.lw[15]";
connectAttr "C_hair_b.liw" "skinCluster1.lw[16]";
connectAttr "R_hair_a.liw" "skinCluster1.lw[17]";
connectAttr "R_hair_b.liw" "skinCluster1.lw[18]";
connectAttr "L_hair_a.liw" "skinCluster1.lw[19]";
connectAttr "L_hair_b.liw" "skinCluster1.lw[20]";
connectAttr "R_shou.liw" "skinCluster1.lw[21]";
connectAttr "R_arm.liw" "skinCluster1.lw[22]";
connectAttr "R_forearm.liw" "skinCluster1.lw[23]";
connectAttr "R_hand.liw" "skinCluster1.lw[24]";
connectAttr "R_little_up.liw" "skinCluster1.lw[25]";
connectAttr "R_little_mid.liw" "skinCluster1.lw[26]";
connectAttr "R_little_low.liw" "skinCluster1.lw[27]";
connectAttr "R_ring_up.liw" "skinCluster1.lw[28]";
connectAttr "R_ring_mid.liw" "skinCluster1.lw[29]";
connectAttr "R_ring_low.liw" "skinCluster1.lw[30]";
connectAttr "R_mid_up.liw" "skinCluster1.lw[31]";
connectAttr "R_mid_mid.liw" "skinCluster1.lw[32]";
connectAttr "R_mid_low.liw" "skinCluster1.lw[33]";
connectAttr "R_index_up.liw" "skinCluster1.lw[34]";
connectAttr "R_index_mid.liw" "skinCluster1.lw[35]";
connectAttr "R_index3_low.liw" "skinCluster1.lw[36]";
connectAttr "R_thumb_up.liw" "skinCluster1.lw[37]";
connectAttr "R_thumb_mid.liw" "skinCluster1.lw[38]";
connectAttr "L_shou.liw" "skinCluster1.lw[39]";
connectAttr "L_arm.liw" "skinCluster1.lw[40]";
connectAttr "L_forearm.liw" "skinCluster1.lw[41]";
connectAttr "L_hand.liw" "skinCluster1.lw[42]";
connectAttr "L_little_up.liw" "skinCluster1.lw[43]";
connectAttr "L_little_mid.liw" "skinCluster1.lw[44]";
connectAttr "L_little_low.liw" "skinCluster1.lw[45]";
connectAttr "L_ring_up.liw" "skinCluster1.lw[46]";
connectAttr "L_ring_mid.liw" "skinCluster1.lw[47]";
connectAttr "L_ring_low.liw" "skinCluster1.lw[48]";
connectAttr "L_mid_up.liw" "skinCluster1.lw[49]";
connectAttr "L_mid_mid.liw" "skinCluster1.lw[50]";
connectAttr "L_mid_low.liw" "skinCluster1.lw[51]";
connectAttr "L_index_up.liw" "skinCluster1.lw[52]";
connectAttr "L_index_mid.liw" "skinCluster1.lw[53]";
connectAttr "L_index_low.liw" "skinCluster1.lw[54]";
connectAttr "L_thumb_up.liw" "skinCluster1.lw[55]";
connectAttr "L_thumb_mid.liw" "skinCluster1.lw[56]";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "CHAR1_Body1ShapeOrig.w" "groupParts37.ig";
connectAttr "groupId1549.id" "groupParts37.gi";
connectAttr "groupParts39.og" "tweak4.ip[0].ig";
connectAttr "groupId1551.id" "tweak4.ip[0].gi";
connectAttr "skinCluster13GroupId.msg" "skinCluster13Set.gn" -na;
connectAttr "CHAR1_Body1Shape.iog.og[7]" "skinCluster13Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster13Set.ub[0]";
connectAttr "tweak4.og[0]" "skinCluster13GroupParts.ig";
connectAttr "skinCluster13GroupId.id" "skinCluster13GroupParts.gi";
connectAttr "groupId1551.msg" "tweakSet13.gn" -na;
connectAttr "CHAR1_Body1Shape.iog.og[8]" "tweakSet13.dsm" -na;
connectAttr "tweak4.msg" "tweakSet13.ub[0]";
connectAttr "groupParts37.og" "groupParts39.ig";
connectAttr "groupId1551.id" "groupParts39.gi";
connectAttr "char1_swordSG.pa" ":renderPartition.st" -na;
connectAttr "Char1_char1_body_d_tga.pa" ":renderPartition.st" -na;
connectAttr "char1_sword.msg" ":defaultShaderList1.s" -na;
connectAttr "Char1_char1_body_d_tga1.msg" ":defaultShaderList1.s" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
// End of CHAR1_ATTACK_WEAK_1.ma
